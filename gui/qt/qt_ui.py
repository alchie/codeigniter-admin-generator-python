from PyQt4 import QtCore, QtGui

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s

try:
    _encoding = QtGui.QApplication.UnicodeUTF8
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig, _encoding)
except AttributeError:
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig)

class Ui_MainWindow(object):

    def _icon(self):
        icon = QtGui.QIcon()
        icon.addPixmap(QtGui.QPixmap(_fromUtf8("images/ci_logo2.gif")), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        return icon
    
    def _tabs(self, MainWindow):
        self.tabWidget = QtGui.QTabWidget(self.centralwidget)
        self.tabWidget.setObjectName(_fromUtf8("tabWidget"))
        self.saveTab = QtGui.QWidget()
        self.saveTab.setObjectName(_fromUtf8("saveTab"))
        self.tabWidget.addTab(self.saveTab, _fromUtf8(""))
        self.mvcTab = QtGui.QWidget()
        self.mvcTab.setObjectName(_fromUtf8("mvcTab"))
        self.tabWidget.addTab(self.mvcTab, _fromUtf8(""))
        self.menuTab = QtGui.QWidget()
        self.menuTab.setObjectName(_fromUtf8("menuTab"))
        self.tabWidget.addTab(self.menuTab, _fromUtf8(""))
        self.tableTab = QtGui.QWidget()
        self.tableTab.setObjectName(_fromUtf8("tableTab"))
        self.tabWidget.addTab(self.tableTab, _fromUtf8(""))
        self.fieldTab = QtGui.QWidget()
        self.fieldTab.setObjectName(_fromUtf8("fieldTab"))
        self.tabWidget.addTab(self.fieldTab, _fromUtf8(""))
        self.verticalLayout.addWidget(self.tabWidget)

        font = QtGui.QFont()
        font.setPointSize(12)
        font.setBold(True)
        font.setWeight(75)
        self.tabWidget.setFont(font)

    def _menus(self, MainWindow):
        self.menubar = QtGui.QMenuBar(MainWindow)
        self.menubar.setGeometry(QtCore.QRect(0, 0, 955, 20))
        self.menubar.setObjectName(_fromUtf8("menubar"))
        self.menuDatabases = QtGui.QMenu(self.menubar)
        self.menuDatabases.setObjectName(_fromUtf8("menuDatabases"))
        self.menuExit = QtGui.QMenu(self.menubar)
        self.menuExit.setObjectName(_fromUtf8("menuExit"))

    def _styles(self, MainWindow):
        palette = QtGui.QPalette()
        brush = QtGui.QBrush(QtGui.QColor(170, 170, 127))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Active, QtGui.QPalette.Base, brush)
        brush = QtGui.QBrush(QtGui.QColor(170, 170, 127))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Active, QtGui.QPalette.Window, brush)
        brush = QtGui.QBrush(QtGui.QColor(170, 170, 127))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Inactive, QtGui.QPalette.Base, brush)
        brush = QtGui.QBrush(QtGui.QColor(170, 170, 127))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Inactive, QtGui.QPalette.Window, brush)
        brush = QtGui.QBrush(QtGui.QColor(170, 170, 127))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Disabled, QtGui.QPalette.Base, brush)
        brush = QtGui.QBrush(QtGui.QColor(170, 170, 127))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Disabled, QtGui.QPalette.Window, brush)
        MainWindow.setPalette(palette)
        
    def setupUi(self, MainWindow):
        MainWindow.setObjectName(_fromUtf8("MainWindow"))
        MainWindow.resize(955, 721)
        MainWindow.setMinimumSize(QtCore.QSize(955, 721))
        MainWindow.setMaximumSize(QtCore.QSize(955, 721))
        MainWindow.setWindowIcon(self._icon())
        
        self.centralwidget = QtGui.QWidget(MainWindow)
        self.centralwidget.setObjectName(_fromUtf8("centralwidget"))
        self.verticalLayout = QtGui.QVBoxLayout(self.centralwidget)
        self.verticalLayout.setObjectName(_fromUtf8("verticalLayout"))
        
        self._tabs(MainWindow)
        self._menus(MainWindow)
        self._styles(MainWindow)
        
        MainWindow.setCentralWidget(self.centralwidget)
        MainWindow.setMenuBar(self.menubar)
        self.actionDB1 = QtGui.QAction(MainWindow)
        self.actionDB1.setObjectName(_fromUtf8("actionDB1"))
        self.actionDB2 = QtGui.QAction(MainWindow)
        self.actionDB2.setObjectName(_fromUtf8("actionDB2"))
        self.menuDatabases.addAction(self.actionDB1)
        self.menuDatabases.addAction(self.actionDB2)
        self.menubar.addAction(self.menuDatabases.menuAction())
        self.menubar.addAction(self.menuExit.menuAction())

        self.retranslateUi(MainWindow)
        self.tabWidget.setCurrentIndex(0)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)

    def retranslateUi(self, MainWindow):
        MainWindow.setWindowTitle(_translate("MainWindow", "CodeIgniter API Generator v2.0", None))
        self.tabWidget.setTabText(self.tabWidget.indexOf(self.saveTab), _translate("MainWindow", "Save Settings", None))
        self.tabWidget.setTabText(self.tabWidget.indexOf(self.mvcTab), _translate("MainWindow", "MVC Settings", None))
        self.tabWidget.setTabText(self.tabWidget.indexOf(self.menuTab), _translate("MainWindow", "Menu Settings", None))
        self.tabWidget.setTabText(self.tabWidget.indexOf(self.tableTab), _translate("MainWindow", "Table Settings", None))
        self.tabWidget.setTabText(self.tabWidget.indexOf(self.fieldTab), _translate("MainWindow", "Field Settings", None))
        self.menuDatabases.setTitle(_translate("MainWindow", "Databases", None))
        self.menuExit.setTitle(_translate("MainWindow", "Exit", None))
        self.actionDB1.setText(_translate("MainWindow", "DB1", None))
        self.actionDB2.setText(_translate("MainWindow", "DB2", None))
