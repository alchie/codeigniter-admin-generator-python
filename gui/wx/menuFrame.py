import os
# using Tkinter's Optionmenu() as a combobox
try:
    # Python2
    import Tkinter as tk
except ImportError:
    # Python3
    import tkinter as tk

from lib.mysql import MySQLConnect
from lib import sqlite
from lib import unzip, create_file
from lib import models, views, controllers, langs, configs, menu, generate
from ui import *

class menuFrame:
	
	root = None
	action = 'add'
	parent_menu_option = None
	
	def __init__(self, root):
		self.root = root

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
			
	def display(self):
		self.root.hideActiveFrames('menu')
		
		if self.root.menuSettingsFrame != None:
			self.root.menuSettingsFrame.destroy()
			self.root.menuSettingsFrame = None
			
		self.root.menuSettingsFrame = tk.Frame()
		self.root.menuSettingsFrame.config(width=805, height=440,padx=20,pady=20,bd=1,relief=tk.RAISED,bg="#DDD")
		self.root.menuSettingsFrame.grid(row=4, column=0)
		self.root.menuSettingsFrame.grid_propagate(False)
		
		thisFrame = self.root.menuSettingsFrame
		
		self.root.menuSettingsButton.config(bg='red', activebackground='red', activeforeground='white')
		
		tk_label(thisFrame, Label="MENU SETTINGS", xPos=0, yPos=0, Width=58, bG="#DDD", Font=('Serif',(14),"bold"), fG="#000", Anchor=None)
		
		currentMenu = sqlite.menuSettings( self.root.saveFile, self.root.selectedDB )
		
		scrollbar = tk.Scrollbar(self.root.menuSettingsFrame, orient=tk.VERTICAL)
		listbox = tk.Listbox(self.root.menuSettingsFrame, yscrollcommand=scrollbar.set)
		listbox.config(width=21,height=19)
		listbox.place(x=0, y=50)
		scrollbar.config(command=listbox.yview)
		scrollbar.place(x=175, y=50, height=310)
		
		for item in currentMenu.menulist():
			index = currentMenu.get_value(item[1], 'index')
			if index == None:
				index = tk.END
			else:
				index = int(index)
				
			listbox.insert(index, item[1])
			
		formInputFrame = tk.Frame(self.root.menuSettingsFrame)
		formInputFrame.config(width=560, height=310,padx=20,pady=20,bd=1,relief=tk.RAISED,bg="#DDD")
		formInputFrame.place(x=200,y=50)
		
		settingsCaption = tk.Label(formInputFrame, text="Menu Item", font=('Serif',(14),"bold"))
		settingsCaption.config(bg="#DDD",fg="#000", width=39)
		settingsCaption.place(x=0,y=0)
		
		menu_id_var = tk.StringVar()
		_, menu_id_entry = tk_entry(menu_id_var, formInputFrame, Label="Menu ID: ", xPos=0, yPos=40, bG="#DDD", Width=30)
		#~ menu_id_label = tk.Label(formInputFrame, text="Menu ID: ")
		#~ menu_id_label.config(bg="#DDD", anchor='w', width=30,pady=2, padx=2)
		#~ menu_id_label.place(x=0,y=40)
		#~ menu_id_entry = tk.Entry(formInputFrame, width=30, textvariable=menu_id_var)
		#~ menu_id_entry.place(x=0,y=60)
		
		menu_label_var = tk.StringVar()
		tk_entry(menu_label_var, formInputFrame, Label="Menu Label: ", xPos=0, yPos=90, bG="#DDD", Width=30)
		#~ menu_label_label = tk.Label(formInputFrame, text="Menu Label: ")
		#~ menu_label_label.config(bg="#DDD", anchor='w', width=30,pady=2, padx=2)
		#~ menu_label_label.place(x=0,y=90)
		#~ menu_label_entry = tk.Entry(formInputFrame, width=30, textvariable=menu_label_var)
		#~ menu_label_entry.place(x=0,y=110)

		menu_url_var = tk.StringVar()
		tk_entry(menu_url_var, formInputFrame, Label="Menu URL: ", xPos=0, yPos=140, bG="#DDD", Width=30)
		#~ menu_url_label = tk.Label(formInputFrame, text="Menu URL: ")
		#~ menu_url_label.config(bg="#DDD", anchor='w', width=30,pady=2, padx=2)
		#~ menu_url_label.place(x=0,y=140)
		#~ menu_url_entry = tk.Entry(formInputFrame, width=30, textvariable=menu_url_var)
		#~ menu_url_entry.place(x=0,y=160)
	
		iam_parent_menu = tk.IntVar()
		iam_parent_menu_checkbox = tk.Checkbutton(formInputFrame, text="I am Parent", variable=iam_parent_menu)
		iam_parent_menu_checkbox.config( bd=0, anchor="w", width=28 )
		iam_parent_menu_checkbox.place(x=0,y=195)

		# COLUMN 2
		
		menu_order_var = tk.StringVar()
		menu_order_var.set( str(listbox.size()) )
		tk_entry(menu_order_var, formInputFrame, Label="Menu Order: ", xPos=265, yPos=40, bG="#DDD", Width=30)
		#~ menu_order_label = tk.Label(formInputFrame, text="Menu Order: ")
		#~ menu_order_label.config(bg="#DDD", anchor='w', width=30,pady=2, padx=2)
		#~ menu_order_label.place(x=265,y=40)
		#~ menu_order_entry = tk.Entry(formInputFrame, width=30, textvariable=menu_order_var)
		#~ menu_order_entry.place(x=265,y=60)
		
		parent_menu_var = tk.StringVar()
		parent_menu_label = tk.Label(formInputFrame, text="Parent Menu: ")
		parent_menu_label.config(bg="#DDD", anchor="w", width=30,pady=2, padx=2)
		parent_menu_label.place(x=265,y=90)
		
		def parent_menu():
			PARENT_MENUS = ["none"]
			for item in currentMenu.parentlist():
				PARENT_MENUS.append(item[1])
			
			if self.parent_menu_option != None:
				self.parent_menu_option.place_forget()
				self.parent_menu_option.remove()
				self.parent_menu_option = None
			
			parent_menu_var.set("none")
			parent_menu_option = tk.OptionMenu(formInputFrame, parent_menu_var, *PARENT_MENUS)
			parent_menu_option.place(x=265,y=110)
			parent_menu_option.config(width=26, pady=2, bd=0, bg="#FFF")
		
		parent_menu()
		
		menu_icon_var = tk.StringVar()
		menu_icon_var.set("fa fa-bars fa-fw")
		tk_entry(menu_icon_var, formInputFrame, Label="Menu Icon Class: ", xPos=265, yPos=140, bG="#DDD", Width=30)
		
		deleteButton = tk.Button(formInputFrame,text="Delete", bg="red")
				
		def clear_form():
			self.action = 'add'
			menu_order_var.set( str( listbox.size() ) )
			menu_id_var.set('')
			menu_id_entry.config(state=tk.NORMAL)
			menu_label_var.set('')
			menu_url_var.set('')
			iam_parent_menu.set(0)
			parent_menu_var.set('none')
			menu_icon_var.set("fa fa-bars fa-fw")
			deleteButton.place_forget()
		
		def update_item():
			if  menu_label_var.get() != '' and  menu_label_var.get() != '':
				currentMenu.insert(menu_id_var.get(), 'index', menu_order_var.get())
				currentMenu.insert(menu_id_var.get(), 'label', menu_label_var.get())
				currentMenu.insert(menu_id_var.get(), 'url', menu_url_var.get())
				currentMenu.insert(menu_id_var.get(), 'parent', str(iam_parent_menu.get()))
				currentMenu.insert(menu_id_var.get(), 'parent_menu', parent_menu_var.get())
				currentMenu.insert(menu_id_var.get(), 'icon', menu_icon_var.get())
				
				if self.action == 'add':
					listbox.insert(tk.END, menu_id_var.get())
				
				parent_menu()	
				clear_form()
			
		addButton = tk.Button(formInputFrame,text="Save", command=update_item, bg="green")
		addButton.place(x=0,y=240)
		
		def delete_item():
			selection = listbox.curselection()
			if len(selection) > 0:
				index, = selection
				
				listbox.delete( index )
				currentMenu.delete_menu( menu_id_var.get() )
			
			clear_form()
			
		deleteButton.config( command=delete_item )
		
		def edit_item(evt):
			selection = listbox.curselection()
			if len(selection) > 0:
				index, = selection
				menu_id = listbox.get(index)
				menu_id_var.set( menu_id )
				menu_id_entry.config(state=tk.DISABLED)
				if currentMenu.get_value(menu_id, 'label') != None:
					menu_label_var.set( currentMenu.get_value(menu_id, 'label') )

				if currentMenu.get_value(menu_id, 'url') != None:
					menu_url_var.set( currentMenu.get_value(menu_id, 'url') )
					
				if currentMenu.get_value(menu_id, 'parent') != None:
					iam_parent_menu.set( int(currentMenu.get_value(menu_id, 'parent')) )

				if currentMenu.get_value(menu_id, 'index') != None:
					menu_order_var.set( currentMenu.get_value(menu_id, 'index') )
				
				if currentMenu.get_value(menu_id, 'parent_menu') != None:
					parent_menu_var.set( currentMenu.get_value(menu_id, 'parent_menu') )
				
				if currentMenu.get_value(menu_id, 'icon') != None:
					menu_icon_var.set( currentMenu.get_value(menu_id, 'icon') )
					
				self.action = 'edit'
				
				deleteButton.place(x=450,y=240)
				
		listbox.bind("<<ListboxSelect>>", edit_item)
		
		mvc_settings = sqlite.mvcSettings(self.root.saveFile, self.root.selectedDB)
		saveDir = mvc_settings.get_value('directory')
		
		mvc_folder = mvc_settings.get_value('mvc_directory')
		app_folder = mvc_settings.get_value('application_directory')
		
		if saveDir != '' and saveDir != None:
			
			mvcDir = os.path.join(saveDir,mvc_folder)
			appDir = os.path.join(mvcDir,app_folder)
			
			if not os.path.exists(mvcDir):
				os.makedirs(mvcDir)
				print("Folder Created: " + mvcDir)
			
			if not os.path.exists(appDir):
				os.makedirs(appDir)
				print("Folder Created: " + appDir)
				
			gen_menu = configs.MenuFile( self.root.saveFile, self.root.selectedDB, appDir )
		
			generateButton = tk.Button(self.root.menuSettingsFrame,text="Generate Menu", command=gen_menu.create)
			generateButton.config(bg="green")
			generateButton.place(x=0,y=0)
