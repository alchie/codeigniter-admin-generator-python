from AppMain import *
import gtk
import egg.trayicon

WINDOW_OPENED = False

class SystemTray:
	filename = None
	app = None
	
	def __init__(self, filename):
		self.filename = filename

		self.tray = egg.trayicon.TrayIcon("TrayIcon")

		eventbox = gtk.EventBox()
		image = gtk.Image()
		image.set_from_file("images/tray-icon.gif")

		eventbox.connect("button-release-event", self.icon_clicked)

		eventbox.add(image)
		self.tray.add(eventbox)
		self.tray.show_all()

		gtk.main()
	
	def icon_clicked(self, widget, event):
		global WINDOW_OPENED
		if event.button == 1:
			if not WINDOW_OPENED:
				WINDOW_OPENED = True
				self.app = AppMain()
				self.app.display(self.filename)
			else:
				WINDOW_OPENED = False

		if event.button == 3:
			menu = gtk.Menu()
			menuitem_about = gtk.MenuItem("About")
			menuitem_exit = gtk.MenuItem("Exit")
			menu.append(menuitem_about)
			menu.append(menuitem_exit)
			menuitem_about.connect("activate", self.aboutdialog)
			menuitem_exit.connect("activate", lambda w: gtk.main_quit())
			menu.show_all()
			menu.popup(None, None, None, event.button, event.time, self.tray)

	def aboutdialog(self, widget):
		aboutdialog = gtk.AboutDialog()
		aboutdialog.set_name("CodeIgniter Admin Generator")
		aboutdialog.set_version("1.0")

		aboutdialog.run()
		aboutdialog.destroy()
