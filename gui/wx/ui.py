try:
    # Python2
    import Tkinter as tk
except ImportError:
    # Python3
    import tkinter as tk

def tk_frame(Parent=None,PosX=0,PosY=0,Width=100,Height=100,PadX=0,PadY=0,BD=0,Relief=tk.FLAT,BG="#CCC"):
	
	if Parent == None:
		frame = tk.Frame()
	else:
		frame = tk.Frame(Parent)
		
	frame.config(width=Width,height=Height,padx=PadX,pady=PadY,bd=BD,relief=Relief,bg=BG)
	frame.place(x=PosX, y=PosY)
	return frame
	
def tk_label(Frame, Label="Label: ", xPos=0, yPos=0, Width=27, bG="#CCC", Font=None, fG="#000", Anchor='w'):
	label = tk.Label(Frame, text=Label)
	label.config(bg=bG, fg=fG, anchor=Anchor, width=Width, pady=2, padx=2, font=Font)
	label.place(x=xPos,y=yPos) 
	return label
	
def tk_entry(Variable, Frame, Label="Label: ", xPos=0, yPos=0, bG="#CCC", Width=27):
	label = tk.Label(Frame, text=Label)
	label.config(bg=bG, anchor="w", width=Width,pady=2, padx=2)
	label.place(x=xPos, y=yPos)
	entry = tk.Entry(Frame, width=Width, textvariable=Variable)
	entry.place(x=xPos, y=(yPos+20))
	return (label, entry)

def tk_optionmenu(Variable, Frame, Label="Label: ", ITEMS=[""], xPos=0, yPos=0, bG="#CCC",Command=None, Width=23):
	label = tk.Label(Frame, text=Label)
	label.config(bg=bG, anchor="w", width=(Width+4) ,pady=2, padx=2)
	label.place(x=xPos, y=yPos)
	option = tk.OptionMenu(Frame, Variable, *ITEMS, command=Command)
	option.config(width=Width, pady=2, bd=0, bg="#FFF")
	option.place(x=xPos, y=(yPos+20))
	return (label, option)

def tk_checkbox(Variable, Frame, Label="Label: ", xPos=0, yPos=0, bG="#CCC", Width=27, Command=None):
	checkbox = tk.Checkbutton(Frame, text=Label, variable=Variable)
	checkbox.config(relief=tk.FLAT, borderwidth=0, highlightbackground=bG, bg=bG, bd=0, anchor="w", width=Width )
	checkbox.config(command=Command)
	checkbox.place(x=xPos,y=yPos) 
	return checkbox

def tk_radio(Variable, Frame, Label="Label: ", Value="", xPos=0, yPos=0, bG="#CCC", Width=27, Command=None):
	radio = tk.Radiobutton(Frame, text=Label, value=Value, variable=Variable)
	radio.config(relief=tk.FLAT, borderwidth=0, highlightbackground=bG, bg=bG, bd=0, anchor="w", width=Width )
	radio.config(command=Command)
	radio.place(x=xPos,y=yPos) 
	return radio

def tk_button(Frame, Label="Update", xPos=0, yPos=0, bCommand=None, bG='#CCC', Width=19):
	button = tk.Button(Frame, text=Label)
	button.place(x=xPos,y=yPos) 
	button.config(command=bCommand, bg=bG, width=Width)
	return button
		
def settings_button(Frame, xPos=0, yPos=0, bCommand=None, bG='#CCC', bImage=None, bState=tk.DISABLED):
	button = tk.Button(Frame)
	button.config(width=16, height=16, image=bImage, overrelief=tk.FLAT, justify=tk.LEFT)
	button.config(borderwidth=0,relief=tk.FLAT, bg=bG, highlightbackground=bG, state=bState)
	button.config(foreground=bG, activebackground=bG, activeforeground=bG, padx=0, pady=0)
	button.place(x=xPos,y=yPos) 
	button.config(command=bCommand)
	return button

def close_button(Frame, xPos=0, yPos=0, bCommand=None, bG='#CCC', bImage=None, bState=tk.DISABLED):
	button = tk.Button(Frame, justify=tk.LEFT)
	button.config(width=16, height=16, image=bImage, overrelief=tk.FLAT)
	button.config(borderwidth=0,relief=tk.FLAT, bg=bG, highlightbackground=bG, state=bState)
	button.config(foreground=bG, activebackground=bG, activeforeground=bG, padx=0, pady=0)
	button.place(x=xPos,y=yPos) 
	button.config(command=bCommand)
	return button

def image_button(Frame, xPos=0, yPos=0, bCommand=None, bG='#CCC', bImage=None, bState=tk.DISABLED):
	button = tk.Button(Frame, justify=tk.LEFT)
	button.config(width=16, height=16, image=bImage, overrelief=tk.FLAT)
	button.config(borderwidth=0,relief=tk.FLAT, bg=bG, highlightbackground=bG, state=bState)
	button.config(foreground=bG, activebackground=bG, activeforeground=bG, padx=0, pady=0)
	button.place(x=xPos,y=yPos) 
	button.config(command=bCommand)
	return button
	
def getConfigValue(Config, Var, Key, Int=False, Default=""):	
	Value = Config.get_value( Key )
	if (Value==None or Value==""):
		if Default == "" and Int:
			Default = 0	
		Value = Default
	if Int:
		Value = int( Value )
	Var.set( Value )	
	return Value
