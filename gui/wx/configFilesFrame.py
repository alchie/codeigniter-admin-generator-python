try:
    # Python2
    import Tkinter as tk
except ImportError:
    # Python3
    import tkinter as tk

from ui import *
from lib import sqlite, configs
import operator, os

class configFilesFrame:
	
	root = None
	parentFrame = None
	mainFrame = None
	filename = None
	
	selected_app_file = None
	config_var_frame = None
	
	def __init__(self, root, parentFrame, filename):
		self.root = root
		self.parentFrame = parentFrame
		self.filename = filename
		
	def display(self):
		
		def closeThisFrame():
			self.mainFrame.destroy()
			self.mainFrame = None
		
		if self.mainFrame != None:
			closeThisFrame()
			
		self.mainFrame = tk_frame(Parent=self.parentFrame,PosX=0,PosY=0,Width=765,Height=400,PadX=10,PadY=10,BD=1,Relief=tk.RAISED,BG="#DDD")
		tk_label(self.mainFrame, Label=self.filename, xPos=0, yPos=0, Width=57, bG="#DDD", Font=('Serif',(14),"bold"), fG="#000", Anchor=None)
		close_button(self.mainFrame, 720, 0, bCommand=closeThisFrame, bG='#DDD', bImage=self.root.closeImage, bState=tk.NORMAL)
		save_button = tk_button(self.mainFrame, Label="Save", xPos=0, yPos=0, bCommand=None, bG='green', Width=10)
		update_button = tk_button(self.mainFrame, Label="Update File", xPos=120, yPos=0, bCommand=None, bG='orange', Width=10)
		
		subFrame = tk_frame(Parent=self.mainFrame,PosX=0,PosY=35,Width=745,Height=345,PadX=0,PadY=0,BD=0,Relief=tk.RAISED,BG="#DDD")
		
		if self.filename == 'autoload.php':
			self.displayAutoload(subFrame, save_button, update_button)
		elif self.filename == 'config.php':
			self.displayConfig(subFrame, save_button, update_button)
			
	def getValue(self, DB, Var, Key, Int=False, Default="", Empty=False):
		Value = DB.get_value( Key )
		if Value==None:
			if Default == "" and Int:
				Default = 0
			Value = Default
			
		if Value == "" and Empty == False:
			Value = Default
			
		if Int:
			Value = int( Value )
		Var.set( Value )
		return Value
			
	def displayAutoload(self, Frame, saveButton, updateButton):
		return Frame

	def displayConfig(self, Frame, saveButton, updateButton):
		saveButton.place_forget()
		currentMVC = sqlite.mvcSettings(self.root.saveFile , self.root.selectedDB)
		
		def update():
			saveDir = currentMVC.get_value("directory")
			mvc_folder = currentMVC.get_value("mvc_directory")
			app_folder = currentMVC.get_value("application_directory")
			
			if saveDir != '' or saveDir != None:
				
				mvcDir = os.path.join(saveDir, mvc_folder)
				appDir = os.path.join(saveDir, mvc_folder, app_folder)
				
				if not os.path.exists(mvcDir):
					os.makedirs(mvcDir)
					print( "Folder Created: " + mvcDir )
				
				if not os.path.exists(appDir):
					os.makedirs(appDir)
					print( "Folder Created: " + appDir )

				configs.ConfigFile(self.root.saveFile, self.root.selectedDB, appDir).save()
			
		updateButton.config(command=update)
		
		scrollbar = tk.Scrollbar(Frame, orient=tk.VERTICAL)
		listbox = tk.Listbox(Frame, yscrollcommand=scrollbar.set)
		listbox.config(width=21,height=20)
		listbox.place(x=0, y=10)
		scrollbar.config(command=listbox.yview)
		scrollbar.place(x=175, y=10, height=325)
		
		config_vars = {
			'base_url' : {'index' : 0, 'type' : 'entry', 'default' : '', 'items' : [], 'empty' : False},
			'index_page' : {'index' : 1, 'type' : 'entry', 'default' : 'index.php', 'items' : [], 'empty' : True},
			'uri_protocol' : {'index' : 2, 'type' : 'optionmenu', 'default' : 'AUTO', 'items' : ["AUTO","PATH_INFO","QUERY_STRING","REQUEST_URI","ORIG_PATH_INFO"], 'empty' : False},
			'url_suffix' : {'index' : 3, 'type' : 'entry', 'default' : '', 'items' : [], 'empty' : False},
			'language' : {'index' : 4, 'type' : 'entry', 'default' : 'english', 'items' : [], 'empty' : False},
			'charset' : {'index' : 5, 'type' : 'entry', 'default' : 'UTF-8', 'items' : [], 'empty' : False},
			'enable_hooks' : {'index' : 6, 'type' : 'optionmenu', 'default' : 'FALSE', 'items' : ["TRUE", "FALSE"], 'empty' : False},
			'subclass_prefix' : {'index' : 7, 'type' : 'entry', 'default' : 'MY_', 'items' : [], 'empty' : False},
			'permitted_uri_chars' : {'index' : 8, 'type' : 'entry', 'default' : 'a-z 0-9~%.:_\-', 'items' : [], 'empty' : False},
			'allow_get_array' : {'index' : 9, 'type' : 'optionmenu', 'default' : 'TRUE', 'items' : ["TRUE", "FALSE"], 'empty' : False},
			'enable_query_strings' : {'index' : 10, 'type' : 'optionmenu', 'default' : 'FALSE', 'items' : ["TRUE", "FALSE"], 'empty' : False},
			'controller_trigger' : {'index' : 11, 'type' : 'entry', 'default' : 'c', 'items' : [], 'empty' : False},
			'function_trigger' : {'index' : 12, 'type' : 'entry', 'default' : 'm', 'items' : [], 'empty' : False},
			'directory_trigger' : {'index' : 13, 'type' : 'entry', 'default' : 'd', 'items' : [], 'empty' : False},
			'log_threshold' : {'index' : 14, 'type' : 'optionmenu', 'default' : '0', 'items' : [0,1,2,3,4], 'empty' : False},
			'log_path' : {'index' : 15, 'type' : 'entry', 'default' : '', 'items' : [], 'empty' : False},
			'log_date_format' : {'index' : 16, 'type' : 'entry', 'default' : 'Y-m-d H:i:s', 'items' : [], 'empty' : False},
			'cache_path' : {'index' : 17, 'type' : 'entry', 'default' : '', 'items' : [], 'empty' : False},
			'encryption_key' : {'index' : 18, 'type' : 'entry', 'default' : 'OqbzWkV5qYIF31CBdj9dNJjD5MttorJV', 'items' : [], 'empty' : False},
			'sess_cookie_name' : {'index' : 19, 'type' : 'entry', 'default' : 'ci_session', 'items' : [], 'empty' : False},
			'sess_expiration' : {'index' : 20, 'type' : 'entry', 'default' : '7200', 'items' : [], 'empty' : False},
			'sess_expire_on_close' : {'index' : 21, 'type' : 'optionmenu', 'default' : 'TRUE', 'items' : ["TRUE", "FALSE"], 'empty' : False},
			'sess_encrypt_cookie' : {'index' : 22, 'type' : 'optionmenu', 'default' : 'FALSE', 'items' : ["TRUE", "FALSE"], 'empty' : False},
			'sess_use_database' : {'index' : 23, 'type' : 'optionmenu', 'default' : 'TRUE', 'items' : ["TRUE", "FALSE"], 'empty' : False},
			'sess_table_name' : {'index' : 24, 'type' : 'entry', 'default' : 'ci_admins_sessions', 'items' : [], 'empty' : False},
			'sess_match_ip' : {'index' : 25, 'type' : 'optionmenu', 'default' : 'TRUE', 'items' : ["TRUE", "FALSE"], 'empty' : False},
			'sess_match_useragent' : {'index' : 26, 'type' : 'optionmenu', 'default' : 'TRUE', 'items' : ["TRUE", "FALSE"], 'empty' : False},
			'sess_time_to_update' : {'index' : 27, 'type' : 'entry', 'default' : '300', 'items' : [], 'empty' : False},
			'cookie_prefix' : {'index' : 28, 'type' : 'entry', 'default' : '', 'items' : [], 'empty' : False},
			'cookie_domain' : {'index' : 29, 'type' : 'entry', 'default' : '', 'items' : [], 'empty' : False},
			'cookie_path' : {'index' : 30, 'type' : 'entry', 'default' : '/', 'items' : [], 'empty' : False},
			'cookie_secure' : {'index' : 31, 'type' : 'optionmenu', 'default' : 'FALSE', 'items' : ["TRUE", "FALSE"], 'empty' : False},
			'global_xss_filtering' : {'index' : 32, 'type' : 'optionmenu', 'default' : 'FALSE', 'items' : ["TRUE", "FALSE"], 'empty' : False},
			'csrf_protection' : {'index' : 33, 'type' : 'optionmenu', 'default' : 'FALSE', 'items' : ["TRUE", "FALSE"], 'empty' : False},
			'csrf_token_name' : {'index' : 34, 'type' : 'entry', 'default' : 'csrf_test_name', 'items' : [], 'empty' : False},
			'csrf_cookie_name' : {'index' : 35, 'type' : 'entry', 'default' : 'csrf_cookie_name', 'items' : [], 'empty' : False},
			'csrf_expire' : {'index' : 36, 'type' : 'entry', 'default' : '7200', 'items' : [], 'empty' : False},
			'compress_output' : {'index' : 37, 'type' : 'optionmenu', 'default' : 'FALSE', 'items' : ["TRUE", "FALSE"], 'empty' : False},
			'time_reference' : {'index' : 38, 'type' : 'optionmenu', 'default' : 'local', 'items' : ["local", "gmt"], 'empty' : False},
			'rewrite_short_tags' : {'index' : 39, 'type' : 'optionmenu', 'default' : 'FALSE', 'items' : ["TRUE", "FALSE"], 'empty' : False},
			'proxy_ips' : {'index' : 40, 'type' : 'entry', 'default' : '', 'items' : [], 'empty' : False},
		}
		
		config_raw = {}
		for key in config_vars:
			config_raw[config_vars[key]['index']] = key
			
		for i, key in sorted(config_raw.items(), key=operator.itemgetter(0)):
			listbox.insert(tk.END, key)
		
		def edit_item(evt):
			selection = listbox.curselection()
			if len(selection) > 0:
				index, = selection
				config_id = listbox.get(index)
				
				if self.config_var_frame != None:
					self.config_var_frame.destroy()
					self.config_var_frame = None
					
				self.config_var_frame = tk_frame(Parent=Frame,PosX=200,PosY=10,Width=540,Height=320,PadX=10,PadY=10,BD=1,Relief=tk.RAISED,BG="#DDD")
				
				tk_label(self.config_var_frame, Label=config_id, xPos=0, yPos=0, Width=56, bG="#CCC", Font=('Serif',(10),"bold"), fG="#000", Anchor=None)
				
				Variable = tk.StringVar()
				self.getValue(currentMVC, Variable, "config_"+config_id, Int=False, Default=config_vars[config_id]['default'], Empty=config_vars[config_id]['empty'])
				
				if config_vars[config_id]['type'] == 'entry':
					tk_entry(Variable, self.config_var_frame, Label=config_id+": ", xPos=0, yPos=30, bG="#DDD", Width=50)
				elif config_vars[config_id]['type'] == 'optionmenu':
					tk_optionmenu(Variable, self.config_var_frame, Label=config_id+": ", ITEMS=config_vars[config_id]['items'], xPos=0, yPos=30, bG="#DDD",Command=None, Width=46)
				
				def Save():
					currentMVC.insert( 'config_'+config_id, Variable.get() )
					
				save_button = tk_button(self.config_var_frame, Label="Save", xPos=0, yPos=270, bCommand=Save, bG='green', Width=10)
				
		listbox.bind("<<ListboxSelect>>", edit_item)
		
