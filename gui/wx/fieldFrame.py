import os
# using Tkinter's Optionmenu() as a combobox
try:
    # Python2
    import Tkinter as tk
except ImportError:
    # Python3
    import tkinter as tk

from lib.mysql import MySQLConnect    
from lib import sqlite
from lib import unzip, create_file
from lib import models, views, controllers, langs, configs, menu
from ui import *
from validationFrame import validationFrame
from formInputFrame import formInputFrame

class fieldFrame:
	
	root = None
	optionsFrame = None
	optionsFrameName = None
	currentField = None
	currentFormInput = None
	
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #

	def __init__(self, root):
		self.root = root
		self.closeImage = root.closeImage
		self.settingsImage = root.settingsImage

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #

	def init_vars(self):
		self.currentField = sqlite.fieldSettings(self.root.saveFile, self.root.selectedDB, self.root.selectedTable, self.root.selectedField)
		
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #

	def showOptionsFrame(self, name='', Label="Options Frame Label", button=False):
		self.optionsFrameName = name
		
		if self.optionsFrame != None:
			self.optionsFrame.destroy()
			self.optionsFrame = None
			
		self.optionsFrame = tk.Frame(self.root.fieldSettingsFrame)
		self.optionsFrame.config(width=560, height=360, padx=10,pady=10,bd=1,relief=tk.RAISED,bg="#CCC")
		self.optionsFrame.place(x=200,y=40)
		
		caption = tk.Label(self.optionsFrame, text=Label, font=('Sans',(12),"bold"))
		caption.config(bg="#CCC",fg="#000", width=48)
		caption.place(x=0,y=0)
		
		def closeThisFrame():
			self.optionsFrameName = None
			self.optionsFrame.destroy()
			
		close_button(self.optionsFrame, 515, 0, bCommand=closeThisFrame, bG='#CCC', bImage=self.closeImage, bState=tk.NORMAL)
			
		
		saveButton = tk.Button(self.optionsFrame,text="Save")
		saveButton.config(bg="green", width=12)
		
		if button:
			saveButton.place(x=0,y=0) 

		innerFrame = tk.Frame(self.optionsFrame)
		innerFrame.config(width=535, height=290, padx=0,pady=0,bd=0, bg="#CCC")
		innerFrame.place(x=0,y=45)
		
		return (innerFrame, saveButton)

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #

	def commandOptionsFrame(self, Command, Name=None, Label="Label Here", ShowButton=False, Options=None, Renew=False):
		if self.optionsFrameName == Name:
			self.optionsFrameName = None
			self.optionsFrame.destroy()
			self.optionsFrame = None
			if Renew:
				optionsFrame, saveButton = self.showOptionsFrame(Name, Label, ShowButton)
				Command(optionsFrame, saveButton, options=Options)
		else:
			optionsFrame, saveButton = self.showOptionsFrame(Name, Label, ShowButton)
			Command(optionsFrame, saveButton, options=Options)

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #

	def toggleOptionsFrame(self, Name=None, Button=None):
		if Button['state'] == 'disabled':
			Button.config(state=tk.NORMAL)
		else:
			Button.config(state=tk.DISABLED)
			if self.optionsFrameName == Name:
				self.optionsFrameName = None
				self.optionsFrame.destroy()	
				self.optionsFrame = None

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #

	def getValue(self, Var, Key, Int=False, Default=""):
		
		self.init_vars()
		
		Value = self.currentField.get_value( Key )

		if (Value==None or Value==""):
			if Default == "" and Int:
				Default = 0
				
			Value = Default

		if Int:
			Value = int( Value )
		
		Var.set( Value )	
		return Value
		
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
			
	def display(self):
		self.root.hideActiveFrames('field')
		self.optionsFrameName = None
		
		if self.root.fieldSettingsFrame != None:
			self.root.fieldSettingsFrame.destroy()
			self.root.fieldSettingsFrame = None
			
		self.root.fieldSettingsFrame = tk.Frame()
		self.root.fieldSettingsFrame.config(width=805, height=440,padx=20,pady=20,bd=1,relief=tk.RAISED,bg="#BBB")
		self.root.fieldSettingsFrame.grid(row=4, column=0)
		self.root.fieldSettingsFrame.grid_propagate(False)
						
		thisFrame = self.root.fieldSettingsFrame
		
		################################################################
		
		self.root.fieldSettingsButton.config(bg='red', activebackground='red', activeforeground='white')
		
		################################################################
		
		tk_label(thisFrame, Label="FIELD SETTINGS", xPos=0, yPos=0, Width=58, bG="#BBB", Font=('Serif',(14),"bold"), fG="#000", Anchor=None)
				
		################################################################

		field_title_var = tk.StringVar()
		tk_entry(field_title_var, thisFrame, "Field Title: ", 0, 30, bG="#BBB", Width=21)
		self.getValue(field_title_var, "field_title")
		
		################################################################
		
		tk_label(thisFrame, "Generate Pages: ", 0, 80, Width=21, bG="#BBB")
		
		################################################################
		
		def toggle_list_options():
			self.toggleOptionsFrame('list_options', list_page_options)
				
		def show_list_options_frame():
			self.commandOptionsFrame(self.listOptionsFrame, Name='list_options', Label="List Page Options", ShowButton=True)

		list_var = tk.IntVar()		
		list_page = tk_checkbox(list_var, thisFrame, "List Page ", 0, 105, bG="#BBB", Width=17, Command=None)			
		list_page_options = settings_button(thisFrame, 155, 103, show_list_options_frame, '#BBB', bImage=self.settingsImage, bState=tk.NORMAL )

		if self.getValue(list_var, "list_page", True, 0) == 1:
			list_page_options.config(state=tk.NORMAL)
			
		################################################################
		
		def toggle_add_options():
			self.toggleOptionsFrame('add_options', add_page_options)
				
		def show_add_options_frame():
			self.commandOptionsFrame(self.addOptionsFrame, Name='add_options', Label="Add Page Options", ShowButton=True)
			
		add_var = tk.IntVar()		
		add_page = tk_checkbox(add_var, thisFrame, "Add Page ", 0, 130, bG="#BBB", Width=17, Command=toggle_add_options)			
		add_page_options = settings_button(thisFrame, 155, 128, show_add_options_frame, '#BBB', bImage=self.settingsImage )
		
		if self.getValue(add_var, "add_page", True, 0) == 1:
			add_page_options.config(state=tk.NORMAL)
		
		################################################################
		
		def toggle_edit_options():
			self.toggleOptionsFrame('edit_options', edit_page_options)
				
		def show_edit_options_frame():
			self.commandOptionsFrame(self.editOptionsFrame, Name='edit_options', Label="Edit Page Options", ShowButton=True)
			
		edit_var = tk.IntVar()	
		edit_page = tk_checkbox(edit_var, thisFrame, "Edit Page ", 0, 155, bG="#BBB", Width=17, Command=toggle_edit_options)			
		edit_page_options = settings_button(thisFrame, 155, 153, show_edit_options_frame, '#BBB', bImage=self.settingsImage )
		
		if self.getValue(edit_var, "edit_page", True, 0) == 1:
			edit_page_options.config(state=tk.NORMAL)
		
		################################################################
		
		def form_type_select( val ):
			if val == 'none' and val == '':
				form_type_options.config(state=tk.DISABLED)
			else:
				form_type_options.config(state=tk.NORMAL)
		
				if self.currentFormInput != val and self.optionsFrame != None:
					self.commandOptionsFrame(self.formInputOptionsFrame, Name='form_input_options', Label="Form Input Options", ShowButton=True, Options=val, Renew=True)
			
			self.currentFormInput = val
		
		form_type_var = tk.StringVar()
		
		def show_form_type_options():
			self.commandOptionsFrame(self.formInputOptionsFrame, Name='form_input_options', Label="Form Input Options", ShowButton=True, Options=form_type_var.get())
		
		FORM_TYPES = ("none", "hidden", "text", "textarea", "password", "dropdown", "checkbox", "radio", "library", 'view_only')	
		tk_optionmenu(form_type_var, thisFrame, Label="Form Input Type: ", ITEMS=FORM_TYPES, xPos=0, yPos=180, bG="#BBB",Command=form_type_select, Width=14)
		form_type_options = settings_button(thisFrame, xPos=155, yPos=180, bCommand=show_form_type_options, bG='#BBB', bImage=self.settingsImage, bState=tk.DISABLED)
		form_type_val = self.getValue(form_type_var, "form_input_type", Default='none')
		
		if form_type_val != 'none' and form_type_val != '':
			form_type_options.config(state=tk.NORMAL)		

		################################################################

		def show_validation_rules_options_frame():
			self.commandOptionsFrame(self.validationRulesOptionsFrame, Name='validation_rules_options', Label="Validation Rules", ShowButton=False)

		tk_label(thisFrame, "Validation Rules", 0, 230, Width=17, bG="#BBB")
		validation_options = settings_button(thisFrame, xPos=155, yPos=230, bCommand=show_validation_rules_options_frame, bG='#BBB', bImage=self.settingsImage, bState=tk.NORMAL)
		
		################################################################
		
		def toggle_linked_table_options():
			self.toggleOptionsFrame('linked_table_options', linked_table_options)
				
		def show_linked_table_options_frame():
			self.commandOptionsFrame(self.linkedTableOptionsFrame, Name='linked_table_options', Label="Linked Table", ShowButton=True)
			
		linked_table_var = tk.IntVar()	
		linked_table = tk_checkbox(linked_table_var, thisFrame, "Linked Table ", 0, 260, bG="#BBB", Width=17, Command=toggle_linked_table_options)			
		linked_table_options = settings_button(thisFrame, 155, 257, show_linked_table_options_frame, '#BBB', bImage=self.settingsImage )

		if self.getValue(linked_table_var, "linked_table", True, 0) == 1:
			linked_table_options.config(state=tk.NORMAL)				
		
		################################################################
		
		def show_model_settings_options_frame():
			self.commandOptionsFrame(self.modelSettingsOptionsFrame, Name='model_settings_options', Label="Model Settings", ShowButton=True)
			
		tk_label(thisFrame, "Model Settings", 0, 290, Width=21, bG="#BBB")
		model_settings_options = settings_button(thisFrame, 155, 287, show_model_settings_options_frame, '#BBB', bImage=self.settingsImage, bState=tk.NORMAL )
		
		################################################################
		
		column_order_var = tk.StringVar()
		column_order_var.set(0)
		column_order_count = [i for i in range(1,(self.root.fields_length+1))]
		tk_optionmenu(column_order_var, thisFrame, Label="Column Order: ", ITEMS=column_order_count, xPos=0, yPos=340, bG="#BBB",Command=None, Width=14)
		self.getValue(column_order_var, "list_column_order")
				
		################################################################


		def save():
			self.init_vars()
			self.currentField.insert('field_title', field_title_var.get()) # field title
			self.currentField.insert('list_page', str(list_var.get())) # list page
			self.currentField.insert('add_page', str(add_var.get())) # add page
			self.currentField.insert('edit_page', str(edit_var.get())) # edit page
			self.currentField.insert('form_input_type', str(form_type_var.get())) # form_input_type
			self.currentField.insert('linked_table', str(linked_table_var.get())) # linked_table
			self.currentField.insert('list_column_order', column_order_var.get()) # list_column_order

		saveButton = tk.Button(thisFrame ,text="Save Settings", command=save)
		saveButton.place(x=0,y=0) 
		saveButton.config(bg="green", width=19)
		
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #

	def validationRulesOptionsFrame(self, Frame, SaveButton,**kwargs):
		validationFrame(self.root, Frame, SaveButton)

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #

	def formInputOptionsFrame(self, Frame, SaveButton, options='none'):
		formInputFrame(self.root, Frame, SaveButton, options=options)
		
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
		
	def listOptionsFrame(self, Frame, SaveButton, **kwargs):
		
		currentField = currentField = sqlite.fieldSettings(self.root.saveFile, self.root.selectedDB, self.root.selectedTable, self.root.selectedField)
		
		searchable_var = tk.IntVar()
		tk_checkbox(searchable_var, Frame, Label="Searchable", xPos=0, yPos=0, bG="#CCC", Width=24)
		self.getValue(searchable_var, "list_searchable", True)

		################################################################
		
		list_filter_value_var = tk.IntVar()
		tk_checkbox(list_filter_value_var, Frame, Label="Show Filter Value", xPos=0, yPos=25, bG="#CCC", Width=24)
		self.getValue(list_filter_value_var, "list_filter_value", True)
		
		################################################################

		anchor_filter_value_var = tk.IntVar()
		tk_checkbox(anchor_filter_value_var, Frame, Label="Anchor Filter Value", xPos=0, yPos=50, bG="#CCC", Width=24)
		self.getValue(anchor_filter_value_var, "anchor_filter_value", True)
		
		################################################################
		
		filterable_var = tk.IntVar()
		tk_checkbox(filterable_var, Frame, Label="Filterable", xPos=0, yPos=75, bG="#CCC", Width=24)
		self.getValue(filterable_var, "list_filterable", True)
		
		################################################################
		
		check_cross_var = tk.IntVar()
		tk_checkbox(check_cross_var, Frame, Label="Check / Cross", xPos=0, yPos=100, bG="#CCC", Width=24)
		self.getValue(check_cross_var, "list_check_cross", True)
		
		################################################################
		
		show_value_var = tk.IntVar()
		tk_checkbox(show_value_var, Frame, Label="Show Linked Table Value", xPos=0, yPos=125, bG="#CCC", Width=24)
		self.getValue(show_value_var, "list_show_linked_table_value", True)
		
		################################################################
		
		column_width_var = tk.StringVar()
		tk_entry(column_width_var, Frame, Label="Column Width: ", xPos=250, yPos=0, bG="#CCC", Width=24)
		self.getValue(column_width_var, "list_column_width")
					
		################################################################
		
		def SaveCommand():
			
			self.init_vars()
			self.currentField.insert('list_searchable', str(searchable_var.get())) # list_searchable
			self.currentField.insert('list_filter_value', str(list_filter_value_var.get())) # list_filter_value
			self.currentField.insert('anchor_filter_value', str(anchor_filter_value_var.get())) # anchor_filter_value
			self.currentField.insert('list_filterable', str(filterable_var.get())) # list_filterable
			self.currentField.insert('list_check_cross', str(check_cross_var.get())) # list_column_width
			self.currentField.insert('list_show_linked_table_value', str(show_value_var.get())) # list_show_linked_table_value
			self.currentField.insert('list_column_width', column_width_var.get()) # list_column_width
			
		SaveButton.config(command=SaveCommand)

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #

	def addOptionsFrame(self, Frame, SaveButton, **kwargs):
			
		form_input_class_var = tk.StringVar()
		tk_entry(form_input_class_var, Frame, Label="Form Input Class: ", xPos=0, yPos=0, bG="#CCC", Width=21)
		self.getValue(form_input_class_var, "form_input_class_add")
		
		upload_shortcut_button_var = tk.IntVar()
		tk_checkbox(upload_shortcut_button_var, Frame, Label="Upload Shortcut Button", xPos=0, yPos=50, bG="#CCC", Width=21)
		self.getValue(upload_shortcut_button_var, "add_upload_shortcut_button", True)
		
		################################################################
		
		def SaveCommand():
			self.init_vars()
			self.currentField.insert('form_input_class_add', form_input_class_var.get()) # form_input_class_add
			self.currentField.insert('add_upload_shortcut_button', str(upload_shortcut_button_var.get())) 
			
		SaveButton.config(command=SaveCommand,**kwargs)

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
		
	def editOptionsFrame(self, Frame, SaveButton,**kwargs):
		
		form_input_class_var = tk.StringVar()
		tk_entry(form_input_class_var, Frame, Label="Form Input Class: ", xPos=0, yPos=0, bG="#CCC", Width=21)
		self.getValue(form_input_class_var, "form_input_class_edit")

		edit_change_deliberately = tk.IntVar()
		tk_checkbox(edit_change_deliberately, Frame, Label="Change Deliberately", xPos=0, yPos=50, bG="#CCC", Width=21)
		self.getValue(edit_change_deliberately, "edit_change_deliberately", True)
		
		upload_shortcut_button_var = tk.IntVar()
		tk_checkbox(upload_shortcut_button_var, Frame, Label="Upload Shortcut Button", xPos=0, yPos=75, bG="#CCC", Width=21)
		self.getValue(upload_shortcut_button_var, "edit_upload_shortcut_button", True)
		
		def SaveCommand():
			self.init_vars()
			self.currentField.insert('form_input_class_edit', form_input_class_var.get()) # form_input_class_edit
			self.currentField.insert('edit_change_deliberately', str(edit_change_deliberately.get())) 
			self.currentField.insert('edit_upload_shortcut_button', str(upload_shortcut_button_var.get())) 
			
		SaveButton.config(command=SaveCommand)

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #

	def linkedTableOptionsFrame(self, Frame, SaveButton, **kwargs):
		table_name_var = tk.StringVar()
		table_key_var = tk.StringVar()
		table_value_var = tk.StringVar()
		table_custom_var = tk.StringVar()
		tablename_custom_var = tk.StringVar()
		
		mysql = MySQLConnect( self.root.saveFile, self.root.selectedDB )
		
		def table_selected(table):
			
			def update_custom(fld):
				table_custom_var.set( fld )
			
			tk_entry(tablename_custom_var, Frame, Label="Custom Table Name: ", xPos=0, yPos=50, bG="#CCC", Width=20)
			self.getValue(tablename_custom_var, 'linked_table_custom_table', Default=table)
			
			FIELDS = mysql.fieldList(table)
			if FIELDS:
				tk_optionmenu(table_key_var, Frame, Label="Table Key: ", ITEMS=FIELDS, xPos=0, yPos=100, bG="#CCC",Command=None, Width=15)
				tk_optionmenu(table_value_var, Frame, Label="Table Value: ", ITEMS=FIELDS, xPos=0, yPos=150, bG="#CCC",Command=update_custom, Width=15)
				self.getValue(table_key_var, 'linked_table_key')
				self.getValue(table_value_var, 'linked_table_value')
			
			tk_entry(table_custom_var, Frame, Label="Custom Name: ", xPos=0, yPos=200, bG="#CCC", Width=20)
			self.getValue(table_custom_var, 'linked_table_custom')
			
		TABLES = mysql.tableList(  )
		tk_optionmenu(table_name_var, Frame, Label="Table Name: ", ITEMS=TABLES, xPos=0, yPos=0, bG="#CCC",Command=table_selected, Width=15)
		table = self.getValue(table_name_var, 'linked_table_name')
			
		if table != None and table != "":
			table_selected(table)

		def SaveCommand():
			self.init_vars()
			self.currentField.insert('linked_table_name', table_name_var.get())
			self.currentField.insert('linked_table_custom_table', tablename_custom_var.get())
			self.currentField.insert('linked_table_key', table_key_var.get())
			self.currentField.insert('linked_table_value', table_value_var.get())
			self.currentField.insert('linked_table_custom', table_custom_var.get())
			
		SaveButton.config(command=SaveCommand)
		
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #

	def modelSettingsOptionsFrame(self, Frame, SaveButton, **kwargs):
		
		################################################################
		
		required_var = tk.IntVar()
		tk_checkbox(required_var, Frame, "Required Field", 0, 0, bG="#CCC", Width=17)	
		self.getValue(required_var, "required", True, 0)
		
		################################################################

		model_has_default = tk.IntVar()
		tk_checkbox(model_has_default, Frame, "Has Default Value", 0, 25, bG="#CCC", Width=17)	
		self.getValue(model_has_default, "model_has_default", True, 0)
		
		################################################################
		
		model_default_value = tk.StringVar()
		tk_entry(model_default_value, Frame, Label="Default Field Value: ", xPos=0, yPos=50, bG="#CCC", Width=17)
		self.getValue(model_default_value, "model_default_value")
		
		################################################################
		
		model_default_on_empty = tk.IntVar()
		tk_checkbox(model_default_on_empty, Frame, "Set Default On Empty", 0, 100, bG="#CCC", Width=17)	
		self.getValue(model_default_on_empty, "model_default_on_empty", True, 0)
		
		################################################################
		
		def SaveCommand():
			self.init_vars()
			self.currentField.insert('required', str(required_var.get())) # required
			self.currentField.insert('model_has_default', str(model_has_default.get())) # required
			self.currentField.insert('model_default_value', model_default_value.get()) # model_default_value
			self.currentField.insert('model_default_on_empty', str(model_default_on_empty.get())) # model_default_on_empty
			
		SaveButton.config(command=SaveCommand)
