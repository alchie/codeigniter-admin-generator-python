import os
# using Tkinter's Optionmenu() as a combobox
try:
    # Python2
    import Tkinter as tk
except ImportError:
    # Python3
    import tkinter as tk
   
from lib.mysql import MySQLConnect
from lib import sqlite
from lib import unzip, create_file
from lib import models, views, controllers_admin, controllers_api, langs, configs, menu
from lib.generate import Generate as GenerateMVC

from mvcFrame import mvcFrame
from menuFrame import menuFrame
from tableFrame import tableFrame
from fieldFrame import fieldFrame
from ui import *
from images import image_resources

class AppMain(tk.Tk):
	
	appTitle = 'CodeIgniter Admin Generator'
	appVersion = '1.7.4'
	
	saveFile = None
	
	# frames
	mainBar = None
	navBar = None
	mvcSettingsFrame = None
	menuSettingsFrame = None
	tableSettingsFrame = None
	fieldSettingsFrame = None
	formInputFrame = None
	validationFrame = None
	bodyFrame = None
	footerFrame = None
	currentMainFrame = 'mvc'
	popupFrame = None
	
	# selected Vars
	selectedDB = None
	selectedTable = None
	selectedField = None
	
	# options
	fieldOption = None
	tableOption = None
	dbOption = None
	
	#buttons
	defaultButtonColor = None
	mvcSettingsButton = None
	menuSettingsButton = None
	tableSettingsButton = None
	tableSettingsButton = None
	generateTableButton = None
	generateMVCButton = None
	
	#images
	settingsImage = None
	
	mysqlConnected = False
	mysql = None
	
	dbDefault = None

	mvcClass = None
	menuClass = None
	tableClass = None
	fieldClass = None
	
	fields_length = 0
	
	generate_model_files = None
	generate_view_files = None
	generate_controler_files = None
	generate_lang_files = None
	
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
	
	def __init__(self, *args, **kwargs):
		tk.Tk.__init__(self, *args, **kwargs)
		self.title(self.appTitle + ' v' +self.appVersion)
		self.config(padx=10,pady=10,bg="#5A5A5A")
		self.resizable(width=False, height=False)
		#logo = tk.PhotoImage(file="images/ci_logo2.gif")
		logo = tk.PhotoImage(data=image_resources.ci_logo2_gif)
		self.tk.call('wm', 'iconphoto', self._w, logo)
		
		self.preload_images()

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
		
	def preload_images(self):
		#self.closeImage = tk.PhotoImage(file="images/close.gif")
		#self.settingsImage = tk.PhotoImage(file="images/settings.gif")
		#self.homeImage = tk.PhotoImage(file="images/home.gif")

		self.closeImage = tk.PhotoImage(data=image_resources.close_gif)
		self.settingsImage = tk.PhotoImage(data=image_resources.settings_gif)
		self.homeImage = tk.PhotoImage(data=image_resources.home_gif)

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
		
	def display(self, config):
		
		self.saveFile = config
		
		self.mysql = MySQLConnect( config )
		self.mysqlConnected = self.mysql.isConnected()

		################################################################

		self.mainBar = tk.Frame()
		self.navBar = tk.Frame()
		self.bodyFrame = tk.Frame()
		self.footerFrame = tk.Frame()

		################################################################
		
		AppLabel = tk.Label(self.mainBar, text="CAG v"+self.appVersion, font=('Arial',(18),"bold"))
		AppLabel.place(x=60, y=0) #.grid(row=0, column=0)
		AppLabel.config(padx=9,bg="#7F7F7F",fg="#020202")
		
		#ciLogo3 = tk.PhotoImage(file="images/ci_logo3.gif")
		ciLogo3 = tk.PhotoImage(data=image_resources.ci_logo3_gif)
		ciLogoPanel3 = tk.Label(self.mainBar, image = ciLogo3, bg="#7F7F7F")
		ciLogoPanel3.config( height=30 )
		ciLogoPanel3.place(x=20, y=0)
		
		################################################################
		
		self.mvcClass = mvcFrame(self)
		self.menuClass = menuFrame(self)
		self.tableClass = tableFrame(self)
		self.fieldClass = fieldFrame(self)
		
		################################################################
		
		self.dbDefault = tk.StringVar(self)
		self.dbDefault.set("Select a Database")
		self.dbOption = tk.OptionMenu(self.mainBar, self.dbDefault, None)
		self.dbOption.config(width=18,bd=0,state=tk.DISABLED)
		
		if self.mysqlConnected:
			self.dbOption.destroy()
			self.dbOption = None
			DBS = self.mysql.dbList()
			self.dbOption = tk.OptionMenu(self.mainBar, self.dbDefault, *DBS, command=self.dbSelected)
			self.dbOption.config(width=18,bd=0)
		
		self.dbOption.place(x=220, y=0) #.grid(row=0, column=1)
		
		################################################################
		
		tableDefault = tk.StringVar(self)
		tableDefault.set("Select a Table") 
		self.tableOption = tk.OptionMenu(self.mainBar, tableDefault, "None")
		self.tableOption.config(width=18,bd=0,state=tk.DISABLED)
		self.tableOption.place(x=410, y=0) #.grid(row=0, column=6)
		
		################################################################
		
		fieldOptionVariable = tk.StringVar(self)
		fieldOptionVariable.set("Select a Field")
		self.fieldOption = tk.OptionMenu(self.mainBar, fieldOptionVariable, "None")
		self.fieldOption.config(width=18,bd=0,state=tk.DISABLED)
		self.fieldOption.place(x=600, y=0) #.grid(row=0, column=7)
		
		################################################################
				
		self.mainBar.config(width=805,height=50,padx=5,pady=9,bd=1,relief=tk.RAISED,bg="#7F7F7F")
		self.mainBar.grid(columnspan=8)
		self.mainBar.grid_propagate(False)
		
		################################################################
				
		separator = tk.Frame()
		separator.config(height=5,bg="#5A5A5A")
		separator.grid()
		
		################################################################
				
		self.navBar.config(width=805,height=52,padx=10,pady=10,bd=1,relief=tk.RAISED,bg="#7F7F7F")
		self.navBar.grid(columnspan=8)
		self.navBar.grid_propagate(False)

		################################################################
		
		separator = tk.Frame()
		separator.config(height=5,bg="#5A5A5A")
		separator.grid()

		################################################################
		
		self.bodyFrame.config(width=805,height=440,padx=10,pady=20,bd=1,relief=tk.RAISED,bg="#ff3c00")
		self.bodyFrame.grid(row=4, column=0)
		self.bodyFrame.grid_propagate(False)
		
		################################################################
				
		#ciLogo = tk.PhotoImage(file='images/ci_logo.gif')
		ciLogo = tk.PhotoImage(data=image_resources.ci_logo_gif)
		ciLogoPanel = tk.Label(self.bodyFrame, image = ciLogo, bg="#ff3c00")
		ciLogoPanel.config( height=320 )
		ciLogoPanel.place(x=280, y=0) #grid(row=0,column=0, columnspan=15)

		bodyLabel = tk.Label(self.bodyFrame, text="CodeIgniter 2.2.0 \nAdmin Generator v"+self.appVersion, font=('Serif',(20),"bold"))
		bodyLabel.config(bg="#ff3c00",fg="#FFF")
		bodyLabel.place(x=220, y=320) #.grid(row=1, column=0, columnspan=15)

		################################################################
				
		saveFileLabel = tk_label(self.bodyFrame, Label="SAVING TO: " + self.saveFile, xPos=20, yPos=0, Width=27, bG="#ff3c00", Font=('Serif',(9),"bold"), fG="#FFF", Anchor='w')
		
		def saveFileOption():
						
			saveFileFrame = tk.Frame(self.bodyFrame)
			saveFileFrame.config(width=245,height=215,padx=10,pady=20,bd=1,relief=tk.RAISED,bg="#ffa000")
			saveFileFrame.place(x=0, y=0)
			
			selectSaveFile = tk.StringVar()
			selectSaveFile.set( self.saveFile )
			
			availableSaveFiles=[]
			
			if os.path.exists('save'):
				availableSaveFiles = [f for f in os.listdir('save') if os.path.isfile(os.path.join('save',f))]
										
			def update_label():
				if selectSaveFile.get() != '':
					self.saveFile = selectSaveFile.get()
					saveFileLabel.config(text="SAVING TO: " + selectSaveFile.get())
					create_file('.config', selectSaveFile.get())
					saveFileFrame.destroy()
					self.dbOption.config(state=tk.DISABLED)
					self.tableOption.config(state=tk.DISABLED)
					self.fieldOption.config(state=tk.DISABLED)
					self.showLocalDBFrame()
			
			if len(availableSaveFiles) > 0:
				tk_optionmenu(selectSaveFile, saveFileFrame, Label="Select Save File: ", ITEMS=availableSaveFiles, xPos=0, yPos=0, bG="#ffa000",Command=None, Width=22)
				tk_button(saveFileFrame, Label="Update Save File", xPos=0, yPos=50, bCommand=update_label, bG='#CCC', Width=24)
			
			newSaveFilename = tk.StringVar()
			tk_entry(newSaveFilename, saveFileFrame, Label="New Filename: ", xPos=0, yPos=100, bG="#ffa000", Width=27)
			
			def create_new_file():
				newFile = sqlite.createNewSaveFile( newSaveFilename.get() + ".db" )
				newFile.create()
				selectSaveFile.set( newSaveFilename.get() + ".db" )
				create_file('.config', newSaveFilename.get() + ".db")
				update_label()
				self.mysqlConnected = False
				self.dbOption.config(state=tk.DISABLED)
				self.showLocalDBFrame()
				
			tk_button(saveFileFrame, Label="Create New File", xPos=0, yPos=150, bCommand=create_new_file, bG='#CCC', Width=24)
			
		settings_button(self.bodyFrame, xPos=0, yPos=0, bCommand=saveFileOption, bG='#ff3c00', bImage=self.settingsImage, bState=tk.NORMAL)
		
		if len(self.saveFile) == 0:
			saveFileOption()
		
		if not self.mysqlConnected and len(self.saveFile) > 0:
			self.showLocalDBFrame()

		################################################################
		
		separator = tk.Frame()
		separator.config(height=5,bg="#5A5A5A")
		separator.grid()

		################################################################
		
		self.footerFrame.config(width=805, height=74,padx=10,pady=10,bd=1,relief=tk.RAISED,bg="#7F7F7F")
		self.footerFrame.grid(row=6, column=0)
		self.footerFrame.grid_propagate(False)

		################################################################
		
		self.generate_model_files = tk.IntVar()
		self.generate_model_files.set( 1 )
		tk_checkbox(self.generate_model_files, self.footerFrame, Label="Model v" + GenerateMVC.model_version, xPos=0, yPos=35, bG="#7F7F7F", Width=20)
		
		self.generate_view_files = tk.IntVar()
		self.generate_view_files.set( 0 )
		tk_checkbox(self.generate_view_files, self.footerFrame, Label="View", xPos=200, yPos=35, bG="#7F7F7F", Width=20)
		
		self.generate_controler_files = tk.IntVar()
		self.generate_controler_files.set( 0 )
		tk_checkbox(self.generate_controler_files, self.footerFrame, Label="Controller", xPos=400, yPos=35, bG="#7F7F7F", Width=20)
		
		self.generate_lang_files = tk.IntVar()
		self.generate_lang_files.set( 0 )
		tk_checkbox(self.generate_lang_files, self.footerFrame, Label="Language", xPos=600, yPos=35, bG="#7F7F7F", Width=20)
		
		################################################################
		
		self.mvcSettingsButton = tk.Button(self.navBar,text="MVC Settings", width=21, command=self.mvcClass.display)
		self.mvcSettingsButton.grid(row=0, column=0)
		self.mvcSettingsButton.config( state=tk.DISABLED, bd=0, activebackground='red', activeforeground='white')
		self.defaultButtonColor = self.mvcSettingsButton.cget('bg')
		
		self.menuSettingsButton = tk.Button(self.navBar,text="Menu Settings", width=21, command=self.menuClass.display)
		self.menuSettingsButton.grid(row=0, column=1)
		self.menuSettingsButton.config( state=tk.DISABLED, bd=0, activebackground='red', activeforeground='white')

		self.tableSettingsButton = tk.Button(self.navBar,text="Table Settings", width=21, command=self.tableClass.display)
		self.tableSettingsButton.grid(row=0, column=2)
		self.tableSettingsButton.config( state=tk.DISABLED, bd=0, activebackground='red', activeforeground='white')
		
		self.fieldSettingsButton = tk.Button(self.navBar, text="Field Settings", width=22, command=self.fieldClass.display)
		self.fieldSettingsButton.grid(row=0, column=3)
		self.fieldSettingsButton.config( state=tk.DISABLED, bd=0, activebackground='red', activeforeground='white')

		################################################################
		
		def _sqlDump():
			# create SQL Dump
			currentMVC = sqlite.mvcSettings(self.saveFile, self.selectedDB)
			saveDir = currentMVC.get_value('directory')
			mvc_folder = currentMVC.get_value("mvc_directory")
			app_folder = currentMVC.get_value('application_directory')
			
			if saveDir != '' and saveDir != None:
				mvcDir = os.path.join( saveDir, mvc_folder )
				appDir = os.path.join( mvcDir , app_folder)
				
				if not os.path.exists(mvcDir):
					os.makedirs(mvcDir)
					print("Folder Created: " + mvcDir)

				if not os.path.exists(appDir):
					os.makedirs(appDir)
					print("Folder Created: " + appDir)
					
				#configs.MenuFile(self.saveFile, self.selectedDB, appDir).create()
				configs.SQLdump(self.saveFile, self.selectedDB, mvcDir).save()
		
		################################################################
				
		def generateTable():
			current_table = self.selectedTable
			table_settings = sqlite.tableSettings( self.saveFile, self.selectedDB, self.selectedTable )
			generate = GenerateMVC(self.saveFile, self.selectedDB, current_table)
			generate.set_generate( bool(self.generate_model_files), bool(self.generate_view_files), bool(self.generate_controler_files), bool(self.generate_lang_files) )
			generate.table_files()
			
			dumpTable = configs.DumpTextData( self.saveFile, self.selectedDB, current_table )
			dumpTable.save()
			
			if table_settings.get_value( 'parent' ) != 'none' and table_settings.get_value( 'parent' ) != None and table_settings.get_value( 'parent' ) != '' :
				current_table = table_settings.get_value( 'parent' )
				generate2 = GenerateMVC(self.saveFile, self.selectedDB, current_table)
				generate2.table_files()
				
				dumpTable = configs.DumpTextData( self.saveFile, self.selectedDB, current_table )
				dumpTable.save()
			
			if table_settings.get_value( 'has_children' ) == '1':
				table_children = sqlite.tableChildren( self.saveFile, self.selectedDB, self.selectedTable )
				for child_item in table_children.item_list():
					generate_child = GenerateMVC(self.saveFile, self.selectedDB, child_item[2])
					generate_child.table_files()
					dumpTable_child = configs.DumpTextData( self.saveFile, self.selectedDB, child_item[2] )
					dumpTable_child.save()
			
			_sqlDump()
			
		self.generateTableButton = tk.Button(self.footerFrame,text="Generate Table", command=generateTable)
		self.generateTableButton.grid(row=0, column=0)
		self.generateTableButton.config( state=tk.DISABLED,width=36, bd=0, font=('Serif',(11),"bold"), activebackground='red', activeforeground='white')

		################################################################
				
		def generateMVC():
			
			mysql = MySQLConnect( self.saveFile, self.selectedDB )
			TABLES = mysql.tableList()
			
			for tbl in TABLES:
				generate = GenerateMVC(self.saveFile, self.selectedDB, tbl)
				generate.set_generate( bool(self.generate_model_files.get()), bool(self.generate_view_files.get()), bool(self.generate_controler_files.get()), bool(self.generate_lang_files.get()) )
				generate.table_files()
				
				dumpTable = configs.DumpTextData( self.saveFile, self.selectedDB, tbl )
				dumpTable.save()
			
			_sqlDump()
			
		self.generateMVCButton = tk.Button(self.footerFrame,text="Generate MVC", command=generateMVC)
		self.generateMVCButton.grid(row=0, column=1)
		self.generateMVCButton.config( state=tk.DISABLED,width=37, bd=0, font=('Serif',(11),"bold"), activebackground='red', activeforeground='white')

		################################################################
				
		self.mainloop()

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 

	def hideActiveFrames(self, exclude='mvc'):
		self.currentMainFrame = exclude
		self.bodyFrame.grid_remove()
		
		if self.mvcSettingsFrame != None and exclude != 'mvc':
			self.mvcSettingsFrame.grid_remove()
			self.mvcSettingsButton.config(bg=self.defaultButtonColor)
			
		if self.menuSettingsFrame != None and exclude != 'menu':
			self.menuSettingsFrame.grid_remove()
			self.menuSettingsButton.config(bg=self.defaultButtonColor)
			
		if self.tableSettingsFrame != None and exclude != 'table':
			self.tableSettingsFrame.grid_remove()
			self.tableSettingsButton.config(bg=self.defaultButtonColor)
			
		if self.fieldSettingsFrame != None and exclude != 'field':
			self.fieldSettingsFrame.grid_remove()
			self.fieldSettingsButton.config(bg=self.defaultButtonColor)

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 

	def showLocalDBFrame(self):
		dbsettingsFrame = tk.Frame(self.bodyFrame)
		dbsettingsFrame.config(width=245,height=290,padx=10,pady=20,bd=1,relief=tk.RAISED,bg="#ffa000")
		dbsettingsFrame.place(x=510, y=30)
		
		saveFile = os.path.join( 'save', self.saveFile )
		
		if os.path.exists( saveFile ):
			settings = sqlite.Settings( self.saveFile )
			
			dbhost_var = tk.StringVar()
			dbhost_var.set("localhost")
			tk_entry(dbhost_var, dbsettingsFrame, Label="DB Host: ", xPos=0, yPos=0, bG="#ffa000", Width=27)
				
			dbuser_var = tk.StringVar()
			dbuser_var.set("root")
			tk_entry(dbuser_var, dbsettingsFrame, Label="DB User: ", xPos=0, yPos=50, bG="#ffa000", Width=27)
			
			dbpasswd_var = tk.StringVar()
			
			tk_entry(dbpasswd_var, dbsettingsFrame, Label="DB Password: ", xPos=0, yPos=100, bG="#ffa000", Width=27)
			
			dbport_var = tk.StringVar()
			dbport_var.set("3306")
			tk_entry(dbport_var, dbsettingsFrame, Label="DB Port: ", xPos=0, yPos=150, bG="#ffa000", Width=27)

			def save_dbsettings():
				settings.insert('dbhost', dbhost_var.get())
				settings.insert('dbuser', dbuser_var.get())
				settings.insert('dbpasswd', dbpasswd_var.get())
				settings.insert('dbport', dbport_var.get())
				
				self.mysql = MySQLConnect( self.saveFile )
				self.mysqlConnected = self.mysql.isConnected()
				
				if self.mysqlConnected:
					dbsettingsFrame.destroy()
					self.dbOption.destroy()
					self.dbOption = None
					DBS = self.mysql.dbList()
					self.dbOption = tk.OptionMenu(self.mainBar, self.dbDefault, *DBS, command=self.dbSelected)
					self.dbOption.config(width=18,bd=0)
					self.dbOption.place(x=220, y=0) #.grid(row=0, column=1)
					
				else:
					tk_label(dbsettingsFrame, Label="Unable to connect to MySQL Server", xPos=0, yPos=240, Width=27, bG="#ffa000", Font=None, fG="#F00", Anchor=None)
					
			tk_button(dbsettingsFrame, Label="Save and Connect", xPos=0, yPos=210, bCommand=save_dbsettings, bG='#FFF', Width=24)
			
			if settings.get_value('dbhost') != None:
				dbhost_var.set( settings.get_value('dbhost') )
			
			if settings.get_value('dbuser') != None:
				dbuser_var.set( settings.get_value('dbuser') )
			
			if settings.get_value('dbpasswd') != None:
				dbpasswd_var.set( settings.get_value('dbpasswd') )
			
			if settings.get_value('dbport') != None:
				dbport_var.set( settings.get_value('dbport') )

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 

	def dbSelected(self, db):
		self.selectedDB = db
				
		self.mvcClass.display()
		
		def tableSelected(table):
			self.selectedTable = table
			
			self.tableClass.display()
				
			def fieldSelected(field):
				self.selectedField = field
				self.fieldSettingsButton.config(state=tk.NORMAL)
				
				self.fieldClass.display()
				
			if self.fieldOption != None:
				self.fieldOption.destroy()
			
			self.menuSettingsButton.config(state=tk.NORMAL)
			self.tableSettingsButton.config(state=tk.NORMAL)
			self.generateTableButton.config(state=tk.NORMAL)
			self.fieldSettingsButton.config(state=tk.DISABLED)
			
			mysql = MySQLConnect(self.saveFile, db)
			FIELDS = mysql.fieldList( table )
			self.fields_length = len(FIELDS)
			if( len(FIELDS) > 0 ):
				fieldOptionVariable = tk.StringVar(self)
				fieldOptionVariable.set("Select a Field")
				self.fieldOption = tk.OptionMenu(self.mainBar, fieldOptionVariable, *FIELDS, command=fieldSelected)
				self.fieldOption.config(width=18,bd=0)
				self.fieldOption.place(x=600, y=0) #.grid(row=0, column=7)
				
		if self.tableOption != None:
			self.tableOption.config(state=tk.DISABLED)
		if self.fieldOption != None:
			self.fieldOption.config(state=tk.DISABLED)
		
		self.mvcSettingsButton.config(state=tk.NORMAL)
		self.menuSettingsButton.config(state=tk.NORMAL)
		self.generateMVCButton.config(state=tk.NORMAL)
		self.generateTableButton.config(state=tk.DISABLED)
		self.tableSettingsButton.config(state=tk.DISABLED)
		self.fieldSettingsButton.config(state=tk.DISABLED)
		
		mysql = MySQLConnect(self.saveFile, db)
		TABLES = mysql.tableList()
		if( len(TABLES) > 0 ):
			if self.tableOption != None:
				self.tableOption.destroy()
			tableDefault = tk.StringVar(self)
			tableDefault.set("Select a Table") 
			self.tableOption = tk.OptionMenu(self.mainBar, tableDefault, *TABLES, command=tableSelected)
			self.tableOption.config(width=18,bd=0)
			self.tableOption.place(x=410, y=0) #.grid(row=0, column=6)
	
