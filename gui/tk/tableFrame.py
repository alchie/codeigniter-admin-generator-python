import os, operator
# using Tkinter's Optionmenu() as a combobox
try:
    # Python2
    import Tkinter as tk
except ImportError:
    # Python3
    import tkinter as tk

from lib.mysql import MySQLConnect
from lib import sqlite
from lib import unzip, create_file
from lib import models, views, controllers_admin, controllers_api, langs, configs, menu
from ui import *

class tableFrame:
	
	root = None
	action = 'add'
	child_key_label, child_key_option = (None, None)
	currentTable = None
	
	optionsFrameName = None
	optionsFrame = None
	
	innerOptionsFrame = None
	
	def __init__(self, root):
		self.root = root
		self.closeImage = root.closeImage
		self.settingsImage = root.settingsImage

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #

	def init_vars(self):
		self.currentTable = sqlite.tableSettings(self.root.saveFile, self.root.selectedDB, self.root.selectedTable)
		
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #

	def showOptionsFrame(self, name='', Label="Options Frame Label", button=False):
		self.optionsFrameName = name
		
		if self.optionsFrame != None:
			self.optionsFrame.destroy()
			self.optionsFrame = None
			
		self.optionsFrame = tk.Frame(self.root.tableSettingsFrame)
		self.optionsFrame.config(width=560, height=360, padx=10,pady=10,bd=1,relief=tk.RAISED,bg="#CCC")
		self.optionsFrame.place(x=200,y=40)
		
		caption = tk.Label(self.optionsFrame, text=Label, font=('Sans',(12),"bold"))
		caption.config(bg="#CCC",fg="#000", width=48)
		caption.place(x=0,y=0)
		
		def closeThisFrame():
			self.optionsFrameName = None
			self.optionsFrame.destroy()
			
		close_button(self.optionsFrame, 515, 0, bCommand=closeThisFrame, bG='#CCC', bImage=self.closeImage, bState=tk.NORMAL)
			
		
		saveButton = tk.Button(self.optionsFrame,text="Save")
		saveButton.config(bg="green", width=12)
		
		if button:
			saveButton.place(x=0,y=0) 

		innerFrame = tk.Frame(self.optionsFrame)
		innerFrame.config(width=535, height=290, padx=0,pady=0,bd=0, bg="#CCC")
		innerFrame.place(x=0,y=45)
		
		return (innerFrame, saveButton)

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #

	def commandOptionsFrame(self, Command, Name=None, Label="Label Here", ShowButton=False, Options=None, Renew=False):
		if self.optionsFrameName == Name:
			self.optionsFrameName = None
			self.optionsFrame.destroy()
			self.optionsFrame = None
			if Renew:
				optionsFrame, saveButton = self.showOptionsFrame(Name, Label, ShowButton)
				Command(optionsFrame, saveButton, options=Options)
		else:
			optionsFrame, saveButton = self.showOptionsFrame(Name, Label, ShowButton)
			Command(optionsFrame, saveButton, options=Options)

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #

	def toggleOptionsFrame(self, Name=None, Button=None, IntVar=0):
		if IntVar == 1 and Button['state'] == 'disabled':
			Button.config(state=tk.NORMAL)
		else:
			Button.config(state=tk.DISABLED)
			if self.optionsFrameName == Name:
				self.optionsFrameName = None
				self.optionsFrame.destroy()	
				self.optionsFrame = None

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #

	def getValue(self, Var, Key, Int=False, Default="", Empty=False):
		
		self.init_vars()
		
		Value = self.currentTable.get_value( Key )

		#~ if (Value==None or Value==""):
			#~ if Default == "" and Int:
				#~ Default = 0
				#~ 
			#~ Value = Default
#~ 
		#~ if Int:
			#~ Value = int( Value )
		
		if Value==None:
			if Default == "" and Int:
				Default = 0
			Value = Default
			
		if Value == "" and Empty == False:
			Value = Default
			
		if Int:
			Value = int( Value )
		
		Var.set( Value )
		return Value
		
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
					
	def display(self):
		self.root.hideActiveFrames('table')
		self.optionsFrameName = None
		
		if self.root.tableSettingsFrame != None:
			self.root.tableSettingsFrame.destroy()
			self.root.tableSettingsFrame = None
		
		self.root.tableSettingsFrame = tk.Frame()
		self.root.tableSettingsFrame.config(width=805, height=440,padx=20,pady=20,bd=1,relief=tk.RAISED,bg="#CCC")
		self.root.tableSettingsFrame.grid(row=4, column=0)
		self.root.tableSettingsFrame.grid_propagate(False)
		
		thisFrame = self.root.tableSettingsFrame

		################################################################
		
		self.root.tableSettingsButton.config(bg='red', activebackground='red', activeforeground='white')
		
		################################################################
		
		tk_label(thisFrame, Label="TABLE SETTINGS", xPos=0, yPos=0, Width=58, bG="#CCC", Font=('Serif',(14),"bold"), fG="#000", Anchor=None)
			
		################################################################
				
		tk_label(thisFrame, Label="Generate Files", xPos=0, yPos=50, Width=27, bG="#CCC", Font=None, fG="#000", Anchor='w')
		
		################################################################

		def show_generate_model_frame():
			self.commandOptionsFrame(self.generateModelFrame, Name='generate_model', Label="Model File Options", ShowButton=True)
			
		generate_model_var = tk.IntVar()
		generate_model = tk_checkbox(generate_model_var, thisFrame, "Model File", 0, 75, bG="#CCC", Width=17, Command=None)
		generate_model_options = settings_button(thisFrame, 155, 73, show_generate_model_frame, '#CCC', bImage=self.settingsImage, bState=tk.NORMAL )
		self.getValue(generate_model_var, "generate_model", True, 1)
		
		################################################################
		
		def show_generate_view_frame():
			self.commandOptionsFrame(self.generateViewFrame, Name='generate_view', Label="View File Options", ShowButton=True)
			
		generate_view_var = tk.IntVar()
		generate_view = tk_checkbox(generate_view_var, thisFrame, "View File", 0, 100, bG="#CCC", Width=17, Command=None)			
		generate_view_options = settings_button(thisFrame, 155, 98, show_generate_view_frame, '#CCC', bImage=self.settingsImage, bState=tk.NORMAL )
		self.getValue(generate_view_var, "generate_view", True, 0)
		show_generate_view_frame()
		
		################################################################
		
		def show_generate_controller_frame():
			self.commandOptionsFrame(self.generateControllerFrame, Name='generate_controller', Label="Controller File Options", ShowButton=True)
			
		generate_controller_var = tk.IntVar()
		generate_controller = tk_checkbox(generate_controller_var, thisFrame, "Controller File", 0, 125, bG="#CCC", Width=17, Command=None)			
		generate_controller_options = settings_button(thisFrame, 155, 123, show_generate_controller_frame, '#CCC', bImage=self.settingsImage, bState=tk.NORMAL )
		self.getValue(generate_controller_var, "generate_controller", True, 0)
		
		################################################################

		generate_lang_var = tk.IntVar()
		generate_lang = tk_checkbox(generate_lang_var, thisFrame, "Language File", 0, 150, bG="#CCC", Width=17, Command=None)
		self.getValue(generate_lang_var, "generate_lang", True, 0)
		
		################################################################
		
		tk_label(thisFrame, Label="More Settings", xPos=0, yPos=175, Width=20, bG="#CCC", Font=None, fG="#000", Anchor='w')
		
		################################################################
		
		def toggle_add_to_menu():
			self.toggleOptionsFrame('add_to_menu', add_to_menu_options, add_to_menu_var.get())
				
		def show_add_to_menu_frame():
			self.commandOptionsFrame(self.addToMenuFrame, Name='add_to_menu', Label="Add to Menu Options", ShowButton=True)
			
		add_to_menu_var = tk.IntVar()
		add_to_menu = tk_checkbox(add_to_menu_var, thisFrame, "Add to Menu", 0, 200, bG="#CCC", Width=17, Command=toggle_add_to_menu)			
		add_to_menu_options = settings_button(thisFrame, 155, 198, show_add_to_menu_frame, '#CCC', bImage=self.settingsImage )

		if self.getValue(add_to_menu_var, "menu", True, 0) == 1:
			add_to_menu_options.config(state=tk.NORMAL)
		
		################################################################

		def toggle_has_children():
			self.toggleOptionsFrame('has_children', has_children_options, has_children_var.get())
				
		def show_has_children_frame():
			self.commandOptionsFrame(self.hasChildrenFrame, Name='has_children', Label="Table Children Options")
			
		has_children_var = tk.IntVar()
		has_children = tk_checkbox(has_children_var, thisFrame, "Has Children", 0, 225, bG="#CCC", Width=17, Command=toggle_has_children)			
		has_children_options = settings_button(thisFrame, 155, 223, show_has_children_frame, '#CCC', bImage=self.settingsImage )

		if self.getValue(has_children_var, "has_children", True, 0) == 1:
			has_children_options.config(state=tk.NORMAL)
		
		################################################################

		def toggle_upload_table():
			self.toggleOptionsFrame('upload_table', upload_table_options, upload_table_var.get())
				
		def show_upload_table_frame():
			self.commandOptionsFrame(self.uploadTableFrame, Name='upload_table', Label="File Upload Options", ShowButton=True)
			
		upload_table_var = tk.IntVar()
		upload_table = tk_checkbox(upload_table_var, thisFrame, "Upload Table", 0, 250, bG="#CCC", Width=17, Command=toggle_upload_table)			
		upload_table_options = settings_button(thisFrame, 155, 248, show_upload_table_frame, '#CCC', bImage=self.settingsImage )

		if self.getValue(upload_table_var, "upload_table", True, 0) == 1:
			upload_table_options.config(state=tk.NORMAL)
		
		################################################################
		
		def save():
			self.init_vars()
			self.currentTable.insert('generate_model', str(generate_model_var.get())) 
			self.currentTable.insert('generate_view', str(generate_view_var.get()))
			self.currentTable.insert('generate_controller', str(generate_controller_var.get()))
			self.currentTable.insert('generate_lang', str(generate_lang_var.get()))
			self.currentTable.insert('menu', str(add_to_menu_var.get()))
			self.currentTable.insert('has_children', str(has_children_var.get()))
			self.currentTable.insert('upload_table', str(upload_table_var.get()))

		saveButton = tk.Button(thisFrame ,text="Save Settings", command=save)
		saveButton.place(x=0,y=0) 
		saveButton.config(bg="green", width=19)
		
		################################################################
	
	def generateModelFrame(self, Frame, SaveButton, options='none'):
		mysql = MySQLConnect( self.root.saveFile, self.root.selectedDB )
		
		FIELDS = mysql.fieldList( self.root.selectedTable )
		FIELDS = ['none'] + FIELDS
		
		count_field_var = tk.StringVar()
		tk_optionmenu(count_field_var, Frame, "Count Field: ", FIELDS, xPos=0, yPos=0, Width=14)
		self.getValue(count_field_var, "count_field", Default='none')

		################################################################
		
		def save():
			self.init_vars()
			self.currentTable.insert('increment_decrement', increment_decrement_var.get()) 
			
		SaveButton.config(command=save)
		mysql.close()
		
	def generateViewFrame(self, Frame, SaveButton, options='none'):
		
		mysql = MySQLConnect( self.root.saveFile, self.root.selectedDB )
		
		title_var = tk.StringVar()
		tk_entry(title_var, Frame, Label="Title: ", xPos=0, yPos=0, bG="#CCC", Width=19)
		self.getValue(title_var, "title")
		
		################################################################

		singular_var = tk.StringVar()
		tk_entry(singular_var, Frame, Label="Singular Title: ", xPos=0, yPos=45, bG="#CCC", Width=19)
		self.getValue(singular_var, "singular")
		
		################################################################
		
		plural_var = tk.StringVar()
		tk_entry(plural_var, Frame, Label="Plural Title: ", xPos=0, yPos=90, bG="#CCC", Width=19)
		self.getValue(plural_var, "plural")
		
		################################################################
		
		tableprefix_var = tk.StringVar()
		tk_entry(tableprefix_var, Frame, Label="Table Prefix: ", xPos=190, yPos=0, bG="#CCC", Width=19)
		self.getValue(tableprefix_var, "table_prefix")
		
		################################################################
		
		FIELDS = mysql.fieldList( self.root.selectedTable )
		FIELDS = ['none'] + FIELDS
		
		orderby_var = tk.StringVar()
		tk_optionmenu(orderby_var, Frame, "List Order By: ", FIELDS, xPos=190, yPos=45, Width=14)
		self.getValue(orderby_var, "order_by", Default=FIELDS[1])

		################################################################
		
		orderorientation_var = tk.StringVar()
		tk_optionmenu(orderorientation_var, Frame, "List Order Sort: ", ["DESC","ASC"], xPos=190, yPos=90, Width=14)
		self.getValue(orderorientation_var, "order_orientation", Default="DESC")
		
		################################################################
		
		limit_var = tk.StringVar()
		tk_entry(limit_var, Frame, "List Limit: ", xPos=190, yPos=135, Width=19)
		self.getValue(limit_var, "listlimit", Default=20)
		
		################################################################

		view_template_var = tk.StringVar()
		tk_optionmenu(view_template_var, Frame, "View Template: ", ["default", "template-1"], xPos=0, yPos=135, Width=14)
		self.getValue(view_template_var, "view_template", Default='default')
		
		################################################################
		
		TABLES = mysql.tableList()
		TABLES = ['none'] + TABLES
		
		FIELDS = mysql.fieldList( self.root.selectedTable )
		FIELDS = ['none'] + FIELDS
		
		################################################################
		
		parent_var = tk.StringVar()
		tk_optionmenu(parent_var, Frame, "Parent Table: ", TABLES, xPos=0, yPos=180, Width=14)
		self.getValue(parent_var, "parent", Default='none')
		
		################################################################
		
		reqfilter_var = tk.StringVar()
		tk_optionmenu(reqfilter_var, Frame, "Required Filter: ", FIELDS, xPos=0, yPos=225, Width=14)
		self.getValue(reqfilter_var, "filter", Default='none')
		
		################################################################
		
		primary_key_var = tk.StringVar()
		tk_optionmenu(primary_key_var, Frame, "Primary Key: ", FIELDS, xPos=190, yPos=180, Width=14)
		self.getValue(primary_key_var, "primary_key", Default=FIELDS[1])
		
		################################################################
		
		primary_title_var = tk.StringVar()
		tk_optionmenu(primary_title_var, Frame, "Primary Title: ", FIELDS, xPos=190, yPos=225, Width=14)
		self.getValue(primary_title_var, "primary_title", Default=FIELDS[1])
		
		################################################################
		
		slug_field_var = tk.StringVar()
		tk_optionmenu(slug_field_var, Frame, "Slug Field: ", FIELDS, xPos=375, yPos=0, Width=14)
		self.getValue(slug_field_var, "slug_field", Default='none')
		
		################################################################
		
		slug_value_var = tk.StringVar()
		tk_optionmenu(slug_value_var, Frame, "Slug Value: ", FIELDS, xPos=375, yPos=45, Width=14)
		self.getValue(slug_value_var, "slug_value", Default='none')
		
		################################################################
		
		active_field_var = tk.StringVar()
		tk_optionmenu(active_field_var, Frame, "Active Field: ", FIELDS, xPos=375, yPos=90, Width=14)
		self.getValue(active_field_var, "active_field", Default='none')
		
		################################################################
		
		tk_label(Frame, Label="List Actions Buttons", xPos=375, yPos=140, Width=14, bG="#CCC", Font=None, fG="#000", Anchor='w')
		add_action_var = tk.IntVar()
		self.getValue(add_action_var, "actions_add", True, Default='1')
		add_button = tk_checkbox(add_action_var, Frame, "Add", 375, 160, bG="#CCC", Width=16)
		
		edit_action_var = tk.IntVar()
		self.getValue(edit_action_var, "actions_edit", True, Default='1')
		edit_button = tk_checkbox(edit_action_var, Frame, "Edit", 375, 180, bG="#CCC", Width=16)			
		
		delete_action_var = tk.IntVar()
		self.getValue(delete_action_var, "actions_delete", True, Default='1')
		delete_button = tk_checkbox(delete_action_var, Frame, "Delete", 375, 200, bG="#CCC", Width=16)	

		################################################################
		
		def save():
			self.init_vars()
			self.currentTable.insert('title',title_var.get()) 
			self.currentTable.insert('singular',singular_var.get()) 
			self.currentTable.insert('plural',plural_var.get()) 
			self.currentTable.insert('table_prefix',tableprefix_var.get()) 
			self.currentTable.insert('order_by',orderby_var.get()) 
			self.currentTable.insert('order_orientation',orderorientation_var.get()) 
			self.currentTable.insert('listlimit',limit_var.get()) 
			self.currentTable.insert('view_template',view_template_var.get()) 
			
			self.currentTable.insert('parent', parent_var.get()) # parent
			self.currentTable.insert('filter', reqfilter_var.get()) # filter
			self.currentTable.insert('primary_key', primary_key_var.get()) # primary_key
			self.currentTable.insert('primary_title', primary_title_var.get()) # primary_title
			self.currentTable.insert('slug_field', slug_field_var.get()) # slug_field
			self.currentTable.insert('slug_value', slug_value_var.get()) # slug_value
			self.currentTable.insert('active_field', active_field_var.get()) # active_field
			
			self.currentTable.insert('actions_add', str(add_action_var.get()))
			self.currentTable.insert('actions_edit', str(edit_action_var.get()))
			self.currentTable.insert('actions_delete', str(delete_action_var.get()))
			
		SaveButton.config(command=save)
		mysql.close()
		
	def generateControllerFrame(self, Frame, SaveButton, options='none'):
		
		allow_duplicates_add_var = tk.IntVar()
		self.getValue(allow_duplicates_add_var, "allow_duplicates_add", True, Default='0')
		allow_duplicates_add = tk_checkbox(allow_duplicates_add_var, Frame, Label="Allow Duplicates", xPos=0, yPos=0, bG="#CCC", Width=27)
		
		def save():
			self.init_vars()
			self.currentTable.insert('allow_duplicates_add', str(allow_duplicates_add_var.get())) 
			
		SaveButton.config(command=save)
		
		

	def addToMenuFrame(self, Frame, SaveButton, options='none'):
		
		currentMenu = sqlite.menuSettings(self.root.saveFile, self.root.selectedDB)
		menulist = currentMenu.parentlist()
		
		PARENT_MENUS = ["none"]
		for menu in menulist:
			PARENT_MENUS.append(menu[1])
		
		parent_menu_var = tk.StringVar()
		tk_optionmenu(parent_menu_var, Frame, "Parent Menu: ", PARENT_MENUS, xPos=0, yPos=0, Width=16)
		self.getValue(parent_menu_var, "parent_menu", Default='none')
		
		menu_order_var = tk.StringVar()
		tk_entry(menu_order_var, Frame, "Menu Order: ", xPos=0, yPos=50, Width=21)
		self.getValue(menu_order_var, "menu_order", Default='0')
		
		menu_title_var = tk.StringVar()
		tk_entry(menu_title_var, Frame, "Menu Title: ", xPos=0, yPos=100, Width=21)
		self.getValue(menu_title_var, "menu_title")
		
		menu_icon_var = tk.StringVar()
		tk_entry(menu_icon_var, Frame, "Menu Icon: ", xPos=0, yPos=150, Width=21)
		self.getValue(menu_icon_var, "menu_icon", Default="fa fa-bars fa-fw")
		
		def save():
			self.init_vars()
			self.currentTable.insert('parent_menu', str(parent_menu_var.get())) # parent menu
			self.currentTable.insert('menu_order', menu_order_var.get()) # menu_order
			self.currentTable.insert('menu_title', menu_title_var.get()) # menu_title
			self.currentTable.insert('menu_icon', menu_icon_var.get()) # menu_icon
			
		SaveButton.config(command=save)

	def hasChildrenFrame(self, Frame, SaveButton, options='none'):

		scrollbar = tk.Scrollbar(Frame, orient=tk.VERTICAL)
		listbox = tk.Listbox(Frame, yscrollcommand=scrollbar.set)
		listbox.config(width=20,height=14)
		listbox.place(x=0,y=50) 
		scrollbar.config(command=listbox.yview)
		scrollbar.place(x=167,y=50, height=228)

		TableChildren = sqlite.tableChildren(self.root.saveFile, self.root.selectedDB, self.root.selectedTable)

		child_list = TableChildren.item_list()
		for child_item in child_list:
			listbox.insert(tk.END, child_item[2] )
		
		table_name_var = tk.StringVar()
		table_field_var = tk.StringVar()
		parent_field_var = tk.StringVar()
		title_var = tk.StringVar()
		button_title_var = tk.StringVar()
		table_name_custom_var = tk.StringVar()
		order_var = tk.StringVar()
		
		self.table_field_lbl, self.parent_key_lbl, self.table_field, self.parent_key = (None, None, None, None)
		self.add_button, self.sav_button, self.del_button = (None, None, None)
		self.table_title_lbl, self.table_title = (None, None)
		self.button_title_lbl, self.button_title = (None, None)
		self.table_name_selected_frame = None
		self.table_name_custom_lbl, self.table_name_custom = (None, None)
		self.child_order_lbl, self.child_order = (None, None)
		
		def reset_child_form(resetList=False):
			
			if resetList:
				listbox.delete(0, tk.END)
			
				child_list = TableChildren.item_list()
				for child_item in child_list:
					listbox.insert(tk.END, child_item[2] )
			
			if self.table_field != None:
				table_field_var.set('')
				self.table_field.destroy()
				self.table_field = None
				
			if self.table_field_lbl != None:
				self.table_field_lbl.destroy()
				self.table_field_lbl = None
							
			if self.parent_key != None:
				parent_field_var.set('')
				self.parent_key.destroy()
				self.parent_key = None
				
			if self.parent_key_lbl != None:
				self.parent_key_lbl.destroy()
				self.parent_key_lbl = None

			if self.table_title_lbl != None:
				self.table_title_lbl.destroy()
				self.table_title_lbl = None

			if self.table_title != None:
				title_var.set('')
				self.table_title.destroy()
				self.table_title = None
				
			if self.button_title_lbl != None:
				self.button_title_lbl.destroy()
				self.button_title_lbl = None

			if self.button_title != None:
				button_title_var.set('')
				self.button_title.destroy()
				self.button_title = None
				
			if self.add_button != None:
				self.add_button.destroy()
				self.add_button = None

			if self.sav_button != None:
				self.sav_button.destroy()
				self.sav_button = None

			if self.del_button != None:
				self.del_button.destroy()
				self.del_button = None
				
			if self.table_name_selected_frame != None:
				self.table_name_selected_frame.destroy()
				self.table_name_selected_frame = None
			
			table_name_custom_var.set(self.root.selectedTable)
			order_var.set(  str(listbox.size()) )
			
		def table_name_selected( table ):
			
			reset_child_form()
					
			self.table_name_selected_frame = tk_frame(Frame,PosX=180,PosY=0,Width=350,Height=290,PadX=10,PadY=10,BD=1,Relief=tk.FLAT,BG="#CCC")
				
			caption = tk.Label(self.table_name_selected_frame, text=table, font=('Sans',(9),"bold"), Anchor=None)
			caption.config(bg="#CCC",fg="#000", width=40)
			caption.place(x=0,y=0)
			
			def close_frame():
				table_name_var.set('')
				reset_child_form()
				
			#close_button(self.table_name_selected_frame, 490, 0, bCommand=close_frame, bG='#DDD', bImage=self.closeImage, bState=tk.NORMAL)
			
			mysql = MySQLConnect(self.root.saveFile, self.root.selectedDB)
			NEWFIELDS = mysql.fieldList( table )
			if NEWFIELDS:
				self.table_field_lbl,self.table_field = tk_optionmenu(table_field_var, self.table_name_selected_frame, "Table Field: ", NEWFIELDS, 0, 25, bG="#CCC", Width=12) 
			
			FIELDS = mysql.fieldList( self.root.selectedTable )
			if FIELDS:
				self.parent_key_lbl,self.parent_key = tk_optionmenu(parent_field_var, self.table_name_selected_frame, "Parent Key: ", FIELDS, 0,75, bG="#CCC", Width=12) 
			
			self.table_title_lbl, self.table_title = tk_entry(title_var, self.table_name_selected_frame, Label="Navigation Title: ", xPos=0, yPos=125, bG="#CCC", Width=16) 
			
			self.button_title_lbl, self.button_title = tk_entry(button_title_var, self.table_name_selected_frame, Label="Button Title: ", xPos=0, yPos=175, bG="#CCC", Width=16) 
			
			self.table_name_custom_lbl, self.table_name_custom = tk_entry(table_name_custom_var, self.table_name_selected_frame, Label="Custom Parent Table: ", xPos=150, yPos=25, bG="#CCC", Width=16) 
			
			self.child_order_lbl, self.child_order = tk_entry(order_var, self.table_name_selected_frame, Label="Order: ", xPos=150, yPos=75, bG="#CCC", Width=16) 
			
			action=False
			if TableChildren.get_value(table, 'child_key') != None:
				table_field_var.set( TableChildren.get_value( table, 'child_key') )
				action=True
			
			if TableChildren.get_value(table, 'parent_key') != None:
				parent_field_var.set( TableChildren.get_value( table, 'parent_key') )
				action=True
			
			if TableChildren.get_value(table, 'table_title') != None:
				title_var.set( TableChildren.get_value( table, 'table_title') )
				action=True
			
			if TableChildren.get_value(table, 'button_title') != None:
				button_title_var.set( TableChildren.get_value( table, 'button_title') )
				action=True
				
			if TableChildren.get_value(table, 'table_name_custom') != None:
				table_name_custom_var.set( TableChildren.get_value( table, 'table_name_custom') )
				action=True
				
			if TableChildren.get_value(table, 'order') != None:
				order_var.set( TableChildren.get_value( table, 'order') )
				action=True
				
			if action:
				def save_item():
					if table_field_var.get() == '' or parent_field_var.get() == '':
						return
					if table_name_var.get() != self.root.selectedTable:
						TableChildren.insert(table_name_var.get(), 'child_key', table_field_var.get())
						TableChildren.insert(table_name_var.get(), 'parent_key', parent_field_var.get())
						TableChildren.insert(table_name_var.get(), 'table_title', title_var.get())
						TableChildren.insert(table_name_var.get(), 'button_title', button_title_var.get())
						TableChildren.insert(table_name_var.get(), 'table_name_custom', table_name_custom_var.get())
						TableChildren.insert(table_name_var.get(), 'order', order_var.get())
						
					reset_child_form()
					table_name_var.set('')
					
				self.sav_button = tk_button(self.table_name_selected_frame, Label="Save", xPos=0, yPos=240, bCommand=save_item, bG='green', Width=15)
				
				def delete_item():
					TableChildren.delete_item( table_name_var.get() )
					table_name_var.set('')
					reset_child_form(True)
						
				self.del_button = tk_button(self.table_name_selected_frame, Label="Delete", xPos=180, yPos=240, bCommand=delete_item, bG='red', Width=15)
			else:
				def add_item():
					if table_field_var.get() == '' or parent_field_var.get() == '':
						return
					if table_name_var.get() != self.root.selectedTable:
						TableChildren.insert(table_name_var.get(), 'child_key', table_field_var.get())
						TableChildren.insert(table_name_var.get(), 'parent_key', parent_field_var.get())
						TableChildren.insert(table_name_var.get(), 'table_title', title_var.get())
						TableChildren.insert(table_name_var.get(), 'button_title', button_title_var.get())
						TableChildren.insert(table_name_var.get(), 'table_name_custom', table_name_custom_var.get())
						TableChildren.insert(table_name_var.get(), 'order', order_var.get())
						
					listbox.insert(tk.END, table_name_var.get() )
					reset_child_form()
					table_name_var.set('')
						
				self.add_button = tk_button(self.table_name_selected_frame, Label="Add", xPos=0, yPos=240, bCommand=add_item, bG='green', Width=15)
			
		mysql = MySQLConnect( self.root.saveFile, self.root.selectedDB )
		TABLES = mysql.tableList()
		tk_optionmenu(table_name_var, Frame, "Table Name: ", TABLES, 0, 0, Command=table_name_selected, Width=16, bG="#CCC")
		
		def child_selected( evt ):
			selection = listbox.curselection()
			if len(selection) > 0:
				index, = selection
				table_name = listbox.get(index)
				
				table_name_var.set( table_name )			
				table_name_selected( table_name )
			
		listbox.bind("<<ListboxSelect>>", child_selected)

	def uploadTableFrame(self, Frame, SaveButton, options='none'):
		
		mysql = MySQLConnect( self.root.saveFile, self.root.selectedDB )
		
		FIELDS = mysql.fieldList( self.root.selectedTable )
		FIELDS = ['none'] + FIELDS
		
		scrollbar = tk.Scrollbar(Frame, orient=tk.VERTICAL)
		listbox = tk.Listbox(Frame, yscrollcommand=scrollbar.set)
		listbox.config(width=20,height=17)
		listbox.place(x=0,y=0) 
		scrollbar.config(command=listbox.yview)
		scrollbar.place(x=167,y=0, height=278)
		
		config_vars = {
			'upload_url' : {'index' : 499, 'type' : 'entry', 'default' : '', 'items' : [], 'empty' : True, 'prefix' : 'upload_config_'},
			'upload_path' : {'index' : 500, 'type' : 'entry', 'default' : '', 'items' : [], 'empty' : True, 'prefix' : 'upload_config_'},
			'allowed_types' : {'index' : 501, 'type' : 'entry', 'default' : '', 'items' : [], 'empty' : True, 'prefix' : 'upload_config_'},
			'file_name' : {'index' : 502, 'type' : 'entry', 'default' : '', 'items' : [], 'empty' : True, 'prefix' : 'upload_config_'},
			'overwrite' : {'index' : 503, 'type' : 'optionmenu', 'default' : 'FALSE', 'items' : ["TRUE", "FALSE"], 'empty' : False, 'prefix' : 'upload_config_'},
			'max_size' : {'index' : 504, 'type' : 'entry', 'default' : '0', 'items' : [], 'empty' : False, 'prefix' : 'upload_config_'},
			'max_width' : {'index' : 505, 'type' : 'entry', 'default' : '0', 'items' : [], 'empty' : False, 'prefix' : 'upload_config_'},
			'max_height' : {'index' : 506, 'type' : 'entry', 'default' : '0', 'items' : [], 'empty' : False, 'prefix' : 'upload_config_'},
			'max_filename' : {'index' : 507, 'type' : 'entry', 'default' : '0', 'items' : [], 'empty' : False, 'prefix' : 'upload_config_'},
			'encrypt_name' : {'index' : 508, 'type' : 'optionmenu', 'default' : 'FALSE', 'items' : ["TRUE", "FALSE"], 'empty' : False, 'prefix' : 'upload_config_'},
			'remove_spaces' : {'index' : 509, 'type' : 'optionmenu', 'default' : 'TRUE', 'items' : ["TRUE", "FALSE"], 'empty' : False, 'prefix' : 'upload_config_'},
			

			'file_name' : {'index' : 1000, 'type' : 'optionmenu', 'default' : 'none', 'items' : FIELDS, 'empty' : False, 'prefix' : 'upload_field_'},
			'file_type' : {'index' : 1001, 'type' : 'optionmenu', 'default' : 'none', 'items' : FIELDS, 'empty' : False, 'prefix' : 'upload_field_'},
			'file_path' : {'index' : 1002, 'type' : 'optionmenu', 'default' : 'none', 'items' : FIELDS, 'empty' : False, 'prefix' : 'upload_field_'},
			'full_path' : {'index' : 1003, 'type' : 'optionmenu', 'default' : 'none', 'items' : FIELDS, 'empty' : False, 'prefix' : 'upload_field_'},
			'raw_name' : {'index' : 1004, 'type' : 'optionmenu', 'default' : 'none', 'items' : FIELDS, 'empty' : False, 'prefix' : 'upload_field_'},
			'orig_name' : {'index' : 1005, 'type' : 'optionmenu', 'default' : 'none', 'items' : FIELDS, 'empty' : False, 'prefix' : 'upload_field_'},
			'client_name' : {'index' : 1006, 'type' : 'optionmenu', 'default' : 'none', 'items' : FIELDS, 'empty' : False, 'prefix' : 'upload_field_'},
			'file_ext' : {'index' : 1007, 'type' : 'optionmenu', 'default' : 'none', 'items' : FIELDS, 'empty' : False, 'prefix' : 'upload_field_'},
			'file_size' : {'index' : 1008, 'type' : 'optionmenu', 'default' : 'none', 'items' : FIELDS, 'empty' : False, 'prefix' : 'upload_field_'},
			'is_image' : {'index' : 1009, 'type' : 'optionmenu', 'default' : 'none', 'items' : FIELDS, 'empty' : False, 'prefix' : 'upload_field_'},
			'image_width' : {'index' : 1010, 'type' : 'optionmenu', 'default' : 'none', 'items' : FIELDS, 'empty' : False, 'prefix' : 'upload_field_'},
			'image_height' : {'index' : 1011, 'type' : 'optionmenu', 'default' : 'none', 'items' : FIELDS, 'empty' : False, 'prefix' : 'upload_field_'},
			'image_type' : {'index' : 1012, 'type' : 'optionmenu', 'default' : 'none', 'items' : FIELDS, 'empty' : False, 'prefix' : 'upload_field_'},
			'image_size_str' : {'index' : 1013, 'type' : 'optionmenu', 'default' : 'none', 'items' : FIELDS, 'empty' : False, 'prefix' : 'upload_field_'},
			}
		
		config_raw = {}
		for key in config_vars:
			config_raw[config_vars[key]['index']] = key
			
		for i, key in sorted(config_raw.items(), key=operator.itemgetter(0)):
			listbox.insert(tk.END, key)
		
		def edit_item(evt):
			selection = listbox.curselection()
			if len(selection) > 0:
				index, = selection
				config_id = listbox.get(index)
				
				if self.innerOptionsFrame != None:
					self.innerOptionsFrame.destroy()
					self.innerOptionsFrame = None
					
				self.innerOptionsFrame = tk_frame(Parent=Frame,PosX=190,PosY=0,Width=340,Height=285,PadX=10,PadY=10,BD=1,Relief=tk.RAISED,BG="#DDD")
				
				tk_label(self.innerOptionsFrame, Label=config_id, xPos=0, yPos=0, Width=34, bG="#CCC", Font=('Serif',(10),"bold"), fG="#000", Anchor=None)
				
				Variable = tk.StringVar()
				self.getValue(Variable, config_vars[config_id]['prefix']+config_id, Int=False, Default=config_vars[config_id]['default'], Empty=config_vars[config_id]['empty'])
				
				if config_vars[config_id]['type'] == 'entry':
					tk_entry(Variable, self.innerOptionsFrame, Label=config_id+": ", xPos=0, yPos=30, bG="#DDD", Width=38)
				elif config_vars[config_id]['type'] == 'optionmenu':
					tk_optionmenu(Variable, self.innerOptionsFrame, Label=config_id+": ", ITEMS=config_vars[config_id]['items'], xPos=0, yPos=30, bG="#DDD",Command=None, Width=34)
				
				def Save():
					self.currentTable.insert( config_vars[config_id]['prefix']+config_id, Variable.get() )
					
				save_button = tk_button(self.innerOptionsFrame, Label="Save", xPos=0, yPos=230, bCommand=Save, bG='green', Width=10)
				
		listbox.bind("<<ListboxSelect>>", edit_item)
		
	def uploadTableFrame1(self, Frame, SaveButton, options='none'):
		mysql = MySQLConnect( self.root.saveFile, self.root.selectedDB )
		
		FIELDS = mysql.fieldList( self.root.selectedTable )
		FIELDS = ['none'] + FIELDS
		
		################################################################
		
		upload_path_var = tk.StringVar()
		tk_entry(upload_path_var, Frame, "upload_path: ", xPos=0, yPos=0, Width=14)
		self.getValue(upload_path_var, "upload_config_upload_path", Default='')
		
		################################################################
		
		allowed_types_var = tk.StringVar()
		tk_entry(allowed_types_var, Frame, "allowed_types: ", xPos=0, yPos=49, Width=14)
		self.getValue(allowed_types_var, "upload_config_allowed_types", Default='')
		
		################################################################
		
		file_name_var = tk.StringVar()
		tk_entry(file_name_var, Frame, "file_name: ", xPos=0, yPos=98, Width=14)
		self.getValue(file_name_var, "upload_config_file_name", Default='')
		
		################################################################
		
		overwrite_var = tk.StringVar()
		tk_optionmenu(overwrite_var, Frame, "overwrite: ", ["TRUE", "FALSE"], xPos=0, yPos=147, Width=10)
		self.getValue(overwrite_var, "upload_config_overwrite", Default='FALSE')
		
		################################################################
		
		max_size_var = tk.StringVar()
		tk_entry(max_size_var, Frame, "max_size: ", xPos=0, yPos=195, Width=14)
		self.getValue(max_size_var, "upload_config_max_size", Default='0')
		
		################################################################
		
		max_width_var = tk.StringVar()
		tk_entry(max_width_var, Frame, "max_width: ", xPos=0, yPos=245, Width=14)
		self.getValue(max_width_var, "upload_config_max_width", Default='0')
		
		################################################################
		
		max_height_var = tk.StringVar()
		tk_entry(max_height_var, Frame, "max_height: ", xPos=135, yPos=0, Width=14)
		self.getValue(max_height_var, "upload_config_max_height", Default='0')
		
		################################################################
		
		max_filename_var = tk.StringVar()
		tk_entry(max_filename_var, Frame, "max_filename: ", xPos=135, yPos=49, Width=14)
		self.getValue(max_filename_var, "upload_config_max_filename", Default='0')
		
		################################################################
		
		encrypt_name_var = tk.StringVar()
		tk_optionmenu(encrypt_name_var, Frame, "encrypt_name: ", ["TRUE", "FALSE"], xPos=135, yPos=98, Width=10)
		self.getValue(encrypt_name_var, "upload_config_encrypt_name", Default='FALSE')
		
		################################################################
		
		remove_spaces_var = tk.StringVar()
		tk_optionmenu(remove_spaces_var, Frame, "remove_spaces: ", ["TRUE", "FALSE"], xPos=135, yPos=147, Width=10)
		self.getValue(remove_spaces_var, "upload_config_remove_spaces", Default='TRUE')
		
		################################################################
		
		file_name_field_var = tk.StringVar()
		tk_optionmenu(file_name_field_var, Frame, "file_name: ", FIELDS, xPos=135, yPos=195, Width=10)
		self.getValue(file_name_field_var, "upload_field_file_name", Default='none')
		
		################################################################
		
		file_type_field_var = tk.StringVar()
		tk_optionmenu(file_type_field_var, Frame, "file_type: ", FIELDS, xPos=135, yPos=245, Width=10)
		self.getValue(file_type_field_var, "upload_field_file_type", Default='none')
		
		################################################################
		
		file_path_field_var = tk.StringVar()
		tk_optionmenu(file_path_field_var, Frame, "file_path: ", FIELDS, xPos=270, yPos=0, Width=10)
		self.getValue(file_path_field_var, "upload_field_file_path", Default='none')
		
		################################################################
		
		full_path_field_var = tk.StringVar()
		tk_optionmenu(full_path_field_var, Frame, "full_path: ", FIELDS, xPos=270, yPos=49, Width=10)
		self.getValue(full_path_field_var, "upload_field_full_path", Default='none')
		
		################################################################
		
		raw_name_field_var = tk.StringVar()
		tk_optionmenu(raw_name_field_var, Frame, "raw_name: ", FIELDS, xPos=270, yPos=98, Width=10)
		self.getValue(raw_name_field_var, "upload_field_raw_name", Default='none')
		
		################################################################
		
		orig_name_field_var = tk.StringVar()
		tk_optionmenu(orig_name_field_var, Frame, "orig_name: ", FIELDS, xPos=270, yPos=147, Width=10)
		self.getValue(orig_name_field_var, "upload_field_orig_name", Default='none')
		
		################################################################
		
		client_name_field_var = tk.StringVar()
		tk_optionmenu(client_name_field_var, Frame, "client_name: ", FIELDS, xPos=270, yPos=195, Width=10)
		self.getValue(client_name_field_var, "upload_field_client_name", Default='none')
		
		################################################################
		
		file_ext_field_var = tk.StringVar()
		tk_optionmenu(file_ext_field_var, Frame, "file_ext: ", FIELDS, xPos=270, yPos=245, Width=10)
		self.getValue(file_ext_field_var, "upload_field_file_ext", Default='none')
		
		################################################################
		
		file_size_field_var = tk.StringVar()
		tk_optionmenu(file_size_field_var, Frame, "file_size: ", FIELDS, xPos=405, yPos=0, Width=10)
		self.getValue(file_size_field_var, "upload_field_file_size", Default='none')
		
		################################################################
		
		is_image_field_var = tk.StringVar()
		tk_optionmenu(is_image_field_var, Frame, "is_image: ", FIELDS, xPos=405, yPos=49, Width=10)
		self.getValue(is_image_field_var, "upload_field_is_image", Default='none')
		
		################################################################
		
		image_width_field_var = tk.StringVar()
		tk_optionmenu(image_width_field_var, Frame, "image_width: ", FIELDS, xPos=405, yPos=98, Width=10)
		self.getValue(image_width_field_var, "upload_field_image_width", Default='none')
		
		################################################################
		
		image_height_field_var = tk.StringVar()
		tk_optionmenu(image_height_field_var, Frame, "image_height: ", FIELDS, xPos=405, yPos=147, Width=10)
		self.getValue(image_height_field_var, "upload_field_image_height", Default='none')
		
		################################################################
		
		image_type_field_var = tk.StringVar()
		tk_optionmenu(image_type_field_var, Frame, "image_type: ", FIELDS, xPos=405, yPos=195, Width=10)
		self.getValue(image_type_field_var, "upload_field_image_type", Default='none')
		
		################################################################
		
		image_size_str_field_var = tk.StringVar()
		tk_optionmenu(image_size_str_field_var, Frame, "image_size_str: ", FIELDS, xPos=405, yPos=245, Width=10)
		self.getValue(image_size_str_field_var, "upload_field_image_size_str", Default='none')
		
		################################################################
		
		def save():
			self.init_vars()
			self.currentTable.insert('upload_config_upload_path', upload_path_var.get())
			self.currentTable.insert('upload_config_allowed_types', allowed_types_var.get())
			self.currentTable.insert('upload_config_file_name', file_name_var.get())
			self.currentTable.insert('upload_config_overwrite', overwrite_var.get())
			self.currentTable.insert('upload_config_max_size', max_size_var.get())
			self.currentTable.insert('upload_config_max_width', max_width_var.get())
			self.currentTable.insert('upload_config_max_height', max_height_var.get())
			self.currentTable.insert('upload_config_max_filename', max_filename_var.get())
			self.currentTable.insert('upload_config_encrypt_name', encrypt_name_var.get())
			self.currentTable.insert('upload_config_remove_spaces', remove_spaces_var.get())
			
			self.currentTable.insert('upload_field_file_name', file_name_field_var.get())
			self.currentTable.insert('upload_field_file_type', file_type_field_var.get())
			self.currentTable.insert('upload_field_file_path', file_path_field_var.get())
			self.currentTable.insert('upload_field_full_path', full_path_field_var.get())
			self.currentTable.insert('upload_field_raw_name', raw_name_field_var.get())
			self.currentTable.insert('upload_field_orig_name', orig_name_field_var.get())
			self.currentTable.insert('upload_field_client_name', client_name_field_var.get())
			self.currentTable.insert('upload_field_file_ext', file_ext_field_var.get())
			self.currentTable.insert('upload_field_file_size', file_size_field_var.get())
			self.currentTable.insert('upload_field_is_image', is_image_field_var.get())
			self.currentTable.insert('upload_field_image_width', image_width_field_var.get())
			self.currentTable.insert('upload_field_image_height', image_height_field_var.get())
			self.currentTable.insert('upload_field_image_type', image_type_field_var.get())
			self.currentTable.insert('upload_field_image_size_str', image_size_str_field_var.get())
			
		SaveButton.config(command=save)
