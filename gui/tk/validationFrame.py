try:
    # Python2
    import Tkinter as tk
except ImportError:
    # Python3
    import tkinter as tk

from ui import *
from lib import sqlite

class validationFrame:
	
	root = None
	
	def __init__(self, root, thisFrame, SaveButton):
		self.root = root
		fieldValidations = sqlite.fieldValidations(self.root.saveFile, self.root.selectedDB, self.root.selectedTable, self.root.selectedField)
		
		validation_scrollbar = tk.Scrollbar(thisFrame, orient=tk.VERTICAL)
		validation_listbox = tk.Listbox(thisFrame, yscrollcommand=validation_scrollbar.set)
		validation_listbox.config(width=25,height=18)
		validation_listbox.place(x=0,y=0) 
		validation_scrollbar.config(command=validation_listbox.yview)
		validation_scrollbar.place(x=206,y=0, height=290)
		
		added_validations = fieldValidations.get_all()
		
		validations = (
			"trim|0",
			"required|1", 
			"matches|2", 
			"is_unique|3", 
			"min_length|4", 
			"max_length|5", 
			"exact_length|6", 
			"greater_than|7", 
			"less_than|8",
			"alpha|9",
			"alpha_numeric|10",
			"alpha_dash|11",
			"numeric|12",
			"integer|13",
			"decimal|14",
			"is_natural|15",
			"is_natural_no_zero|16",
			"valid_email|17",
			"valid_emails|18",
			"valid_ip|19",
			"valid_base64|20",
			"xss_clean|21",
			"prep_for_form|22",
			"prep_url|23",
			"strip_image_tags|24",
			"encode_php_tags|25",
			"sha1|26",
			"md5|27",
		)
		
		with_option = ("matches", 
			"is_unique", 
			"min_length", 
			"max_length", 
			"exact_length", 
			"greater_than", 
			"less_than")
			
		for validation in validations: 
			valString = validation.split('|')
			if valString[0] in added_validations:
				validation = "*** " + validation + " ***"
			validation_listbox.insert(tk.END, validation)
		
		rule_label = tk_label(thisFrame, Label="RULE: ", xPos=240, yPos=0, Width=27, bG="#CCC", Font=("Sans",(12), "bold"))
		
		validation_key_var = tk.StringVar()
		_,validation_key = tk_entry(validation_key_var, thisFrame, Label="Key:", xPos=240, yPos=40, bG="#CCC", Width=16)
		validation_key.config(state=tk.DISABLED)
		
		validation_option_var = tk.StringVar()
		validation_option_label,validation_option_entry = tk_entry(validation_option_var, thisFrame, Label="Option:", xPos=390, yPos=40, bG="#CCC", Width=16)
		validation_option_label.place_forget()
		validation_option_entry.place_forget()
		
		validation_add_var = tk.IntVar()
		tk_checkbox(validation_add_var, thisFrame, Label="Add Page", xPos=240, yPos=95, bG="#CCC", Width=10, Command=None)
		
		validation_edit_var = tk.IntVar()
		tk_checkbox(validation_edit_var, thisFrame, Label="Edit Page", xPos=390, yPos=95, bG="#CCC", Width=10, Command=None)
		
		validation_order_var = tk.StringVar()
		
		validation_saveButton = tk.Button(thisFrame,text="Save")
		validation_saveButton.config(bg="green", width=12)
		
		def validation_selected(evt):
			
			selected_index, = validation_listbox.curselection()
			selected_item = validation_listbox.get(selected_index).replace("***", "").strip()
			valString = selected_item.split('|')
			validation_key_var.set( valString[0] )
			validation_order_var.set( valString[1] )
			rule_label.config(text="RULE: " + valString[0])
			
			currentValidation = fieldValidations.get_value( valString[0] )

			if currentValidation != None:
				validation_add_var.set( int(currentValidation[0]) )
				validation_edit_var.set( int(currentValidation[1]) )
				validation_option_var.set( str(currentValidation[2]) )
			else:
				validation_add_var.set( 0 )
				validation_edit_var.set( 0 )
				validation_option_var.set( '' )
			
			if valString[0] in with_option:
				validation_option_label.place(x=390, y=40)
				validation_option_entry.place(x=390, y=60)
			else:
				validation_option_label.place_forget()
				validation_option_entry.place_forget()
			
			def save_validation():
				deleted = fieldValidations.insert( validation_key_var.get(), str(validation_add_var.get()), str(validation_edit_var.get()), str(validation_option_var.get()), order=validation_order_var.get())
				
				if deleted:
					validation_listbox.delete(selected_index)
					validation_listbox.insert(selected_index, selected_item)
				else:
					validation_listbox.delete(selected_index)
					validation_listbox.insert(selected_index, "*** " + selected_item + " ***")
					
				validation_key_var.set('')
				validation_option_var.set('')
				validation_add_var.set( 0 )
				validation_edit_var.set( 0 )
				validation_saveButton.place_forget()
			
			validation_saveButton.config(command=save_validation)
			validation_saveButton.place(x=240,y=130) 
						
		#validation_listbox.bind("<Double-Button-1>", validation_selected)
		validation_listbox.bind("<<ListboxSelect>>", validation_selected)
