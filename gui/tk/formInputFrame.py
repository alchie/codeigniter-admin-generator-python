try:
    # Python2
    import Tkinter as tk
except ImportError:
    # Python3
    import tkinter as tk

from ui import *
from lib import sqlite
from lib.mysql import MySQLConnect

class formInputFrame:
	
	root = None
	thisFrame = None
	SaveButton = None
	currentField = None
	
	inner_options_frame = None
	
	def __init__(self, root, thisFrame, SaveButton, options='none'):
		
		self.root = root
		self.thisFrame = thisFrame
		self.SaveButton = SaveButton
		
		if options == '':
			options = 'none'
			
		inputs = {
					"none" : self.none,
					"hidden" : self.hidden,
					"text" : self.text,
					"textarea" : self.textarea, 
					"password" : self.password, 
					"dropdown" : self.dropdown, 
					"checkbox" : self.checkbox, 
					"radio" : self.radio, 
					"library" : self.library,
					"view_only" : self.none,
				}
				
		inputs[options]()

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #

	def init_vars(self, reset=''):
		
		if self.currentField == None and reset == 'currentField':
			self.currentField = sqlite.fieldSettings(self.root.saveFile, self.root.selectedDB, self.root.selectedTable, self.root.selectedField)

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #

	def getValue(self, Var, Key, Int=False, Default=""):
		
		self.init_vars('currentField')
		
		Value = self.currentField.get_value( Key )

		if (Value==None or Value==""):
			if Default == "" and Int:
				Default = 0
				
			Value = Default

		if Int:
			Value = int( Value )
		
		Var.set( Value )	
		return Value
		
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #

	def none(self):
		return None;

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #

	def hidden(self):
		
		tk_label(self.thisFrame, Label="HIDDEN", xPos=0, yPos=0, Width=48, bG="#CCC", Font=('Serif',(12),"bold"), fG="#F00", Anchor=None)

		default_var = tk.StringVar()
		tk_entry(default_var, self.thisFrame, Label="Default Value:", xPos=0, yPos=25, bG="#CCC", Width=21)
		self.getValue(default_var, 'hidden_type_default')
			
		def update():
			self.init_vars('currentField')
			self.currentField.insert('hidden_type_default', default_var.get()) # hidden_type_default
		
		self.SaveButton.config(command=update)
		
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #

	def text(self):
		
		tk_label(self.thisFrame, Label="TEXT", xPos=0, yPos=0, Width=48, bG="#CCC", Font=('Serif',(12),"bold"), fG="#F00", Anchor=None)

		tk_label(self.thisFrame, "Default Type: ", 0, 25, Width=19, bG="#CCC")
		
		default_var = tk.StringVar()
		tk_radio(default_var, self.thisFrame, Label="Text ", Value="TEXT_VALUE", xPos=0, yPos=45, bG="#CCC", Width=19, Command=None)
		tk_radio(default_var, self.thisFrame, Label="Current Timestamp ", Value="CURRENT_TIMESTAMP", xPos=0, yPos=65, bG="#CCC", Width=19, Command=None)
		tk_radio(default_var, self.thisFrame, Label="Random String", Value="RANDOM_STRING", xPos=0, yPos=85, bG="#CCC", Width=19, Command=None)
		self.getValue(default_var, 'text_type_default', Default='TEXT_VALUE')
		
		default_value_var = tk.StringVar()
		tk_entry(default_value_var, self.thisFrame, Label="Default Value:", xPos=0, yPos=110, bG="#CCC", Width=19)
		self.getValue(default_value_var, 'text_type_default_value')
		
		default_length_var = tk.StringVar()
		tk_entry(default_length_var, self.thisFrame, Label="Default Length:", xPos=0, yPos=155, bG="#CCC", Width=19)
		self.getValue(default_length_var, 'text_type_default_length')
		
		search_linked_table = tk.IntVar()
		tk_checkbox(search_linked_table, self.thisFrame, "Search Linked Table", 0, 205, bG="#CCC", Width=17, Command=None)			
		self.getValue(search_linked_table, 'text_type_search_linked_table', True)
		
		def update():
			self.init_vars('currentField')
			self.currentField.insert('text_type_default', default_var.get())
			self.currentField.insert('text_type_default_value', default_value_var.get()) 
			self.currentField.insert('text_type_default_length', default_length_var.get()) 
			self.currentField.insert('text_type_search_linked_table', str(search_linked_table.get())) 
		
		self.SaveButton.config(command=update)
		

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #

	def textarea(self):
		
		tk_label(self.thisFrame, Label="TEXTAREA", xPos=0, yPos=0, Width=48, bG="#CCC", Font=('Serif',(12),"bold"), fG="#F00", Anchor=None)
		
		default_var = tk.StringVar()
		tk_entry(default_var, self.thisFrame, Label="Default Value:", xPos=0, yPos=25, bG="#CCC", Width=21)
		self.getValue(default_var, 'textarea_type_default')

		rows_var = tk.StringVar()
		tk_entry(rows_var, self.thisFrame, Label="Rows Height:", xPos=0, yPos=75, bG="#CCC", Width=21)
		self.getValue(rows_var, 'textarea_type_rows')
		
		wysiwyg_var = tk.IntVar()
		tk_checkbox(wysiwyg_var, self.thisFrame, "WYSIWYG Editor", 0, 125, bG="#CCC", Width=21, Command=None)			
		self.getValue(wysiwyg_var, 'textarea_type_wysiwyg', True)
		
		def update():
			self.init_vars('currentField')
			self.currentField.insert('textarea_type_default', default_var.get()) 
			self.currentField.insert('textarea_type_rows', rows_var.get()) 
			self.currentField.insert('textarea_type_wysiwyg', str(wysiwyg_var.get())) 
		
		self.SaveButton.config(command=update)
		
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #

	def password(self):
		
		tk_label(self.thisFrame, Label="PASSWORD", xPos=0, yPos=0, Width=48, bG="#CCC", Font=('Serif',(12),"bold"), fG="#F00", Anchor=None)
		
		confirm_var = tk.IntVar()
		tk_checkbox(confirm_var, self.thisFrame, Label="Confirm Password", xPos=0, yPos=25, bG="#CCC", Width=21)
		self.getValue(confirm_var, 'password_type_confirm', True, 0)
		
		name_var = tk.StringVar()
		tk_entry(name_var, self.thisFrame, Label="Confirm Name:", xPos=0, yPos=50, bG="#CCC", Width=21)
		self.getValue(name_var, 'password_type_name')
		
		label_var = tk.StringVar()
		tk_entry(label_var, self.thisFrame, Label="Confirm Label:", xPos=0, yPos=100, bG="#CCC", Width=21)
		self.getValue(label_var, 'password_type_label')
		
		def update():
			self.init_vars('currentField')
			self.currentField.insert('password_type_confirm', str(confirm_var.get())) # password_type_confirm
			self.currentField.insert('password_type_name', name_var.get()) # password_type_name
			self.currentField.insert('password_type_label', label_var.get()) # password_type_label
		
		self.SaveButton.config(command=update)

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #

	def _manual_table(self, Name, Label):
		tk_label(self.thisFrame, Label=Label, xPos=0, yPos=0, Width=48, bG="#CCC", Font=('Serif',(12),"bold"), fG="#F00", Anchor=None)

		tk_label(self.thisFrame, "Value Source: ", 0, 25, Width=19, bG="#CCC")
				
		table_name_var = tk.StringVar()
		table_key_var = tk.StringVar()
		table_value_var = tk.StringVar()
		table_custom_var = tk.StringVar()
		table_filter_var = tk.IntVar()
		table_filter_key_var = tk.StringVar()
		table_filter_value_var = tk.StringVar()
		table_order_var = tk.IntVar()
		table_order_by_var = tk.StringVar()
		table_order_sort_var = tk.StringVar()
		
		def manual_source():
			
			if self.inner_options_frame != None:
				self.inner_options_frame.destroy()
				self.inner_options_frame == None
				
			self.inner_options_frame = tk.Frame(self.thisFrame)
			self.inner_options_frame.config(width=410, height=250, padx=10,pady=10,bd=1,relief=tk.RAISED,bg="#CCC")
			self.inner_options_frame.place(x=120, y=35)
			
			scrollbar = tk.Scrollbar(self.inner_options_frame, orient=tk.VERTICAL)
			listbox = tk.Listbox(self.inner_options_frame, yscrollcommand=scrollbar.set)
			listbox.config(width=20,height=14)
			listbox.place(x=0,y=0) 
			scrollbar.config(command=listbox.yview)
			scrollbar.place(x=166,y=0, height=228) 


			ListItems = sqlite.fieldItems( self.root.saveFile, self.root.selectedDB, self.root.selectedTable, self.root.selectedField, Name )
				
			listbox.delete(0, tk.END)
			for item in ListItems.item_list():
				index = ListItems.get_value(item[1], 'index')
				if index == None:
					index = tk.END
				else:
					index = int(index)

				listbox.insert(index, item[4])
			
			key_var = tk.StringVar()
			_, key_entry = tk_entry(key_var, self.inner_options_frame, Label="Item Value:", xPos=190, yPos=0, bG="#CCC", Width=23)
			
			value_var = tk.StringVar()
			tk_entry(value_var, self.inner_options_frame, Label="Item Label:", xPos=190, yPos=50, bG="#CCC", Width=23)
			
			order_var = tk.StringVar()
			tk_entry(order_var, self.inner_options_frame, Label="Item Order:", xPos=190, yPos=100, bG="#CCC", Width=23)
			
			order_var.set( str( listbox.size() ) )
			
			def add_item():
				if key_var.get() != '' and order_var.get() != '' and value_var.get() != '':
					ListItems.insert(key_var.get(), "index", order_var.get())
					ListItems.insert(key_var.get(), "label", value_var.get())
					listbox.insert(tk.END, key_var.get())
					order_var.set( str( listbox.size() ) )
					key_var.set('')
					value_var.set('')
				
			add_button = tk_button(self.inner_options_frame, Label="Add", xPos=190, yPos=150, bCommand=add_item, bG='green')
			
			save_button = tk_button(self.inner_options_frame, Label="Save", xPos=190, yPos=150, bCommand=None, bG='green',Width=6)
			save_button.place_forget()
			del_button = tk_button(self.inner_options_frame, Label="Delete this item", xPos=190, yPos=190, bCommand=None, bG='red')
			del_button.place_forget()
			set_button = tk_button(self.inner_options_frame, Label="Set Default", xPos=295, yPos=150, bCommand=None, bG='orange',Width=7)
			set_button.place_forget()
					
			def item_selected( evt ):
				selection = listbox.curselection()
				if len(selection) > 0:
					index, = selection
					item = listbox.get(index) 
						
					key_var.set( item )
					value_var.set( ListItems.get_value(item, "label") )
					order_var.set( ListItems.get_value(item, "index") )
					
					key_entry.config(state=tk.DISABLED)
					add_button.place_forget()
					save_button.place(x=190, y=150)
					set_button.place(x=285, y=150)
					del_button.place(x=190, y=190)
						
					if self.currentField.get_value("default_"+Name+"_item") == item:
						set_button.place_forget()
						
					def set_item():
						self.currentField.insert("default_"+Name+"_item", item)
						set_button.place_forget()
					
					set_button.config(command=set_item)
						
					def reset_input():
						key_entry.config(state=tk.NORMAL)
						save_button.place_forget()
						set_button.place_forget()
						del_button.place_forget()
						add_button.place(x=190, y=150)
						
						key_var.set( '' )
						value_var.set( '' )
						order_var.set( str( listbox.size() ) )
						
					def save_item():
						ListItems.insert(key_var.get(), "index", order_var.get())
						ListItems.insert(key_var.get(), "label", value_var.get())
						reset_input()
						
								
					save_button.config(command=save_item)

					def del_item():
						ListItems.delete_item( item )
						listbox.delete( index ) 
						reset_input()
						
					del_button.config(command=del_item)
			
			listbox.bind("<<ListboxSelect>>", item_selected)
			
			
			
		def table_source():
			
			if self.inner_options_frame != None:
				self.inner_options_frame.destroy()
				self.inner_options_frame == None
				
			self.inner_options_frame = tk.Frame(self.thisFrame)
			self.inner_options_frame.config(width=410, height=250, padx=10,pady=10,bd=1,relief=tk.RAISED,bg="#CCC")
			self.inner_options_frame.place(x=120, y=35)
			
			mysql = MySQLConnect( self.root.saveFile, self.root.selectedDB)
			
			def table_selected( tbl ):
				
				table_key_var.set("")
				table_value_var.set("")
				table_custom_var.set("")
				
				FIELDS = mysql.fieldList( tbl )
				def update_custom( tbl ):
					table_custom_var.set( tbl )
					
				tk_optionmenu(table_key_var, self.inner_options_frame, Label="Table Key: ", ITEMS=FIELDS, xPos=0, yPos=50, bG="#CCC",Command=None, Width=13)
				tk_optionmenu(table_value_var, self.inner_options_frame, Label="Table Value: ", ITEMS=FIELDS, xPos=0, yPos=100, bG="#CCC",Command=update_custom, Width=13)
				self.getValue(table_key_var, Name+'_type_table_key')
				self.getValue(table_value_var, Name+'_type_table_value')
				
				tk_entry(table_custom_var, self.inner_options_frame, Label="Custom Name: ", xPos=0, yPos=150, bG="#CCC", Width=17)
				self.getValue(table_custom_var, Name+'_type_table_custom')
				
				tk_optionmenu(table_filter_key_var, self.inner_options_frame, Label="Filter Key: ", ITEMS=FIELDS, xPos=170, yPos=30, bG="#CCC",Command=None, Width=10)
				tk_entry(table_filter_value_var, self.inner_options_frame, Label="Filter Value: ", xPos=170, yPos=80, bG="#CCC", Width=14)
				self.getValue(table_filter_key_var, Name+'_type_table_filter_key')
				self.getValue(table_filter_value_var, Name+'_type_table_filter_value')
				
				tk_optionmenu(table_order_by_var, self.inner_options_frame, Label="Order By: ", ITEMS=FIELDS, xPos=170, yPos=130, bG="#CCC",Command=None, Width=10)
				tk_optionmenu(table_order_sort_var, self.inner_options_frame, Label="Order Sort: ", ITEMS=['ASC', 'DESC'], xPos=170, yPos=180, bG="#CCC",Command=None, Width=10)
				self.getValue(table_order_by_var, Name+'_type_table_order_by')
				self.getValue(table_order_sort_var, Name+'_type_table_order_sort')
				
				tk_checkbox(table_filter_var, self.inner_options_frame, Label="Set Filter", xPos=170, yPos=0, bG="#CCC", Width=10, Command=None)
				self.getValue(table_filter_var, Name+'_type_table_filter', True, 0)
				
				tk_checkbox(table_order_var, self.inner_options_frame, Label="Set Order", xPos=270, yPos=0, bG="#CCC", Width=10, Command=None)
				self.getValue(table_order_var, Name+'_type_table_order', True, 0)
				
			TABLES = mysql.tableList()
			tk_optionmenu(table_name_var, self.inner_options_frame, Label="Table Name: ", ITEMS=TABLES, xPos=0, yPos=0, bG="#CCC",Command=table_selected, Width=13)
			table = self.getValue(table_name_var, Name+'_type_table_name')
			
			if table != None and table != "":
				table_selected(table)
				
		type_var = tk.StringVar()
		tk_radio(type_var, self.thisFrame, Label="Manual", Value="MANUAL", xPos=0, yPos=45, bG="#CCC", Width=10, Command=manual_source)
		tk_radio(type_var, self.thisFrame, Label="Table", Value="TABLE", xPos=0, yPos=65, bG="#CCC", Width=10, Command=table_source)
		source = self.getValue(type_var, Name+'_type_source', Default="MANUAL")
		
		custom_value_var = tk.IntVar()
		
		if Name == 'dropdown':
			tk_checkbox(custom_value_var, self.thisFrame, Label="Custom Value", xPos=0, yPos=95, bG="#CCC", Width=12)
			self.getValue(custom_value_var, Name+'_type_table_custom_value', Int=True, Default="1")
			
		if source == 'MANUAL':
			manual_source()
			
		if source == 'TABLE':
			table_source()

		def update():
			self.init_vars('currentField')
			self.currentField.insert(Name+'_type_source', type_var.get()) 
			self.currentField.insert(Name+'_type_table_name', table_name_var.get()) 
			self.currentField.insert(Name+'_type_table_key', table_key_var.get()) 
			self.currentField.insert(Name+'_type_table_value', table_value_var.get()) 
			self.currentField.insert(Name+'_type_table_custom', table_custom_var.get()) 
			self.currentField.insert(Name+'_type_table_filter', str(table_filter_var.get()))
			self.currentField.insert(Name+'_type_table_filter_key', table_filter_key_var.get())
			self.currentField.insert(Name+'_type_table_filter_value', table_filter_value_var.get())
			self.currentField.insert(Name+'_type_table_order', str(table_order_var.get()))
			self.currentField.insert(Name+'_type_table_order_by', table_order_by_var.get())
			self.currentField.insert(Name+'_type_table_order_sort', table_order_sort_var.get())
			self.currentField.insert(Name+'_type_table_custom_value', str(custom_value_var.get()))
		
		self.SaveButton.config(command=update)
		
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #

	def dropdown(self):
		self._manual_table("dropdown", "DROPDOWN")

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
	
	def checkbox(self):
		self._manual_table("checkbox", "CHECKBOX")

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #

	def radio(self):
		self._manual_table("radio", "RADIO")
		
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #

	def library(self):
		
		tk_label(self.thisFrame, Label="LIBRARY", xPos=0, yPos=0, Width=48, bG="#CCC", Font=('Serif',(12),"bold"), fG="#F00", Anchor=None)

		filename_var = tk.StringVar()
		tk_entry(filename_var, self.thisFrame, Label="Filename ({Filename}_lib.php):", xPos=0, yPos=25, bG="#CCC", Width=30)
		self.getValue(filename_var, 'library_type_filename')
		
		init_var = tk.StringVar()
		tk_entry(init_var, self.thisFrame, Label="Init Function:", xPos=0, yPos=75, bG="#CCC", Width=30)
		self.getValue(init_var, 'library_type_init')
		
		def update():
			self.init_vars('currentField')
			self.currentField.insert('library_type_filename', filename_var.get()) 
			self.currentField.insert('library_type_init', init_var.get()) 
		
		self.SaveButton.config(command=update)


