import os
# using Tkinter's Optionmenu() as a combobox
try:
    # Python2
    import Tkinter as tk
except ImportError:
    # Python3
    import tkinter as tk

from lib.mysql import MySQLConnect    
from lib import sqlite
from lib import unzip, create_file
from lib import models, views, controllers_admin, controllers_api, langs, configs, menu
from lib.defaults import AdminsTable, MediaUploadsTable,  AdminsAccessTable,  AdminsSessionsTable
from ui import *
from configFilesFrame import configFilesFrame

class mvcFrame:
	
	root = None
	currentMVC = None
	
	advance_directory_frame = None
	
	def __init__(self, root):
		self.root = root
		self.closeImage = root.closeImage
		self.settingsImage = root.settingsImage
		
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #

	def init_vars(self):
		self.currentMVC = sqlite.mvcSettings(self.root.saveFile , self.root.selectedDB)
		
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #

	def getValue(self, Var, Key, Int=False, Default=""):
		
		self.init_vars()
		
		Value = self.currentMVC.get_value( Key )

		if (Value==None or Value==""):
			if Default == "" and Int:
				Default = 0
				
			Value = Default

		if Int:
			Value = int( Value )
		
		Var.set( Value )	
		return Value
		
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
			
	def display(self):
		self.root.hideActiveFrames('mvc')
		
		if self.root.mvcSettingsFrame != None:
			self.root.mvcSettingsFrame.destroy()
			self.root.mvcSettingsFrame = None
			
		self.root.mvcSettingsFrame = tk.Frame()
		self.root.mvcSettingsFrame.config(width=805,height=440,padx=20,pady=20,bd=1,relief=tk.RAISED,bg="#EEE")
		self.root.mvcSettingsFrame.grid(row=4, column=0)
		self.root.mvcSettingsFrame.grid_propagate(False)
		
		thisFrame = self.root.mvcSettingsFrame
		

		self.root.mvcSettingsButton.config(bg='red', activebackground='red', activeforeground='white')
		
		tk_label(thisFrame, Label="MVC SETTINGS", xPos=0, yPos=0, Width=58, bG="#EEE", Font=('Serif',(14),"bold"), fG="#000", Anchor=None)
		
		project_title = tk.StringVar()
		tk_entry(project_title, thisFrame, Label="Project Name: ", xPos=0, yPos=50, bG="#EEE", Width=27)
		self.getValue(project_title, "title")

		project_website = tk.StringVar()
		tk_entry(project_website, thisFrame, Label="Project Website: ", xPos=0, yPos=100, bG="#EEE", Width=27)
		self.getValue(project_website, "project_website")
		
		author_name = tk.StringVar()
		tk_entry(author_name, thisFrame, Label="Author Name: ", xPos=0, yPos=150, bG="#EEE", Width=27)
		self.getValue(author_name, "author", Default='Chester Alan Tagudin')
		
		author_website = tk.StringVar()
		tk_entry(author_website, thisFrame, Label="Author Website: ", xPos=0, yPos=200, bG="#EEE", Width=27)
		self.getValue(author_website, "author_website", Default='http://www.chesteralan.com/')

		ci_output = tk.StringVar()
		self.getValue(ci_output, "ci_output", Default="admin")
		
		tk_label(thisFrame, Label="CodeIgniter Output: ", xPos=0, yPos=250, Width=27, bG="#EEE")
		tk_radio(ci_output, thisFrame, Label="Control Panel", Value="admin", xPos=0, yPos=270, bG="#EEE", Width=20)
		tk_radio(ci_output, thisFrame, Label="API", Value="api", xPos=0, yPos=290, bG="#EEE", Width=20)
		
		def show_config_frame(config_file):
			frame = configFilesFrame(self.root, thisFrame, config_file)
			frame.display()
		
		config_file = tk.StringVar()
		CONFIGFILES = ('autoload.php', 'config.php', 'migration.php','profiler.php')
		tk_optionmenu(config_file, thisFrame, Label="Config File Settings: ", ITEMS=CONFIGFILES, xPos=0, yPos=325, bG="#EEE",Command=show_config_frame, Width=23)
		
		application_dir = tk.StringVar()
		self.getValue(application_dir, "application_directory", Default="application")
		
		mvc_dir = tk.StringVar()
		self.getValue(mvc_dir, "mvc_directory", Default=self.root.selectedDB+"-ci")
		
		system_dir = tk.StringVar()
		self.getValue(system_dir, "system_directory", Default="system")
		
		def advance_directory():
			
			def closeThisFrame():
				self.advance_directory_frame.destroy()
				self.advance_directory_frame = None
			
			if self.advance_directory_frame != None:
				closeThisFrame()
				
			self.advance_directory_frame = tk_frame(thisFrame, 240, 40, PadX=5, PadY=5, Width=240, Height=360, Relief=tk.RAISED,BD=1,BG="#EEE")
			tk_label(self.advance_directory_frame, Label="Advance Directory Settings", xPos=0, yPos=0, Width=25, bG="#EEE", Font=('Serif',(9),"bold"), fG="#000", Anchor='w')
			
			tk_entry(mvc_dir, self.advance_directory_frame, Label="MVC Directory: ", xPos=0, yPos=30, bG="#EEE", Width=27)
			
			tk_entry(application_dir, self.advance_directory_frame, Label="Application Directory: ", xPos=0, yPos=80, bG="#EEE", Width=27)
			
			tk_entry(system_dir, self.advance_directory_frame, Label="System Directory: ", xPos=0, yPos=130, bG="#EEE", Width=27)
			
			close_button(self.advance_directory_frame, 208, 0, bCommand=closeThisFrame, bG='#EEE', bImage=self.closeImage, bState=tk.NORMAL)
			
		save_directory = tk.StringVar()
		tk_entry(save_directory, thisFrame, Label="Save Directory: ", xPos=250, yPos=50, bG="#EEE", Width=27)
		settings_button( thisFrame, 450, 48, advance_directory, '#EEE', bImage=self.settingsImage, bState=tk.NORMAL )
		self.getValue(save_directory, "directory")
		
		mvc_template_var = tk.StringVar()
		self.getValue(mvc_template_var, "template", Default="template_1")
		
		tk_label(thisFrame, Label="MVC Template: ", xPos=250, yPos=100, Width=27, bG="#EEE")
		tk_radio(mvc_template_var, thisFrame, Label="Template 1", Value="template_1", xPos=250, yPos=120, bG="#EEE", Width=20)
		
		index_environment = tk.StringVar()
		self.getValue(index_environment, "index_environment", Default="development")
		
		tk_label(thisFrame, Label="MVC Environment: ", xPos=250, yPos=150, Width=27, bG="#EEE")
		tk_radio(index_environment, thisFrame, Label="Development", Value="development", xPos=250, yPos=170, bG="#EEE", Width=20)
		tk_radio(index_environment, thisFrame, Label="Testing", Value="testing", xPos=250, yPos=190, bG="#EEE", Width=20)
		tk_radio(index_environment, thisFrame, Label="Production", Value="production", xPos=250, yPos=210, bG="#EEE", Width=20)
		
		ci_version = tk.StringVar()
		self.getValue(ci_version, "ci_version", Default="3")
		
		tk_label(thisFrame, Label="CodeIgniter Version: ", xPos=250, yPos=240, Width=27, bG="#EEE")
		tk_radio(ci_version, thisFrame, Label="3.0", Value="3", xPos=250, yPos=260, bG="#EEE", Width=20)
		tk_radio(ci_version, thisFrame, Label="2.x", Value="2", xPos=250, yPos=280, bG="#EEE", Width=20)
		
		def initiate_project():
			
			def closeThisFrame():
				self.advance_directory_frame.destroy()
				self.advance_directory_frame = None
			
			if self.advance_directory_frame != None:
				closeThisFrame()
				
			self.advance_directory_frame = tk_frame(thisFrame, 240, 40, PadX=5, PadY=5, Width=240, Height=360, Relief=tk.RAISED,BD=1,BG="#EEE")
			tk_label(self.advance_directory_frame, Label="Initiate Project Files", xPos=0, yPos=0, Width=25, bG="#EEE", Font=('Serif',(9),"bold"), fG="#000", Anchor='w')
			
			tk_button(self.advance_directory_frame, Label="Core Files", xPos=0, yPos=50, bCommand=self.initiate_core_files, bG='orange', Width=24)
			tk_button(self.advance_directory_frame, Label="Application Files", xPos=0, yPos=85, bCommand=self.initiate_application_files, bG='orange', Width=24)
			
			close_button(self.advance_directory_frame, 208, 0, bCommand=closeThisFrame, bG='#EEE', bImage=self.closeImage, bState=tk.NORMAL)
		
		tk_button(thisFrame, Label="Update Index File", xPos=250, yPos=325, bCommand=self.initiate_index_file, bG='orange', Width=24)
		tk_button(thisFrame, Label="Inititate Project Files", xPos=250, yPos=360, bCommand=initiate_project, bG='red', Width=24)
		
		
		dbhost_var = tk.StringVar()
		tk_entry(dbhost_var, thisFrame, Label="DB Host: ", xPos=500, yPos=50, bG="#EEE", Width=27)
		self.getValue(dbhost_var, "dbhost", Default="localhost")

		dbuser_var = tk.StringVar()
		tk_entry(dbuser_var, thisFrame, Label="DB User: ", xPos=500, yPos=100, bG="#EEE", Width=27)
		self.getValue(dbuser_var, "dbuser", Default="root")
		
		dbpass_var = tk.StringVar()
		tk_entry(dbpass_var, thisFrame, Label="DB Password: ", xPos=500, yPos=150, bG="#EEE", Width=27)
		self.getValue(dbpass_var, "dbpass")

		dbport_var = tk.StringVar()
		tk_entry(dbport_var, thisFrame, Label="DB Port: ", xPos=500, yPos=200, bG="#EEE", Width=27)
		self.getValue(dbport_var, "dbport", Default=3306)
		
		tableprefix_var = tk.StringVar()
		tk_entry(tableprefix_var, thisFrame, Label="Table Prefix: ", xPos=500, yPos=250, bG="#EEE", Width=27)
		self.getValue(tableprefix_var, "tblprefix")

		def initiate_tables():
			
			def closeThisFrame():
				self.advance_directory_frame.destroy()
				self.advance_directory_frame = None
			
			if self.advance_directory_frame != None:
				closeThisFrame()
				
			self.advance_directory_frame = tk_frame(thisFrame, 490, 40, PadX=5, PadY=5, Width=240, Height=360, Relief=tk.RAISED,BD=1,BG="#EEE")
			tk_label(self.advance_directory_frame, Label="Initiate Project Tables", xPos=0, yPos=0, Width=25, bG="#EEE", Font=('Serif',(9),"bold"), fG="#000", Anchor='w')
			
			tk_button(self.advance_directory_frame, Label="Admins Tables", xPos=0, yPos=50, bCommand=self.prepare_admin_tables, bG='orange', Width=24)
			
			tk_button(self.advance_directory_frame, Label="Media Uploads", xPos=0, yPos=85, bCommand=self.prepare_media_uploads, bG='orange', Width=24)

			close_button(self.advance_directory_frame, 208, 0, bCommand=closeThisFrame, bG='#EEE', bImage=self.closeImage, bState=tk.NORMAL)

		tk_button(thisFrame, Label="Update Database File", xPos=500, yPos=325, bCommand=self.initiate_database_file, bG='orange', Width=24)
		tk_button(thisFrame, Label="Inititate Tables", xPos=500, yPos=360, bCommand=initiate_tables, bG='red', Width=24)
		
		def save_mvc():
			self.init_vars()
			self.currentMVC.insert('title', project_title.get())
			self.currentMVC.insert('project_website', project_website.get())
			self.currentMVC.insert('author', author_name.get())
			self.currentMVC.insert('author_website', author_website.get())
			self.currentMVC.insert('ci_output', ci_output.get())
			self.currentMVC.insert('directory', save_directory.get())
			self.currentMVC.insert('mvc_directory', mvc_dir.get())
			self.currentMVC.insert('application_directory', application_dir.get())
			self.currentMVC.insert('system_directory', system_dir.get())
			self.currentMVC.insert('template', mvc_template_var.get())
			self.currentMVC.insert('index_environment', index_environment.get())
			self.currentMVC.insert('ci_version', ci_version.get())
			self.currentMVC.insert('dbhost', dbhost_var.get())
			self.currentMVC.insert('dbuser', dbuser_var.get())
			self.currentMVC.insert('dbpass', dbpass_var.get())
			self.currentMVC.insert('dbport', dbport_var.get())
			self.currentMVC.insert('tblprefix', tableprefix_var.get())
		
		tk_button(thisFrame, Label="Save Settings", xPos=0, yPos=0, bCommand=save_mvc, bG='green', Width=19)
		
		def return_home():
			self.root.hideActiveFrames('body')
			self.root.bodyFrame.grid()
			
		image_button(thisFrame, xPos=740, yPos=0, bCommand=return_home, bG='#EEE', bImage=self.root.homeImage, bState=tk.NORMAL)
        
	def initiate_core_files(self):
		saveDir = self.currentMVC.get_value("directory")		
		mvc_folder = self.currentMVC.get_value("mvc_directory")
		app_folder = self.currentMVC.get_value("application_directory")
		sys_folder = self.currentMVC.get_value("system_directory")
		
		if saveDir != '' or saveDir != None:
			
			mvcDir = os.path.join(saveDir, mvc_folder)
			appDir = os.path.join(saveDir, mvc_folder, app_folder)
			sysDir = os.path.join(saveDir, mvc_folder, sys_folder)
                        
			if not os.path.exists(mvcDir):
				os.makedirs(mvcDir)
				print( "Folder Created: " + mvcDir )
						
			if not os.path.exists(appDir):
				os.makedirs(appDir)
				print( "Folder Created: " + appDir )

			if not os.path.exists(sysDir):
				os.makedirs(sysDir)
				print( "Folder Created: " + sysDir )
					
			unzip('mvc/system.zip', sysDir)
			print( "mvc/system.zip => " + sysDir )
			
			unzip('mvc/application.zip', appDir)
			print( "mvc/application.zip => " + appDir )
				
	def initiate_application_files(self):
		saveDir = self.currentMVC.get_value("directory")
		template = self.currentMVC.get_value("template")
		mvc_folder = self.currentMVC.get_value("mvc_directory")
		app_folder = self.currentMVC.get_value("application_directory")
		
		if saveDir != '' or saveDir != None:
			
			mvcDir = os.path.join(saveDir, mvc_folder)
			appDir = os.path.join(saveDir, mvc_folder, app_folder)
			
			if not os.path.exists(mvcDir):
				os.makedirs(mvcDir)
				print( "Folder Created: " + mvcDir )
			
			if not os.path.exists(appDir):
				os.makedirs(appDir)
				print( "Folder Created: " + appDir )
				
			unzip('mvc/'+template+'/source/assets.zip', mvcDir)
			print( "mvc/"+template+"/source/assets.zip => " + mvcDir )
			
			unzip('mvc/'+template+'/source/application/application.zip', appDir)
			print( "mvc/"+template+"/source/application/application.zip => " + appDir )
			
			configs.DatabaseFile(self.root.saveFile, self.root.selectedDB, appDir).save()
			configs.TemplateData(self.root.saveFile, self.root.selectedDB, appDir).save()
			configs.ConfigFile(self.root.saveFile, self.root.selectedDB, appDir).save()
	
	def initiate_index_file(self):
		saveDir = self.currentMVC.get_value("directory")
		template = self.currentMVC.get_value("template")
		mvc_folder = self.currentMVC.get_value("mvc_directory")
		
		if saveDir != '' or saveDir != None:
			mvcDir = os.path.join(saveDir, mvc_folder)
			
			if not os.path.exists(mvcDir):
				os.makedirs(mvcDir)
				print( "Folder Created: " + mvcDir )
			
			configs.InDexFile(self.root.saveFile, self.root.selectedDB, mvcDir).save()

	def initiate_database_file(self):
		saveDir = self.currentMVC.get_value("directory")		
		mvc_folder = self.currentMVC.get_value("mvc_directory")
		app_folder = self.currentMVC.get_value("application_directory")
		
		if saveDir != '' or saveDir != None:			
			mvcDir = os.path.join(saveDir, mvc_folder)
			appDir = os.path.join(saveDir, mvc_folder, app_folder)
			
			if not os.path.exists(mvcDir):
				os.makedirs(mvcDir)
				print( "Folder Created: " + mvcDir )

			if not os.path.exists(appDir):
				os.makedirs(appDir)
				print( "Folder Created: " + appDir )
				
			configs.DatabaseFile(self.root.saveFile, self.root.selectedDB, appDir).save()
			
	def prepare_admin_tables(self):
		tblprefix = self.currentMVC.get_value("tblprefix")
		
		if tblprefix == None:
			tblprefix = ''
			
		admins_init = AdminsTable.Admins(self.root.saveFile, self.root.selectedDB, tblprefix )
		admins_access_init = AdminsAccessTable.AdminsAccess(self.root.saveFile, self.root.selectedDB, tblprefix )
		admins_sessions_init = AdminsSessionsTable.AdminsSessions(self.root.saveFile, self.root.selectedDB, tblprefix )
		
		if admins_init.create_table():
			print( "Admins Table Initiated!" )
		if admins_access_init.create_table():
			print( "Admins Access Table Initiated!" )
		if admins_sessions_init.create_table():
			print( "Admins Sessions Table Initiated!" )

	def prepare_media_uploads(self):
		tblprefix = self.currentMVC.get_value("tblprefix")
		
		if tblprefix == None:
			tblprefix = ''
		
		init = MediaUploadsTable.MediaUploads(self.root.saveFile, self.root.selectedDB, tblprefix )
		if init.create_table():
			print( "Media Uploads Initiated!" )
