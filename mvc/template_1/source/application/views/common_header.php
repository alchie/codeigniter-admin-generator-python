<!DOCTYPE html>
<html>

<head>
	<meta name="robots" content="noindex">
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title><?php echo $page_title; ?></title>

    <link href="<?php echo $base_url; ?>assets/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo $base_url; ?>assets/font-awesome/css/font-awesome.css" rel="stylesheet">
    <link href="<?php echo $base_url; ?>assets/js/plugins/bootstrap-select/bootstrap-select.min.css" rel="stylesheet">
    <link href="<?php echo $base_url; ?>assets/css/plugins/dataTables/dataTables.bootstrap.css" rel="stylesheet">
    <link href="<?php echo $base_url; ?>assets/js/plugins/datetimepicker/jquery.datetimepicker.css" rel="stylesheet">
    <link href="<?php echo $base_url; ?>assets/js/plugins/fancyBox/jquery.fancybox.css" rel="stylesheet">
    <link href="<?php echo $base_url; ?>assets/css/sb-admin.css" rel="stylesheet">
    
	<script src="<?php echo $base_url; ?>assets/js/jquery-1.10.2.js"></script>
</head>
<body>
	
    <div id="wrapper">

        <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation" style="margin-bottom: 0">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".sidebar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="<?php echo site_url('dashboard'); ?>">Admin Panel</a>
            </div>
            <!-- /.navbar-header -->

            <ul class="nav navbar-top-links navbar-right">
                <li>Welcome <?php echo  $active_session['admin_name']; ?>! &middot; <small><strong>Last Login:</strong> <?php echo  $active_session['last_login']; ?> (<?php echo  $active_session['last_login_ip']; ?>)</small> &nbsp; </li>
                <li><a href="<?php echo site_url('change_password'); ?>"><i class="fa fa-sign-out fa-fw"></i> Change Password</a></li>
                 <li><a href="<?php echo site_url('logout'); ?>"><i class="fa fa-sign-out fa-fw"></i> Logout</a></li>
            </ul>
            <!-- /.navbar-top-links -->

        </nav>
        <!-- /.navbar-static-top -->

        <nav class="navbar-default navbar-static-side" role="navigation">
            <div class="sidebar-collapse">
                <ul class="nav" id="side-menu">

<?php $this->load->view('sidebar_nav'); ?>

                </ul>
                <!-- /#side-menu -->
            </div>
            <!-- /.sidebar-collapse -->
        </nav>
        <!-- /.navbar-static-side -->

        <div id="page-wrapper">
