<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Login extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->output->enable_profiler(TRUE);   
    }
    	
	public function index()
	{
	    
	    $this->load->library(array('form_validation') );
	    
	    $this->form_validation->set_rules('username', 'Username', 'trim|required|xss_clean');
		$this->form_validation->set_rules('password', 'Password', 'trim|required|xss_clean|sha1|md5');
		
		if ($this->form_validation->run() == FALSE)
		{
		     if( $this->input->post() ) {
		         $this->template_data->alert( validation_errors(), 'danger');
		    }
		} 
		else 
		{
		    
		    $this->load->model(array('Admins_model', 'Admins_access_model'));
		    
		    $admin_login = new $this->Admins_model;
		    $admin_login->setUsername( $this->input->post( 'username'), true );
            $admin_login->setPassword( $this->input->post( 'password'), true );
		    $admin_login->setActive( 1, true );
		    
		    if ( $admin_login->nonEmpty() === TRUE ) { 
				  $admin_results = $admin_login->getResults();
				  
				  $userdata = array(
						'admin_logged_in' => TRUE,
						'admin_name' => $admin_results->name,
						'admin_username' => $admin_results->username,
						'admin_id' => $admin_results->id,
						'admin_email' => $admin_results->email,
						'last_login' => $admin_results->last_login,
						'last_login_ip' => $admin_results->last_login_ip
					);
				  
				  $admin_access = new $this->Admins_access_model;
				  $admin_access->setAdminId( $admin_results->id, true );
				  $admin_access->setLimit(0);
				  $admin_access_results = $admin_access->populate();

				  $access_data = array();
				  if( $admin_access ) foreach( $admin_access_results as $access ) {
					  $access_data['controller_' . $access->controller] = (object) array( 
					  'can_add' => (bool) $access->add,
					  'can_edit' => (bool) $access->edit,
					  'can_delete' => (bool) $access->delete
					  );
				  }
				  
				  $userdata['admin_access'] = base64_encode( json_encode($access_data) );
				  
				  $this->session->set_userdata( $userdata );
				  
					$this->load->helper( array('url', 'date') );
					$admin_login->setLastLogin( unix_to_human( time(), TRUE, 'eu' ), false, true );
					$admin_login->setLastLoginIp( $this->input->ip_address(), false, true );
					$admin_login->update();		       
					
				   redirect('dashboard', 'location', 301);
		    } else {
				$this->template_data->alert( 'Unable to login!', 'danger');
			}
		}
		
		$this->load->view('login', $this->template_data->get() );	

	}
	
	public function logout() {
	    $this->session->sess_destroy();
	    redirect('login', 'refresh');
	    exit;
	}
	
}

