(function($) {
var methods = {
	list_filters : [],
	list_start : [],
	lock_page : function(cancel) {
		cancel = typeof cancel !== 'undefined' ? true : false;
		if( cancel ) {
			$('.disable-entire-page').remove();
			$('body').css('overflow', '');
		} else {
			if( $('.disable-entire-page').length == 0 ) {
				var lockpage = $('<div />').addClass('disable-entire-page');
				lockpage.css({ 
					width 			: '100%',
					height			: '100%',
					position 		: 'fixed',
					zIndex 			: 99999,
					top 			: 0,
					backgroundColor	: 'rgba(0,0,0,0.05)',
					margin: '52px 0 0 250px',
				});
				lockpage.appendTo('body');
				var loader = $('<img />').attr('src', methods.baseURL + 'assets/images/ajax-loader4.gif');
				loader.css({ 
					marginLeft		: (($(window).width() - 350) / 2),
					marginTop		: (($(window).height() - 152) / 3)
				});
				loader.appendTo(lockpage);
				$('body').css('overflow', 'hidden');
			}
		}
	},
	show_alert : function( success, data ) {
		var alert_container = $('<div />').attr('id', 'alert-container').prependTo( $('#page-wrapper') );
		if( success ) {
			$('<div class="alert alert-success alert-dismissable" style="display:none;position: fixed;width: 84%;"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>'+data.message+'</div>').appendTo(alert_container).slideDown().delay(1000).slideUp(function() { $(this).remove(); alert_container.remove() });
		} else {
			$('<div class="alert alert-danger alert-dismissable" style="display:none;position: fixed;width: 84%"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>'+data.message+'</div>').appendTo(alert_container).slideDown().delay(2000).slideUp(function() { 
				$(this).remove(); 
				alert_container.remove();
					if( data.redirect ) {
						window.location.href = data.url;
					}
				});
		}
	},
	edit_form : function( data ) {
		$('#edit-view-'+data.table+' input[type="text"]').val('');
		methods.filters_to_dropdown( $('#edit-view-'+data.table+' select.dropdown-table'), false );
		methods.current_data = data.results;
		
		var primary_title = methods.tables[data.table].primary_title;
		
		for(key in data.results) {
			var value = data.results[key],
			input = $('.edit_'+data.table+'_'+key);
			
			if( key == primary_title ) {
				var page_header = $('.page-header');
				if( page_header.children('.label').length == 0 ) {
					var page_header_title = $('<span />').addClass('label label-primary pull-right');
					page_header_title.text( value ).appendTo(page_header);
					page_header_title.attr('data-table', data.table);
				}
			}
			
			input.each(function() {
				var datatype = $(this).attr('data-type'), 
				type = $(this).prop('type'), 
				node = $(this).prop('nodeName'),
				datafield = $(this).attr('data-field'),
				datalabel = $(this).attr('data-label'),
				datakey = $(this).attr('data-key'),
				datavalue = $(this).attr('data-value'),
				datasource = $(this).attr('data-source');
				
				if( node == 'INPUT' ) {
					if( type == 'checkbox' || type == 'radio') {
						if( $(this).val() == value ) {
							$(this).prop('checked', true);
						} else {
							$(this).prop('checked', false);
						}
					} else {
						if( datatype != 'password') {
							$(this).val( value );
						} else {
							$(this).val('');
						}
					}
				} 
				else if (node == 'SELECT') {
					if( type == 'select-one') {
						$('select#edit_'+data.table+'_'+key+' option').each(function() {
							if( $(this).val() == value ) {
								$(this).prop('selected', true);
								$(this).parent('select').selectpicker({dropupAuto : true, val : value }).selectpicker('refresh');
							}
						});
					}
				}
				else if (node == 'TEXTAREA') {
					if( $(this).attr('data-wysiwyg') == 1 ) {
						CKEDITOR.instances[$(this).attr('id')].setData( value );
					} else {
						$(this).val( value );
					}
				}
				else if (node == 'DIV') {
					if( datatype == 'checkbox' && datasource == 'table' ){
						if( typeof methods.filters_data != 'undefined' && typeof methods.filters_data[key] != 'undefined' ) {
							methods.filters_to_checkbox(data.table, methods.filters_data, $(this), value);
						}
					}
					if(  datatype == 'radio' && datasource == 'table' ){
						if( typeof methods.filters_data != 'undefined' && typeof methods.filters_data[key] != 'undefined' ) {
							methods.filters_to_checkbox(data.table, methods.filters_data, $(this), value);
						}
					}
				} 
				else {
					if( datatype != 'password') {
						$(this).val( value );
					}
				}
			});
			var text_value = $('.text-value.'+data.table+'.'+key), 
			textDataType = text_value.attr('data-type');
			if (textDataType == 'password') {
				text_value.text( "**********" );
			} else {
				text_value.text( value );
			}
			
			if( $('.text-searchable.'+key).length != 0 ) {
				var text_val = methods.text_searchable_update(key, data.results);
				text_value.text( text_val );
			}
			
		}
		
		var upload_settings = methods.tables[data.table].upload_settings;
		if( typeof upload_settings != 'undefined') {
			if( data.results[methods.tables[data.table].upload_settings.is_image] == 1 ) {
				var image_url = methods.tables[data.table].upload_settings.upload_url + data.results[methods.tables[data.table].upload_settings.file_name];
				var panel_body = $('#edit-view-'+data.table+' .panel-body');
				if( panel_body.children('#image-preview').length > 0 ) {
					panel_body.children('#image-preview').remove();
				}
				var img_row = $('<div />').attr('id', 'image-preview').addClass('row').prependTo( panel_body );
				var img_col = $('<div />').addClass('col-md-8 col-md-offset-2').appendTo( img_row );
				var img_link = $('<a />').addClass('thumbnail').attr('href', '#').appendTo( img_col );
				var img = $('<img />').attr('src', image_url).appendTo( img_link );
			}
		}
		$('#update-action-'+data.table).attr('data-id', data.id).attr('data-table', data.table);
		$('.update-back-'+data.table).attr('data-table', data.table);
		$('.child-nav').each(function() {
			var href = $(this).attr('data-href');
			$(this).prop('href', href + data.id);
		});
		
		$('#list-view-'+data.table).slideUp('slow', function(){
			$('#edit-view-'+data.table).slideDown(function() {
				methods.lock_page(true);
			});
		});
		
		$('.change-deliberately.button .btn.update.'+data.table).click( methods.change_deliberately );
	},
	add_data : function( table ) {
		var postData = {action : 'add'}, 
		current_table = methods.tables[ table ],
		ajaxURL = methods.baseURL+'index.php/'+table+'/ajax';
		for( i in current_table.add_fields) {
			var current_field = current_table.add_fields[i];
			var input = $('.add_'+table+'_'+current_field);
			input.each(function(){
				if( $.inArray($(this).prop('nodeName'), ['INPUT','SELECT','TEXTAREA']) > -1 ) {
					if( $.inArray($(this).attr( 'type' ), ['checkbox','radio']) > -1 ) {
						if( $(this).is(':checked') ) {
							postData[current_field] = $(this).val();
						}
					} else {
						postData[current_field] = $(this).val();
					}
				}
			});
		}
		if( typeof current_table.required_key != 'undefined' && typeof current_table.required_value != 'undefined' && typeof methods.current_data != 'undefined' && typeof methods.current_data[current_table.required_key] != 'undefined') {
			postData[current_table.required_value] = methods.current_data[current_table.required_key];
		}
		methods.lock_page();
		methods.ajax(postData, ajaxURL, function(data, methods){
			methods.data_added( data, true );
		});
	},
	data_added : function( data, showAlert ) {
		var showAlert = typeof showAlert !== 'undefined' ? showAlert : true;
		if(showAlert == true) {
			methods.show_alert(true, data);
		}
		current_table = methods.tables[ data.table ];
		var tr = $('<tr />').attr('id', data.table+'-list-item-'+data.id).prependTo('#list-view-'+data.table+' #dataTables tbody');
		var active_checkbox = $('<input />');
		if( typeof current_table.active_key != 'undefined' && current_table.active_key != '') {
				var active_column = $('<td />').appendTo( tr );
				active_checkbox.prop('type', 'checkbox').appendTo( active_column );
				active_checkbox.prop('checked', true );
				active_checkbox.attr('data-id', data.id);
				active_checkbox.attr('data-table', data.table);
				active_checkbox.click(methods.activate_item);
			}
		
		for( i in current_table.list_fields ) {
			var field = current_table.list_fields[i];
			var item_value = data.results[field];
			var new_data = $('<span />').addClass( field ).text( item_value );
			var result = item_value;
			
				if((typeof current_table.check_cross != 'undefined') && ( $.inArray(field, current_table.check_cross) > -1 )) {
					if ( item_value == '1' ) {
						result = $('<span />').addClass('glyphicon glyphicon-ok text-center').css('display', 'block');
					}
					else if ( item_value == '0' ) {
						result = $('<span />').addClass('glyphicon glyphicon-remove text-center').css('display', 'block');
					}
				} 
			
			if( data.results[field] != '' && typeof current_table.filters != 'undefined' && typeof current_table.filters[field] != 'undefined' ) {
				if( current_table.filters[field].type == 'table' ) {
					result = methods.filters_data[field][data.results[field]][current_table.filters[field].value];
				} else if( current_table.filters[field].type == 'manual' ) {
					result = methods.filters_data[field][data.results[field]];
				}
				if( current_table.filters[field].anchor == 1 ) {
					result = methods.anchor_filter(data.table, field, data.results[field], result);
				} 
			} 
			
			//if( new_data.text() == '' && typeof methods.text_searchable_selected != 'undefined' ) {
				//result = methods.text_searchable_selected;
			//}
			
			new_data.html( result );
			
			$('<td />').html( new_data ).appendTo( tr );
		}
		if( current_table.actions_edit == 1 || current_table.actions_delete == 1) {
			
			var action_td = $('<td />').appendTo( tr );
			
			if( current_table.actions_edit == 1 ) {
				var edit_button = $('<a />').attr('href', 'javascript:void(0);').text('Edit').prependTo( action_td );
				edit_button.attr('id', data.table+'-edit-button-'+data.id);
				edit_button.attr('data-id', data.id);
				edit_button.attr('data-primary_key', current_table.primary_key);
				edit_button.attr('data-table', data.table);
				edit_button.addClass('btn btn-success btn-xs');
				edit_button.css('width', '49px');
				edit_button.click(function(){
					var postData = {action : 'get'}, 
					id = $(this).attr('data-id'), 
					primary_key = $(this).attr('data-primary_key'),
					table = $(this).attr('data-table');
					postData[primary_key] = id;
					methods.currently_updating = id;
					methods.ajax(postData, methods.baseURL+'index.php/'+table+'/ajax', methods.edit_form);
				});
				
				if( current_table.actions_edit == 1 && current_table.actions_delete == 1) {
					$('<span> &nbsp; </span>').insertAfter( edit_button );
				}
			}
			if( current_table.actions_delete == 1 ) {
				var delete_button = $('<a />').attr('href', 'javascript:void(0);').text('Delete').appendTo( action_td );
				delete_button.addClass('btn btn-danger btn-xs');
				delete_button.attr('id', data.table+'-delete-button-'+data.id);
				delete_button.attr('data-id', data.id);
				delete_button.attr('data-primary_key', current_table.primary_key);
				delete_button.attr('data-table', data.table);
				delete_button.css('width', '49px');
				delete_button.click(function(){
					if( confirm('Are you sure?') ) {
						methods.remove_data( $(this).attr('data-table'), $(this).attr('data-primary_key'), $(this).attr('data-id') );
					}
				});
			}
		
		}
		$('#add-view-'+data.table+' .panel-body input[type="text"]').val('');
		$('#add-view-'+data.table+' .panel-body input[type="password"]').val('');
		$('#add-view-'+data.table+' .panel-body textarea').val('');
		$('#add-view-'+data.table+' .panel-body input[type="checkbox"]').prop('checked', false);
		$('#add-view-'+data.table+' .panel-body input[type="radio"]').prop('checked', false);
		$('#add-view-'+data.table+' .panel-body select > option').prop('selected', false);
		$('select.selectpicker').selectpicker({dropupAuto : true }).selectpicker('render');
		
		if( data.results[current_table.active_key] == 0 ) {
			$('#'+data.table+'-list-item-'+data.id).addClass('danger');
			active_checkbox.prop('checked', false );
		} else {
			$('#'+data.table+'-list-item-'+data.id).addClass('success');
		}
		if( $('.add-panel-'+data.table+' .returnToList').is(':checked') ) {
			$('#add-back-'+data.table).trigger('click');
		}
		delete(methods['text_searchable_selected']);
		methods.increment_total_items( data.table );
		methods.text_searchable_reset();
		methods.lock_page(true);
	},
	update_data : function( table, id ) {
		methods.updated_row = {};
		var postData = {action : 'edit'}, 
		current_table = methods.tables[table],
		ajaxURL = methods.baseURL+'index.php/'+table+'/ajax';
		postData[current_table.primary_key] = id;
		methods.id = id,
		n = 0;
		
		var editFormValues = function( postData, fields ) {
			for( i in fields) {
				var current_field = fields[i];
				var input = $('.edit_'+table+'_'+current_field);
				input.each(function(){
					if( $.inArray($(this).prop('nodeName'), ['INPUT','SELECT','TEXTAREA']) > -1 ) {
						if(  $(this).attr( 'type' ) == 'checkbox' || $(this).attr( 'type' ) == 'radio' ) {
							if( $(this).is(':checked') ) {
								postData[current_field] = $(this).val();
							}
						} else if( $(this).prop('nodeName') == 'TEXTAREA') {
							if( $(this).attr('data-wysiwyg') == 1 ) {
								postData[current_field] = CKEDITOR.instances[$(this).attr('id')].getData();
							} else {
								postData[current_field] = $(this).val();
							}
						} else {
							postData[current_field] = $(this).val();
						}
						if( $.inArray(current_field, current_table.list_fields) > -1 ) {
							if( $(this).attr( 'type' ) == 'select-one' ) {
								methods.updated_row[current_field] = $(this).text();
								n++;
							} else if( $.inArray($(this).attr( 'type' ), ['checkbox','radio']) > -1 ) {
								if( $(this).is(':checked') ) {
									methods.updated_row[current_field] = $(this).val();
									n++;
								}
							} else {
								methods.updated_row[current_field] = $(this).val();
								n++;
							}
						}
					}
				});
			}
			return postData;
		}
		
		postData = editFormValues( postData, current_table.edit_fields );
		postData = editFormValues( postData, current_table.extra_action_fields );
		
		if( typeof postData[current_table.active_key] == 'undefined' ) {
			postData[current_table.active_key] = 0;
		}
		
		methods.lock_page();
		methods.ajax( postData, ajaxURL, methods.data_updated );
	},
	data_updated : function( data ) {
		methods.show_alert(true, data);
		var current_table = methods.tables[data.table];
		var tr = $('#'+data.table+'-list-item-'+data.id).removeClass();
		if( data.results[current_table.active_key] == 0 ) {
			tr.addClass('danger');
		} else {
			tr.addClass('warning');
		}
		
		for( i in current_table.list_fields ) {
			var field = current_table.list_fields[i];
			var result = data.results[field];
			
				if((typeof current_table.check_cross != 'undefined') && ( $.inArray(field, current_table.check_cross) > -1 )) {
					if ( result == '1' ) {
						result = $('<span />').addClass('glyphicon glyphicon-ok text-center').css('display', 'block');
					}
					else if ( result == '0' ) {
						result = $('<span />').addClass('glyphicon glyphicon-remove text-center').css('display', 'block');
					}
				}
				
			if( data.results[field] != '' && typeof current_table.filters != 'undefined' && typeof current_table.filters[field] != 'undefined' ) {
				
				if( current_table.filters[field].type == 'table' ) {
					result = methods._get_value([field, data.results[field], current_table.filters[field].value], methods.filters_data);
				} else if( current_table.filters[field].type == 'manual' ) {
					result = methods._get_value([field, data.results[field]], methods.filters_data);
				}
				if( current_table.filters[field].anchor == 1 ) {
					result = methods.anchor_filter(data.table, field, data.results[field], result);
				} 

			} 
			$('#'+data.table+'-list-item-'+data.id+' .'+field).html(  result );
		}
		methods.lock_page(true);
		if( $('.edit-panel-'+data.table+' .returnToList').is(':checked') ) {
			$('#update-back-'+data.table).trigger('click');
		}
	},
	anchor_filter : function(table, key, value, text) {
		var link = $('<a />').attr('href', 'javascript:void(0);');
						link.addClass('list_filter');
						link.attr('title', 'Add Filter');
						link.attr('data-key', key).attr('data-value', value).attr('data-table', table);
						link.click( methods.add_list_filter );
		var span = $('<span />').text( text );
		span.appendTo(link);
		span.addClass('list_item').attr('data-key', key).attr('data-value', value).attr('data-table', table);
		return link;
	},
	remove_data : function(table, primary_key, id) {
		var postData = {action : 'delete'};
		postData[primary_key] = id;
		$('#'+table+'-list-item-'+id).addClass('danger');
		methods.ajax(postData, methods.baseURL+'index.php/'+table+'/ajax', methods.data_removed);
	},
	data_removed : function( data ) {
		methods.lock_page(true);
		$('#'+data.table+'-list-item-'+data.id).fadeOut(function() { 
			$(this).remove(); 
			methods.decrement_total_items( data.table );
			methods.show_alert(true, data);
		});
	},
	data_error : function( data ) {
		methods.lock_page(true);
		methods.show_alert(false, data);
	},
	list_pagination : function(items, limit, start, table) {
		start = typeof start !== 'undefined' ? start : 0;
		if( $('#list-pagination-'+table).length != 0 ) {
			var wrapper = $('#list-pagination-'+table).empty();
		} else {
			var wrapper = $('<div />').attr('id', 'list-pagination-'+table).addClass('text-center').insertAfter('#list-view-'+table+' #dataTables');
		}
		var ul = $('<ul />').addClass('pagination pagination-sm').appendTo( wrapper );
		ul.appendTo( wrapper );
		var pages = Math.ceil( items / limit );
		if( pages > 1 ) {
			for( i=1; i <= pages;i++) {
				var li = $('<li />').appendTo( ul );
				var a = $('<a />').attr('href', 'javascript:void(0)').text(i);
				a.attr('data-start', ((i-1) * limit));
				if( start == ( (i - 1) * limit ) ) {
					li.addClass('disabled');
				} else {
					a.click(function() {
						methods.list_start[table] = $(this).attr('data-start');
						methods.init_list(table);
					});
				}
				a.appendTo( li );
			}
		}
		item_label = 'Item';
		if (items > 1) {
			item_label = 'Items';
		}
		if( $('#total-items-'+table).length != 0 ) {
			$('#total-items-'+table).html( "<em><strong><small class='count'>" + items + "</small></strong></em> " +item_label + " found!" );
		} else {
			$('<div />').html( "<em><strong><small class='count'>" + items + "</small></strong></em> "+item_label+" found!" ).attr('id', 'total-items-'+table).addClass('pull-left').insertAfter('#list-view-'+table+' #dataTables');
		}
	},
	increment_total_items : function(table) {
		var total = $('#total-items-'+table+' .count');
		var newvalue = parseInt( total.text() ) + 1;
		total.text( newvalue );
	},
	decrement_total_items : function(table) {
		var total = $('#total-items-'+table+' .count');
		var newvalue = parseInt( total.text() ) - 1;
		if (newvalue < 0 ) {
			newvalue = 0;
		}
		total.text( newvalue );
	},
	init_list : function( table ) {
		if( typeof table == 'undefined' ) table = methods.current_table;
		var current_table = methods.tables[ table ], 
		list_filters = methods.list_filters[ table ],
		list_start = typeof methods.list_start[ table ] == 'undefined' ? 0 : methods.list_start[ table ],
		postData = {
			action:'list', 
			limit: current_table.list_limit, 
			order_by : current_table.order_by, 
			order_sort: current_table.order_sort,
			filter : []
			},
		ajaxURL = methods.baseURL+'index.php/' + table + '/ajax';
		
		if(typeof list_filters !== 'undefined' && list_filters.length > 0 ) {
			postData.filter = list_filters;
		}
		
		if( typeof current_table.required_key != 'undefined' && typeof methods.current_data != 'undefined' && typeof methods.current_data[current_table.required_key] != 'undefined') {
			
			if( typeof current_table.required_table != 'undefined' ) {
				required_table = current_table.required_table;
			} else {
				required_table = methods.current_table;
			}
			
			postData.filter[postData.filter.length] = { 
				key: current_table.required_key, 
				value: methods.current_data[current_table.required_key], 
				table : required_table 
				} ;
		}
		
		if( typeof list_start !== 'undefined' ) {
			postData.list_start = list_start;
		}
		
		methods.ajax( postData, ajaxURL, methods.data_list );
	},
	data_list : function( data ) {
		var tbody = $('#list-view-'+data.table+' #dataTables tbody').empty(),
		current_table = methods.tables[ data.table ];
		for( i in data.results ) {
			var item = data.results[i], id = item[current_table.primary_key]; 
			
			if( $("#"+data.table+'-list-item-'+id).length != 0) { continue; }
			
			var tr = $('<tr />').attr('id', data.table+'-list-item-'+id).appendTo( tbody );
			
			if( typeof current_table.active_key != 'undefined' && current_table.active_key != '') {
				var active_column = $('<td />').appendTo( tr );
				var active_checkbox = $('<input />').prop('type', 'checkbox').appendTo( active_column );
				active_checkbox.prop('checked', true );
				active_checkbox.attr('data-id', id);
				active_checkbox.attr('data-table', data.table);
				active_checkbox.click(methods.activate_item);
				if( item[current_table.active_key] != 1 ) {
					tr.addClass('danger');
					active_checkbox.prop('checked', false );
				}
			}
			
			for( key in current_table.list_fields ) {
				var field = current_table.list_fields[key];
				var item_value = item[field];
				
				if( typeof current_table.filters != 'undefined' && typeof current_table.filters[field] != 'undefined' ) {
					var item_value = item[current_table.filters[field].value];
				}
				
				if((typeof current_table.check_cross != 'undefined') && ( $.inArray(field, current_table.check_cross) > -1 )) {
					if ( item_value == '1' ) {
						item_value = $('<span />').addClass('glyphicon glyphicon-ok text-center').css('display', 'block');
					}
					else if ( item_value == '0' ) {
						item_value = $('<span />').addClass('glyphicon glyphicon-remove text-center').css('display', 'block');
					}
				} 
				
					var span = $('<span />');
						span.addClass('list_item');
						span.attr('data-key', field);
						span.attr('data-value', item[field]);
						span.attr('data-table', data.table);
						
					item_value = span.html( item_value );
				
					if( typeof current_table.filters != 'undefined' && typeof current_table.filters[field] != 'undefined' && !!current_table.filters[field].anchor) {
						
						var link = $('<a />').attr('href', 'javascript:void(0);');
						link.addClass('list_filter');
						link.addClass('list-filter-'+field);
						link.addClass('list-filter-'+field+'-'+item[field]);
						link.attr('title', 'Add Filter');
						link.attr('data-key', field)
						link.attr('data-value', item[field]);
						link.attr('data-table', data.table);
						link.click( methods.add_list_filter );
						
						item_value = link.html( item_value );
						
					} 
					
					if( typeof current_table.actual_values != 'undefined' && typeof current_table.actual_values[field] != 'undefined' ) {
						span.text( item[current_table.actual_values[field]] );
					} else {
						if( typeof methods.filters_data != 'undefined' && typeof current_table.filters != 'undefined' && typeof current_table.filters[field] != 'undefined') {
							
							if(	current_table.filters[field].type == 'table') {
								if( typeof methods.filters_data[current_table.filters[field].table] != 'undefined' && typeof methods.filters_data[current_table.filters[field].table][item[field]] != 'undefined' ) { 
									span.text( methods.filters_data[current_table.filters[field].table][item[field]][current_table.filters[field].value] );
								}
							}
							else if( current_table.filters[field].type == 'manual' ) { 
								if( typeof methods.filters_data[field][item[field]] !== 'undefined') {
									span.text( methods.filters_data[field][item[field]] );
								} else {
									span.text( item[field] );
								}
							}
						}
					}
					
				var span_wrapper = $('<span />').addClass( field ).html( item_value ),
				td = $('<td />').html( span_wrapper ).appendTo( tr );
			}
			
			if( current_table.actions_edit == 1 || current_table.actions_delete == 1) {
			
				var actions_td = $('<td />').appendTo( tr );
				
				if( current_table.actions_edit == 1 ) {
					var edit_link = $('<a />').attr('href', 'javascript:void(0);');
					edit_link.addClass('btn btn-success btn-xs pull-left');
					edit_link.attr('data-id', id);
					edit_link.attr('data-primary_key', current_table.primary_key);
					edit_link.attr('data-table', data.table);
					edit_link.attr('id', data.table+"-edit-button-"+id);
					edit_link.text("Edit");
					edit_link.css('width', '49px');
					edit_link.prependTo(actions_td);
					edit_link.click(function(){
						var postData = {action : 'get'}, 
						id = $(this).attr('data-id'),
						primary_key = $(this).attr('data-primary_key'),
						table = $(this).attr('data-table');
						$('#edit-view-'+table+' .nav-tabs a.tab-item').attr('data-key', primary_key).attr('data-value', id);
						$('.update-back-'+table).attr('data-table', table);
						postData[primary_key] = id;
						methods.currently_updating = id;
						methods.ajax(postData, methods.baseURL+'index.php/'+table+'/ajax', methods.edit_form);
					});
					
					if( current_table.actions_edit == 1 && current_table.actions_delete == 1) {
						$('<span> &nbsp; </span>').insertAfter( edit_link );
					}
				}

				if( current_table.actions_delete == 1 ) {
					var delete_link = $('<a />').attr('href', 'javascript:void(0);');
					delete_link.addClass('btn btn-danger btn-xs pull-right');
					delete_link.attr('data-id', id);
					delete_link.attr('data-primary_key', current_table.primary_key);
					delete_link.attr('data-table', data.table);
					delete_link.attr('id', data.table +"-delete-button-"+id);
					delete_link.text("Delete");
					delete_link.css('width', '49px');
					delete_link.appendTo(actions_td);
					delete_link.click(function(){
						var id = $(this).attr('data-id'),
						primary_key = $(this).attr('data-primary_key'),
						table = $(this).attr('data-table');
						if( confirm('Are you sure?') ) {
							methods.remove_data(table, primary_key, id );
						}
					});
				}
			}
			
		}

		methods.list_pagination(data.total_items, data.limit, data.start, data.table);
		methods.lock_page(true);
	},
	activate_item : function(){
			var id=$(this).attr('data-id'),table=$(this).attr('data-table');
			var postData = {action : 'update_field'}, 
			current_table = methods.tables[table],
			ajaxURL = methods.baseURL+'index.php/'+table+'/ajax';
			postData[current_table.primary_key] = id;
			methods.id = id;
				if( $(this).prop('checked') ) {
					postData['field'] = current_table.active_key;
					postData['value'] = 1
					methods.ajax( postData, ajaxURL, function(msg){
						methods.show_alert(true, msg);
						$("#"+table+'-list-item-'+id).removeClass('danger');
						methods.lock_page(true);
					});
				} else {
					postData['field'] = current_table.active_key;
					postData['value'] = 0
					methods.ajax( postData, ajaxURL, function(msg){
						methods.show_alert(true, msg);
						$("#"+table+'-list-item-'+id).addClass('danger');
						methods.lock_page(true);
					});
				}
	},
	filter_variables : function(key, value, remove, removeByKey, replace, table, filter_table) {
		
		if( typeof table == 'undefined' ) table = methods.current_table;
		if( typeof methods.list_filters == 'undefined' ) {
			methods.list_filters = [];
		}
		if( typeof methods.list_filters[table] == 'undefined' ) {
			methods.list_filters[table] = [];
		}
		
		var list_filters = methods.list_filters[table];
		
		filter_table = typeof filter_table !== 'undefined' ? filter_table : table;
		remove = typeof remove !== 'undefined' ? Boolean(remove) : false;
		removeByKey = typeof removeByKey !== 'undefined' ? Boolean(removeByKey) : false;
		replace = typeof replace !== 'undefined' ? Boolean(replace) : false;
		
		var returnVar = { added : false, removed : false, exists : false, keyExists : false };
		var exists = false, keyExists = false;
		var sameKeys = [];
		
		for( i in list_filters ) {
			if(list_filters[i].key == key && list_filters[i].value == value && list_filters[i].table == filter_table) {
				var exactIndex = parseInt(i);
				exists = true;
				returnVar['exists'] = true;
			}
			if(list_filters[i].key == key) {
				var keyIndex = parseInt(i);
				sameKeys.push( parseInt(i) );
				keyExists = true;
				returnVar['keyExists'] = true;
			}
		}
		
		if( keyExists === true && removeByKey === true ) {
			for( i in sameKeys ) {
				list_filters.splice( sameKeys[i], 1 );
			}
			methods.list_filters[table] = list_filters;
			returnVar['removed'] = true;
			return returnVar;
		}

		if( exists === true && remove === true ) {
			list_filters.splice( exactIndex, 1 );
			methods.list_filters[table] = list_filters;
			returnVar['removed'] = true;
			return returnVar;
		}
		
		if( keyExists === true && replace === true ) {
			list_filters[keyIndex].value = value;
			methods.list_filters[table] = list_filters;
			returnVar['replaced'] = true;
			return returnVar;
		}
		
		if( keyExists === false && remove === false ) {
			list_filters[ list_filters.length ] = { key: key, value: value, table: filter_table };
			methods.list_filters[table] = list_filters;
			returnVar['added'] = true;
			return returnVar;
		}
		
		return returnVar;
	},
	isKeyInListFilters : function(key, list_filters) {
		for( i in list_filters ) {
			if(list_filters[i].key == key) {
				return true;
			}
		}
		return false;
	},
	isFilterExists : function(key, value, list_filters) {
		for( i in list_filters ) {
			if(list_filters[i].key == key && list_filters[i].value == value) {
				return true;
			}
		}
		return false;
	},
	filter_container : function( table ) {
		if( $('#filter-container-'+table).length == 0 ) {
			return $('<div />').attr('id', 'filter-container-'+table).addClass('filter-container pull-left').css({position : 'absolute', width : '84%'}).prependTo('#list-view-'+table+' .panel-heading');
		} else {
			return $('#filter-container-'+table).addClass('filter-container');
		}
	},
	add_list_search : function() {
		var title = $(this).attr('title'),
		key = $(this).attr('data-key'),
		linked = $(this).attr('data-linked'),
		table = $(this).attr('data-table'),
		filter_container = methods.filter_container( table );
		if( $('#list_search_'+key).length == 0 ) {
			$(this).css('visibility', 'hidden');
			var span1 = $('<span />').addClass('col-md-3 pull-left').attr('id', 'list_search_'+key).appendTo(filter_container);
			var span2 = $('<span />').addClass('input-group input-group-sm').appendTo(span1);
			var input = $('<input />').attr('type', 'text').addClass('form-control input-value').appendTo(span2);
			if( typeof linked != 'undefined' ) {
				input.attr('data-linked', linked);
			}
			input.attr('data-table', table);
			input.attr('data-key', key);
			input.attr('data-refresh', 0);
			input.focus();
			input.attr('placeholder', title);
			input.keyup(function(e) {
				var code = e.which;
				if(code==13)e.preventDefault();
				if(code==32||code==13||code==188||code==186){
					methods.submit_list_search( $(this) );
				} 
				if(e.keyCode==27) {
					methods.remove_list_search( $(this) )
				}
			});
			var searchButton = $('<span />').addClass('input-group-addon btn btn-success btn-sm btn-submit').appendTo(span2);
			if( typeof linked != 'undefined' ) {
				searchButton.attr('data-linked', linked);
			}
			searchButton.attr('data-table', table);
			searchButton.attr('data-key', key);
			searchButton.click(methods.submit_list_search);
			var searchIcon = $('<i />').addClass('fa fa-search').appendTo( searchButton );
			var closeButton = $('<span />').addClass('input-group-addon btn btn-warning btn-sm btn-remove').appendTo(span2);
			if( typeof linked != 'undefined' ) {
				closeButton.attr('data-linked', linked);
			}
			closeButton.attr('data-table', table);
			closeButton.attr('data-key', key);
			closeButton.attr('data-refresh', 0);
			closeButton.click(methods.remove_list_search);
			var closeIcon = $('<i />').addClass('fa fa-times').appendTo( closeButton );
		}
	},
	
	submit_list_search : function( evt ) {
		if (evt.type == 'click') {
			var linked = $(this).attr('data-linked'),
			table = $(this).attr('data-table'),
			key = $(this).attr('data-key'),
			value = $('#list_search_'+key+' .input-value').val();
		} else {
			var table = evt.attr('data-table'),
			linked = evt.attr('data-linked'),
			key = evt.attr('data-key'),
			value = evt.val();
		}
		
		if( typeof methods.tables[table].filters != 'undefined' && typeof methods.tables[table].filters[key] != 'undefined' ) {
			key = methods.tables[table].filters[key].value;
		}

		if( value !== '') {
			$('#list_search_'+key+' .input-value').attr('data-refresh', 1);
			$('#list_search_'+key+' .btn-remove').attr('data-refresh', 1);
			var result = methods.filter_variables( key + " LIKE", "%" + value + "%", false, false, true, table, linked );
			methods.list_start[table] = 0;
			methods.init_list(table);
		}
	},
	
	remove_list_search : function(evt) {
		if (evt.type == 'click') {
			var linked = $(this).attr('data-linked'),
			table = $(this).attr('data-table'),
			key = $(this).attr('data-key'),
			refresh = $(this).attr('data-refresh'),
			value = $('#list_search_'+key+' .input-value').val();
		} else {
			var linked = evt.attr('data-linked'),
			table = evt.attr('data-table'),
			key = evt.attr('data-key'),
			refresh = evt.attr('data-refresh'),
			value = evt.val();
		}
		$('#list_search_button_'+key).css('visibility', 'visible');
		$('#list_search_'+key).remove();
		methods.filter_variables( key + " LIKE", "%" + value + "%", true, true, false, table, linked );
		if( refresh==1 ) {
			methods.init_list(table);
		}
	},
	add_list_filter : function() {
		var key = $(this).attr('data-key'), 
		value = $(this).attr('data-value'), 
		table = $(this).attr('data-table'), 
		filter_container = methods.filter_container( table );
		
		var result = methods.filter_variables( key , value, false, false, true, table );
		
		if( result['added'] ) {
			var clone = $(this).clone().addClass( key ).attr('title', 'Remove Filter').click( methods.remove_list_filter ).appendTo( filter_container );
			$('<i class="fa fa-times"></i>').css('margin-left', '3px').appendTo(clone);
			methods.list_start[table] = 0;
			methods.init_list(table);
		}
		else if( result['replaced'] && !result['exists'] ) {
			var current = $('#filter-container-'+table+' .list_filter.'+key).remove();
			var clone = $(this).clone().addClass( key ).attr('title', 'Remove Filter').click( methods.remove_list_filter ).appendTo( filter_container );
			$('<i class="fa fa-times"></i>').css('margin-left', '3px').appendTo( clone );
			if( current.attr('data-value') != value) {
				methods.list_start[table] = 0;
				methods.init_list( table );
			}
		}
	},
	remove_list_filter : function() {
		var key = $(this).attr('data-key'), 
		value = $(this).attr('data-value'), 
		table = $(this).attr('data-table');
		var result = methods.filter_variables( key, value, true, false, false, table );
		if( result['removed'] ) {
			methods.init_list( table );
		}
		$(this).remove();
	},
	init_filters : function( table ) {
		if( typeof table == 'undefined' ) { table = methods.current_table; }
		if( typeof methods.tables[table].filters != 'undefined' ) {
			if(typeof methods.filters_data == 'undefined') { methods.filters_data = {}; }
			for( field in methods.tables[table].filters ) {
				if( methods.tables[table].filters[field].type == 'table' ) {
					methods.get_filters( field, methods.tables[table].filters[field] );
				}
			}
		}
	},
	get_filters : function( field, filter ) {
		var postData = {action:'list', filter: []};
		if (typeof filter.filter != 'undefined' && filter.filter == 1 ) {
			postData.filter[ postData.filter.length ] = {key: filter.filter_key, value: filter.filter_value, table: filter.table };
		}
		methods.ajax(postData, methods.baseURL+'index.php/'+filter.table+'/ajax', function(data){
			methods.data_filters( field, filter, data );
		});
	},
	data_filters : function( field, filter, data ) {
		var filter_items = [];
		for( i in data.results ) {
			var item = data.results[i];
			filter_items[item[filter.key]] = data.results[i]; 
		}
		
		keys = [filter.table, field];
		
		if( filter.filter_key != '' ) {
			keys.push(filter.filter_key);
		}
		if( filter.filter_value != '' ) {
			keys.push(filter.filter_value);
		}
		
		methods._add_value(field, methods.filters_data, filter_items);
				
		methods.apply_filters(field, filter, filter_items);
		
	},
	apply_filters : function(field, filter, filter_data) {
		for(i in filter_data) {
			$('.list-filter-'+field+'-'+filter_data[i][filter.key]+' .list_item').text(filter_data[i][filter.value]);
		}
	},
	filters_to_dropdown : function(dropdown_input, refresh) {
		refresh = typeof refresh == 'undefined' ? true : Boolean(refresh);
		dropdown_input.each(function(){
			var table = $(this).attr('data-table'),
			field = $(this).attr('data-field'),
			key = $(this).attr('data-key'),
			value = $(this).attr('data-value'),
			label = $(this).attr('data-label');

			if (typeof table !== 'undefined') {
				$(this).html('<option value="" disabled="disabled" selected="selected">- - Select '+label+'- -</option>');
				if( typeof methods.filters_data != 'undefined' && typeof methods.filters_data[field] != 'undefined' ) {
					for( i in methods.filters_data[field] ) {
						var option = $('<option />');
						option.attr('value', methods.filters_data[field][i][key]);
						option.text(methods.filters_data[field][i][value]);
						option.appendTo( $(this) );
					}
				}
			} else {
				$(this).selectpicker({dropupAuto : true }).selectpicker('render');
			}
		});
		if( refresh ) {
			dropdown_input.selectpicker({dropupAuto : true }).selectpicker('render');
		}
	},
	filters_to_checkbox : function( table, filters_data, checkbox_table, selected, addprefix ) { 
		addprefix = typeof addprefix !== 'undefined' ? 'add' : 'edit';
		checkbox_table.each(function() {
			var datatype = $(this).attr('data-type'), 
			datakey = $(this).attr('data-key'),
			datavalue = $(this).attr('data-value'),
			field = $(this).attr('data-field');
			this_table = $(this).attr('data-table');
			
			$(this).html('').empty();
			
			for( i in filters_data[field] ) {
				var divdata = $('<div />').addClass( datatype );
				var label = $('<label />');
				var inputbox = $('<input />');
				inputbox.attr('type', datatype);
				inputbox.attr('name', addprefix + '_'+table+'_'+field);
				inputbox.addClass( addprefix + '_'+table+'_'+field );
				inputbox.attr('value', filters_data[field][i][datakey]);
				if( selected == i ) {
					inputbox.prop('checked', true);
				}
				label.text( filters_data[field][i][datavalue] );
				inputbox.prependTo( label );
				label.appendTo( divdata );
				divdata.appendTo( $(this) );
			}
			
		});
	},
	filters_to_radio : function( table, filters_data, radio_table, selected, addprefix ) { 
		addprefix = typeof addprefix !== 'undefined' ? 'add' : 'edit';
		radio_table.each(function() {
			var datatype = $(this).attr('data-type'), 
			datakey = $(this).attr('data-key'),
			datavalue = $(this).attr('data-value'),
			field = $(this).attr('data-field');
			$(this).html('').empty();
			for( i in filters_data[field] ) {
				var divdata = $('<div />').addClass( datatype );
				var label = $('<label />');
				var inputbox = $('<input />');
				inputbox.attr('type', datatype);
				inputbox.attr('name', addprefix + '_'+table+'_'+field);
				inputbox.addClass( addprefix + '_'+table+'_'+field );
				inputbox.attr('value', filters_data[field][i][datakey]);
				if( selected == i ) {
					inputbox.prop('checked', true);
				}
				label.text( filters_data[field][i][datavalue] );
				inputbox.prependTo( label );
				label.appendTo( divdata );
				divdata.appendTo( $(this) );
			}
		});
	},
	dropdown_filters : function() {
		var filter = $(this).attr('data-filter'), 
		dTable =  $(this).attr('data-table'),
		current_table = methods.tables[dTable],
		filter_type = current_table.filters[filter].type,
		text_field = current_table.filters[filter].value,
		value_field = current_table.filters[filter].key,
		filter_table = current_table.filters[filter].table;

		if (filter_type == 'table' ) {
			var data_filters = methods.filters_data[ filter ];
		} else {
			var data_filters = methods.filters_data[ filter ];
		}
		
		$('.dropdown-filter-list').each(function() {
			if( $(this).attr('id') != 'dropdown-filter-'+dTable+'-'+ filter ) {
				$(this).css('display', 'none');
			}
		});
		$('.dropdown-filter .glyphicon').removeClass('glyphicon-chevron-up').addClass('glyphicon-chevron-down');
		var dropdown_filter = $('#dropdown-filter-'+dTable+'-'+filter);
		if( dropdown_filter.length == 0 ) {
			var timeout;
			var html = $('<div />').addClass('dropdown-filter-list').attr('id', 'dropdown-filter-'+dTable+'-'+filter).css({
				display : 'block',
				position : 'absolute',
				border : '1px solid #000',
				padding : '10px',
				backgroundColor : '#FFF',
				boxShadow: '2px 2px 2px #999'
				}).hover(function(){
					clearTimeout( timeout );
				},function(){
				timeout = setTimeout(function(){
					$('#dropdown-filter-'+dTable+'-'+filter).css('display', 'none');
				$('.glyphicon.glyphicon-chevron-up.'+dTable+'-'+filter).removeClass('glyphicon-chevron-up').addClass('glyphicon-chevron-down');
					},1000);
				});
			var list = $('<ul />').css({
				listStyle : 'none',
				padding: 0,
				margin: 0
				});
			for (i in data_filters) {
				if(filter_type == 'table') {
					var link_value = data_filters[i][value_field], link_text = data_filters[i][text_field];
				} else {
					var link_value = i, link_text = data_filters[i];
				}
				
				var link = $('<a />').attr('href', 'javascript:void(0);');
						link.addClass('list_filter ' + filter);
						link.attr('title', 'Add Filter');
						link.attr('data-key', filter);
						link.attr('data-value', link_value);
						link.attr('data-table', dTable);
						link.html( link_text );
						link.click( methods.add_list_filter );
				list.append( $('<li />').append(link) )
			}
			
			html.append( list )
			$(this).parent('div').append( html );
			$(this).children('.glyphicon').removeClass('glyphicon-chevron-down').addClass('glyphicon-chevron-up '+dTable+'-'+ filter);
		} else {
			if( dropdown_filter.css('display') == 'none' ) {
				dropdown_filter.css('display', 'block');
				$(this).children('.glyphicon').removeClass('glyphicon-chevron-down').addClass('glyphicon-chevron-up');
			} else {
				dropdown_filter.css('display', 'none');
				$(this).children('.glyphicon').removeClass('glyphicon-chevron-up').addClass('glyphicon-chevron-down '+dTable+'-'+ filter);
			}
		}
	},
	text_searchable_update : function(dkey, item) {
		
		var self = $('.edit.text-searchable.'+dkey);
		var field = self.attr('data-field'),
		table = self.attr('data-table'),
		key = self.attr('data-key'),
		value = self.attr('data-value'), 
		display = self.attr('data-display');
		
		self.show()
		$('.edit.text-searchable.selectedItem.br.'+dkey).remove();
		$('.edit.text-searchable.selectedItem.'+dkey).remove();
		
		if ( typeof item[display] != 'undefined' && item[display] != null && item[display] != 'null' && item[display] != '' && item[display] != 0 && item[display] != '0' ) {
			self.hide();
			
				var selectedItem = $('<div />').attr('id', 'text-searchable-item-'+field+'-'+dkey).addClass(field).addClass('edit text-searchable selectedItem btn btn-default btn-md btn-block').attr('data-field', field).html( item[display] + ' <i class="fa fa-times"></i>'  ).insertAfter( self ).click(function(){
					var field = $(this).attr('data-field');
					$('.edit.text-searchable.'+field).show().val('').focus();
					$('.edit.text-searchable-key-'+field).val('');
					$('.edit.text-searchable.selectedItem.br.'+field).remove();
					$(this).remove();
				}); 
			
			$('<br />').addClass('edit text-searchable selectedItem br '+field).insertAfter( self );
			return item[display];
		}
	},
	text_searchable_reset : function() {
		$('.text-searchable').show();
		$('.text-searchable-key').val('');
		$('.text-searchable.selectedItem.br').remove();
		$('.text-searchable.selectedItem').remove();
	},
	text_searchable : function() {
		var timeout, 
		list_container = function(self, field, table_name, table_key, table_value) {
			if( $('#text-searchable-result-list-'+table_name).length == 0 ) {
				var panel = $('<div />').addClass('panel panel-default').attr('id', 'text-searchable-result-'+table_name).insertAfter( self );
				panel.css({
					position: 'absolute',
					width: self.css('width'),
					overflow: 'hidden', 
					zIndex : 9999
				}).hover(function(){
					if (typeof methods.searchable_timeout != 'undefined') {
						clearTimeout(methods.searchable_timeout);
					}
				}, function(){
					var self = $(this);
					methods.searchable_timeout = setTimeout(function(){
						self.remove();
					}, 1000);
				});
				var panelBody = $('<div />').addClass('panel-body').appendTo( panel )
				list = $('<div />').addClass('list-group');
				list.attr('id', 'text-searchable-result-list-'+table_name);
				list.attr('data-field', field);
				list.attr('data-table', table_name);
				list.attr('data-key', table_key);
				list.attr('data-value', table_value);
				list.appendTo( panelBody );
				return list;
			} else {
				return $('#text-searchable-result-list-'+table_name);
			}
		}
		display_search = function(self, searchKey) {
			var field = self.attr('data-field'), 
			table_name = self.attr('data-table'),
			table_key = self.attr('data-key'),
			table_value = self.attr('data-value'),
			action = self.attr('data-action');
			var list = list_container(self, field, table_name, table_key, table_value );
			methods.ajax({action:'list', filter:[{key: table_value+" LIKE", value: "%"+searchKey+"%", table : table_name}]}, methods.baseURL+'index.php/'+table_name+'/ajax', function(data){
				var list = $('#text-searchable-result-list-' + data.table).empty();
				var field = list.attr('data-field'),
						table_key = list.attr('data-key'),
						table_value = list.attr('data-value');
						if (data.update == true) {
							action = 'edit';
						}else if( data.added == true) {
							action = 'add';
						}
				if( data.results.length > 0) {
					for (i in data.results) {
						var listItem = $('<a />').addClass('list-group-item').appendTo( list ).prop('href', 'javascript:void(0);').attr('data-field', field).attr('data-table', data.table).attr('data-action', action);
						var listText = $('<span />').addClass('value').text( data.results[i][table_value] ).prependTo( listItem );
						var itemId = $('<span />').addClass('key badge').prependTo( listItem ).text( data.results[i][table_key] );
						listItem.click(function() {
							var field = $(this).attr('data-field'), 
							table_name = $(this).attr('data-table'), 
							action = $(this).attr('data-action');
							
							var textbox = $('.'+action+'.text-searchable.'+field);
							$('.'+action+'.text-searchable-key-'+field).val( $(this).children('.key').text() );
							textbox.val( $(this).children('.value').text() );
							textbox.hide();
							
							var selectedItem = $('<div />').attr('id', 'text-searchable-item-'+field+'-'+$(this).children('.key').text()).addClass(action+' text-searchable selectedItem btn btn-default btn-md btn-block').addClass(field).attr('data-field', field).attr('data-action', action).html( textbox.val() + ' <i class="fa fa-times"></i>'  ).insertAfter( textbox ).click(function(){
								var field = $(this).attr('data-field'), action = list.attr('data-action');
								$('.text-searchable.'+field).show().val('').focus();
								$('.text-searchable-key-'+field).val('');
								$('.text-searchable.selectedItem.br.'+field).remove();
								$(this).remove();

							}); 
							
							$('<br />').addClass(action+' text-searchable selectedItem br '+field).insertAfter( textbox );
							
							$('#text-searchable-result-'+table_name).remove();
							methods.text_searchable_selected = $(this).children('.value').text();
						});
					}
					count_label = (data.results.length > 1) ? data.results.length + ' items found!' : data.results.length + ' item found!';
					if( $('#text-searchable-count').length == 0 ) {
						$('<span />').attr('id', 'text-searchable-count').text( count_label).insertAfter( list );
					} else {
						$('#text-searchable-count').text( count_label );
					}
				} else {
					list.html('<strong><em>No Match Found!</em></strong>');
				}
				methods.lock_page( true );
				});
		},
		search = function(self) {
			var key = self.val();
			if(key.length > 2) {
				display_search( self, key.trim() ); 
			}
		};
		$('.text-searchable').keyup(function(){
			var self = $(this);
			clearTimeout(timeout);
			timeout = setTimeout(function(){
				search(self);
			}, 1000);
		}).click(function(){
			search( $(this) );
		});
		
		$('.text-searchable-list').click(function(){
			var field = $(this).attr('data-field'), 
			table_name = $(this).attr('data-table'),
			table_key = $(this).attr('data-key'),
			table_value = $(this).attr('data-value'),
			action = $(this).attr('data-action'),
			target_id = $(this).attr('data-target');
				
			(function(table_name, field, table_key, table_value, target_id, action) { 
				var results = $(target_id + ' .modal-body');
				if( results.children('#'+action+'-modal-results-list-'+table_name+'-'+field).length == 0 ) {
					var results_list = $('<div />').attr('id', action+'-modal-results-list-'+table_name+'-'+field).appendTo( results );
					results_list.attr('data-field', field);
					results_list.attr('data-key', table_key);
					results_list.attr('data-value', table_value);
					results_list.attr('data-table', table_name);
					results_list.attr('data-target', target_id);
					results_list.attr('data-action', action);
				}
			})(table_name, field, table_key, table_value, target_id, action);
			
			var modal_callback = function( data, action, field ){
				var list = $('#'+action+'-modal-results-list-'+data.table+'-'+field).empty();
				var field = list.attr('data-field'),
						table_key = list.attr('data-key'),
						table_value = list.attr('data-value'),
						table_name = list.attr('data-table'),
						target_id = list.attr('data-target');
						
				if( data.results.length > 0) {
					for (i in data.results) {
						var listItem = $('<a />').addClass('list-group-item').appendTo( list ).prop('href', 'javascript:void(0);').attr('data-field', field).attr('data-table', data.table).attr('data-action', action);
						var listText = $('<span />').addClass('value').text( data.results[i][table_value] ).prependTo( listItem );
						var itemId = $('<span />').addClass('key badge').prependTo( listItem ).text( data.results[i][table_key] );
						listItem.click(function() {
							var field = $(this).attr('data-field'), 
							table_name = $(this).attr('data-table'), 
							action = list.attr('data-action');
							
							$('.'+action+'.text-searchable.selectedItem.br.'+field).remove();
							$('.'+action+'.text-searchable.selectedItem.'+field).remove();
							
							$('.'+action+'.text-searchable-key-'+field).val( $(this).children('.key').text() );
							var textbox = $('.'+action+'.text-searchable.'+field);
							textbox.val( $(this).children('.value').text() );
							textbox.hide();
							$(target_id).modal('hide');
							
							
							
							var selectedItem = $('<div />').addClass(action).addClass('text-searchable selectedItem btn btn-default btn-md btn-block').addClass(field).attr('data-field', field).html( textbox.val() + ' <i class="fa fa-times"></i>'  ).insertAfter( textbox ).click(function(){
								var field = $(this).attr('data-field'), action = list.attr('data-action');
								$('.'+action+'.text-searchable.'+field).show().val('').focus();
								$('.'+action+'.text-searchable-key-'+field).val('');
								$('.'+action+'.text-searchable.selectedItem.br.'+field).remove();
								$(this).remove();
							}); 
							
							$('<br />').addClass(action).addClass('text-searchable selectedItem br').addClass(field).insertAfter( textbox );
							
							methods.text_searchable_selected = $(this).children('.value').text();
							
						});
					}
				} else {
					list.html('<strong><em>No Match Found!</em></strong>');
				}
				
				var pages = Math.ceil( data.total_items / 10 );
				if( pages > 1 ) {
					var modal_box = $(target_id+' .modal-dialog .modal-content');
					if( modal_box.children('.modal-footer').length == 0 ) {
						var modal_footer = $('<div />').addClass('modal-footer').appendTo( modal_box );
					} else {
						var modal_footer = modal_box.children('.modal-footer');
					}
					modal_footer.empty();
					var total_items = $('<span />').addClass('pull-left small').text( data.total_items + ' items found').appendTo( modal_footer );
					var pagination = $('<ul />').addClass('pagination pagination-sm').appendTo( modal_footer );
					for(i=0;i<pages;i++) {
						var pageitem = $('<li>').appendTo( pagination );
						var pageItemLink = $('<a>').attr('data-start', i * 10).text( i + 1 ).prop('href', 'javascript:void(0);').appendTo(pageitem);
						if( data.start == (i * 10) ) {
							pageitem.addClass('active');
							pageItemLink.prop('disabled', true);
						} else {
							pageItemLink.click(function(){
								var start = $(this).attr('data-start');
								methods.ajax({ action:'list', list_start: start,  limit : 10 }, methods.baseURL+'index.php/'+table_name+'/ajax', function(data){
									modal_callback(data, action, field);
								});
							});
						}
					}
				}
				methods.lock_page( true );
			};
			methods.ajax({ action:'list', limit : 10 }, methods.baseURL+'index.php/'+table_name+'/ajax', function(data){
				modal_callback( data, action, field );
			});
		});
	},
	extra_action_fields : function(table, field, remove) {
		
		remove = (typeof remove == 'undefined') ? false : remove;
		
		if( typeof methods.tables[table]['extra_action_fields'] == 'undefined' ) {
			methods.tables[table]['extra_action_fields'] = []
		}
		
		var existing = methods.tables[table]['extra_action_fields'],
		exists = false, existsId = null;
		for ( i in existing ) {
			if( existing[i] == field) {
				exists = true;
				existsId = i;
			}
		}
		if( remove == true ) {
			methods.tables[table]['extra_action_fields'].splice( existsId, 1 );
			return;
		}
		if( exists == false ) {
			methods.tables[table]['extra_action_fields'].push( field );
		}
	},
	change_deliberately_reset : function( table ) {
		methods.tables[table]['extra_action_fields'] = [];
		$('.change-deliberately.button').css('display', 'block');
		$('.change-deliberately.form').css('display', 'none');
		$('.change-deliberately.cancel').remove();
	},
	change_deliberately : function() {
		var table = $(this).attr('data-table'), fields = $(this).attr('data-field').split("|");
		field = fields[0];
		for( i in fields ) {
			methods.extra_action_fields(table, fields[i], false);
		}
		var formBox = $('.change-deliberately.form.'+table+'.'+field);
		var buttonBox = $('.change-deliberately.button.'+table+'.'+field);
		var cancelBtn = $('.change-deliberately.form .cancel.'+table+'.'+field);
		
		buttonBox.slideUp(function(){
			formBox.slideDown();
			cancelBtn.unbind('click.change_deliberately_cancel');
			cancelBtn.bind('click.change_deliberately_cancel', function(){
				var table = $(this).attr('data-table'), field = $(this).attr('data-field');
				methods.extra_action_fields(table, field, true);
				var formBox = $('.change-deliberately.form.'+table+'.'+field);
				var buttonBox = $('.change-deliberately.button.'+table+'.'+field);
				var cancelBtn = $('.change-deliberately.cancel.'+table+'.'+field);
				formBox.slideUp(function(){
					buttonBox.slideDown();
				});
			});
		});
	},
	_add_value : function(keys, data, value) {
		if( typeof keys == 'object') {
			
		} else {
			data[keys] = value;
		}
	},
	_get_value : function(list, data) {
		var _value = data;
		if( typeof list == 'object') {
			for(i in list) {
				if( typeof _value[list[i]] == 'undefined' ) {
					return false;
				} else {
					_value = _value[list[i]];
				}
			}
		} else {
			_value = _value[list];
		}
		return _value;
	},
	ajax : function( data, ajax_url, callback ) {
		methods.lock_page(); 
		$.ajax({
                url: ajax_url,
                type: 'post',
                data: data,
                dataType : 'json',                
                success: function(data, status) { 
					console.log( data );
					if( data.error ) {
						methods.data_error( data );
						return;
					} else if( data.not_logged_in ) {
						window.location = methods.baseURL+'index.php/login';
						return;
					} else {
						if( typeof callback === 'function') {
							callback( data, methods );
						}
						return;
					}
                },
                error: function(xhr, desc, err) {
                  console.log(xhr);
                  console.log("Details: " + desc + "\\nError:" + err);
                }
            });
        console.log( data );
        console.log( ajax_url );
	},
	upload_file : function(table, group, key) {
		
		var imageCont = $('#files-to-upload-'+table+'-'+group+'-'+key);
		var buttons = imageCont.children('.buttons').addClass('progress').empty().css({
			width : '100px',
			display: 'block',
			marginLeft: ((parseInt(imageCont.css('width')) - 100) / 2) + 'px',
			marginTop: '65px',
			});
			
		imageCont.children('.pending').remove();
		
		var progressBar = $('<div />').addClass('progress-bar progress-bar-success');
		progressBar.attr('role', 'progressbar');
		progressBar.attr('aria-valuenow', '0');
		progressBar.attr('aria-valuemin', '0');
		progressBar.attr('aria-valuemax', '100');
		progressBar.attr('id', 'progressbar-'+table+'-'+group+'-'+key);
		progressBar.css('width', '0%');
		progressBar.appendTo( buttons );
		
		var sr_only = $('<span />').addClass('sr-only').text('0% Complete (success)').appendTo(progressBar);
		
		var formData = new FormData();
		formData.append('action', 'upload');
		formData.append('key', key);
		formData.append('group', group);
		formData.append('userfile', methods.selected_files[group][key]);
		
		$.ajax({
			xhr: function()
			  {
				var xhr = new window.XMLHttpRequest();
				//Upload progress
				xhr.upload.addEventListener("progress", function(evt){
				  if (evt.lengthComputable) {
					var percentComplete = evt.loaded / evt.total;
					//Do something with upload progress
					//console.log(percentComplete);
					progressBar.attr('aria-valuenow', (percentComplete*100));
					progressBar.css('width', (percentComplete*100)+'%');
				  }
				}, false);
				//Download progress
				xhr.addEventListener("progress", function(evt){
				  if (evt.lengthComputable) {
					var percentComplete = evt.loaded / evt.total;
					//Do something with download progress
					//console.log(percentComplete);
					progressBar.attr('aria-valuenow', (percentComplete*100));
					progressBar.css('width', (percentComplete*100)+'%');
				  }
				}, false);
				return xhr;
			  },
			url: methods.baseURL+'index.php/'+table+'/ajax',
			type: 'POST',
			data:  formData ,
			cache: false,
			dataType: 'json',
			processData: false, // Don't process the files
			contentType: false, // Set content type to false as jQuery will tell the server its a query string request
			success: function(data, textStatus, jqXHR)
			{
				
				var item = $('#files-to-upload-'+data.table+'-'+data.group+'-'+data.key);
				item.children('.progress').remove();
				if( data.error ) {
					item.addClass('files-to-upload-error');
					item.attr('data-html', 'true').attr('title', data.message).tooltip('hide');
				} else {
					var upload_settings = methods.tables[data.table].upload_settings;
					methods.data_added( data, false );
					item.addClass('files-to-upload-success');
					var filePreviewLink = $('<a />').addClass('btn btn-info').prependTo( item );
					filePreviewLink.attr('href', upload_settings.upload_url+data.upload_data.file_name);
					filePreviewLink.css({
					position: 'absolute',
					marginTop: ((parseInt(item.css('height')) - 40) / 2) + 'px',
					marginLeft: ((parseInt(item.css('width')) - 40) / 2) + 'px',
					});
					filePreviewLink.fancybox({
						padding: 0,

						openEffect : 'elastic',
						openSpeed  : 150,

						closeEffect : 'elastic',
						closeSpeed  : 150,

						closeClick : true,

						helpers : {
							overlay : null
						}
					});
					var filePreviewIcon = $('<span />').addClass('glyphicon glyphicon-search').appendTo( filePreviewLink );
				}
				
				item.removeClass('files-to-upload files-to-upload-'+data.table);
				item.addClass('uploaded-file uploaded-file-'+table);
				
				(function(group, key) {
						for (i in methods.selected_files_to_upload) {
							if( methods.selected_files_to_upload[i] == group+'-'+key ) {
								methods.selected_files_to_upload.splice(i, 1);
							}
						}
				})(data.group, data.key);
				
				if( methods.file_uploading_started == true ) {
					methods.file_uploading_next( data.table );
				}
			},
			error: function(xhr, desc, err) {
				console.log(xhr);
				console.log("Details: " + desc + "\\nError:" + err);
            }
		});
	},
	file_upload : function(table) {
		$('#input-file-'+table).on('change', function(evt){
			if( typeof methods.selected_files == 'undefined') {
				methods.selected_files = [];
			}
			if( typeof methods.upload_added_files_count == 'undefined') {
				methods.upload_added_files_count = 0;
			} else {
				methods.upload_added_files_count++;
			}
			if( typeof methods.selected_files_to_upload == 'undefined') {
				methods.selected_files_to_upload = [];
			}
			methods.selected_files.push(event.target.files);
			
			var checkImage = function(iType, iSize) {
				var upload_settings = methods.tables[table].upload_settings;
				var allowed_types = upload_settings.allowed_types.split(',');
				var max_size = parseInt(upload_settings.max_size);
				var result = false;
				var errorMsg = '';
				
				if(iType != '') {
					var MyType = iType.split('/');
					if (typeof MyType[1] != 'undefined') {
						if( $.inArray(MyType[1], allowed_types) > -1 ) {
							result = false;
							errorMsg = '';
						} else {
							result = true;
							errorMsg += 'Filetype not allowed!\n';
						}
					} else {
						result = true;
						errorMsg += 'Filetype not allowed!\n';
					}
				} else {
					result = true;
					errorMsg += 'Filetype not allowed!\n';
				}
				
				if ( iSize > max_size ) {
					result = true;
					errorMsg += 'Exceeds max size!';
				}
				return {error : result, msg : errorMsg};
			}
			
			var preview = $('#fileupload-'+table+'-preview'); //.empty();

			$.each(event.target.files, function(key, value) {
				
				var fileThumb = $('<div />').addClass('col-sm-6 col-md-3').appendTo(preview);
				var fileThumb_link = $('<div />').attr('id', 'files-to-upload-'+table+'-'+methods.upload_added_files_count+'-'+key);
				fileThumb_link.addClass('thumbnail files-to-upload').appendTo(fileThumb);
				fileThumb_link.css({height :'150px', overflow: 'hidden' });
				fileThumb_link.attr('title', value.name);
				fileThumb_link.attr('data-key', key);
								
				var iconsCont = $('<div />').addClass('buttons').appendTo( fileThumb_link );
				var marginLeft = (parseInt(iconsCont.css('width')) - 40) / 2;
				iconsCont.css({
					position : 'absolute',
					marginTop : '56px',
					marginLeft : marginLeft + 'px',
					display : 'none',
					});
				var fileUploadLink = $('<a />').addClass('btn btn-success');
				var fileUploadIcon = $('<span />').addClass('glyphicon glyphicon-upload').appendTo(fileUploadLink);
				
				var fileRemoveLink = $('<a />').addClass('btn btn-danger btn-xs files-to-upload-remove').appendTo( fileThumb_link );
				fileRemoveLink.css({
					top : '-10px',
					right : '10px',
					position: 'absolute',
					borderRadius: '20px',
					display: 'none',
					});
				var fileRemoveIcon = $('<span />').addClass('glyphicon glyphicon-remove').appendTo(fileRemoveLink);
				
				fileUploadLink.attr('data-group', methods.upload_added_files_count);
				fileUploadLink.attr('data-key', key);
				fileUploadLink.attr('data-table', table);
				
				fileRemoveLink.attr('data-key', key);
				fileRemoveLink.attr('data-group', methods.upload_added_files_count);
				fileRemoveLink.attr('data-table', table);
				
				errorCheck = checkImage( value.type, value.size );
				if( errorCheck.error == true ) {
					fileThumb_link.addClass('files-to-upload-error');
					iconsCont.css('margin-left', ((parseInt(fileThumb_link.css('width')) - 40) / 2) + 'px');
					fileThumb_link.attr('title', errorCheck.msg).tooltip('hide');
				} else {
					methods.selected_files_to_upload.push( methods.upload_added_files_count+'-'+key);
					fileThumb_link.addClass('files-to-upload-'+table);
					fileUploadLink.prependTo( iconsCont );
				}
				
				var remove_item_to_upload = function(group, key) {
						for (i in methods.selected_files_to_upload) {
							if( methods.selected_files_to_upload[i] == group+'-'+key ) {
								methods.selected_files_to_upload.splice(i, 1);
							}
						}
					};
				fileRemoveLink.on('click', function(){
					var group = $(this).attr('data-group'), key = $(this).attr('data-key'), table = $(this).attr('data-table');
					$('#files-to-upload-'+table+'-'+group+'-'+key).parent().remove();
					remove_item_to_upload(group, key);
				});
				
				fileUploadLink.on('click', function(){
					var group = $(this).attr('data-group'), key = $(this).attr('data-key'), table = $(this).attr('data-table');
					methods.upload_file(table, group, key);
					remove_item_to_upload(group, key);
				});
				
				var fileThumb_img = $('<img />').appendTo(fileThumb_link);
				var fileReader = new FileReader();
				fileReader.onload = function (e) {
					fileThumb_img.attr('src', e.target.result);
				}
				fileReader.readAsDataURL( value );
				
			});
			$(this).replaceWith($(this).val('').clone(true));
		});
		$('#upload-action-'+table).on('click', function() {
			var table = $(this).attr('data-table');
			var items = $('.files-to-upload-'+table);
			var loading = $('<img />').attr('src', methods.baseURL+'assets/images/ajax-loader3.gif').addClass('pending');
			loading.appendTo(items).css({
				position: 'absolute',
				top : '70px',
				marginLeft : ((parseInt(items.css('width')) - 16) / 2) + 'px'
			});
			if( methods.selected_files_to_upload.length > 0) {
				methods.file_uploading_started = true;
				methods.file_uploading_next( table );
				$(this).prop('disabled', true);
			}
		});
	},
	file_uploading_next : function( table ) {
		if (typeof methods.selected_files_to_upload[0] != 'undefined' ) {
			var groupKey = methods.selected_files_to_upload[0].split('-');
			methods.upload_file(table, groupKey[0], groupKey[1]);
			methods.selected_files_to_upload.splice(0,1);
			if( methods.selected_files_to_upload.length == 0) {
				methods.file_uploading_started = false;
				$('#upload-action-'+table).prop('disabled', false);
			}
		}
	},
	setupHandlers : function( table ) {
		if( typeof table == 'undefined' ) table = methods.current_table;
		if( $('#list-view-'+table).length > 0 ) {
			var current_table = methods.tables[table];
			$('#add-button-'+table).prop('href', 'javascript:void(0)').click(function() {
				methods.filters_to_dropdown( $('#add-view-'+table+' select.dropdown') );
				methods.filters_to_checkbox( table, methods.filters_data, $('#add-view-'+table+' div.checkbox-table'), '', 1);
				methods.filters_to_radio( table, methods.filters_data, $('#add-view-'+table+' div.radio-table'), '', 1);
				
				$('#list-view-'+table).slideUp('slow', function(){
					$('#add-view-'+table).slideDown();
				});
			});
			$('#add-back-'+table).prop('href', 'javascript:void(0)').click(function() {
				$('#add-view-'+table).slideUp('slow', function(){
					$('#list-view-'+table).slideDown();
				});
			});
			$('#add-action-'+table).click(function() {
				methods.add_data( table );
			});
			$('.update-back-'+table).prop('href', 'javascript:void(0)').click(function() {
				table = $(this).attr('data-table');
				$('#edit-view-'+table).slideUp('slow', function(){
					$('#edit-view-'+table+' .nav.nav-tabs li.active').removeClass('active').attr('data-refresh', 0);
					$('#edit-view-'+table+' .nav.nav-tabs li.parent-tab').addClass('active');
					$('#edit-view-'+table+' .tab-content.active').removeClass('active').css('display', 'none');
					$('#edit-view-'+table+' .tab-content.parent').addClass('active').css('display', 'block');
					$('#edit-view-'+table+' .nav.nav-tabs li a').attr('data-refresh', 0);
					$('#list-view-'+table).slideDown();
					methods.change_deliberately_reset( table );
					
					var page_header = $('.page-header');
					if( page_header.children('.label').length == 1 ) {
						var page_header_child = page_header.children('.label'),
						page_header_table = page_header_child.attr('data-table');
						if( page_header_table == table ) {
							page_header_child.remove();
						}
					}
					
				});
			});	
			$('#update-action-'+table).click(function() {
				var table = $(this).attr('data-table'), id = $(this).attr('data-id');
				methods.update_data( table, id );
			});
			$('.list-search-'+table).click(methods.add_list_search);
			$('#list-view-'+table+' .dropdown-filter a').click(methods.dropdown_filters);
			$('#edit-view-'+table+' .nav.nav-tabs a.tab-item').click(function() {
				if( $(this).parent('li').hasClass('active') == false ) {
					var table = $(this).attr('data-table'), 
					child = $(this).attr('data-child'), 
					refresh = $(this).attr('data-refresh');
					$('#edit-view-'+methods.current_table+' .nav.nav-tabs li.active').removeClass('active');
					$('#edit-view-'+methods.current_table+' .tab-content.active').slideUp('slow', function(){
						$('#edit-view-'+methods.current_table+' .tab-content-'+table).slideDown().addClass('active');
					}).removeClass('active');
					
					if( child == 1 && refresh != 1) {
						methods.init_list( table );
						methods.init_filters( table );
						$(this).attr('data-refresh', 1);
					}
					
					$(this).parent('li').addClass('active');
				}
			});
			$('#upload-back-'+table).prop('href', 'javascript:void(0)').click(function() {
				$('#add-view-'+table).slideUp('slow', function(){
					$('#list-view-'+table).slideDown();
					$('#fileupload-'+table+'-preview').remove();
					delete methods.selected_files;
					
				});
			});
			methods.file_upload( table );
		}
	},
	load : function( options ) {
		if( typeof options === 'object' ) {
			for( i in options ) {
				methods[i] = options[i];
			}
		}
		methods.init_list( methods.current_table );
		methods.init_filters( methods.current_table );
		for(table in methods.tables) {
			methods.setupHandlers( table );
		}
		methods.text_searchable();
	}
};
    $.fn.AdminActions = function(methodOrOptions) {
        if ( methods[methodOrOptions] ) {
            return methods[ methodOrOptions ].apply( this, Array.prototype.slice.call( arguments, 1 ));
        } else if ( typeof methodOrOptions === 'object' || ! methodOrOptions ) {
            // Default to "init"
            return methods.load.apply( this, arguments );
        } else {
            $.error( 'Method ' +  methodOrOptions + ' does not exist on jQuery.tooltip' );
        }    
    };
})(jQuery);
