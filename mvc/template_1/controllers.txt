<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class {class_name} extends MY_Controller {

    function __construct() {
        parent::__construct();

        $this->load->library('template_data');
        $this->lang->load('{table_name2}');
        $this->load->model( array('{class_name}_model') );
        
        $this->template_data->set('main_page', '{parent_menu}' ); 
        $this->template_data->set('sub_page', '{table_name2}' ); 
        $this->template_data->set('page_title', '{table_title}' ); 

    }
    
    public function index()
	{
        $this->load->view('{view_filename}', $this->template_data->get() );
	}
	
	public function ajax($required=NULL, $value=NULL) {

		if(  ! $this->template_data->get('admin_access')->controller_{table_name2} ) {
			{controller_authorization}
		}
		
		switch($this->input->post('action')) {
			case 'list':
				$list_limit = ( $this->input->post('limit') != '') ? (int) $this->input->post('limit') : 20;
				$order_by = ( $this->input->post('order_by') != '') ? $this->input->post('order_by') : '{primary_field}';
				$order_sort = ( $this->input->post('order_sort') != '') ? $this->input->post('order_sort') : 'desc';
				$list_start = ( $this->input->post('list_start') != '') ? $this->input->post('list_start') : 0;
				
				$list = new $this->{class_name}_model;
				$pagination = new $this->{class_name}_model;
				
{list_join_statements}				$pagination->setSelect('COUNT(*) as total_items');
{pagination_join_statements}
				
				if( is_array($this->input->post('filter')) && count($this->input->post('filter')) > 0 ) {
					foreach( $this->input->post('filter') as $filter ) {
						$list->setFilter($filter['key'],$filter['value'],$filter['table']);
						$pagination->setFilter($filter['key'],$filter['value'],$filter['table']);
					}
				}
				
				$list->setOrder($order_by, $order_sort);
				$list->setStart($list_start);
				$list->setLimit($list_limit);
				
				echo json_encode( array(
							'table' => '{table_name2}',
							'total_items' => (int) $pagination->get()->total_items,
							'start' => $list_start,
							'limit' => $list_limit,
							'error' => false,
							'list' => true,
							'results' => $list->populate()
						) );
				exit;
			break;
			case 'get':
				$item = $this->{class_name}_model;
				$item->set{primary_field_function}( $this->input->post('{primary_field}'), true );

{item_join_statements}				echo json_encode( array(
							'id' => $this->input->post('{primary_field}'),
							'table' => '{table_name2}',
							'error' => false,
							'get' => true,
							'results' => $item->get()
						) );
						exit;
			break;
			case 'add':
				echo json_encode($this->submission('add'));
				exit;
			break;
			case 'edit':
				echo json_encode($this->submission('edit'));
				exit;
			break;
			case 'delete':
				$results = array(
							'id' => $this->input->post('{primary_field}'),
							'table' => '{table_name2}',
							'error' => true,
							'removed' => false,
							'message' => 'Unable to delete!'
						);
				$this->{class_name}_model->set{primary_field_function}( $this->input->post('{primary_field}') );
				if( $this->{class_name}_model->deleteBy{primary_field_function}() ) {
					$results['error'] = false;
					$results['removed'] = true;
					$results['message'] = 'Successfully Deleted!';
				}
				echo json_encode( $results );
				exit;
			break;
		}
		echo 0;
		exit;
	}
	
	private function submission($action) {
	    $this->load->library('form_validation');
	    $results = array(
			'error' => true,
			'message' => 'No Response!',
			'table' => '{table_name2}',
	    );

		if( $action == 'add' ) {{add_rules}
		}
		elseif( $action == 'edit' ) {{edit_rules}
		}

		if ($this->form_validation->run() == FALSE)
		{
			if( $this->input->post() ) {
				$this->template_data->alert( validation_errors(), 'danger');
				$results['message'] = validation_errors();
			}
		} 
		else 
		{
			$container = new $this->{class_name}_model;
{submission_fields}{slug_check}
			if( $action == 'add' ) { 
				if( $container->insert() ) {
					$this->template_data->alert( 'Successfully Added!', 'success');
					$results['added'] = true;
					$results['error'] = false;
					$results['message'] = 'Successfully Added!';
				} else {
					$results['added'] = false;
					$results['error'] = true;
					$results['message'] = 'Unable to add data!';
				}
			} 
			elseif( $action == 'edit' ) { 
				if( $container->updateBy{primary_field_function}() ) {
					$this->template_data->alert( 'Successfully Updated!', 'success');
					$results['updated'] = true;
					$results['error'] = false;
					$results['message'] = 'Successfully Updated!';
				} else {
					$results['updated'] = false;
					$results['error'] = true;
					$results['message'] = 'Unable to update data!';
				}
			}
			{container_join_statements}
			$results['id'] = $container->get{primary_field_function}();
			$results['results'] = $container->getBy{primary_field_function}();
		}

	    return $results;
	}
	
}
/* End of file {table_name2}.php */
/* Location: ./application/controllers/{table_name2}.php */
