import base64
from os import walk

def create_file(filename, content):
	target = open (filename, 'w')
	target.write( content )
	target.close();

f = []
for (dirpath, dirnames, filenames) in walk("."):
	f.extend(filenames)
	break
content = ''
for file in f:
	if file[-3:] in ['gif','png','jpg']:
		with open(file, "rb") as image_file:
			encoded_string = base64.b64encode(image_file.read())
			varname = file.replace('.','_').replace('-','_') 
			content += varname + " = \""
			content += encoded_string
			content += "\"\n\n"
			create_file("image_resources.py", content)
			print("Encoded Image: "+ file)
