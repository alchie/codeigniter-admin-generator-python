#!/usr/bin/python
import os
from gui.tk import *

WINDOW_OPENED = False

configFile = None
filename = None
if os.path.isfile(".config"):
	configFile = open( ".config", "r" )
	filename = configFile.read()
	if not os.path.exists( os.path.join('save', filename.strip() ) ):
		filename = ''

if __name__ == "__main__":
    app = AppMain()
    app.display(filename)
