import os
import sqlite, mysql
from lib import unzip, create_file
import views, controllers_admin, controllers_api, langs, configs, menu
import modelsv9 as models

class Generate:
        model_version = models.Model.version
	mvc_settings = None
	table_settings = None
	content = None
	dbname = None
	tblname = None
	fields = None
	filename = None
	generate_models = False
	generate_views = False
	generate_controllers = False
	generate_langs = False
	
	def __init__(self, filename, db, table):
		self.filename = filename
		self.dbname = db
		self.tblname = table
		self.mvc_settings = sqlite.mvcSettings(filename, db)
		self.menu_settings = sqlite.menuSettings(filename, db)
		self.table_settings = sqlite.tableSettings(filename, db, table)
		print(" - - - - - - - - - - - " + table + " - - - - - - - - - - - ")

	def set_generate(self, models=False, views=False, controllers=False, langs=False):
		self.generate_models = models
		self.generate_views = views
		self.generate_controllers = controllers
		self.generate_langs = langs
		
	def table_files(self):
		filename = self.filename
		db = self.dbname
		table = self.tblname
		tblname = self.tblname
		saveDir = self.mvc_settings.get_value('directory')
		table_prefix = self.table_settings.get_value('table_prefix')
		
		mvc_folder = self.mvc_settings.get_value('mvc_directory')
		app_folder = self.mvc_settings.get_value('application_directory')
		ci_version = self.mvc_settings.get_value('ci_version')
		ci_output = self.mvc_settings.get_value('ci_output')
		
		withPrefix = False
		if table_prefix != '' and table_prefix != None:
			tblname = tblname[len(table_prefix):]
			withPrefix = True
		
		if ci_version == '3':
			tblname = tblname.capitalize()
			
		if saveDir != '' and saveDir != None:
			
			mvcDir = os.path.join(saveDir,mvc_folder)
			appDir = os.path.join(mvcDir,app_folder)
			
			if not os.path.exists(mvcDir):
				os.makedirs(mvcDir)
				print("Folder Created: " + mvcDir)
			
			if not os.path.exists(appDir):
				os.makedirs(appDir)
				print("Folder Created: " + appDir)
				
			if self.generate_models:
				# create model
				if not os.path.exists( os.path.join(appDir, "models") ):
                                        os.makedirs(os.path.join(appDir, "models"))
                                        
				modelFile = os.path.join(appDir, "models", tblname + "_model.php" )
				modelContent = models.Model(filename, db, table)
				
				#if os.path.exists( modelFile ):
					#os.remove( modelFile )
					#print("File Removed: " + modelFile)
				
				if( withPrefix ):
					modelFile2 = os.path.join(appDir, "models", table + "_model.php" )
					if os.path.exists( modelFile2 ):
						os.remove( modelFile2 )
						print("File Removed: " + modelFile2)
						
				create_file( modelFile, modelContent.get_content() )
				#if self.table_settings.get_value('generate_model') == '1':
					#create_file( modelFile, modelContent.get_content() )
					#print("File Created: " + modelFile)
			
			if self.generate_views:
				# create view
				if not os.path.exists( os.path.join(appDir, "views") ):
                                        os.makedirs( os.path.join(appDir, "views") )
                                        
				viewFile = os.path.join( appDir , "views" , tblname + ".php" )
				viewContent = views.View(filename, db, table)
				
				#if os.path.exists( viewFile ):
					#os.remove( viewFile )
					#print("File Removed: " + viewFile)
				
				if( withPrefix ):
					viewFile2 = os.path.join( appDir , "views" , table + ".php" )
					if os.path.exists( viewFile2 ):
						#os.remove( viewFile2 )
						print("File Removed: " + viewFile2)
						
				if self.table_settings.get_value('generate_view') == '1':
					create_file( viewFile, viewContent.get() )
					print("File Created: " + viewFile)
			
			if self.generate_controllers:
				# create controller
				if not os.path.exists( os.path.join(appDir, "controllers") ):
                                        os.makedirs(os.path.join(appDir, "controllers"))
                                        
				controllerFile = os.path.join( appDir, "controllers", tblname + ".php" )
                                if ci_output == 'api':
                                        controllerContent = controllers_api.ControllerAPI(filename, db, table)
                                else:
                                        controllerContent = controllers_admin.ControllerAdmin(filename, db, table)
				
				#if os.path.exists( controllerFile ):
					#os.remove( controllerFile )
					#print("File Removed: " + controllerFile)
				
				if( withPrefix ):
					controllerFile2 = os.path.join( appDir, "controllers", table + ".php" )
					if os.path.exists( controllerFile2 ):
						#os.remove( controllerFile2 )
						print("File Removed: " + controllerFile2)
				
				if self.table_settings.get_value('generate_controller') == '1':
					create_file( controllerFile, controllerContent.get_content() )
					print("File Created: " + controllerFile)
				
			if self.generate_langs:
				# create language
				if not os.path.exists( os.path.join(appDir, "language") ):
                                        os.makedirs(os.path.join(appDir, "language"))

                                if not os.path.exists( os.path.join(appDir, "language", "english") ):
                                        os.makedirs(os.path.join(appDir, "language", "english"))
                                        
				langFile = os.path.join( appDir , "language", "english" , tblname + "_lang.php" )
				langContent = langs.Lang(filename, db, table)
				
				#if os.path.exists( langFile ):
					#os.remove( langFile )
					#print("File Removed: " + langFile)
				
				if( withPrefix ):
					langFile2 = os.path.join( appDir , "language", "english" , table + "_lang.php" )
					if os.path.exists( langFile2 ):
						#os.remove( langFile2 )
						print("File Removed: " + langFile2)
						
				if self.table_settings.get_value('generate_lang') == '1':
					create_file( langFile, langContent.get() )
					print("File Created: " + langFile)
