import sqlite3, os

def _fetchone( filename, sql ):
	connection = sqlite3.connect('save/' + filename)
	cursor = connection.cursor()  
	cursor.execute(sql)
	data = cursor.fetchone()
	connection.commit()
	connection.close()
	return data

def _fetchall( filename, sql ):
	connection = sqlite3.connect('save/' + filename)
	cursor = connection.cursor()  
	cursor.execute(sql)
	data = cursor.fetchall()
	connection.commit()
	connection.close()
	return data
	
def _query( filename, sql ):
	connection = sqlite3.connect('save/' + filename)
	cursor = connection.cursor()  
	cursor.execute(sql)
	connection.commit()
	connection.close()
	
class Settings:
		
	def __init__(self, filename):
		if filename == None or filename == '':
			return
		self.filename = filename  
	
	def get_value(self, key):
		sql = 'SELECT * FROM `settings` WHERE key="'+key+'";'
		data = _fetchone(self.filename, sql)
		if data != None:
			return data[1]
	
	def add_value(self, key, value):
		sql = 'INSERT INTO `settings` VALUES ( "' + key + '", "' + value + '");'
		_query(self.filename, sql)
	
	def update_value(self, key, value):
		sql = 'UPDATE `settings` SET value= "' + value + '" WHERE key="'+key+'";'
		_query(self.filename, sql)
	
	def insert(self, key, value):
		if self.get_value( key ) == None:
			self.add_value(key, value)
		else:
			self.update_value(key, value)

class mvcSettings:
	
	filename = None
	dbname = None
	title = None
	author = None
	directory = None
	
	def __init__(self, filename, db):
		
		if filename == None or filename == '':
			return
		self.filename = filename
		self.dbname = db 
	
	def get_value(self, key, default=None):
		sql = 'SELECT * FROM `mvc_settings` WHERE db="' + self.dbname+ '" and key="'+key+'"'
		data = _fetchone(self.filename, sql)
		if data != None:
			return data[2]
		else:
			return default
	
	def add_value(self, key, value):
		sql = 'INSERT INTO `mvc_settings` VALUES ( "' + self.dbname+ '", "' + key + '", "' + value + '");'
		_query(self.filename, sql)
	
	def update_value(self, key, value):
		sql = 'UPDATE `mvc_settings` SET value= "' + value + '" WHERE db= "' + self.dbname+ '" and key="'+key+'";'
		_query(self.filename, sql)
	
	def insert(self, key, value):
		if self.get_value( key ) == None:
			self.add_value(key, value)
		else:
			self.update_value(key, value)


class tableSettings:
	
	filename = None
	dbname = None
	tblname = None
	title = None
	singular = None
	plural = None
	parent = None
	reqfilter = None
	orderby = None
	orderorientation = 'ASC'
	limit = '20'
	
	def __init__(self, filename, db, tbl=''):

		if filename == None or filename == '':
			return
			
		if tbl != '':
			self.tblname = tbl
		self.filename = filename
		self.dbname = db
			   
	def get_value(self, key, default=None):
		sql = 'SELECT * FROM `table_settings` WHERE db="' + self.dbname+ '" and tbl="'+self.tblname+'" and key="'+key+'"'
		data = _fetchone(self.filename , sql)
		if data != None:
			return data[3]
		else:
			return default
			
	def add_value(self, key, value):
		sql = 'INSERT INTO `table_settings` VALUES ( "' + self.dbname+ '", "' + self.tblname+ '", "' + key + '", "' + value + '");'
		_query(self.filename, sql)

	
	def update_value(self, key, value):
		sql = 'UPDATE `table_settings` SET value= "' + value + '" WHERE db= "' + self.dbname+ '" and tbl="'+self.tblname+'" and key="'+key+'";'
		_query(self.filename, sql)
		
	def insert(self, key, value):
		if self.get_value( key ) == None:
			self.add_value(key, value)
		else:
			self.update_value(key, value)

	def get_table(self, key, value):
		sql = 'SELECT * FROM `table_settings` WHERE db="' + self.dbname + '" '
		sql += 'AND key="' + str(key) + '" AND value="'+ str(value) + '";'
		data = _fetchall(self.filename, sql)
		if data != None:
			return data
			
	def get_keys(self):
		sql = 'SELECT * FROM `table_settings` WHERE db="' + self.dbname+ '" and tbl="'+self.tblname+'"'
		data = _fetchall(self.filename, sql)
		if data != None:
			return data
			
class fieldSettings:
	
	filename = None
	dbname = None
	tblname = None
	fldname = None
	
	def __init__(self, filename, db, tbl, fld):
		
		if filename == None or filename == '':
			return
		
		self.filename = filename
		self.dbname = db
		self.tblname = tbl
		self.fldname = fld
			   
	def get_value(self, key):
		sql = 'SELECT * FROM `field_settings` WHERE db="' + self.dbname+ '" and tbl="'+ self.tblname +'" and fld="'+ self.fldname +'" and key="'+key+'"'
		data = _fetchone(self.filename, sql)
		if data != None:
			return data[4]
	
	def add_value(self, key, value):
		sql = 'INSERT INTO `field_settings` VALUES ( "' + self.dbname+ '", "' + self.tblname+ '", "' + self.fldname + '", "' + key + '", "' + value + '");'
		_query(self.filename, sql)
	
	def update_value(self, key, value):
		sql = 'UPDATE `field_settings` SET value= "' + value + '" WHERE db= "' + self.dbname+ '" and tbl="'+self.tblname+'" and fld="'+self.fldname+'" and key="'+key+'";'
		_query(self.filename, sql)

	def insert(self, key, value):
		if self.get_value( key ) == None:
			self.add_value(key, value)
		else:
			self.update_value(key, value)

	def get_keys(self):
		sql = 'SELECT * FROM `field_settings` WHERE db="' + self.dbname+ '" and tbl="'+ self.tblname +'" and fld="'+ self.fldname +'"'
		data = _fetchall(self.filename, sql)
		if data != None:
			return data
	
class menuSettings:
	
	filename = None
	dbname = None
	tblname = None
	fldname = None
	
	def __init__(self, filename, db):
		
		if filename == None or filename == '':
			return
		
		self.filename = filename
		self.dbname = db
	  
	def get_value(self, item, key):
		sql = 'SELECT * FROM `menu_settings` WHERE db="' + self.dbname+ '" and id="'+ item +'" and key="'+key+'"'
		data = _fetchone(self.filename, sql)
		if data != None:
			return data[3]
	
	def add_value(self, item, key, value):
		sql = 'INSERT INTO `menu_settings` VALUES ( "' + self.dbname+ '", "' + item + '", "' + key + '", "' + value + '");'
		_query(self.filename, sql)
	
	def update_value(self, item, key, value):
		sql = 'UPDATE `menu_settings` SET value= "' + value + '" WHERE db= "' + self.dbname+ '" and id ="'+item+'" and key="'+key+'";'
		_query(self.filename, sql)
		
	def insert(self, item, key, value):
		if self.get_value( item, key ) == None:
			self.add_value(item, key, value)
		else:
			self.update_value(item, key, value)
			
	def delete_menu(self, item):
		sql = 'DELETE FROM `menu_settings` WHERE db= "' + self.dbname+ '" and id ="'+item+'";'
		_query(self.filename, sql)
		
	def menulist(self):
		sql = 'SELECT * FROM `menu_settings` WHERE db="' + self.dbname+ '" and  key="label"'
		data = _fetchall(self.filename, sql)
		if data != None:
			return data

	def parentlist(self):
		sql = 'SELECT *, (SELECT `value` FROM  `menu_settings` sub WHERE sub.key="index" AND sub.id = main.id) AS `order1` FROM `menu_settings` main WHERE main.key = "parent" AND main.value="1" AND main.db="' + self.dbname+ '" ORDER BY order1 ASC;'
		data = _fetchall(self.filename, sql)
		if data != None:
			return data

	def childrenlist(self, item):
		sql = 'SELECT *, (SELECT `value` FROM  `menu_settings` sub WHERE sub.key="parent_menu" AND sub.id = main.id) AS `parent_menu`, (SELECT `value` FROM  `menu_settings` sub WHERE sub.key="index" AND sub.id = main.id) AS `order1` FROM `menu_settings` main WHERE main.key = "parent" AND main.value="0" AND main.db="' + self.dbname + '" and `parent_menu` = "' + item + '" ORDER BY order1 ASC;'
		data = _fetchall(self.filename, sql)
		if data != None:
			return data
				
class fieldItems:
	
	filename = None
	dbname = None
	tblname = None
	fldname = None
	
	def __init__(self, filename, db, tbl, fld, input_type):
		
		if filename == None or filename == '':
			return
		self.filename = filename
		self.dbname = db
		self.tblname = tbl
		self.fldname = fld
		self.input_type = input_type
	  
	def get_value(self, item, key):
		sql = 'SELECT * FROM `field_items` WHERE db="'+ self.dbname +'" and tbl="'+ self.tblname +'" and fld="' + self.fldname+ '" and itype="' + self.input_type+ '" and item="'+item+'" and key="'+key+'";'
		data = _fetchone(self.filename, sql)
		if data != None:
			return data[6]
	
	def add_value(self, item, key, value):
		sql = 'INSERT INTO `field_items` VALUES ( "' + self.dbname+ '", "' + self.tblname+ '", "' + self.fldname+ '", "'+ self.input_type +'", "' + item + '", "' + key + '", "' + value + '");'
		_query(self.filename, sql)
	
	def update_value(self, item, key, value):
		sql = 'UPDATE `field_items` SET value= "' + value + '" WHERE db= "' + self.dbname+ '" and tbl="' + self.tblname+ '" and fld= "' + self.fldname+ '" and itype="' + self.input_type+ '" and item="'+item+'" and key="'+key+'";'
		_query(self.filename, sql)
		
	def insert(self, item, key, value):
		if self.get_value( item, key ) == None:
			self.add_value(item, key, value)
		else:
			self.update_value(item, key, value)
			
	def delete_item(self, item):
		sql = 'DELETE FROM `field_items` WHERE db= "' + self.dbname+ '" and tbl="' + self.tblname+ '" and fld= "' + self.fldname+ '" and itype="' + self.input_type+ '" and item ="'+item+'";'
		_query(self.filename, sql)
		
	def item_list(self, by="label", order="`value` ASC"):
		sql = 'SELECT * FROM `field_items` WHERE db= "' + self.dbname+ '" and tbl= "' + self.tblname+ '" and fld= "' + self.fldname+ '" and itype="' + self.input_type+ '" and key="'+by+'" ORDER BY '+order+';'
		data = _fetchall(self.filename, sql)
		if data != None:
			return data

class fieldValidations:
	
	filename = None
	dbname = None
	tblname = None
	fldname = None
	
	def __init__(self, filename, db, tbl, fld):

		if filename == None or filename == '':
			return
		self.filename = filename
		self.dbname = db
		self.tblname = tbl
		self.fldname = fld

	  
	def get_value(self, key):
		sql = 'SELECT `add`, `edit`, `option` FROM `field_validations` WHERE db="'+ self.dbname +'" and tbl="'+ self.tblname +'" and fld="' + self.fldname+ '" and key="'+key+'";'
		data = _fetchone(self.filename, sql)
		if data != None:
			return data
	
	def add_value(self, key, add='0', edit='0', options="", order='0'):
		sql = 'INSERT INTO `field_validations` VALUES ( "' + self.dbname+ '", "' + self.tblname+ '", "' + self.fldname+ '", "'+ key +'", "' + add + '", "' + edit + '", "' + options + '", "' + order + '");'
		_query(self.filename, sql)
	
	def update_value(self, key, add='0', edit='0', options="", order='0'):
		sql = 'UPDATE `field_validations` SET `add`="' + add + '", `edit`="' + edit + '", `option`="' + options + '", `order`="'+order+'" WHERE db= "' + self.dbname+ '" and tbl="' + self.tblname+ '" and fld= "' + self.fldname+ '" and key="'+key+'";'
		_query(self.filename, sql)
		
	def insert(self, key, add='0', edit='0', options="", order='0'):
		if self.get_value( key ) == None:
			self.add_value(key, add, edit, options, order)
		else:
			if add == '0' and edit == '0':
				self.delete_value( key )
				return True
			else:
				self.update_value(key, add, edit, options, order)
		return False
		
	def get_add_validations(self):
		sql = 'SELECT `key`, `option` FROM `field_validations` WHERE db="'+ self.dbname +'" and tbl="'+ self.tblname +'" and fld="' + self.fldname+ '" and `add`="1" ORDER BY `order` ASC;'
		data = _fetchall(self.filename, sql)
		if data != None:
			return data

	def get_edit_validations(self):
		sql = 'SELECT `key`, `option` FROM `field_validations` WHERE db="'+ self.dbname +'" and tbl="'+ self.tblname +'" and fld="' + self.fldname+ '" and `edit`="1" ORDER BY `order` ASC;'
		data = _fetchall(self.filename, sql)
		if data != None:
			return data

	def get_all(self):
		sql = 'SELECT `key` FROM `field_validations` WHERE db="'+ self.dbname +'" and tbl="'+ self.tblname +'" and fld="' + self.fldname+ '";'
		data = _fetchall(self.filename, sql)
		if data != None:
			return [str(key[0]) for key in data]
			
	def delete_value(self, key):
		sql = 'DELETE FROM `field_validations` WHERE db= "' + self.dbname+ '" and tbl="' + self.tblname+ '" and fld= "' + self.fldname+ '" and key="' + key+ '";'
		_query(self.filename, sql)

class tableChildren:
	
	filename = None
	dbname = None
	tblname = None
		
	def __init__(self, filename, db, table):
		
		if filename == None or filename == '':
			return
		self.filename = filename
		self.dbname = db
		self.tblname = table
	  
	def get_value(self, child, key):
		sql = 'SELECT * FROM `table_children` WHERE db="' + self.dbname+ '" and tbl="' + self.tblname + '" and child="'+ child +'" and key="'+key+'"'
		data = _fetchone(self.filename, sql)
		if data != None:
			return data[4]
	
	def add_value(self, child, key, value):
		sql = 'INSERT INTO `table_children` VALUES ( "' + self.dbname+ '", "' + self.tblname + '", "' + child + '", "' + key + '", "' + value + '");'
		_query(self.filename, sql)
	
	def update_value(self, child, key, value):
		sql = 'UPDATE `table_children` SET value= "' + value + '" WHERE db= "' + self.dbname+ '" and tbl= "' + self.tblname+ '" and child ="'+child+'" and key="'+key+'";'
		_query(self.filename, sql)
		
	def insert(self, child, key, value):
		if self.get_value( child, key ) == None:
			self.add_value(child, key, value)
		else:
			self.update_value(child, key, value)

	def delete_item(self, child):
		sql = 'DELETE FROM `table_children` WHERE db= "' + self.dbname+ '" and tbl="' + self.tblname+ '" and child="' + child + '";'
		_query(self.filename, sql)
		
	def item_list(self, key="child_key"):
		sql = 'SELECT *, (SELECT `value` FROM `table_children` sub WHERE sub.key = "order" AND sub.db="%(db)s" AND sub.tbl="%(tbl)s" AND sub.child = main.child) AS `index1` FROM `table_children` main WHERE main.db="%(db)s" and main.tbl="%(tbl)s" and main.key="%(key)s" ORDER BY index1 ASC;' % {
			'db' : self.dbname,
			'tbl' : self.tblname,
			'key' : key
		}
		data = _fetchall(self.filename, sql)
		if data != None:
			return data

class createNewSaveFile:
	
	connection = None
	cursor = None
	
	def __init__(self, filename):
		
		if not os.path.exists('save'):
			os.makedirs('save')
			
		self.connection = sqlite3.connect( 'save/'+filename )
		self.cursor = self.connection.cursor() 
		
	def create(self):
		
		sqls = [
		'CREATE TABLE "config_files" ("db" TEXT,"app_file" TEXT,"key" TEXT,"value" TEXT);',
		'CREATE TABLE "field_items" ("db" TEXT,"tbl" TEXT,"fld" TEXT,"itype" TEXT,"item" TEXT,"key" TEXT,"value" TEXT);',
		'CREATE TABLE "field_settings" ("db" TEXT,"tbl" TEXT,"fld" TEXT,"key" TEXT,"value" TEXT);',
		'CREATE TABLE "field_validations" ("db" TEXT,"tbl" TEXT,"fld" TEXT,"key" TEXT,"add" TEXT,"edit" TEXT,"option" TEXT, "order" TEXT);',
		'CREATE TABLE "menu_settings" ("db" TEXT, "id" TEXT, "key" TEXT, "value" TEXT);',
		'CREATE TABLE "mvc_settings" ("db" TEXT,"key" TEXT,"value" TEXT);',
		'CREATE TABLE "settings" ("key" TEXT,"value" TEXT);',
		'CREATE TABLE "table_children" ("db" TEXT,"tbl" TEXT,"child" TEXT,"key" TEXT,"value" TEXT);',
		'CREATE TABLE "table_settings" ("db" TEXT,"tbl" TEXT,"key" TEXT,"value" TEXT);',
		]
		
		for sql in sqls:
			self.cursor.execute(sql)
			
		self.connection.commit()
		self.connection.close()
