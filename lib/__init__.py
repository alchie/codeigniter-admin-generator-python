import zipfile

def unzip(source_filename, dest_dir):
	with zipfile.ZipFile( source_filename , "r") as z:
		z.extractall( dest_dir )

def create_file(filename, content):
	target = open (filename, 'w')
	target.write( content )
	target.close();
