import sqlite
from lib.mysql import MySQLConnect

class Model:
	
	mvc_settings = None
	table_settings = None
	content = None
	dbname = None
	tblname = None
	fields = None
	filename = None
	
	def __init__(self, filename, db, table):
		self.dbname = db
		self.tblname = table
		self.filename = filename
		mysql = MySQLConnect( filename, db )
		self.fields = mysql.fieldList(table)
		self.mvc_settings = sqlite.mvcSettings( filename, db )
		self.table_settings = sqlite.tableSettings( filename, db, table )
		#template = self.mvc_settings.get_value('template')
		#self.content = open('mvc/'+template+'/models.txt', 'r')
	
	def get(self):
		new = ''
		for line in self.content:
			if '{table_name}' in line:
				line = line.replace('{table_name}', self.tblname)
			if '{table_name2}' in line:
				line = line.replace('{table_name2}', self.get_table_name())
			if '{class_name}' in line:
				line = line.replace('{class_name}', self.get_class_name())
			if '{fields_var}' in line:
				line = line.replace('{fields_var}', self.get_field_vars())
			if '{required_fields}' in line:
				line = line.replace('{required_fields}', self.get_required_fields())
			if '{fields_list}' in line:
				line = line.replace('{fields_list}', '"'+ '","'.join(self.fields)+'"')
			if '{fields_controller}' in line:
				line = line.replace('{fields_controller}', self.get_fields_controller())
			if '{primary_field}' in line:
				line = line.replace('{primary_field}', self.get_primary_field())
			if '{primary_field_function}' in line:
				line = line.replace('{primary_field_function}', self.get_primary_field(True))
			if '{create_table}' in line:
				line = line.replace('{create_table}', self.get_create_table())
			if '{project_name}' in line:
				line = line.replace('{project_name}', self.mvc_settings.get_value('title'))
			if '{project_website}' in line:
				line = line.replace('{project_website}', self.mvc_settings.get_value('project_website'))
			if '{author_name}' in line:
				line = line.replace('{author_name}', self.mvc_settings.get_value('author'))
			if '{author_website}' in line:
				line = line.replace('{author_website}', self.mvc_settings.get_value('author_website'))
			if '{count_field}' in line:
				line = line.replace('{count_field}', self.table_settings.get_value('count_field',''))
			
			new += line
			
		return new
	
	def get_variables(self):
		return {
			'table_name' : self.tblname,
			'table_name2' : self.get_table_name(),
			'class_name' : self.get_class_name(),
			'fields_var' : self.get_field_vars(),
			'required_fields' : self.get_required_fields(),
			'fields_list' : '"'+ '","'.join(self.fields)+'"',
			'fields_controller' : self.get_fields_controller(),
			'primary_field' : self.get_primary_field(),
			'primary_field_function' : self.get_primary_field(True),
			'create_table' : self.get_create_table(),
			'project_name' : self.mvc_settings.get_value('title'),
			'project_website' : self.mvc_settings.get_value('project_website'),
			'author_name' : self.mvc_settings.get_value('author'),
			'author_website' : self.mvc_settings.get_value('author_website'),
			'count_field' : self.table_settings.get_value('count_field',''),
		}
		
	def get_table_name(self, table=None):
		tblname = table
		if table == None:
			tblname = self.tblname
		table_prefix = self.table_settings.get_value('table_prefix')
		if table_prefix != '' and table_prefix != None:
			tblname = self.tblname[len(table_prefix):]
		return tblname
			
	def get_class_name(self):
		tblname = self.tblname
		table_prefix = self.table_settings.get_value('table_prefix')
		if table_prefix != '' and table_prefix != None:
			tblname = self.tblname[len(table_prefix):]
		return tblname.capitalize()
		
	def get_field_vars(self):
		text = ''
		for field in self.fields:
			field_settings = sqlite.fieldSettings(self.filename, self.dbname, self.tblname, field)
			
			if field_settings.get_value('model_has_default') == '1':
				default_value = field_settings.get_value('model_default_value')
				text += "\n\tprotected $%s = '%s';" % ( field, default_value )
			else:
				text += "\n\tprotected $%s;" % field
				
		return text
		
	def get_fields_controller(self):
		text = ''
		for field in self.fields:
			text += self.get_setter_getter(self.tblname, field)
		return text
	
	def get_setter_getter(self, table, field):
		text = """
	// --------------------------------------------------------------------
	// Start Field: %(field)s
	// -------------------------------------------------------------------- 

	/** 
	* Sets a value to `%(field)s` variable
	* @access public
	* @param  String
	* @return $this;
	*/

	public function set%(field_name)s($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL) {
		$this->%(field)s = ( ($value == '') && ($this->%(field)s != '') ) ? '' : $value;
		if( $setWhere ) {
			$key = '%(table)s.%(field)s';
			if ( $whereOperator != NULL && $whereOperator != '' ) {
				$key = $key . ' ' . $whereOperator;
			}
			$this->where[$key] = $this->%(field)s;
		}
		if( $set_data_field ) {
			$this->dataFields[] = '%(field)s';
		}
		return $this;
	}

	/** 
	* Get the value of `%(field)s` variable
	* @access public
	* @return String;
	*/

	public function get%(field_name)s() {
		return $this->%(field)s;
	}

	/**
	* Get row by `%(field)s`
	* @param %(field)s
	* @return QueryResult
	**/

	public function getBy%(field_name)s() {
		if($this->%(field)s != '') {
			if( $this->select ) {
				$this->db->select( implode(',' , $this->select) );
			}
			if( $this->join ) {
				foreach( $this->join as $join ) {
					$this->db->join( $join['table'], $join['connection'], $join['option'] );
				}
			}
			$query = $this->db->get_where('%(table)s', array('%(table)s.%(field)s' => $this->%(field)s), 1, 0);
			if( $query->num_rows > 0 ) {
				$result = $query->result();
				if( isset( $result[0] ) ) {
					$this->results = $result[0];
					return  $result[0];
				}
			}
		}
	}


	/**
	* Update table by `%(field)s`
	**/

	public function updateBy%(field_name)s() {
		if($this->%(field)s != '') {
			$this->setExclude('%(field)s');
			if( $this->getData() ) {
				return $this->db->update('%(table)s', $this->getData(), array('%(table)s.%(field)s' => $this->%(field)s ) );
			}
		}
	}


	/**
	* Delete row by `%(field)s`
	**/

	public function deleteBy%(field_name)s() {
		if($this->%(field)s != '') {
			return $this->db->delete('%(table)s', array('%(table)s.%(field)s' => $this->%(field)s ) );
		}
	}

	/**
	* Increment row by `%(field)s`
	**/

	public function incrementBy%(field_name)s() {
		if($this->%(field)s != '' && $this->countField != '') {
			$this->db->where('%(field)s', $this->%(field)s);
			$this->db->set($this->countField, $this->countField.'+1', FALSE);
			$this->db->update('%(table)s');
		}
	}

	/**
	* Decrement row by `%(field)s`
	**/

	public function decrementBy%(field_name)s() {
		if($this->%(field)s != '' && $this->countField != '') {
			$this->db->where('%(field)s', $this->%(field)s);
			$this->db->set($this->countField, $this->countField.'-1', FALSE);
			$this->db->update('%(table)s');
		}
	}
	
	// --------------------------------------------------------------------
	// End Field: %(field)s
	// --------------------------------------------------------------------
""" % {	'table' : self.get_table_name(table),
			'table2' : self.get_table_name(table),
			'field' : field,
			'field_name' : field.title().replace('_','')
			}
		return text

	def get_required_fields(self):
		text = []
		primary_key = self.table_settings.get_value('primary_key')
		for field in self.fields:
			if primary_key == field:
				continue;
			field_settings = sqlite.fieldSettings(self.filename, self.dbname, self.tblname, field)
			required = field_settings.get_value('required')
			if(required == '1'):
				text.append( '"' + field + '"' )
		return ','.join(text)
		
	def get_primary_field(self, func=False):
		primary = 'id'
		primary_key = self.table_settings.get_value('primary_key')
		if primary_key != '' and primary_key != None:
			primary = primary_key
		
		if(func):
			primary = primary.title().replace('_', '')
			
		return primary

	def get_create_table(self):
		mysql = MySQLConnect( self.filename, self.dbname )
		create_table = mysql.dump_create_table( self.tblname )
		create_table = create_table[:create_table.find(' ENGINE=InnoDB')] + ";"
		return create_table

	def get_content(self):
		return """<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * %(class_name)s_model Class
 *
 * Manipulates `%(table_name)s` table on database

%(create_table)s

 * @package			Model
 * @project			%(project_name)s
 * @project_link	%(project_website)s
 * @author			%(author_name)s
 * @author_link		%(author_website)s
 */
 
class %(class_name)s_model extends CI_Model {
%(fields_var)s
	protected $dataFields = array();
	protected $select = array();
	protected $join = array();
	protected $where = array();
	protected $filter = array();
	protected $order = array();
	protected $exclude = array();
	protected $required = array(%(required_fields)s);
	protected $countField = '%(count_field)s';
	protected $start = 0;
	protected $limit = 10;
	protected $results = FALSE;

	// --------------------------------------------------------------------

	/**
	* Construct 
	* @access public
	* @param  String
	* @return Boolean;
	*/

	function __construct() {
		parent::__construct();
	}

	// --------------------------------------------------------------------

%(fields_controller)s

	// --------------------------------------------------------------------

	/**
	* Checks if result not empty 
	* @access public
	* @param  String
	* @return Boolean;
	*/

	public function nonEmpty() {
		if ( $this->getWhere() ) {
			if( $this->join ) {
				foreach( $this->join as $join ) {
					$this->db->join( $join['table'], $join['connection'], $join['option'] );
				}
			}
			$this->db->where( $this->getWhere() );
			$query = $this->db->get('%(table_name2)s', 1);
			$result = $query->result();
			if ( isset( $result[0] ) === true ) {
				$this->results = $result[0];
				return true;
			} else {
				return false;
			}
		}
	}


	/**
	* Get Results 
	* @return Mixed;
	*/

	public function getResults() {
		return $this->results;
	}


	// --------------------------------------------------------------------

	/**
	* Delete 
	* @access public
	* @param  String
	* @return Boolean;
	*/

	public function delete() {
		if ( $this->getWhere() ) {
				return $this->db->delete('%(table_name2)s', $this->getWhere() );
		}
	}


	// --------------------------------------------------------------------

	/**
	* Limit Data Fields
	* @access public
	* @param  String
	* @return Boolean;
	*/

	public function limitDataFields($fields) {
		if($fields != '') {
			if( ! is_array($fields) ) {
				$this->dataFields = array($fields);
			} else {
				$this->dataFields = $fields;
			}
		}
	}

	// --------------------------------------------------------------------

	/**
	* Update 
	* @access public
	* @param  String
	* @return Boolean;
	*/

	public function update() {
		if ( ( $this->getData() ) && ( $this->getWhere() ) ) {
				return $this->db->update('%(table_name2)s', $this->getData(), $this->getWhere() );
		}
	}


	// --------------------------------------------------------------------

	/**
	* Insert new row 
	* @access public
	* @param  String
	* @return Boolean;
	*/

	public function insert() {
		if( $this->getData() ) {
			if( $this->db->insert('%(table_name2)s', $this->getData() ) === TRUE ) {
				$this->%(primary_field)s = $this->db->insert_id();
				return TRUE;
			} else {
				return FALSE;
			}
		}
	}


	// --------------------------------------------------------------------

	/**
	* Get First Data 
	* @access public
	* @return Object / False;
	*/

	public function get() {
		$this->db->limit( 1,$this->start);
		if( $this->select ) {
				$this->db->select( implode(',' , $this->select) );
		}
		if( $this->join ) {
			foreach( $this->join as $join ) {
				$this->db->join( $join['table'], $join['connection'], $join['option'] );
			}
		}
		if( $this->getWhere() ) {
			$this->db->where($this->getWhere() ); 
		}
		if( $this->order ) {
			foreach( $this->order as $field=>$orientation ) {
				$this->db->order_by( $field, $orientation );
			}
		}
		$query = $this->db->get('%(table_name2)s');
		$result = $query->result();
		if( isset($result[0]) ) {
			return $result[0];
		}
		return false;
	}
	// --------------------------------------------------------------------

	/**
	* Populate Data 
	* @access public
	* @param  String
	* @return Array;
	*/

	public function populate() {
		if( $this->limit > 0 ) {
			$this->db->limit( $this->limit,$this->start);
		}
		if( $this->select ) {
				$this->db->select( implode(',' , $this->select) );
		}
		if( $this->join ) {
			foreach( $this->join as $join ) {
				$this->db->join( $join['table'], $join['connection'], $join['option'] );
			}
		}
		if( $this->getWhere() ) {
			$this->db->where($this->getWhere() ); 
		}
		if( $this->order ) {
			foreach( $this->order as $field=>$orientation ) {
				$this->db->order_by( $field, $orientation );
			}
		}
		$query = $this->db->get('%(table_name2)s');
		return $query->result();
	}
	// --------------------------------------------------------------------
	/**
	* Recursive 
	* @access public
	* @param  String
	* @return Array;
	*/

	public function recursive($match, $find, $level=10, $conn='children') {
			if( $level == 0 ) return;
			if( $this->limit > 0 ) {
				$this->db->limit( $this->limit,$this->start);
			}
			if( $this->select ) {
					$this->db->select( implode(',' , $this->select) );
			}
				if( $this->join ) {
					foreach( $this->join as $join ) {
						$this->db->join( $join['table'], $join['connection'], $join['option'] );
					}
				}
				$this->where[$match] = $find;
				if( $this->where ) {
					$this->db->where($this->where); 
				}
			if( $this->order ) {
				foreach( $this->order as $field=>$orientation ) {
					$this->db->order_by( $field, $orientation );
				}
			}
			$query = $this->db->get('%(table_name2)s');
			$results=array();
			if( $query->result() ) {
				foreach( $query->result() as $qr ) {
					$children = $this->recursive($match, $qr->%(primary_field)s, ($level-1), $conn ) ;
					$results[] = (object) array_merge( (array) $qr, array($conn=>$children) );
				}
			}
		return $results;
	}

	// --------------------------------------------------------------------

	/**
	* Prepares data 
	* @access private
	* @return Array;
	*/
	public function getData($exclude=NULL) {
		$data = array();

		$fields = array(%(fields_list)s);
		if( count( $this->dataFields ) > 0 ) {
    		            $fields = $this->dataFields;
		}
		foreach( $fields as $field ) {
		    if( ( in_array( $field, $this->required ) ) 
		    && ($this->$field == '') 
		    && ( ! in_array( $field, $this->exclude ) ) 
		    ) {
        		return false;
    		}
    		if( ( in_array( $field, $this->required ) ) 
    		&& ($this->$field != '') 
    		&& ( ! in_array( $field, $this->exclude ) ) 
    		) {
		        $data[$field] = $this->$field;
		    }
		    if( ( ! in_array( $field, $this->required ) ) 
		    && ( ! in_array( $field, $this->exclude ) ) 
		    ) {
        		$data[$field] = $this->$field;
    		}  
		}
		return $data;   
	}


	// --------------------------------------------------------------------

	/**
	* Set Where Clause 
	* @access public
	* @param String / Array
	* @return Array;
	*/
	public function setWhere($where) {
		if($where != '') {
			if( ! is_array($where) ) {
				$this->where = array($where => $this->$where);
			} else {
				$h = array();
				foreach($where as $w) {
					if($w != '') {
						$h[$w] = $this->$w;
					}
				}
				$this->where = $h;
			}
		}
	}

	// --------------------------------------------------------------------

	/**
	* Get Where Clause 
	* @access public
	* @return Array;
	*/
	private function getWhere() {
		$where = array_merge( $this->where, $this->filter );
		if( ( $where ) && ( is_array( $where ) ) ) {
			return $where;
		} 
	}

	// --------------------------------------------------------------------

	/**
	* Clear Where 
	* @access public
	* @return Array;
	*/
	public function clearWhere() {
		unset($this->where);
		$this->where = array();
	}

	// --------------------------------------------------------------------
	/**
	* Set Join Clause 
	* @access public
	* @param String / Array
	* @return Array;
	*/
	public function setJoin($table, $connection, $option='left') {
		$this->join[] = array('table'=>$table, 'connection'=>$connection, 'option'=>$option );
	}
	// --------------------------------------------------------------------

	/**
	* Set Filter 
	* @access public
	* @return Array;
	*/
	public function setFilter($field, $value, $table='%(table_name2)s') {
		if( $table == '' ) { $table = '%(table_name2)s'; }
		$this->filter[$table.'.'.$field] = $value;
	}


	public function clearFilter() {
		unset($this->filter);
		$this->filter = array();
	}

	// --------------------------------------------------------------------

	/**
	* Set Start  
	* @access public
	*/
	public function setStart($value) {
		$this->start = $value;
		return $this;
	}

	/**
	* Set Limit  
	* @access public
	*/
	public function setLimit($value) {
		$this->limit = $value;
		return $this;
	}

	// --------------------------------------------------------------------

	/**
	* Set Order  
	* @access public
	*/

	public function setOrder($field, $orientation='asc') {
		$this->order[$field] = $orientation;
		return $this;
	}

	// --------------------------------------------------------------------

	/**
	* Set Select  
	* @access public
	*/

	public function setSelect($select) {
		$this->select[] = $select;
		return $this;
	}

	// --------------------------------------------------------------------

	/**
	* Set Exclude  
	* @access public
	*/

	public function setExclude($exclude) {
		$this->exclude[] = $exclude;
		return $this;
	}

}

/* End of file %(table_name)s_model.php */
/* Location: ./application/models/%(table_name)s_model.php */
""" % self.get_variables()

