import sqlite
from lib.mysql import MySQLConnect

class Lang:
	
	mvc_settings = None
	table_settings = None
	content = None
	dbname = None
	tblname = None
	fields = None
	filename = None
	
	def __init__(self, filename, db, table):
		self.dbname = db
		self.tblname = table
		self.filename = filename
		mysql = MySQLConnect( filename, db )
		self.fields = mysql.fieldList(self.tblname)
		self.mvc_settings = sqlite.mvcSettings(filename,db)
		self.table_settings = sqlite.tableSettings(filename,db, table)
			
	def get(self):
		content = """<?php

// %s

""" % self.get_table_name()
		 
		for field in self.fields:
			fieldSettings = sqlite.fieldSettings(self.filename, self.dbname, self.tblname, field)
			content += "$lang['%(table)s_%(field)s'] = '%(title)s';\n" % {
				"table" : self.get_table_name(), 
				"field" : field,  
				"title" : self.get_field_title(field, fieldSettings)
			}
		
		content += """
/* End of file %(table)s_lang.php */

/* Location: ./application/language/english/%(table)s_lang.php */
""" % { "table" : self.get_table_name() }
		return content

	def get_table_name(self):
		tblname = self.tblname
		table_settings = sqlite.tableSettings(self.filename, self.dbname, self.tblname)
		table_prefix = table_settings.get_value('table_prefix')
		if table_prefix != '' and table_prefix != None:
			tblname = self.tblname[len(table_prefix):]
		return tblname

	def get_field_title(self, field, fieldSettings):
		field_title = fieldSettings.get_value('field_title')
		if field_title == '' or field_title == None:
			field_title = field.title().replace('_' , " ")
		
		return field_title
