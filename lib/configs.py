import os
from lib import sqlite, create_file
from lib.mysql import MySQLConnect
import menu

class Database:
	
	content = None
	dbname = None
	tblname = None
	savefile = None
	
	def __init__(self, savefile, db, appDir):
		self.savefile = savefile
		self.dbname = db
		self.filename = os.path.join(appDir, 'config', 'database.php')
		self.content = open(self.filename, 'r')
	
	def updateContent(self):
		new = ''
		mvc = sqlite.mvcSettings(self.savefile,self.dbname)
		for line in self.content:
			line = line.replace('{DBHOST}', mvc.get_value('dbhost'))
			line = line.replace('{DBUSER}', mvc.get_value('dbuser'))
			line = line.replace('{DBPASS}', mvc.get_value('dbpass'))
			line = line.replace('{DBNAME}', self.dbname)
			line = line.replace('{DBPREFIX}', mvc.get_value('tblprefix'))
			new += line	
		return new
		
	def save(self):
		create_file(self.filename, self.updateContent())
		print( "File Created: " + self.filename )

class MenuFile:

        filename = None
        dbname = None
        
        def __init__(self, filename, dbname, appDir):
                self.filename = filename
                self.dbname = dbname
                self.appDir = appDir
                
        def create(self):
                if not os.path.exists( os.path.join(self.appDir, "views") ):
                        os.makedirs( os.path.join(self.appDir, "views") )
		navFile = os.path.join(self.appDir, "views", "sidebar_nav.php")
		create_file( navFile,  menu.Menu(self.filename, self.dbname ).get() )
		print( "File Created: " + navFile )

class DumpTextData:
	
	tableDump = None
	FIELDS = None
	savefile = None
	dbname = None
	tablename = None
	
	def __init__(self, savefile, db, table):
		self.savefile = savefile
		self.dbname = db
		self.tablename = table 
		
		self.tableDump = DumpTableSettingsData( savefile, db, table )
		mysql = MySQLConnect( savefile, db )
		self.FIELDS = mysql.fieldList( table )
		
	def save(self):
		if self.tableDump != None:
			self.tableDump.save()
		
		if self.FIELDS != None:
			for field in self.FIELDS:
				fieldDump = DumpFieldSettingsData( self.savefile, self.dbname, self.tablename, field )
				fieldDump.save()
		
class DumpTableSettingsData:
	
	dbname = None
	tblname = None
	savefile = None
	
	def __init__(self, savefile, db, table):
		
		self.savefile = savefile
		self.dbname = db
		self.tblname = table
		self.filename = os.path.join('dumpfiles','txtdumps', savefile, db, table+'-table_settings.txt')

                if not os.path.exists(os.path.join('dumpfiles','txtdumps', savefile, db)):
                        os.makedirs(os.path.join('dumpfiles','txtdumps', savefile, db))
                        
		#if not os.path.exists('txtdumps'):
		#	os.makedirs('txtdumps')
		
		#if not os.path.exists( os.path.join('txtdumps', savefile) ):
		#	os.makedirs( os.path.join('txtdumps', savefile) )

		#if not os.path.exists( os.path.join('txtdumps', savefile, db) ):
		#	os.makedirs( os.path.join('txtdumps', savefile, db) )
		
	def updateContent(self):
		new = ''
		tableSettings = sqlite.tableSettings( self.savefile, self.dbname, self.tblname )
		
		for settings in tableSettings.get_keys():
			new += 'INSERT INTO `table_settings` VALUES ( "' + self.dbname+ '", "' + self.tblname+ '", "' + str(settings[2]) + '", "' + str(settings[3]) + '");\n'
			
		new += '\n\n'
		
		for settings in tableSettings.get_keys():
			new += '\'INSERT INTO `table_settings` VALUES ( "%s", "%s", "' + str(settings[2]) + '", "' + str(settings[3]) + '");\',\n'
			
		return new
		
	def save(self):
		create_file(self.filename, self.updateContent())
		print( "File Created: " + self.filename )

class DumpFieldSettingsData:
	
	dbname = None
	tblname = None
	fieldname = None
	savefile = None
	
	def __init__(self, savefile, db, table, field):
		
		self.savefile = savefile
		self.dbname = db
		self.tblname = table
		self.fieldname = field
		self.filename = os.path.join('dumpfiles','txtdumps', savefile, db, table, field+'-field_settings.txt')
		
		#if not os.path.exists('txtdumps'):
		#	os.makedirs('txtdumps')
		
		#if not os.path.exists( os.path.join('txtdumps', savefile) ):
		#	os.makedirs( os.path.join('txtdumps', savefile) )
		
		#if not os.path.exists( os.path.join('txtdumps', savefile, db) ):
		#	os.makedirs( os.path.join('txtdumps', savefile, db) )
		
		#if not os.path.exists( os.path.join('txtdumps', savefile, db, table) ):
		#	os.makedirs( os.path.join('txtdumps', savefile, db, table) )

		if not os.path.exists( os.path.join('dumpfiles','txtdumps', savefile, db, table) ):
			os.makedirs( os.path.join('dumpfiles','txtdumps', savefile, db, table) )
			
	def updateContent(self):
		new = ''
		fieldSettings = sqlite.fieldSettings( self.savefile, self.dbname, self.tblname, self.fieldname )

		for settings in fieldSettings.get_keys():
			new += 'INSERT INTO `field_settings` VALUES ( "' + self.dbname+ '", "' + self.tblname+ '", "' + self.fieldname + '", "' + str(settings[3]) + '", "' + str(settings[4]) + '");\n'
		
		new += '\n\n'
		
		for settings in fieldSettings.get_keys():
			new += '\'INSERT INTO `field_settings` VALUES ( "%s", "%s", "' + self.fieldname + '", "' + str(settings[3]) + '", "' + str(settings[4]) + '");\',\n'
		
		return new
		
	def save(self):
		create_file(self.filename, self.updateContent())
		print( "File Created: " + self.filename )
		
class SQLdump:
	
	content = None
	dbname = None
	savefile = None
	
	def __init__(self, savefile, db, mvcDir):
		self.savefile = savefile
		self.dbname = db
		self.filename = os.path.join(mvcDir,'sqldump.sql')
	
	def updateContent(self):
		mysql = MySQLConnect( self.savefile, self.dbname)
		tables = mysql.tableList()
		content = ''
		for table in tables:
			content += "-- Table structure for table `%s` \n\n" % table
			SQL = mysql.dump_create_table(table)
			content += SQL[:SQL.find(' ENGINE=InnoDB')]
			content += ";\n\n"
		return content
		
	def save(self):
		create_file(self.filename, self.updateContent())
		print( "File Created: " + self.filename )

		if not os.path.exists(os.path.join('dumpfiles','sqldumps')):
			os.makedirs(os.path.join('dumpfiles','sqldumps'))
			print("Folder Created: sqldumps" )
			
		dumpfile = os.path.join('dumpfiles','sqldumps', self.dbname + ".sql")
		create_file(dumpfile, self.updateContent())
		print( "File Created: " + dumpfile )
		
class TemplateData:
	
	content = None
	dbname = None
	tblname = None
	savefile = None
	
	def __init__(self, savefile, db, appDir):
		self.savefile = savefile
		self.dbname = db
		self.filename = os.path.join(appDir,'libraries', 'Template_data.php')
		self.content = open(self.filename, 'r')
	
	def updateContent(self):
		new = ''
		mvc = sqlite.mvcSettings(self.savefile, self.dbname)
		for line in self.content:
			line = line.replace('{SITE_TITLE}', mvc.get_value('title'))
			new += line	
		return new
		
	def save(self):
		create_file(self.filename, self.updateContent())
		print( "File Created: " + self.filename )

class InDexFile:
	
	content = None
	dbname = None
	tblname = None
	savefile = None
	
	def __init__(self, savefile, db, mvcDir):
		self.savefile = savefile
		self.dbname = db
		self.filename = os.path.join(mvcDir, 'index.php')
			
	def save(self):
		create_file(self.filename, self.indexContent())
		print( "File Created: " + self.filename )

	def indexContent(self):
		
		mvc = sqlite.mvcSettings(self.savefile, self.dbname)
		variables = {
			'ENVIRONMENT' : mvc.get_value("index_environment"),
			'system_path' : mvc.get_value("system_directory"),
			'application_folder' : mvc.get_value("application_directory"),
		}
		
		return """<?php

/*
 *---------------------------------------------------------------
 * APPLICATION ENVIRONMENT
 *---------------------------------------------------------------
 *
 * You can load different configurations depending on your
 * current environment. Setting the environment also influences
 * things like logging and error reporting.
 *
 * This can be set to anything, but default usage is:
 *
 *     development
 *     testing
 *     production
 *
 * NOTE: If you change these, also change the error_reporting() code below
 *
 */
	define('ENVIRONMENT', '%(ENVIRONMENT)s');
/*
 *---------------------------------------------------------------
 * ERROR REPORTING
 *---------------------------------------------------------------
 *
 * Different environments will require different levels of error reporting.
 * By default development will show errors but testing and live will hide them.
 */

if (defined('ENVIRONMENT'))
{
	switch (ENVIRONMENT)
	{
		case 'development':
			error_reporting(E_ALL);
		break;
	
		case 'testing':
		case 'production':
			error_reporting(0);
		break;

		default:
			exit('The application environment is not set correctly.');
	}
}

/*
 *---------------------------------------------------------------
 * SYSTEM FOLDER NAME
 *---------------------------------------------------------------
 *
 * This variable must contain the name of your "system" folder.
 * Include the path if the folder is not in the same  directory
 * as this file.
 *
 */
	$system_path = '%(system_path)s';

/*
 *---------------------------------------------------------------
 * APPLICATION FOLDER NAME
 *---------------------------------------------------------------
 *
 * If you want this front controller to use a different "application"
 * folder then the default one you can set its name here. The folder
 * can also be renamed or relocated anywhere on your server.  If
 * you do, use a full server path. For more info please see the user guide:
 * http://codeigniter.com/user_guide/general/managing_apps.html
 *
 * NO TRAILING SLASH!
 *
 */
	$application_folder = '%(application_folder)s';

/*
 * --------------------------------------------------------------------
 * DEFAULT CONTROLLER
 * --------------------------------------------------------------------
 *
 * Normally you will set your default controller in the routes.php file.
 * You can, however, force a custom routing by hard-coding a
 * specific controller class/function here.  For most applications, you
 * WILL NOT set your routing here, but it's an option for those
 * special instances where you might want to override the standard
 * routing in a specific front controller that shares a common CI installation.
 *
 * IMPORTANT:  If you set the routing here, NO OTHER controller will be
 * callable. In essence, this preference limits your application to ONE
 * specific controller.  Leave the function name blank if you need
 * to call functions dynamically via the URI.
 *
 * Un-comment the $routing array below to use this feature
 *
 */
	// The directory name, relative to the "controllers" folder.  Leave blank
	// if your controller is not in a sub-folder within the "controllers" folder
	// $routing['directory'] = '';

	// The controller class file name.  Example:  Mycontroller
	// $routing['controller'] = '';

	// The controller function you wish to be called.
	// $routing['function']	= '';


/*
 * -------------------------------------------------------------------
 *  CUSTOM CONFIG VALUES
 * -------------------------------------------------------------------
 *
 * The $assign_to_config array below will be passed dynamically to the
 * config class when initialized. This allows you to set custom config
 * items or override any default config values found in the config.php file.
 * This can be handy as it permits you to share one application between
 * multiple front controller files, with each file containing different
 * config values.
 *
 * Un-comment the $assign_to_config array below to use this feature
 *
 */
	// $assign_to_config['name_of_config_item'] = 'value of config item';



// --------------------------------------------------------------------
// END OF USER CONFIGURABLE SETTINGS.  DO NOT EDIT BELOW THIS LINE
// --------------------------------------------------------------------

/*
 * ---------------------------------------------------------------
 *  Resolve the system path for increased reliability
 * ---------------------------------------------------------------
 */

	// Set the current directory correctly for CLI requests
	if (defined('STDIN'))
	{
		chdir(dirname(__FILE__));
	}

	if (realpath($system_path) !== FALSE)
	{
		$system_path = realpath($system_path).'/';
	}

	// ensure there's a trailing slash
	$system_path = rtrim($system_path, '/').'/';

	// Is the system path correct?
	if ( ! is_dir($system_path))
	{
		exit("Your system folder path does not appear to be set correctly. Please open the following file and correct this: ".pathinfo(__FILE__, PATHINFO_BASENAME));
	}

/*
 * -------------------------------------------------------------------
 *  Now that we know the path, set the main path constants
 * -------------------------------------------------------------------
 */
	// The name of THIS file
	define('SELF', pathinfo(__FILE__, PATHINFO_BASENAME));

	// The PHP file extension
	// this global constant is deprecated.
	define('EXT', '.php');

	// Path to the system folder
	define('BASEPATH', str_replace("\\\\", "/", $system_path));

	// Path to the front controller (this file)
	define('FCPATH', str_replace(SELF, '', __FILE__));

	// Name of the "system folder"
	define('SYSDIR', trim(strrchr(trim(BASEPATH, '/'), '/'), '/'));


	// The path to the "application" folder
	if (is_dir($application_folder))
	{
		define('APPPATH', $application_folder.'/');
	}
	else
	{
		if ( ! is_dir(BASEPATH.$application_folder.'/'))
		{
			exit("Your application folder path does not appear to be set correctly. Please open the following file and correct this: ".SELF);
		}

		define('APPPATH', BASEPATH.$application_folder.'/');
	}

/*
 * --------------------------------------------------------------------
 * LOAD THE BOOTSTRAP FILE
 * --------------------------------------------------------------------
 *
 * And away we go...
 *
 */
require_once BASEPATH.'core/CodeIgniter.php';

/* End of file index.php */
/* Location: ./index.php */""" % variables

class DatabaseFile:
	
	content = None
	dbname = None
	tblname = None
	
	def __init__(self, savefile, db, appDir):
		self.savefile = savefile
		self.dbname = db
		self.filename = os.path.join(appDir, 'config', 'database.php')
			
	def save(self):
		create_file(self.filename, self.databaseContent())
		print( "File Created: " + self.filename )

	def databaseContent(self):
		
		mvc = sqlite.mvcSettings(self.savefile, self.dbname)
		variables = {
			'mvc_title' : mvc.get_value("title"),
			'dbhost' : mvc.get_value("dbhost"),
			'dbuser' : mvc.get_value("dbuser"),
			'dbpass' : mvc.get_value("dbpass"),
			'dbport' : mvc.get_value("dbport"),
			'dbname' : self.dbname,
			'tblprefix' : mvc.get_value("tblprefix"),
		}
		
		return """<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
| -------------------------------------------------------------------
| DATABASE CONNECTIVITY SETTINGS
| -------------------------------------------------------------------
| This file will contain the settings needed to access your database.
|
| For complete instructions please consult the 'Database Connection'
| page of the User Guide.
|
| -------------------------------------------------------------------
| EXPLANATION OF VARIABLES
| -------------------------------------------------------------------
|
|	['hostname'] The hostname of your database server.
|	['username'] The username used to connect to the database
|	['password'] The password used to connect to the database
|	['database'] The name of the database you want to connect to
|	['dbdriver'] The database type. ie: mysql.  Currently supported:
				 mysql, mysqli, postgre, odbc, mssql, sqlite, oci8
|	['dbprefix'] You can add an optional prefix, which will be added
|				 to the table name when using the  Active Record class
|	['pconnect'] TRUE/FALSE - Whether to use a persistent connection
|	['db_debug'] TRUE/FALSE - Whether database errors should be displayed.
|	['cache_on'] TRUE/FALSE - Enables/disables query caching
|	['cachedir'] The path to the folder where cache files should be stored
|	['char_set'] The character set used in communicating with the database
|	['dbcollat'] The character collation used in communicating with the database
|				 NOTE: For MySQL and MySQLi databases, this setting is only used
| 				 as a backup if your server is running PHP < 5.2.3 or MySQL < 5.0.7
|				 (and in table creation queries made with DB Forge).
| 				 There is an incompatibility in PHP with mysql_real_escape_string() which
| 				 can make your site vulnerable to SQL injection if you are using a
| 				 multi-byte character set and are running versions lower than these.
| 				 Sites using Latin-1 or UTF-8 database character set and collation are unaffected.
|	['swap_pre'] A default table prefix that should be swapped with the dbprefix
|	['autoinit'] Whether or not to automatically initialize the database.
|	['stricton'] TRUE/FALSE - forces 'Strict Mode' connections
|							- good for ensuring strict SQL while developing
|
| The $active_group variable lets you choose which connection group to
| make active.  By default there is only one group (the 'default' group).
|
| The $active_record variables lets you determine whether or not to load
| the active record class
*/

$active_group = 'default';
$active_record = TRUE;

/*
    %(mvc_title)s
*/
$db['default']['hostname'] = '%(dbhost)s';
$db['default']['username'] = '%(dbuser)s';
$db['default']['password'] = '%(dbpass)s';
$db['default']['database'] = '%(dbname)s';
$db['default']['dbdriver'] = 'mysql';
$db['default']['dbprefix'] = '%(tblprefix)s';
$db['default']['pconnect'] = TRUE;
$db['default']['db_debug'] = TRUE;
$db['default']['cache_on'] = FALSE;
$db['default']['cachedir'] = APPPATH . 'cache';
$db['default']['char_set'] = 'utf8';
$db['default']['dbcollat'] = 'utf8_general_ci';
$db['default']['swap_pre'] = '';
$db['default']['autoinit'] = TRUE;
$db['default']['stricton'] = FALSE;


/* End of file database.php */
/* Location: ./application/config/database.php */
""" % variables

class ConfigFile:
	
	dbname = None
	
	def __init__(self, savefile, db, appDir):
		self.savefile = savefile
		self.dbname = db
		self.filename = os.path.join(appDir, 'config', 'config.php')
			
	def save(self):
		create_file(self.filename, self.configContent())
		print( "File Created: " + self.filename )

	def configContent(self):
		
		mvc = sqlite.mvcSettings(self.savefile, self.dbname)
		variables = {
			'base_url' : mvc.get_value("config_base_url", default=""),
			'index_page' : mvc.get_value("config_index_page", default="index.php"),
			'uri_protocol' : mvc.get_value("config_uri_protocol", default="AUTO"),
			'url_suffix' : mvc.get_value("config_url_suffix", default=""),
			'language' : mvc.get_value("config_language", default="english"),
			'charset' : mvc.get_value("config_charset", default="UTF-8"),
			'enable_hooks' : mvc.get_value("config_enable_hooks", default="FALSE"),
			'subclass_prefix' : mvc.get_value("config_subclass_prefix", default="MY_"),
			'permitted_uri_chars' : mvc.get_value("config_permitted_uri_chars", default="a-z 0-9~%.:_\-"),
			'allow_get_array' : mvc.get_value("config_allow_get_array", default="TRUE"),
			'enable_query_strings' : mvc.get_value("config_enable_query_strings", default="FALSE"),
			'controller_trigger' : mvc.get_value("config_controller_trigger", default="c"),
			'function_trigger' : mvc.get_value("config_function_trigger", default="m"),
			'directory_trigger' : mvc.get_value("config_directory_trigger", default="d"),
			'log_threshold' : mvc.get_value("config_log_threshold", default="0"),
			'log_path' : mvc.get_value("config_log_path", default=""),
			'log_date_format' : mvc.get_value("config_log_date_format", default="Y-m-d H:i:s"),
			'cache_path' : mvc.get_value("config_cache_path", default=""),
			'encryption_key' : mvc.get_value("config_encryption_key", default="OqbzWkV5qYIF31CBdj9dNJjD5MttorJV"),
			'sess_cookie_name' : mvc.get_value("config_sess_cookie_name", default="ci_session"),
			'sess_expiration' : mvc.get_value("config_sess_expiration", default="7200"),
			'sess_expire_on_close' : mvc.get_value("config_sess_expire_on_close", default="TRUE"),
			'sess_encrypt_cookie' : mvc.get_value("config_sess_encrypt_cookie", default="FALSE"),
			'sess_use_database' : mvc.get_value("config_sess_use_database", default="TRUE"),
			'sess_table_name' : mvc.get_value("config_sess_table_name", default="ci_admins_sessions"),
			'sess_match_ip' : mvc.get_value("config_sess_match_ip", default="TRUE"),
			'sess_match_useragent' : mvc.get_value("config_sess_match_useragent", default="TRUE"),
			'sess_time_to_update' : mvc.get_value("config_sess_time_to_update", default="300"),
			'cookie_prefix' : mvc.get_value("config_cookie_prefix", default=""),
			'cookie_domain' : mvc.get_value("config_cookie_domain", default=""),
			'cookie_path' : mvc.get_value("config_cookie_path", default="/"),
			'cookie_secure' : mvc.get_value("config_cookie_secure", default="FALSE"),
			'global_xss_filtering' : mvc.get_value("config_global_xss_filtering", default="FALSE"),
			'csrf_protection' : mvc.get_value("config_csrf_protection", default="FALSE"),
			'csrf_token_name' : mvc.get_value("config_csrf_token_name", default="csrf_test_name"),
			'csrf_cookie_name' : mvc.get_value("config_csrf_cookie_name", default="csrf_cookie_name"),
			'csrf_expire' : mvc.get_value("config_csrf_expire", default="7200"),
			'compress_output' : mvc.get_value("config_compress_output", default="FALSE"),
			'time_reference' : mvc.get_value("config_time_reference", default="local"),
			'rewrite_short_tags' : mvc.get_value("config_rewrite_short_tags", default="FALSE"),
			'proxy_ips' : mvc.get_value("config_proxy_ips", default=""),
		}
		
		return """<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*
|--------------------------------------------------------------------------
| Base Site URL
|--------------------------------------------------------------------------
|
| URL to your CodeIgniter root. Typically this will be your base URL,
| WITH a trailing slash:
|
|	http://example.com/
|
| If this is not set then CodeIgniter will guess the protocol, domain and
| path to your installation.
|
*/
$config['base_url']	= '%(base_url)s';

/*
|--------------------------------------------------------------------------
| Index File
|--------------------------------------------------------------------------
|
| Typically this will be your index.php file, unless you've renamed it to
| something else. If you are using mod_rewrite to remove the page set this
| variable so that it is blank.
|
*/
$config['index_page'] = '%(index_page)s';

/*
|--------------------------------------------------------------------------
| URI PROTOCOL
|--------------------------------------------------------------------------
|
| This item determines which server global should be used to retrieve the
| URI string.  The default setting of 'AUTO' works for most servers.
| If your links do not seem to work, try one of the other delicious flavors:
|
| 'AUTO'			Default - auto detects
| 'PATH_INFO'		Uses the PATH_INFO
| 'QUERY_STRING'	Uses the QUERY_STRING
| 'REQUEST_URI'		Uses the REQUEST_URI
| 'ORIG_PATH_INFO'	Uses the ORIG_PATH_INFO
|
*/
$config['uri_protocol']	= '%(uri_protocol)s';

/*
|--------------------------------------------------------------------------
| URL suffix
|--------------------------------------------------------------------------
|
| This option allows you to add a suffix to all URLs generated by CodeIgniter.
| For more information please see the user guide:
|
| http://codeigniter.com/user_guide/general/urls.html
*/

$config['url_suffix'] = '%(url_suffix)s';

/*
|--------------------------------------------------------------------------
| Default Language
|--------------------------------------------------------------------------
|
| This determines which set of language files should be used. Make sure
| there is an available translation if you intend to use something other
| than english.
|
*/
$config['language']	= '%(language)s';

/*
|--------------------------------------------------------------------------
| Default Character Set
|--------------------------------------------------------------------------
|
| This determines which character set is used by default in various methods
| that require a character set to be provided.
|
*/
$config['charset'] = '%(charset)s';

/*
|--------------------------------------------------------------------------
| Enable/Disable System Hooks
|--------------------------------------------------------------------------
|
| If you would like to use the 'hooks' feature you must enable it by
| setting this variable to TRUE (boolean).  See the user guide for details.
|
*/
$config['enable_hooks'] = %(enable_hooks)s;


/*
|--------------------------------------------------------------------------
| Class Extension Prefix
|--------------------------------------------------------------------------
|
| This item allows you to set the filename/classname prefix when extending
| native libraries.  For more information please see the user guide:
|
| http://codeigniter.com/user_guide/general/core_classes.html
| http://codeigniter.com/user_guide/general/creating_libraries.html
|
*/
$config['subclass_prefix'] = '%(subclass_prefix)s';


/*
|--------------------------------------------------------------------------
| Allowed URL Characters
|--------------------------------------------------------------------------
|
| This lets you specify with a regular expression which characters are permitted
| within your URLs.  When someone tries to submit a URL with disallowed
| characters they will get a warning message.
|
| As a security measure you are STRONGLY encouraged to restrict URLs to
| as few characters as possible.  By default only these are allowed: a-z 0-9~%%.:_-
|
| Leave blank to allow all characters -- but only if you are insane.
|
| DO NOT CHANGE THIS UNLESS YOU FULLY UNDERSTAND THE REPERCUSSIONS!!
|
*/
$config['permitted_uri_chars'] = '%(permitted_uri_chars)s';


/*
|--------------------------------------------------------------------------
| Enable Query Strings
|--------------------------------------------------------------------------
|
| By default CodeIgniter uses search-engine friendly segment based URLs:
| example.com/who/what/where/
|
| By default CodeIgniter enables access to the $_GET array.  If for some
| reason you would like to disable it, set 'allow_get_array' to FALSE.
|
| You can optionally enable standard query string based URLs:
| example.com?who=me&what=something&where=here
|
| Options are: TRUE or FALSE (boolean)
|
| The other items let you set the query string 'words' that will
| invoke your controllers and its functions:
| example.com/index.php?c=controller&m=function
|
| Please note that some of the helpers won't work as expected when
| this feature is enabled, since CodeIgniter is designed primarily to
| use segment based URLs.
|
*/
$config['allow_get_array']		= %(allow_get_array)s;
$config['enable_query_strings'] = %(enable_query_strings)s;
$config['controller_trigger']	= '%(controller_trigger)s';
$config['function_trigger']		= '%(function_trigger)s';
$config['directory_trigger']	= '%(directory_trigger)s'; // experimental not currently in use

/*
|--------------------------------------------------------------------------
| Error Logging Threshold
|--------------------------------------------------------------------------
|
| If you have enabled error logging, you can set an error threshold to
| determine what gets logged. Threshold options are:
| You can enable error logging by setting a threshold over zero. The
| threshold determines what gets logged. Threshold options are:
|
|	0 = Disables logging, Error logging TURNED OFF
|	1 = Error Messages (including PHP errors)
|	2 = Debug Messages
|	3 = Informational Messages
|	4 = All Messages
|
| For a live site you'll usually only enable Errors (1) to be logged otherwise
| your log files will fill up very fast.
|
*/
$config['log_threshold'] = %(log_threshold)s;

/*
|--------------------------------------------------------------------------
| Error Logging Directory Path
|--------------------------------------------------------------------------
|
| Leave this BLANK unless you would like to set something other than the default
| application/logs/ folder. Use a full server path with trailing slash.
|
*/
$config['log_path'] = '%(log_path)s';

/*
|--------------------------------------------------------------------------
| Date Format for Logs
|--------------------------------------------------------------------------
|
| Each item that is logged has an associated date. You can use PHP date
| codes to set your own date formatting
|
*/
$config['log_date_format'] = '%(log_date_format)s';

/*
|--------------------------------------------------------------------------
| Cache Directory Path
|--------------------------------------------------------------------------
|
| Leave this BLANK unless you would like to set something other than the default
| system/cache/ folder.  Use a full server path with trailing slash.
|
*/
$config['cache_path'] = '%(cache_path)s';

/*
|--------------------------------------------------------------------------
| Encryption Key
|--------------------------------------------------------------------------
|
| If you use the Encryption class or the Session class you
| MUST set an encryption key.  See the user guide for info.
|
*/
$config['encryption_key'] = '%(encryption_key)s';

/*
|--------------------------------------------------------------------------
| Session Variables
|--------------------------------------------------------------------------
|
| 'sess_cookie_name'		= the name you want for the cookie
| 'sess_expiration'			= the number of SECONDS you want the session to last.
|   by default sessions last 7200 seconds (two hours).  Set to zero for no expiration.
| 'sess_expire_on_close'	= Whether to cause the session to expire automatically
|   when the browser window is closed
| 'sess_encrypt_cookie'		= Whether to encrypt the cookie
| 'sess_use_database'		= Whether to save the session data to a database
| 'sess_table_name'			= The name of the session database table
| 'sess_match_ip'			= Whether to match the user's IP address when reading the session data
| 'sess_match_useragent'	= Whether to match the User Agent when reading the session data
| 'sess_time_to_update'		= how many seconds between CI refreshing Session Information
|
*/
$config['sess_cookie_name']		= '%(sess_cookie_name)s';
$config['sess_expiration']		= %(sess_expiration)s;
$config['sess_expire_on_close']	= %(sess_expire_on_close)s;
$config['sess_encrypt_cookie']	= %(sess_encrypt_cookie)s;
$config['sess_use_database']	= %(sess_use_database)s;
$config['sess_table_name']		= '%(sess_table_name)s';
$config['sess_match_ip']		= %(sess_match_ip)s;
$config['sess_match_useragent']	= %(sess_match_useragent)s;
$config['sess_time_to_update']	= %(sess_time_to_update)s;

/*
|--------------------------------------------------------------------------
| Cookie Related Variables
|--------------------------------------------------------------------------
|
| 'cookie_prefix' = Set a prefix if you need to avoid collisions
| 'cookie_domain' = Set to .your-domain.com for site-wide cookies
| 'cookie_path'   =  Typically will be a forward slash
| 'cookie_secure' =  Cookies will only be set if a secure HTTPS connection exists.
|
*/
$config['cookie_prefix']	= "%(cookie_prefix)s";
$config['cookie_domain']	= "%(cookie_domain)s";
$config['cookie_path']		= "%(cookie_path)s";
$config['cookie_secure']	= %(cookie_secure)s;

/*
|--------------------------------------------------------------------------
| Global XSS Filtering
|--------------------------------------------------------------------------
|
| Determines whether the XSS filter is always active when GET, POST or
| COOKIE data is encountered
|
*/
$config['global_xss_filtering'] = %(global_xss_filtering)s;

/*
|--------------------------------------------------------------------------
| Cross Site Request Forgery
|--------------------------------------------------------------------------
| Enables a CSRF cookie token to be set. When set to TRUE, token will be
| checked on a submitted form. If you are accepting user data, it is strongly
| recommended CSRF protection be enabled.
|
| 'csrf_token_name' = The token name
| 'csrf_cookie_name' = The cookie name
| 'csrf_expire' = The number in seconds the token should expire.
*/
$config['csrf_protection'] = %(csrf_protection)s;
$config['csrf_token_name'] = '%(csrf_token_name)s';
$config['csrf_cookie_name'] = '%(csrf_cookie_name)s';
$config['csrf_expire'] = %(csrf_expire)s;

/*
|--------------------------------------------------------------------------
| Output Compression
|--------------------------------------------------------------------------
|
| Enables Gzip output compression for faster page loads.  When enabled,
| the output class will test whether your server supports Gzip.
| Even if it does, however, not all browsers support compression
| so enable only if you are reasonably sure your visitors can handle it.
|
| VERY IMPORTANT:  If you are getting a blank page when compression is enabled it
| means you are prematurely outputting something to your browser. It could
| even be a line of whitespace at the end of one of your scripts.  For
| compression to work, nothing can be sent before the output buffer is called
| by the output class.  Do not 'echo' any values with compression enabled.
|
*/
$config['compress_output'] = %(compress_output)s;

/*
|--------------------------------------------------------------------------
| Master Time Reference
|--------------------------------------------------------------------------
|
| Options are 'local' or 'gmt'.  This pref tells the system whether to use
| your server's local time as the master 'now' reference, or convert it to
| GMT.  See the 'date helper' page of the user guide for information
| regarding date handling.
|
*/
$config['time_reference'] = '%(time_reference)s';


/*
|--------------------------------------------------------------------------
| Rewrite PHP Short Tags
|--------------------------------------------------------------------------
|
| If your PHP installation does not have short tag support enabled CI
| can rewrite the tags on-the-fly, enabling you to utilize that syntax
| in your view files.  Options are TRUE or FALSE (boolean)
|
*/
$config['rewrite_short_tags'] = %(rewrite_short_tags)s;


/*
|--------------------------------------------------------------------------
| Reverse Proxy IPs
|--------------------------------------------------------------------------
|
| If your server is behind a reverse proxy, you must whitelist the proxy IP
| addresses from which CodeIgniter should trust the HTTP_X_FORWARDED_FOR
| header in order to properly identify the visitor's IP address.
| Comma-delimited, e.g. '10.0.1.200,10.0.1.201'
|
*/
$config['proxy_ips'] = '%(proxy_ips)s';


/* End of file config.php */
/* Location: ./application/config/config.php */
""" % variables
