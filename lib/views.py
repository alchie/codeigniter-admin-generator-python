import sqlite
from lib.mysql import MySQLConnect
import operator, json

class View:
	
	mvc_settings = None
	table_settings = None
	content = None
	dbname = None
	tblname = None
	fields = None
	filters_data = {}
	filename = None
	
	def __init__(self, filename, db, table):
		self.dbname = db
		self.tblname = table
		self.filename = filename
		self.filters_data = {}

		self.mvc_settings = sqlite.mvcSettings(filename,db)
		self.table_settings = sqlite.tableSettings(filename,db, table)
		self.table_children = sqlite.tableChildren(filename,db, table)
	
	def get(self):	
		content = self.template_content({
			'view_content' : self.get_view_content(),
			'footer_content' : self.get_footer_content()
		})
		
		return content
		
	def get_view_content(self):
		
		content = ''
		content += self.the_content( self.tblname )
		
		return content


	def get_variables(self, dbname, tblname):
		table_settings = sqlite.tableSettings(self.filename, dbname, tblname)
		
		table_name = tblname
		variables = { 'original_table_name' : table_name }
		for table_key in table_settings.get_keys():
			variables['table_'+str(table_key[2])] = str(table_key[3])
		
		table_prefix = table_settings.get_value('table_prefix')
		if table_prefix != '' and table_prefix != None:
			table_name = table_name[len(table_prefix):]
		
		variables['table_name'] = table_name
		mysql = MySQLConnect( self.filename, dbname )
		fields = mysql.fieldList( tblname )
		
		table_fields = []
		fields_raw = {}
		n=1000
		if fields:
			for field in fields:
				fieldSettings = sqlite.fieldSettings(self.filename, dbname, tblname, field)
				
				field_variables = { 'field_name' : field }
				for field_key in fieldSettings.get_keys():
					field_variables[str(field_key[3])] = '' if str(field_key[4]) == 'none' else str(field_key[4])
				
				try:
					list_column_order = int( fieldSettings.get_value('list_column_order') )
				except:
					list_column_order = 0	
				
				if not list_column_order in fields_raw:
					fields_raw[list_column_order] = field_variables
				else:
					fields_raw[n] = field_variables
				n=n+1
		
		fields_sorted = sorted(fields_raw.items(), key=operator.itemgetter(0))
		
		for index, field in fields_sorted:
			table_fields.append( field )
			
		variables['table_fields'] = table_fields
		
		return variables
		
	def the_content(self, tblname, child=False):
		
		variables = self.get_variables(self.dbname, tblname)
		content = '';
		
		if child:
			content += """
			<?php if( isset($admin_access->controller_%(table_name)s) ) { ?>
			<div class="tab-content tab-content-%(table_name)s" style="display:none">""" % variables
		
		content += self.list_content( variables, child=child )
		content += self.add_content( variables, child=child )
		content += self.edit_content( variables, child=child )
		
		if child:
			content += """</div><!-- .tab-content .tab-content-%(table_name)s -->
			<?php } ?>""" % variables
			
		return content
	
	def column_list(self, variables, output, child=False):
		
		field_names = []
		list_columns = []
		column_names = []
		add_fields = []
		edit_fields = []
		
		if child:
			child_key = self.table_children.get_value(variables['original_table_name'], 'child_key')
		
		fields_raw = {}
		
		n=1000
		for field in variables['table_fields']:		
			
			field_names.append(field.get('field_name'))
			
			if field.get('add_page', 0) == '1' and field.get('form_input_type') != 'view_only':
				add_fields.append(field.get('field_name'))
				
			if field.get('edit_page', 0) == '1' and field.get('form_input_type') != 'view_only':
				if field.get('edit_change_deliberately') != '1' or field.get('required') == '1':
					edit_fields.append(field.get('field_name'))
			
			if field.get('form_input_type') == 'password' and field.get('password_type_confirm') == '1':
				field_names.append(field.get('password_type_name'))
				
				if field.get('add_page', 0) == '1':
					add_fields.append(field.get('password_type_name'))
					
				if field.get('edit_page', 0) == '1' and field.get('edit_change_deliberately') != '1':
					edit_fields.append(field.get('password_type_name'))
				
			if field.get('list_page', 0) == '1':
				list_field_name = field.get('field_name')
				linked_table = ''
				if child and child_key == field.get('field_name'):
					continue
				
				if field.get( 'list_show_linked_table_value' ) == '1':
					if field.get('linked_table_custom') != None:
						list_field_name = field.get('linked_table_custom')
					if field.get('linked_table_name') != None:
						linked_table = "data-linked='%s'" % field.get('linked_table_name')
					
				column_list = '<th width="%s">' % field.get('list_column_width')
				
				if field.get( 'list_filterable' ) == '1':
					column_list += """<div class="dropdown-filter"><a href="javascript:void(0);" data-filter="%s" data-table="%s">""" % (field['field_name'], variables['table_name'])
					
				column_list += field.get( 'field_title' )
				
				if field.get( 'list_searchable' ) == '1':
					field['table_name'] = variables['table_name']
					field['list_field_name'] = list_field_name
					field['linked_table_attr'] = linked_table
					column_list += """<span %(linked_table_attr)s data-key="%(list_field_name)s" data-table="%(table_name)s" id="list_search_button_%(list_field_name)s" class="btn btn-primary btn-xs pull-right btn-search list-search-%(table_name)s" title="Search %(field_title)s">
		<i class="fa fa-search"></i></span>""" % field
				
				if field.get( 'list_filterable' ) == '1':
					column_list += ' <span class="glyphicon glyphicon-chevron-down pull-right"></span></a></div>'
					list_field_name = field.get('field_name')
				
				column_list += '</th>'
				list_columns.append( column_list )
				column_names.append( list_field_name )	
		
		if output == 'list_fields' and len( list_columns ) > 0 :
			return ''.join( list_columns ) 
		elif output == 'add_fields' and len( add_fields ) > 0 :
			return '"' + '","'.join( add_fields ) + '"' 
		elif output == 'edit_fields' and len( edit_fields ) > 0 :
			return '"' + '","'.join( edit_fields ) + '"' 
		elif output == 'current_fields' and len( field_names ) > 0 :
			return '"' + '","'.join( field_names ) + '"' 
		elif output == 'current_list_fields' and len( column_names ) > 0 :
			return '"' + '","'.join( column_names ) + '"'
		
		return ''
			
			
	def list_content(self, variables, child=False):

		if child:
			button_title = self.table_children.get_value(variables['original_table_name'], 'button_title')
			if button_title != None and button_title != '':
				variables['table_singular'] = button_title
		
		variables['table_active_column'] = ""
		if variables.get('table_active_field') != 'none' and variables.get('table_active_field') != '':
			variables['table_active_column'] = "<th width='1%'></th>"
			
		variables['list_fields'] = self.column_list( variables, 'list_fields', child=child )
		
		actions_label = 'Action'
		actions_len = '66'
		
		variables['add_button'] = '&nbsp;'
		if variables.get('table_actions_add') == '1':
			
			if variables.get('table_upload_table') == '1':
				variables['add_button'] = """
			<?php if( isset($admin_access->controller_%(table_name)s->can_add) && ($admin_access->controller_%(table_name)s->can_add == 1) ) { ?>
			<a href="javascript:void(0);" class="btn btn-default btn-sm pull-right" id="add-button-%(table_name)s">Upload %(table_singular)s</a>
			<?php } ?>""" % variables
			else:
				variables['add_button'] = """
			<?php if( isset($admin_access->controller_%(table_name)s->can_add) && ($admin_access->controller_%(table_name)s->can_add == 1) ) { ?>
			<a href="javascript:void(0);" class="btn btn-default btn-sm pull-right" id="add-button-%(table_name)s">Add %(table_singular)s</a>
			<?php } ?>""" % variables
		
		if( variables.get('table_actions_edit') == '1' and variables.get('table_actions_delete') == '1'):
			actions_label = 'Actions'
			actions_len = '130'
		
		if( variables.get('table_actions_edit') == '1' or variables.get('table_actions_delete') == '1'):
			variables['list_fields'] = variables['list_fields'] + '<th width="'+actions_len+'">'+actions_label+'</th>'
		
		
		content = """<div id="list-view-%(table_name)s" class="list-view">
<div class="panel panel-default panel-%(table_name)s">
<div class="panel-heading">
%(add_button)s
<div class="clearfix"></div>
</div>
<!-- /.panel-heading -->
<div class="panel-body">
<div class="table-responsive">
<table id="dataTables" class="table table-striped table-bordered table-hover">
<thead>
<tr>
%(table_active_column)s
%(list_fields)s
</tr>
</thead>
<tbody>
</tbody>
</table>
</div>
</div> <!-- .panel-body -->
</div> <!-- .panel .panel-%(table_name)s -->
</div>""" % variables

		return content

	def add_content(self, variables, child=False):
		content = """
		<?php if( isset($admin_access->controller_%(table_name)s->can_add) && ($admin_access->controller_%(table_name)s->can_add == 1) ) { ?>
		<div id="add-view-%(table_name)s" style="display:none">
<div class="panel panel-default add-panel-%(table_name)s">
                        <div class="panel-heading">""" % variables

		if variables.get('table_upload_table') == '1':
			content += """
 <div class="btn btn-success btn-xs btn-file pull-right" id="input-file-%(table_name)s">
    Select %(table_plural)s<input type="file" multiple>
  </div>
""" % variables
			content += """<h3 class="panel-title">Upload %(table_plural)s</h3>""" % variables
		else:
			content += """<h3 class="panel-title">Add %(table_singular)s</h3>""" % variables
			
		content += """<div class="clearfix"></div>
                        </div>
                        <!-- /.panel-heading -->
<div class="panel-body">
""" % variables

		if variables.get('table_upload_table') == '1':
			content += """
			<div class="row" id="fileupload-%(table_name)s-preview" style="margin-top:20px;min-height:200px;"></div>
			
</div> <!-- .panel-body -->

<div class="panel-footer">
<button class="btn btn-success btn-sm action-button" id="upload-action-%(table_name)s" data-table="%(table_name)s">Upload All</button>
<a href="javascript:void(0)" class="btn btn-danger btn-sm upload-back-button" id="upload-back-%(table_name)s">Back</a>
<div class="clearfix"></div>
</div> <!-- .panel-footer -->
</div><!-- .panel .panel-%(table_name)s -->
</div>
<?php } ?>""" % variables

		else:
			for field in variables['table_fields']:
				if field.get('add_page', 0) == '1':
					content += self.form_input(field, variables, child=child)

			content += """
</div> <!-- .panel-body -->

<div class="panel-footer">
<label class="pull-right">\n<input checked="" type="checkbox" class="add returnToList"> On submit, return to list</label>
<button class="btn btn-success btn-sm action-button" id="add-action-%(table_name)s">Submit</button>
<a href="javascript:void(0)" class="btn btn-danger btn-sm add-back-button" id="add-back-%(table_name)s">Back</a>
<div class="clearfix"></div>
</div> <!-- .panel-footer -->
</div><!-- .panel .panel-%(table_name)s -->
</div>
<?php } ?>""" % variables

		return content

	def edit_content(self, variables, child=False):
		
		if child:
			variables['children_navigation'] = ''
			variables['parent_tab_content'] = ''
		else:
			variables['children_navigation'] = self.get_children_navigation( variables )
			variables['parent_tab_content'] = 'parent'
		
		content = """<?php if( isset($admin_access->controller_%(table_name)s->can_edit) && ($admin_access->controller_%(table_name)s->can_edit == 1) ) { ?>
		<div id="edit-view-%(table_name)s" style="display:none">
		%(children_navigation)s
		""" % variables
		
		if not child:
			content += """<div class="tab-content tab-content-%(table_name)s %(parent_tab_content)s active">""" % variables
		
		content += """<div class="panel panel-default edit-panel-%(table_name)s">
<div class="panel-heading">
	 <h3 class="panel-title">Edit %(table_singular)s</h3>
	 <div class="clearfix"></div>
</div>
<!-- /.panel-heading -->
<div class="panel-body">
""" % variables

		for field in variables['table_fields']:
			if field.get('edit_page', 0) == '1':
				content += self.form_input(field, variables, True, child=child)

		content += """
</div> <!-- .panel-body -->
<div class="panel-footer">
<label class="pull-right">\n<input checked="" type="checkbox" class="returnToList"> On submit, return to list</label>
<button class="btn btn-success btn-sm action-button" id="update-action-%(table_name)s">Submit</button>
<a href="javascript:void(0)" class="btn btn-danger btn-sm update-back-%(table_name)s" id="update-back-%(table_name)s">Back</a>
<div class="clearfix"></div>
</div> <!-- .panel-footer -->
</div><!-- .panel .panel-%(table_name)s -->
""" % variables

		if not child:
			content += """</div><!-- .tab-content .tab-content-%(table_name)s -->""" % variables
			children = self.table_children.item_list()
			for child in children:
				content += self.the_content( child[2], True )
		
		content += "</div>\n<?php } ?>"
		return content


	def get_footer_content(self):
		variables = self.get_variables(self.dbname,  self.tblname )
		ts_content = self.table_settings_content( self.tblname )
		filters_data = json.dumps( self.filters_data ) #",\n".join( self.filters_data )
		children = self.table_children.item_list()
		for child in children:
			ts_content += self.table_settings_content( child[2], True )
			
		content = """<script>
jQuery(document).ready(function($) {
	$(document).AdminActions({
		baseURL : '<?php echo base_url(); ?>',
		current_table : '%(table_name)s',
		tables : { %(ts_content)s },
		filters_data : %(filters_data)s,
	});
});
</script>""" % { 'table_name' : variables['table_name'], 'ts_content' : ts_content, 'filters_data' : filters_data }

		return content

	def add_filter_data( self, table, field, key, value ):
		
		if not field in self.filters_data:
			self.filters_data[field] = {}
			
		self.filters_data[field][str(key)] = str(value)
		
	def table_settings_content(self, table, child=False):
		variables = self.get_variables(self.dbname, table)
		variables['current_fields'] = self.column_list( variables, 'current_fields', child=child ) 
		variables['add_fields'] = self.column_list( variables, 'add_fields', child=child ) 
		variables['edit_fields'] = self.column_list( variables, 'edit_fields', child=child ) 
		variables['current_list_fields'] = self.column_list( variables, 'current_list_fields', child=child )  
		variables['current_table_filters'] = self.get_table_filters(variables['table_fields'])
		variables['actual_values'] = self.get_actual_values(variables['table_fields'])
		variables['check_cross'] = self.get_check_cross(variables['table_fields'])
		variables['table_active_field'] = variables.get('table_active_field', '') 
				
		attr = []
		
		if 'table_singular' in variables and variables['table_singular'] != '':
			attr.append( "label : '%s'" % variables.get('table_singular', '') )
		
		if 'current_fields' in variables and variables['current_fields'] != '':
			attr.append( "fields : [%s]" % variables.get('current_fields', '') )

		if 'add_fields' in variables and variables['add_fields'] != '':
			attr.append( "add_fields : [%s]" % variables.get('add_fields', '') )

		if 'edit_fields' in variables and variables['edit_fields'] != '':
			attr.append( "edit_fields : [%s]" % variables.get('edit_fields', '') )
							
		if 'table_listlimit' in variables and variables['table_listlimit'] != '':
			attr.append( "list_limit : %s" % variables.get('table_listlimit', '') )
		
		if 'current_list_fields' in variables and variables['current_list_fields'] != '':
			attr.append( "list_fields : [%s]" % variables['current_list_fields'] )
		
		if 'table_order_by' in variables and variables['table_order_by'] != '':
			attr.append( "order_by : '%s'" % variables['table_order_by'] )
		
		if 'table_order_orientation' in variables and variables['table_order_orientation'] != '':
			attr.append( "order_sort : '%s'" % variables['table_order_orientation'] )
		
		if 'current_table_filters' in variables and variables['current_table_filters'] != '':
			attr.append( "filters : {%s}" % variables['current_table_filters'] )
		
		if 'table_primary_key' in variables and variables['table_primary_key'] != '' and variables['table_primary_key'] != 'none':
			attr.append( "primary_key : '%s'" % variables['table_primary_key'] )
		
		if 'table_primary_title' in variables and variables['table_primary_title'] != '' and variables['table_primary_title'] != 'none':
			attr.append( "primary_title : '%s'" % variables['table_primary_title'] )
		
		if 'table_active_field' in variables and variables['table_active_field'] != '' and variables['table_active_field'] != 'none':
			attr.append( "active_key : '%s'" % variables['table_active_field'] )
		
		if 'actual_values' in variables and variables['actual_values'] != '':
			attr.append( "actual_values : {%s}" % variables['actual_values'] )

		if 'check_cross' in variables and variables['check_cross'] != '':
			attr.append( "check_cross : [%s]" % variables['check_cross'] )
			
		if child:
			required_key = self.table_children.get_value(table, 'parent_key')
			required_value = self.table_children.get_value(table, 'child_key')
			required_table = self.table_children.get_value(table, 'table_name_custom')
			attr.append( "required_key : '%s'" % required_key )
			attr.append( "required_value : '%s'" % required_value )
			if required_table != None:
				attr.append( "required_table : '%s'" % required_table )
		
		if variables.get('table_actions_edit') == '1':
			attr.append( "actions_edit : <?php echo ($admin_access->controller_%(table_name)s->can_edit) ? 1 : 0; ?>" % variables )
		
		if variables.get('table_actions_delete') == '1':
			attr.append( "actions_delete : <?php echo ($admin_access->controller_%(table_name)s->can_delete) ? 1 : 0; ?>" % variables )
		
		if variables.get('table_upload_table') == '1':
			attr.append( """upload_settings : {
				allowed_types : '%(allowed_types)s',
				max_size : '%(max_size)s', 
				upload_url : '%(upload_url)s',
				is_image : '%(is_image)s',
				file_name : '%(file_name)s',
			}""" % {
			'allowed_types' : variables['table_upload_config_allowed_types'],
			'max_size' : variables['table_upload_config_max_size'],
			'upload_url' : variables['table_upload_config_upload_url'],
			'is_image' : variables['table_upload_field_is_image'],
			'file_name' : variables['table_upload_field_file_name'],
			})
			
		content = """
		<?php if( isset($admin_access->controller_%s) ) { ?>
		\n'%s' : { %s },\n
		<?php } ?>
		""" % (variables['table_name'], variables['table_name'], ",\n".join(attr))
		return content
	
	def get_table_filters(self, fields):
		content = ""
		list_filters = []
		for field in fields:
			if field.get("list_filterable") == "1" or field.get("list_filter_value") == "1":
				form_input_type = field.get("form_input_type")
				filter_type = field.get(form_input_type+"_type_source", '')
				
				if filter_type == None or filter_type == '':
					continue
					
				field['filter_type'] = filter_type.lower()
				
				field['anchor_filter_value'] = field.get("anchor_filter_value", '0')
				
				
				filter_line = ''
				if filter_type == 'TABLE':
					
					filter_line += '"' + field.get("field_name") + '":'
					
					d_table = field.get(form_input_type+"_type_table_name")
					tableSettings = sqlite.tableSettings(self.filename, self.dbname,  field.get(form_input_type+"_type_table_name"))
					table_prefix = tableSettings.get_value("table_prefix")
					if table_prefix != '' and table_prefix != None:
						d_table = d_table[len(table_prefix):]
					
					field['filter_table'] = d_table
					field['filter_table_key'] = field.get(form_input_type+"_type_table_key")
					field['filter_table_value'] = field.get(form_input_type+"_type_table_value")
					
					field['filter_table_results'] = field.get(form_input_type+"_type_table_filter", '0')
					field['filter_table_results_key'] = field.get(form_input_type+"_type_table_filter_key", '')
					field['filter_table_results_value'] = field.get(form_input_type+"_type_table_filter_value", '')
					
					field['filter_table_order'] = field.get(form_input_type+"_type_table_order", '0')
					field['filter_table_order_by'] = field.get(form_input_type+"_type_table_order_by", '')
					field['filter_table_order_sort'] = field.get(form_input_type+"_type_table_order_sort", '')
					
					filter_line += '{"type":"%(filter_type)s","anchor":%(anchor_filter_value)s,"table":"%(filter_table)s","key":"%(filter_table_key)s","value":"%(filter_table_value)s", "filter" : %(filter_table_results)s, "filter_key" : "%(filter_table_results_key)s", "filter_value" : "%(filter_table_results_value)s", "order" : %(filter_table_order)s, "order_by" : "%(filter_table_order_by)s", "order_sort" : "%(filter_table_order_sort)s" }' % field
					
				elif filter_type == 'MANUAL':
					filter_line += '"' + field.get("field_name") + '":'
					filter_line += '{"type":"%(filter_type)s","anchor":%(anchor_filter_value)s}' % field
				
				list_filters.append( filter_line)
		return ','.join( list_filters ) 	
		
	def form_input(self, field, variables, edit=False, child=False):
		
		def none(variables):
			return ""
		
		def hidden(variables):				
			return """\n<input data-type="%(form_input_type)s" type="hidden" name="%(field_name)s" id="%(input_id)s" class="%(input_class)s" placeholder="%(field_title)s" value="" />""" % variables

		def view_only( variables ):
					
			field_settings_var = variables['field_settings']
			
			content = """\n<div class="form-group">\n<label for="%(input_id)s">%(field_title)s</label> """ % variables
			content += """\n<p class="text-value %(table_name)s %(field_name)s" data-type="%(form_input_type)s"></p>""" % variables
			content +=  "\n</div>" 
			return content
			
		def text(variables):
					
			field_settings_var = variables['field_settings']
			
			variables['text_type_value'] = ''
			variables['text_extra'] = ''
			
			if field_settings_var.get("text_type_default") == 'TEXT_VALUE':
				variables['text_type_value'] = field_settings_var.get("text_type_default_value")
			elif field_settings_var.get("text_type_default") == 'CURRENT_TIMESTAMP':
				variables['input_class'] += ' datetimepicker'
				variables['text_type_value'] = ''
			elif field_settings_var.get("text_type_default") == 'UNIQUE_ID':
				variables['text_type_value'] = ''
			
			content = """\n<div class="form-group">\n<label for="%(input_id)s">%(field_title)s</label> """ % variables
			if field_settings_var.get("text_type_search_linked_table") == '1' and field_settings_var.get("linked_table") == '1':
				table_name, table_name_custom, table_key, table_value, table_value_custom = (field_settings_var.get("linked_table_name"), field_settings_var.get("linked_table_custom_table"), field_settings_var.get("linked_table_key"), field_settings_var.get("linked_table_value"), field_settings_var.get("linked_table_custom"))
				if table_name != None and table_name != '' and table_key != None and table_key != '' and table_value != None and table_value != '':
					linked_variables = self.get_variables(self.dbname, table_name)
					variables['text_extra'] = variables['text_extra'] + ' data-table="%s" data-key="%s" data-value="%s" data-display="%s" data-action="%s" ' % ( linked_variables['table_name'], table_key, table_value, table_value_custom, variables['action_class'])
					variables['input_class'] = variables['input_class'] + " text-searchable-key-" + variables['field_name']
				content += """\n<input data-type="%(form_input_type)s" type="hidden" name="%(field_name)s" id="%(input_id)s" class="form-control %(input_class)s  %(action_class)s text-searchable-key" />""" % variables
				content += """\n<a href="javascript:void(0)" data-field="%(field_name)s" %(text_extra)s class="text-searchable-list %(field_name)s" data-toggle="modal" data-target="#%(action_class)s-text-searchable-box-%(field_name)s"><span class="glyphicon glyphicon-list"></span></a>
				<input data-type="%(form_input_type)s" type="text" name="%(field_name)s" class="form-control %(action_class)s text-searchable %(field_name)s" placeholder="Search %(field_title)s" data-field="%(field_name)s" %(text_extra)s/>""" % variables
				content += """
				<div class="modal fade %(action_class)s" id="%(action_class)s-text-searchable-box-%(field_name)s" tabindex="-1" role="dialog" aria-labelledby="%(field_title)s" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="myModalLabel">%(field_title)s List</h4>
      </div>
      <div class="modal-body">
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
				""" % variables
			else:
				content += """\n<input data-type="%(form_input_type)s" type="text" name="%(field_name)s" id="%(input_id)s" class="form-control %(input_class)s" placeholder="%(field_title)s" value="%(text_type_value)s"/>""" % variables
			
			content +=  "\n</div>" 
			return content
		
		def textarea(variables):
			field_settings_var = variables['field_settings']
			variables['textarea_rows'] = field_settings_var.get('textarea_type_rows', '5')
			variables['textarea_wysiwyg'] = field_settings_var.get('textarea_type_wysiwyg', '0')
			content = """\n<div class="form-group">\n<label for="%(input_id)s">%(field_title)s</label>""" % variables
			content += """\n<textarea rows="%(textarea_rows)s" data-type="%(form_input_type)s" data-wysiwyg="%(textarea_wysiwyg)s" type="text" name="%(field_name)s" id="%(input_id)s" class="form-control %(input_class)s" placeholder="%(field_title)s" value="" /></textarea>""" % variables
			
			if field_settings_var.get('textarea_type_wysiwyg') == '1':
				content += """\n<script><!-- 
				$(function(){
					CKEDITOR.replace( '%(input_id)s' );
				});
				--></script>""" % variables
			content +=  "\n</div>" 
			return content
			
		def password(variables):
							
			content = """\n<div class="form-group">\n<label for="%(input_id)s">%(field_title)s</label>\n""" % variables
			content += """\n<input data-type="%(form_input_type)s" type="password" name="%(field_name)s" id="%(input_id)s" class="form-control %(input_class)s" placeholder="%(field_title)s" value="" />""" % variables
			content +=  "\n</div>" 
			
			field_settings_var = variables['field_settings']
			
			if field_settings_var.get("password_type_confirm") == '1':
				variables['confirm_name'] = field_settings_var.get("password_type_name", "confirm_password")
				variables['confirm_label'] = field_settings_var.get("password_type_label", "Confirm Password")
				variables['field_name'] = variables['confirm_name']
				variables['input_id'] = "%(action_class)s_%(table_name)s_%(field_name)s" % variables
				variables['input_class'] = "%(action_class)s_%(table_name)s_%(field_name)s %(table_name)s-input  table-%(table_name)s %(action_class)s-table-%(table_name)s %(form_input_type)s %(form_input_class)s" % variables 
				
				content += """\n<div class="form-group">\n<label for="%(input_id)s">%(confirm_label)s</label>\n""" % variables
				content += """\n<input data-type="%(form_input_type)s" type="password" name="%(field_name)s" id="%(input_id)s" class="form-control %(input_class)s" placeholder="%(confirm_label)s" value="" />""" % variables
				content +=  "\n</div>"
								
			return content
			
		def dropdown(variables):
			
			field_settings_var = variables['field_settings']
			dropdown_type = field_settings_var.get('dropdown_type_source')
			
			variables['select_extra'] = ""
			if dropdown_type == 'TABLE':
				
				table_name = field_settings_var.get("dropdown_type_table_name")
				
				if table_name != None and table_name != '':
					d_table = table_name
					dropdown_table = sqlite.tableSettings(self.filename, self.dbname, table_name)
					table_prefix = dropdown_table.get_value("table_prefix")
					if table_prefix != '' and table_prefix != None:
						d_table = d_table[len(table_prefix):]
					
					field_settings_var['dropdown_table'] = d_table
					
				variables['input_class'] += " dropdown-table"
				
				variables['select_extra'] = " data-type=\"%(form_input_type)s\" data-label=\"%(field_title)s\" data-field=\"%(field_name)s\" data-table=\"%(dropdown_table)s\" data-key=\"%(dropdown_type_table_key)s\" data-value=\"%(dropdown_type_table_value)s\"" % field_settings_var
				
				variables['select_extra'] += " data-filter=\"%(dropdown_type_table_filter)s\" data-filter-key=\"%(dropdown_type_table_filter_key)s\" data-filter-value=\"%(dropdown_type_table_filter_value)s\"" % field_settings_var
				
				variables['select_extra'] += " data-order=\"%(dropdown_type_table_order)s\" data-order-by=\"%(dropdown_type_table_order_by)s\" data-order-sort=\"%(dropdown_type_table_order_sort)s\"" % field_settings_var
				
			content = """\n<div class="form-group">\n<label for="add_lessons_%(field_name)s">%(field_title)s</label> 
			<select name="%(field_name)s" id="%(input_id)s" class="selectpicker form-control %(input_class)s" placeholder="%(field_title)s" data-live-search="true" %(select_extra)s>
			<option value="">- - Select %(field_title)s - -</option>\n""" % variables
			
			if dropdown_type == 'MANUAL':
				dropdownfieldItems = sqlite.fieldItems( self.filename, self.dbname, variables['original_table_name'], variables['field_name'], 'dropdown')
				
				for item in dropdownfieldItems.item_list():
					content += """<option value="%(value)s">%(label)s</option>\n""" % { "label" : item[6] , "value" : item[4] }
					self.add_filter_data( variables['table_name'], variables['field_name'], item[4], item[6] )
			
			
			content +=  "</select></div>"
			return content
			
		def checkbox(variables):
			content = """\n<div class="form-group"><strong>%(field_title)s</strong>""" % variables
			
			field_settings_var = variables['field_settings']
			current_form_type = field_settings_var.get('checkbox_type_source')
			
			if current_form_type == 'TABLE':
				
				table_name = field_settings_var.get("checkbox_type_table_name")
				
				if table_name != None and table_name != '':
					c_table = table_name
					checkbox_table = sqlite.tableSettings(self.filename, self.dbname, table_name)
					table_prefix = checkbox_table.get_value("table_prefix")
					if table_prefix != '' and table_prefix != None:
						c_table = c_table[len(table_prefix):]
					
					field_settings_var['checkbox_table'] = c_table
					
				variables['input_class'] += " checkbox-table"
				variables['checkbox_extra'] = " data-type=\"%(form_input_type)s\" data-source=\"table\" data-label=\"%(field_title)s\" data-field=\"%(field_name)s\" data-table=\"%(checkbox_table)s\" data-key=\"%(checkbox_type_table_key)s\" data-value=\"%(checkbox_type_table_value)s\"" % field_settings_var
				
				content += "\n<div id=\"%(input_id)s\" class=\"%(input_class)s\"%(checkbox_extra)s></div>"  % variables
				
			if current_form_type == 'MANUAL':
				currentfieldItems = sqlite.fieldItems( self.filename, self.dbname, variables['original_table_name'], variables['field_name'], 'checkbox')
				
				for item in currentfieldItems.item_list():
					variables['current_item_label'] = item[6]
					variables['current_item_value'] = item[4]
					content += """\n<div class="checkbox">\n<label>\n<input data-type="%(form_input_type)s" type="checkbox" name="%(field_name)s" id="%(input_id)s" class="%(input_class)s" placeholder="%(field_title)s" value="%(current_item_value)s" />%(current_item_label)s</label></div>""" % variables
					self.add_filter_data( variables['table_name'], variables['field_name'], item[4], item[6] )
					
			content +=  "</div>" 
			return content
			
		def radio(variables):
			content = """\n<div class="form-group"><strong>%(field_title)s</strong>""" % variables
			
			field_settings_var = variables['field_settings']
			current_form_type = field_settings_var.get('radio_type_source')
			
			if current_form_type == 'TABLE':
				
				table_name = field_settings_var.get("radio_type_table_name")
				
				if table_name != None and table_name != '':
					r_table = table_name
					radio_table = sqlite.tableSettings(self.filename, self.dbname, table_name)
					table_prefix = radio_table.get_value("table_prefix")
					if table_prefix != '' and table_prefix != None:
						r_table = r_table[len(table_prefix):]
					
					field_settings_var['radio_table'] = r_table
					
				variables['input_class'] += " radio-table"
				variables['radio_extra'] = " data-type=\"%(form_input_type)s\" data-source=\"table\" data-label=\"%(field_title)s\" data-field=\"%(field_name)s\" data-table=\"%(radio_table)s\" data-key=\"%(radio_type_table_key)s\" data-value=\"%(radio_type_table_value)s\"" % field_settings_var
				
				content += "\n<div id=\"%(input_id)s\" class=\"%(input_class)s\"%(radio_extra)s></div>"  % variables
				
			if current_form_type == 'MANUAL':
				currentfieldItems = sqlite.fieldItems( self.filename, self.dbname, variables['original_table_name'], variables['field_name'], 'radio')
				
				for item in currentfieldItems.item_list():
					variables['current_item_label'] = item[6]
					variables['current_item_value'] = item[4]
					content += """\n<div class="radio">\n<label>\n<input data-type="%(form_input_type)s" type="radio" name="%(field_name)s" id="%(input_id)s" class="%(input_class)s" placeholder="%(field_title)s" value="%(current_item_value)s" />%(current_item_label)s</label></div>""" % variables
					self.add_filter_data( variables['table_name'], variables['field_name'], item[4], item[6] )
			
			content +=  "\n</div>" 
			return content
			
		def library(variables):
			content = """\n<div class="form-group">\n<label for="add_lessons_%(field_name)s">%(field_title)s</label> """ % variables
			content += """\n<input data-type="%(form_input_type)s" type="text" name="%(field_name)s" id="%(input_id)s" class="%(input_class)s" placeholder="%(field_title)s" value="" />""" % variables
			content +=  "\n</div>" 
			return content
			
		inputs = {
			"none" : none,
			"hidden" : hidden,
			"text" : text,
			"textarea" : textarea, 
			"password" : password, 
			"dropdown" : dropdown, 
			"checkbox" : checkbox, 
			"radio" : radio, 
			"library" : library,
			"view_only" : view_only,
		}
		
		form_input_type = 'none' if field.get('form_input_type','none') == '' else field.get('form_input_type','none')
		
		if child:
			child_key = self.table_children.get_value(variables['original_table_name'], 'child_key')
			if child_key == field.get('field_name'):
				form_input_type = 'hidden'
			
		variables['form_input_type'] = form_input_type
		variables['field_settings'] = field
		variables['field_name'] = field.get('field_name','')
		variables['field_title'] = field.get('field_title','')
		variables['form_input_class'] = field.get('form_input_class_edit','text') if edit else field.get('form_input_class_add','text')
		variables['action_class'] = 'edit' if edit else 'add'
		variables['input_id'] = "%(action_class)s_%(table_name)s_%(field_name)s" % variables
		variables['input_class'] = "%(action_class)s_%(table_name)s_%(field_name)s %(table_name)s-input  table-%(table_name)s %(action_class)s-table-%(table_name)s %(form_input_type)s %(form_input_class)s" % variables 
		
		content = ""
		
		if field.get('edit_change_deliberately') == '1' and edit:
			
			variables['data_field_names'] = field.get("field_name")
			if form_input_type == 'password':
				variables['data_field_names'] = variables['data_field_names'] + "|" + field.get("password_type_name", "confirm_password")
			
			content += """\n<div class="form-group change-deliberately button %(table_name)s %(field_name)s">\n<label>%(field_title)s</label>
			<a href="javascript:void(0);" class="btn btn-xs btn-success pull-right update %(table_name)s %(field_name)s" data-table="%(table_name)s" data-field="%(data_field_names)s">Update %(field_title)s</a>
			<p class="text-value %(table_name)s %(field_name)s %(input_class)s" data-type="%(form_input_type)s"></p>
			""" % variables
			
			if field.get('required') == '1':
				content += inputs['hidden']( variables )
			
			content += """</div>
			<div class="panel panel-info change-deliberately form %(table_name)s %(field_name)s" style="display:none;">
			<div class="panel-heading">
			<a href="javascript:void(0);" class="btn btn-xs btn-danger pull-right cancel %(table_name)s %(field_name)s" data-table="%(table_name)s" data-field="%(field_name)s">Cancel</a>
			Update %(field_title)s</div>
			<div class="panel-body">
			""" % variables
		
		content += inputs[form_input_type]( variables )
		
		if field.get('edit_change_deliberately') == '1' and edit:
			content += "</div>\n</div>\n"
			
		return content 	

	def get_children_navigation(self, variables):
		parent_variables = self.get_variables(self.dbname, self.tblname)
		
		if parent_variables.get('table_has_children') != '1':
			return ''
		
		content = """<ul class="nav nav-tabs">\n"""
		content += """<li>
		<a href="javascript:void(0);" class="update-back-%(table_name)s" id="update-back-%(table_name)s">
		<span class="glyphicon glyphicon-arrow-left"></span>
		</a>
		</li>""" % parent_variables
		content += """<li class="parent-tab active">
		<a class="tab-item" href="javascript:void(0);" data-table="%(table_name)s" data-child="0">Edit %(table_singular)s</a>
		</li>""" % parent_variables
		
		children = self.table_children.item_list()
		for child in children:
			child_variables = self.get_variables(self.dbname, child[2])
			child_variables['child_table_title'] = self.table_children.get_value(child[2], "table_title")
			content += """
			<?php if( isset($admin_access->controller_%(table_name)s) ) { ?>
			<li>
			<a class="tab-item" href="javascript:void(0);"  data-table="%(table_name)s" data-child="1">%(child_table_title)s</a>
			</li>
			<?php } ?>""" % child_variables
			
		content += """\n</ul><br>"""
		return content

	def get_actual_values(self, fields):
		content = []
		for field in fields:
			show_value = field.get("list_show_linked_table_value")
			field_name = field.get("field_name")
			field_value = field.get("linked_table_custom")
			if field_value == '':
				field_value = field.get("linked_table_value")
			if show_value == "1":
				content.append('"%(field_name)s" : "%(field_value)s"' % {"field_name" : field_name, "field_value" : field_value}) 
		return ",".join(content)

	def get_check_cross(self, fields):
		content = []
		for field in fields:
			if field.get("list_check_cross") == "1":
				content.append( '"%s"' % field.get("field_name") )
		return ",".join( content )

	def template_content(self, variables):
		return """<?php $this->load->view('common_header'); ?>
<div class="row"><div class="col-lg-12">
	<h1 class="page-header"><?php echo $page_title; ?></h1>
	
	%(view_content)s
	
	%(footer_content)s

</div></div><!-- .row . col-lg-12 -->
<?php $this->load->view('common_footer'); ?>""" % variables
