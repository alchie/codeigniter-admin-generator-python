import sqlite
from lib.mysql import MySQLConnect

class Menu:
	
	mvc_settings = None
	menu_settings = None
	table_settings = None
	dbname = None
	filename = None
	
	def __init__(self, filename, db):
		self.dbname = db
		self.filename = filename
		self.mvc_settings = sqlite.mvcSettings(filename,db)
		self.menu_settings = sqlite.menuSettings(filename,db)
		self.table_settings = sqlite.tableSettings(filename,db)
			
	def get(self):
		content = """<li <?php echo ($main_page == 'dashboard') ? 'class="active"' : ''; ?>>
    <a id="menu-dashboard" href="<?php echo site_url('dashboard'); ?>" title="Dashboard"><i class="fa fa-dashboard fa-fw"></i> Dashboard</a>
</li>"""
		
		menus = self.menu_settings.parentlist()
		for menu in menus:
			
			main_menu_label = self.menu_settings.get_value(menu[1], 'label')
			main_menu_url = self.menu_settings.get_value(menu[1], 'url')
			main_menu_icon = self.menu_settings.get_value(menu[1], 'icon')
			
			if main_menu_icon == '' or main_menu_icon == None:
				main_menu_icon = 'fa fa-bars fa-fw'

			submenus_unverified = self.table_settings.get_table('parent_menu', menu[1])
			submenus = []
			for submenu_u in submenus_unverified:
					table_settings = sqlite.tableSettings( self.filename, self.dbname, submenu_u[1] )
					if table_settings.get_value('menu') == '1':
						submenu_id = submenu_u[1]
						
						submenu_title = table_settings.get_value('menu_title')
						if submenu_title == '' or submenu_title == None:
							submenu_title = table_settings.get_value('title')
						
						submenu_order = table_settings.get_value('menu_order')
						if submenu_order == None or submenu_order == '':
							submenu_order = 0
						else:
							submenu_order = int(submenu_order)
							
						table_prefix = table_settings.get_value('table_prefix')
						if table_prefix != '' and table_prefix != None:
							submenu_id = submenu_id[len(table_prefix):]
						
						submenu_url = "<?php echo site_url('%s'); ?>" % submenu_id
						
						submenu_icon = table_settings.get_value('menu_icon', 'fa fa-arrow-right fa-fw')
						
						submenus.insert(submenu_order, (submenu_id, submenu_title, submenu_url, submenu_icon))
			
			childmenus = self.menu_settings.childrenlist(menu[1])
			for child in childmenus:
				child_menu_label = self.menu_settings.get_value(child[1], 'label')
				child_menu_url = self.menu_settings.get_value(child[1], 'url')
				child_menu_icon = self.menu_settings.get_value(child[1], 'icon')
				submenus.append((child[1], child_menu_label, child_menu_url, child_menu_icon))
			
			if len(submenus) > 0:
				submenus_check = []
				for submenu in submenus:
					submenus_check.append( "isset($admin_access->controller_%s)" %  submenu[0] )
				
				content += "\n<?php if( %s ) { ?>\n" % " || ".join(submenus_check)
				
			content += """
<li <?php echo ($main_page == '%(id)s') ? 'class="active"' : ''; ?>>
    <a id="menu-%(id)s" href="%(url)s" title="%(id)s" class=""><i class="%(icon)s"></i> %(label)s<span class="fa arrow"></span></a>
""" % { 'label' : main_menu_label, 'id' : menu[1], 'url' : main_menu_url, 'icon' :  main_menu_icon }
			
			if len(submenus) > 0:
				content += """<ul class="nav nav-second-level" style="height: auto;"> """
				for submenu in submenus:
					content += """
					<?php if( isset($admin_access->controller_%(id)s) ) { ?>
					<li <?php echo ($sub_page == '%(id)s') ? 'class="active"' : ''; ?>>
		<a id="menu-%(id)s" href="%(url)s" title="%(title)s" class=""><i class="%(icon)s"></i> %(title)s</a>
	</li>
	<?php } ?>""" % { 'id' : submenu[0] , 'title' : submenu[1], 'url' : submenu[2], 'icon' : submenu[3] }
				content += "</ul>"
			content += "</li>\n<?php } ?>"

		tables = self.table_settings.get_table('menu', '1')
		for table in tables:
			
			table_settings = sqlite.tableSettings( self.filename, self.dbname, table[1] )
			parent_menu = table_settings.get_value('parent_menu')

			if parent_menu == '' or parent_menu == 'no-parent' or parent_menu == None or parent_menu == 'none':
				table_id = table[1]
				parent_menu_title = table_settings.get_value('menu_title')
				if parent_menu_title == '' or submenu_title == None:
					parent_menu_title = table_settings.get_value('title')
				
				table_prefix = table_settings.get_value('table_prefix')
				if table_prefix != '' and table_prefix != None:
					table_id = table_id[len(table_prefix):]
				
				icon_class = table_settings.get_value('menu_icon', 'fa fa-bars fa-fw')
				
				content += """<?php if( isset($admin_access->controller_%(id)s) ) { ?>
<li <?php echo ($main_page == '%(id)s') ? 'class="active"' : ''; ?>>
    <a id="menu-%(id)s" href="<?php echo site_url('%(id)s'); ?>" title="%(id)s" class=""><i class="%(icon)s"></i> %(label)s<span class="fa arrow"></span></a>
</li>
<?php } ?>""" % { 'label' : parent_menu_title, 'id' : table_id, 'icon' : icon_class }
		
		return content
