import MySQLdb
import sqlite 

class MySQLConnect:
	
	connected = False
	connection = None
	settings = None
	dbname = None
	filename = None
	
	def __init__(self, filename, dbname=None):
		self.settings = sqlite.Settings( filename )
		self.dbname = dbname
		self.filename = filename
		
		try:
			if dbname == None or dbname == '':
				self.connection = MySQLdb.connect(
					host=self.settings.get_value('dbhost'),
					user=self.settings.get_value('dbuser'),
					passwd=self.settings.get_value('dbpasswd'),
					port=int(self.settings.get_value('dbport'))
					)
			else:
				self.connection = MySQLdb.connect(
					host=self.settings.get_value('dbhost'),
					user=self.settings.get_value('dbuser'),
					passwd=self.settings.get_value('dbpasswd'),
					port=int(self.settings.get_value('dbport')),
					db=self.dbname
					)
			self.connected = True
		except:
			self.connected = False
				
	def isConnected(self):
		return self.connected
		
	def dbList(self):
		db = self.connection.cursor()
		db.execute("SHOW DATABASES")
		results = db.fetchall()
		
		dbases = []
		for schema in results:
			dbases.append( schema[0] )
				
		return dbases

	def tableList(self):			
		db = self.connection.cursor()
		db.execute("SHOW TABLES")
		results = db.fetchall()
		
		tables = []
		for table in results:
			tables.append( table[0] )
		
		return tables
		
	def fieldList(self, tblname):
		try:
			db = self.connection.cursor()
			db.execute("SHOW COLUMNS FROM " + tblname )
			results = db.fetchall()
			
			fields = []
			for field in results:
				fields.append( field[0] )
				
			return fields
		except:
			return False

	def dump_create_table(self, tblname):

		db = self.connection.cursor()
		db.execute( "SHOW CREATE TABLE " + tblname )
		_,sql = db.fetchone()
		
		return sql
		
	def query(self, sql):
		if sql != '':
			db = self.connection.cursor()
			db.execute(sql)

	def checkTableExists(self, dbname, table):
		conn = MySQLdb.connect(
			host=self.settings.get_value('dbhost'),
			user=self.settings.get_value('dbuser'),
			passwd=self.settings.get_value('dbpasswd'),
			port=int(self.settings.get_value('dbport')),
			db="information_schema")
		
		db = conn.cursor()
		sql = "SELECT COUNT(*) FROM TABLES WHERE TABLE_SCHEMA = '%(db)s' AND TABLE_NAME = '%(table)s'" % { 'db' : dbname, 'table' : table }
		db.execute(sql)
		
		if db.fetchone()[0] == 1:
			db.close()
			conn.close()
			return True
			
		db.close()
		conn.close()
		return False

	def close(self):
		self.connection.close()
