import sqlite
from lib.mysql import MySQLConnect

class ControllerAPI:
	
	mvc_settings = None
	table_settings = None
	content = None
	dbname = None
	tblname = None
	fields = None
	filename = None
	
	def __init__(self, filename, db, table):
		self.dbname = db
		self.tblname = table
		self.filename = filename
		mysql = MySQLConnect( filename , db )
		self.fields = mysql.fieldList( self.tblname)
		self.mvc_settings = sqlite.mvcSettings(filename,db)
		self.table_settings = sqlite.tableSettings(filename, db, table)
		
		#template = self.mvc_settings.get_value('template')
		#self.content = open('mvc/'+template+'/controllers.txt', 'r')
	
	def get(self):
		new = ''
		for line in self.content:
			if '{table_name}' in line:
				line = line.replace('{table_name}', self.tblname)
			if '{table_name2}' in line:
				line = line.replace('{table_name2}', self.get_table_name())
			if '{controller_authorization}' in line:
				line = line.replace('{controller_authorization}', self.get_controller_authorization())
			if '{view_filename}' in line:
				line = line.replace('{view_filename}', self.get_view_filename())
			if '{class_name}' in line:
				line = line.replace('{class_name}', self.get_class_name())
			if '{table_title}' in line:
				line = line.replace('{table_title}', self.get_table_title())
			if '{primary_field}' in line:
				line = line.replace('{primary_field}', self.get_primary_field())
			if '{primary_field_function}' in line:
				line = line.replace('{primary_field_function}', self.get_primary_field(True))
			if '{parent_menu}' in line:
				line = line.replace('{parent_menu}', self.get_parent_menu())
			if '{add_rules}' in line:
				line = line.replace('{add_rules}', self.get_submission_rules())
			if '{edit_rules}' in line:
				line = line.replace('{edit_rules}', self.get_submission_rules(True))
			if '{submission_fields}' in line:
				line = line.replace('{submission_fields}', self.get_submission_fields())
			if '{slug_check}' in line:
				line = line.replace('{slug_check}', self.get_slug_check())
			if '{list_join_statements}' in line:
				line = line.replace('{list_join_statements}', self.get_join_statements( 'list' ))
			if '{item_join_statements}' in line:
				line = line.replace('{item_join_statements}', self.get_join_statements( 'item' ))
			if '{container_join_statements}' in line:
				line = line.replace('{container_join_statements}', self.get_join_statements( 'container' ))
			if '{pagination_join_statements}' in line:
				line = line.replace('{pagination_join_statements}', self.get_pagination_join_statements())
			new += line
		return new

	def get_variables(self):
		return {
			'table_name' : self.tblname,
			'table_name2' : self.get_table_name(),
			'controller_authorization' : self.get_controller_authorization(),
			'view_filename' : self.get_view_filename(),
			'class_name' : self.get_class_name(),
			'table_title' : self.get_table_title(),
			'list_order_by' : self.get_list_order_by(),
			'list_order_sort' : self.get_list_order_sort(),
			'primary_field' : self.get_primary_field(),
			'primary_field_function' : self.get_primary_field(True),
			'parent_menu' : self.get_parent_menu(),
			'add_rules' : self.get_submission_rules(),
			'edit_rules' : self.get_submission_rules(True),
			'submission_fields' : self.get_submission_fields(),
			'slug_check' : self.get_slug_check(),
			'list_join_statements' : self.get_join_statements( 'list' ),
			'item_join_statements' : self.get_join_statements( 'item' ),
			'container_join_statements' : self.get_join_statements( 'container' ),
			'pagination_join_statements' : self.get_pagination_join_statements(),
			'delete_lines' : self.get_delete_lines(),
			'upload_lines' : self.get_upload_lines(),
		}
		
	def template_var(self, line, varcode, value):
		if varcode in line:
			line = line.replace( varcode, value )
		return line
		
	def get_table_name(self):
		tblname = self.tblname
		table_prefix = self.table_settings.get_value('table_prefix')
		if table_prefix != '' and table_prefix != None:
			tblname = self.tblname[len(table_prefix):]
		return tblname
		
	def get_view_filename(self):
		filename = self.get_table_name()
		if self.table_settings.get_value('generate_view') != "1":
			filename = '404'
		return filename

	def get_controller_authorization(self):
		content = """redirect('dashboard/unauthorized','location');"""
		if self.table_settings.get_value('generate_view') != "1":
			content = """echo json_encode(array(
			'error' => true, 
			'message' => "You are not authorized to access this page!",
			'authorized' => false
			));
			exit;"""
		return content
			
	def get_table_title(self):
		tblname = self.tblname
		
		title = self.table_settings.get_value('title')
		if title == '' or title == None:
			table_prefix = self.table_settings.get_value('table_prefix')
			if table_prefix != '' and table_prefix != None:
				tblname = self.tblname[len(table_prefix):]
			title = tblname.title().replace('_', ' ')
		
		return title
		
		
	def get_class_name(self):
		tblname = self.tblname
		table_prefix = self.table_settings.get_value('table_prefix')
		if table_prefix != '' and table_prefix != None:
			tblname = self.tblname[len(table_prefix):]
		return tblname.capitalize()
	
	def get_primary_field(self, func=False):
		primary = 'id'
		primary_key = self.table_settings.get_value('primary_key')
		if primary_key != '' and primary_key != None:
			primary = primary_key
		
		if(func):
			primary = primary.title().replace('_', '')
			
		return primary
	
	def get_list_order_by(self):
		return self.table_settings.get_value('order_by', self.table_settings.get_value('primary_key'))
	
	def get_list_order_sort(self):
		return self.table_settings.get_value('order_orientation', 'DESC')
		
	def get_parent_menu(self):
		parent_menu = self.table_settings.get_value('parent_menu')
		if parent_menu == '' or parent_menu == None or parent_menu == 'no-parent' or parent_menu == 'none':
			parent_menu = self.get_table_name()
			
		return parent_menu

	def get_submission_rules(self, edit=False):
		content = ""
		primary_key = self.table_settings.get_value('primary_key')
		for field in self.fields:
			
			if primary_key == field and edit == False:
				continue
				
			field_settings = sqlite.fieldSettings(self.filename, self.dbname, self.tblname, field)
			field_validations = sqlite.fieldValidations(self.filename, self.dbname, self.tblname, field)
			
			if edit == True:
				validations = field_validations.get_edit_validations()
			else:
				validations = field_validations.get_add_validations()
			
			if len( validations ) == 0:
				continue
			
			rules = []
			for rule in validations:
				if rule[1] != '':
					rules.append( rule[0] + "["+rule[1]+"]" )
				else:
					rules.append( rule[0] )
			
			variables = { 
				"field_name" : field,
				"table_name" : self.get_table_name(),
				"rules" : "|".join( rules )
			}
			
			content += "\n\t\t\t$this->form_validation->set_rules('%(field_name)s', 'lang:%(table_name)s_%(field_name)s', '%(rules)s');" % variables
			
		return content
	def get_submission_fields(self, varname='container'):
		content_list = []
		
		slug_field = self.table_settings.get_value('slug_field')
		for field in self.fields:
			content = ""
			field_settings = sqlite.fieldSettings(self.filename, self.dbname, self.tblname, field)
			variables = { 
				"field_name" : field,
				"function_name" : field.title().replace('_', ''),
				"class_name" : self.get_class_name(), 
				"default_value" : field_settings.get_value('model_default_value'), 
				"variable_name" : varname,
			}
			
			if field_settings.get_value('required') != '1':
				content += "\t\t\tif( $this->input->post('%(field_name)s') !== FALSE ) {\n\t" % variables
			
			if field == slug_field:
				content += "\t\t\t$%(variable_name)s->set%(function_name)s( url_title( $this->input->post('%(field_name)s'), '-', TRUE ), FALSE, TRUE );\n" % variables
				content += self.get_slug_check()
			else:
				content += "\t\t\t$%(variable_name)s->set%(function_name)s( $this->input->post('%(field_name)s'), FALSE, TRUE );\n" % variables
			
			if field_settings.get_value('required') != '1':
				content += "\t\t\t}\n"
			
				if field_settings.get_value('model_default_on_empty') == '1' and field_settings.get_value('model_has_default') == '1':
					content += "\t\t\telse {\n\t"
					content += "\t\t\t$%(variable_name)s->set%(function_name)s( '%(default_value)s', FALSE, TRUE );\n" % variables
					content += "\t\t\t}\n"
			
			content_list.append( content )
			
		return "\n".join( content_list )

	def get_slug_check(self, varname='container'):
		slug_field = self.table_settings.get_value('slug_field')
		slug_value = self.table_settings.get_value('slug_value')
		
		if slug_field == 'none' or slug_field == None or slug_field == '':
			return ''
			
		if slug_value == 'none' or slug_value == None or slug_value == '':
			return ''
			
		variables = { 
				"slug_field" : slug_field,
				"slug_field_function" : slug_field.title().replace('_', ''),
				"slug_value" : slug_value,
				"class_name" : self.get_class_name(),
				"variable_name" : varname,
			}

		return """\t\t\t\tif($%(variable_name)s->get%(slug_field_function)s() == '') {
\t\t\t\t\t$%(variable_name)s->set%(slug_field_function)s( url_title( $this->input->post('%(slug_value)s') , '-', TRUE), FALSE, TRUE );
\t\t\t\t}\n""" % variables

	def get_join_statements(self, varname):
		content = ""
		for field in self.fields:
			field_settings = sqlite.fieldSettings(self.filename, self.dbname, self.tblname, field)
			linked_table = field_settings.get_value("linked_table")
			if linked_table == "1":
				 linked_table_name = field_settings.get_value("linked_table_name")
				 linked_table_custom_table = field_settings.get_value("linked_table_custom_table") if field_settings.get_value("linked_table_custom_table") != None else linked_table_name
				 linked_table_key = field_settings.get_value("linked_table_key")
				 linked_table_value = field_settings.get_value("linked_table_value")
				 linked_table_custom = field_settings.get_value("linked_table_custom")
				 
				 if linked_table_custom == '':
					 linked_table_custom = linked_table_value
					
				 content += """\t\t\t\t$%(variable_name)s->setJoin('%(linked_table_name)s %(linked_table_name2)s','%(table_name)s.%(field_name)s = %(linked_table_name2)s.%(linked_table_key)s');
\t\t\t\t$%(variable_name)s->setSelect('%(table_name)s.*');
\t\t\t\t$%(variable_name)s->setSelect('%(linked_table_name2)s.%(linked_table_value)s as %(linked_table_custom)s');\n\n""" % {
	"table_name" : self.tblname,
	"field_name" : field,
	"linked_table_name" : linked_table_name,
	"linked_table_key" : linked_table_key,
	"linked_table_value" : linked_table_value,
	"linked_table_custom" : linked_table_custom,
	"linked_table_name2" : linked_table_custom_table,
	"variable_name" : varname
}
		return content
		
	def get_pagination_join_statements(self, varname='pagination'):
		content = ""
		for field in self.fields:
			field_settings = sqlite.fieldSettings(self.filename, self.dbname, self.tblname, field)
			linked_table = field_settings.get_value("linked_table")
			if linked_table == "1":
				 linked_table_name = field_settings.get_value("linked_table_name")
				 linked_table_custom_table = field_settings.get_value("linked_table_custom_table") if field_settings.get_value("linked_table_custom_table") != None else linked_table_name
				 linked_table_key = field_settings.get_value("linked_table_key")
				 linked_table_value = field_settings.get_value("linked_table_value")
				 linked_table_custom = field_settings.get_value("linked_table_custom")
				 
				 if linked_table_custom == '':
					 linked_table_custom = linked_table_value
					
				 content += """\t\t\t\t$%(variable_name)s->setJoin('%(linked_table_name)s %(linked_table_name2)s','%(table_name)s.%(field_name)s = %(linked_table_name2)s.%(linked_table_key)s');\n""" % {
	"table_name" : self.tblname,
	"field_name" : field,
	"linked_table_name" : linked_table_name,
	"linked_table_key" : linked_table_key,
	"linked_table_value" : linked_table_value,
	"linked_table_custom" : linked_table_custom,
	"linked_table_name2" : linked_table_custom_table,
	"variable_name" : varname
}
		return content
		
	def get_delete_lines(self):
		
		variables = {
		'class_name' : self.get_class_name(),
		'primary_field_function' : self.get_primary_field(True),
		'primary_field' : self.get_primary_field(),
		'full_path' : self.table_settings.get_value("upload_field_full_path", '').title().replace('_', '')
		}
		
		content = """$this->%(class_name)s_model->set%(primary_field_function)s( $this->input->post('%(primary_field)s') );
				$data = $this->%(class_name)s_model->getBy%(primary_field_function)s();
		""" % variables
		
		if self.table_settings.get_value("upload_table") == '1':
			content += """
			if( unlink( $data->full_path ) ) {""" % variables
			
		content += """
				
				if( $this->%(class_name)s_model->deleteBy%(primary_field_function)s() ) {
					$results['error'] = false;
					$results['removed'] = true;
					$results['message'] = 'Successfully Deleted!';
				}
		""" % variables
		
		if self.table_settings.get_value("upload_table") == '1':
			content += """
			}
			"""
			
		return content
		
	def get_upload_add(self):
		upload_data = ("file_name", "file_type", "file_path", "full_path", "raw_name", "orig_name", "client_name", "file_ext", "file_size", "is_image", "image_width", "image_height", "image_type", "image_size_str")
		
		content = ""
		for field_key in upload_data:
			field_name = self.table_settings.get_value("upload_field_"+field_key)
			if field_name != None and field_name != 'none' and field_name != '':
				content += """\t\t\t\t\t$container->set%s( $upload_data['%s'], FALSE, TRUE );\n""" % (field_name.title().replace('_', ''), field_key)
		
		return content
		
	def get_upload_lines(self):
		return """case "upload":
				if(  ! $this->template_data->get('admin_access')->controller_%(table_name2)s ) {
					return 0;
				}
				$results = array(
					'group' => $this->input->post('group'),
					'key' => $this->input->post('key'),
					'table' => 'media_uploads',
					'error' => true,
					'removed' => false,
					'message' => 'Unable to upload!'
				);
				
				$config['upload_path'] = '%(upload_path)s';
				$config['allowed_types'] = '%(allowed_types)s';
				$config['max_size']	= '%(max_size)s';
				$config['max_width']  = '%(max_width)s';
				$config['max_height']  = '%(max_height)s';

				$this->load->library('upload', $config);

				if ( ! $this->upload->do_upload())
				{
					$results['error'] = true;
					$results['message'] = $this->upload->display_errors();
					
				}
				else
				{
					$results['error'] = false;
					$results['message'] = "Success!";
					$upload_data = $this->upload->data();
					$results['upload_data'] = $upload_data;
					
					$container = new $this->%(class_name)s_model;
%(upload_add)s
					if( $container->insert() ) {
						$results['id'] = $container->get%(primary_field_function)s();
						$results['results'] = $container->getBy%(primary_field_function)s();
					}
				}
				echo json_encode( $results );
				exit;
			break;
		""" % {
		'class_name' : self.get_class_name(),
		'upload_add' : self.get_upload_add(),
		'primary_field_function' : self.get_primary_field(True),
		'upload_path' : self.table_settings.get_value("upload_config_upload_path"),
		'allowed_types' : self.table_settings.get_value("upload_config_allowed_types", '').replace(',', '|'),
		'max_size' : self.table_settings.get_value("upload_config_max_size"),
		'max_width' : self.table_settings.get_value("upload_config_max_width"),
		'max_height' : self.table_settings.get_value("upload_config_max_height"),
		'table_name2' : self.get_table_name(),
		}
		
	def get_content(self):
		
		variables = self.get_variables()
		
		variables['insert_function'] = "insert" if self.table_settings.get_value("allow_duplicates_add") == "1" else "replace"
		
		return """<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class %(class_name)s extends CI_Controller {

    function __construct() {
        parent::__construct();

        $this->load->model( array('%(class_name)s_model') );

    }
    
    public function index()
	{
        echo "nothing here...";
	}

    public function add()
	{
		%(add_rules)s
	}

	public function edit($group_id)
	{
		%(edit_rules)s
	}
	
	public function populate()
	{
		$group = new $this->%(class_name)s_model;
		$groups->setOrder('priority', 'DESC');
		echo json_encode( $groups->populate() );
	}
	
	public function get($group_id)
	{
		$item = new $this->%(class_name)s_model;
		$item->setGroupId( $group_id, TRUE );
		echo json_encode( $group->get() );
	}
	
	
	
	public function delete()
	{
		if( $this->input->post('group_id') != '' )  {
			$group = new $this->%(class_name)s_model;
			$group->setGroupId( $this->input->post('group_id'), TRUE );
			$group->deleteByGroupId();
		}
	}
	
}
/* End of file %(table_name2)s.php */
/* Location: ./application/controllers/%(table_name2)s.php */
""" % variables

