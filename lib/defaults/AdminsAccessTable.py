from lib import sqlite
from lib.mysql import MySQLConnect

class AdminsAccess:
	
	dbname = None
	savefile = None
	tblprefix = ''
	
	def __init__(self, savefile, db, tblprefix=''):
		self.savefile = savefile
		self.dbname = db
		self.tblprefix = tblprefix
	
	def create_table(self):
		mysql = MySQLConnect(self.savefile, self.dbname )
				
		table_name = self.tblprefix+"admins_access"
		sql_string = """CREATE TABLE `%s` (
			  `id` bigint(20) NOT NULL AUTO_INCREMENT,
			  `admin_id` bigint(20) NOT NULL,
			  `controller` varchar(100) NOT NULL,
			  `add` int(1) DEFAULT '0',
			  `edit` int(1) DEFAULT '0',
			  `delete` int(1) DEFAULT '0',
			  PRIMARY KEY (`id`)
			);""" % table_name
			
		if mysql.checkTableExists( self.dbname, table_name ) == False:
			mysql.query( sql_string  )
			return True
		else:
			return False

