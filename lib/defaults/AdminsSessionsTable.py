from lib import sqlite
from lib.mysql import MySQLConnect

class AdminsSessions:
	
	dbname = None
	savefile = None
	tblprefix = ''
	
	def __init__(self, savefile, db, tblprefix=''):
		self.savefile = savefile
		self.dbname = db
		self.tblprefix = tblprefix
	
	def create_table(self):
		mysql = MySQLConnect(self.savefile, self.dbname )
				
		table_name = self.tblprefix+"admins_sessions"
		sql_string = """CREATE TABLE `%s` (
			  `session_id` varchar(40) NOT NULL DEFAULT '0',
			  `ip_address` varchar(45) NOT NULL DEFAULT '0',
			  `user_agent` varchar(120) NOT NULL,
			  `last_activity` int(10) unsigned NOT NULL DEFAULT '0',
			  `user_data` text NOT NULL,
			  PRIMARY KEY (`session_id`),
			  KEY `last_activity_idx` (`last_activity`)
			);""" % table_name
			
		if mysql.checkTableExists( self.dbname, table_name ) == False:
			mysql.query( sql_string  )
			return True
		else:
			return False
