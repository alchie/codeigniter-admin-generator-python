from lib import sqlite
from lib.mysql import MySQLConnect


class Admins:
	
	dbname = None
	savefile = None
	tblprefix = ''
	
	def __init__(self, savefile, db, tblprefix=''):
		self.savefile = savefile
		self.dbname = db
		self.tblprefix = tblprefix
	
	def create_table(self):
		mysql = MySQLConnect(self.savefile, self.dbname )
		
		table_name = self.tblprefix+"admins"
		sql_string = """CREATE TABLE IF NOT EXISTS `%s` (
			  `id` bigint(20) NOT NULL AUTO_INCREMENT,
		  `name` varchar(100) NOT NULL,
		  `email` varchar(100) NOT NULL,
		  `password` varchar(255) NOT NULL,
		  `username` varchar(100) DEFAULT NULL,
		  `active` int(1) DEFAULT '0',
		  `date_created` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
		  `last_login` datetime DEFAULT NULL,
		  `last_login_ip` varchar(100) DEFAULT NULL,
		  PRIMARY KEY (`id`),
		  UNIQUE KEY `email_UNIQUE` (`email`),
		  UNIQUE KEY `username_UNIQUE` (`username`)
			);""" % table_name
			
		if mysql.checkTableExists( self.dbname, table_name ) == False:
			mysql.query( sql_string  )
			self.tableDefalts(self.dbname, table_name)
			return True
		else:
			return False
	
	def tableDefalts(self, dbname, tablename):
		for sql in self.get_settings():
			sql_statement = sql % (dbname, tablename)
			sqlite._query( self.savefile, sql_statement )
			print( sql_statement )
	
	def get_settings(self):
		return (
# table settings
'INSERT INTO `table_settings` VALUES ( "%s", "%s", "title", "Admins");',
'INSERT INTO `table_settings` VALUES ( "%s", "%s", "singular", "Admin");',
'INSERT INTO `table_settings` VALUES ( "%s", "%s", "plural", "Admins");',
'INSERT INTO `table_settings` VALUES ( "%s", "%s", "parent", "none");',
'INSERT INTO `table_settings` VALUES ( "%s", "%s", "filter", "none");',
'INSERT INTO `table_settings` VALUES ( "%s", "%s", "order_by", "id");',
'INSERT INTO `table_settings` VALUES ( "%s", "%s", "order_orientation", "DESC");',
'INSERT INTO `table_settings` VALUES ( "%s", "%s", "listlimit", "20");',
'INSERT INTO `table_settings` VALUES ( "%s", "%s", "generate", "1");',
'INSERT INTO `table_settings` VALUES ( "%s", "%s", "table_prefix", "ci_");',
'INSERT INTO `table_settings` VALUES ( "%s", "%s", "menu", "1");',
'INSERT INTO `table_settings` VALUES ( "%s", "%s", "parent_menu", "accounts");',
'INSERT INTO `table_settings` VALUES ( "%s", "%s", "menu_order", "0");',
'INSERT INTO `table_settings` VALUES ( "%s", "%s", "menu_title", "Admins");',
'INSERT INTO `table_settings` VALUES ( "%s", "%s", "primary_key", "id");',
'INSERT INTO `table_settings` VALUES ( "%s", "%s", "primary_title", "username");',
'INSERT INTO `table_settings` VALUES ( "%s", "%s", "slug_field", "none");',
'INSERT INTO `table_settings` VALUES ( "%s", "%s", "slug_value", "none");',
'INSERT INTO `table_settings` VALUES ( "%s", "%s", "active_field", "active");',
'INSERT INTO `table_settings` VALUES ( "%s", "%s", "has_children", "1");',
'INSERT INTO `table_settings` VALUES ( "%s", "%s", "generate_model", "1");',
'INSERT INTO `table_settings` VALUES ( "%s", "%s", "generate_view", "1");',
'INSERT INTO `table_settings` VALUES ( "%s", "%s", "generate_controller", "1");',
'INSERT INTO `table_settings` VALUES ( "%s", "%s", "generate_lang", "1");',
'INSERT INTO `table_settings` VALUES ( "%s", "%s", "view_template", "default");',
'INSERT INTO `table_settings` VALUES ( "%s", "%s", "actions_edit", "1");',
'INSERT INTO `table_settings` VALUES ( "%s", "%s", "actions_delete", "1");',
'INSERT INTO `table_settings` VALUES ( "%s", "%s", "actions_add", "1");',

# field settings
'INSERT INTO `field_settings` VALUES ( "%s", "%s", "active", "checkbox_type", "MANUAL");',
'INSERT INTO `field_settings` VALUES ( "%s", "%s", "active", "checkbox_table", "");',
'INSERT INTO `field_settings` VALUES ( "%s", "%s", "active", "checkbox_table_key", "");',
'INSERT INTO `field_settings` VALUES ( "%s", "%s", "active", "checkbox_table_value", "");',
'INSERT INTO `field_settings` VALUES ( "%s", "%s", "active", "checkbox_type_source", "MANUAL");',
'INSERT INTO `field_settings` VALUES ( "%s", "%s", "active", "checkbox_type_table_name", "");',
'INSERT INTO `field_settings` VALUES ( "%s", "%s", "active", "checkbox_type_table_key", "");',
'INSERT INTO `field_settings` VALUES ( "%s", "%s", "active", "checkbox_type_table_value", "");',
'INSERT INTO `field_settings` VALUES ( "%s", "%s", "active", "checkbox_type_table_custom", "");',
'INSERT INTO `field_settings` VALUES ( "%s", "%s", "active", "default_checkbox_item", "1");',
'INSERT INTO `field_settings` VALUES ( "%s", "%s", "active", "field_title", "Active");',
'INSERT INTO `field_settings` VALUES ( "%s", "%s", "active", "list_page", "0");',
'INSERT INTO `field_settings` VALUES ( "%s", "%s", "active", "add_page", "1");',
'INSERT INTO `field_settings` VALUES ( "%s", "%s", "active", "edit_page", "1");',
'INSERT INTO `field_settings` VALUES ( "%s", "%s", "active", "required", "0");',
'INSERT INTO `field_settings` VALUES ( "%s", "%s", "active", "form_input_type", "checkbox");',
'INSERT INTO `field_settings` VALUES ( "%s", "%s", "active", "linked_table", "0");',
'INSERT INTO `field_settings` VALUES ( "%s", "%s", "active", "list_column_order", "6");',
'INSERT INTO `field_settings` VALUES ( "%s", "%s", "active", "model_default_value", "0");',
'INSERT INTO `field_settings` VALUES ( "%s", "%s", "active", "model_has_default", "1");',
'INSERT INTO `field_settings` VALUES ( "%s", "%s", "active", "model_default_on_empty", "1");',

'INSERT INTO `field_settings` VALUES ( "%s", "%s", "date_created", "text_type_default", "CURRENT_TIMESTAMP");',
'INSERT INTO `field_settings` VALUES ( "%s", "%s", "date_created", "text_type_current_timestamp", "1");',
'INSERT INTO `field_settings` VALUES ( "%s", "%s", "date_created", "text_type_unique_id", "0");',
'INSERT INTO `field_settings` VALUES ( "%s", "%s", "date_created", "text_type_default_value", "");',
'INSERT INTO `field_settings` VALUES ( "%s", "%s", "date_created", "text_type_default_length", "");',
'INSERT INTO `field_settings` VALUES ( "%s", "%s", "date_created", "field_title", "Date Created");',
'INSERT INTO `field_settings` VALUES ( "%s", "%s", "date_created", "list_page", "0");',
'INSERT INTO `field_settings` VALUES ( "%s", "%s", "date_created", "add_page", "0");',
'INSERT INTO `field_settings` VALUES ( "%s", "%s", "date_created", "edit_page", "1");',
'INSERT INTO `field_settings` VALUES ( "%s", "%s", "date_created", "required", "0");',
'INSERT INTO `field_settings` VALUES ( "%s", "%s", "date_created", "form_input_type", "view_only");',
'INSERT INTO `field_settings` VALUES ( "%s", "%s", "date_created", "linked_table", "0");',
'INSERT INTO `field_settings` VALUES ( "%s", "%s", "date_created", "list_column_order", "7");',

'INSERT INTO `field_settings` VALUES ( "%s", "%s", "email", "field_title", "Email");',
'INSERT INTO `field_settings` VALUES ( "%s", "%s", "email", "list_page", "1");',
'INSERT INTO `field_settings` VALUES ( "%s", "%s", "email", "add_page", "1");',
'INSERT INTO `field_settings` VALUES ( "%s", "%s", "email", "edit_page", "1");',
'INSERT INTO `field_settings` VALUES ( "%s", "%s", "email", "required", "1");',
'INSERT INTO `field_settings` VALUES ( "%s", "%s", "email", "list_searchable", "1");',
'INSERT INTO `field_settings` VALUES ( "%s", "%s", "email", "list_filterable", "0");',
'INSERT INTO `field_settings` VALUES ( "%s", "%s", "email", "list_check_cross", "0");',
'INSERT INTO `field_settings` VALUES ( "%s", "%s", "email", "list_show_value", "0");',
'INSERT INTO `field_settings` VALUES ( "%s", "%s", "email", "list_column_width", "");',
'INSERT INTO `field_settings` VALUES ( "%s", "%s", "email", "list_column_order", "3");',
'INSERT INTO `field_settings` VALUES ( "%s", "%s", "email", "form_input_type", "text");',
'INSERT INTO `field_settings` VALUES ( "%s", "%s", "email", "form_input_class", "");',
'INSERT INTO `field_settings` VALUES ( "%s", "%s", "email", "linked_table", "0");',
'INSERT INTO `field_settings` VALUES ( "%s", "%s", "email", "form_input_class_edit", "");',
'INSERT INTO `field_settings` VALUES ( "%s", "%s", "email", "edit_change_deliberately", "1");',
'INSERT INTO `field_settings` VALUES ( "%s", "%s", "email", "list_show_linked_table_value", "0");',

'INSERT INTO `field_settings` VALUES ( "%s", "%s", "id", "field_title", "Admin ID");',
'INSERT INTO `field_settings` VALUES ( "%s", "%s", "id", "list_page", "0");',
'INSERT INTO `field_settings` VALUES ( "%s", "%s", "id", "add_page", "0");',
'INSERT INTO `field_settings` VALUES ( "%s", "%s", "id", "edit_page", "1");',
'INSERT INTO `field_settings` VALUES ( "%s", "%s", "id", "required", "0");',
'INSERT INTO `field_settings` VALUES ( "%s", "%s", "id", "list_searchable", "0");',
'INSERT INTO `field_settings` VALUES ( "%s", "%s", "id", "list_filterable", "0");',
'INSERT INTO `field_settings` VALUES ( "%s", "%s", "id", "list_check_cross", "0");',
'INSERT INTO `field_settings` VALUES ( "%s", "%s", "id", "list_show_value", "0");',
'INSERT INTO `field_settings` VALUES ( "%s", "%s", "id", "list_column_width", "");',
'INSERT INTO `field_settings` VALUES ( "%s", "%s", "id", "list_column_order", "1");',
'INSERT INTO `field_settings` VALUES ( "%s", "%s", "id", "form_input_type", "hidden");',
'INSERT INTO `field_settings` VALUES ( "%s", "%s", "id", "form_input_class", "");',
'INSERT INTO `field_settings` VALUES ( "%s", "%s", "id", "hidden_type_default", "test");',
'INSERT INTO `field_settings` VALUES ( "%s", "%s", "id", "linked_table", "0");',

'INSERT INTO `field_settings` VALUES ( "%s", "%s", "last_login", "field_title", "Last Login");',
'INSERT INTO `field_settings` VALUES ( "%s", "%s", "last_login", "list_page", "0");',
'INSERT INTO `field_settings` VALUES ( "%s", "%s", "last_login", "add_page", "0");',
'INSERT INTO `field_settings` VALUES ( "%s", "%s", "last_login", "edit_page", "1");',
'INSERT INTO `field_settings` VALUES ( "%s", "%s", "last_login", "required", "0");',
'INSERT INTO `field_settings` VALUES ( "%s", "%s", "last_login", "form_input_type", "view_only");',
'INSERT INTO `field_settings` VALUES ( "%s", "%s", "last_login", "linked_table", "0");',
'INSERT INTO `field_settings` VALUES ( "%s", "%s", "last_login", "list_column_order", "8");',
'INSERT INTO `field_settings` VALUES ( "%s", "%s", "last_login", "text_type_default", "CURRENT_TIMESTAMP");',
'INSERT INTO `field_settings` VALUES ( "%s", "%s", "last_login", "text_type_default_value", "");',
'INSERT INTO `field_settings` VALUES ( "%s", "%s", "last_login", "text_type_default_length", "");',
'INSERT INTO `field_settings` VALUES ( "%s", "%s", "last_login", "text_type_search_linked_table", "0");',

'INSERT INTO `field_settings` VALUES ( "%s", "%s", "last_login_ip", "field_title", "Last Login IP Address");',
'INSERT INTO `field_settings` VALUES ( "%s", "%s", "last_login_ip", "list_page", "0");',
'INSERT INTO `field_settings` VALUES ( "%s", "%s", "last_login_ip", "add_page", "0");',
'INSERT INTO `field_settings` VALUES ( "%s", "%s", "last_login_ip", "edit_page", "1");',
'INSERT INTO `field_settings` VALUES ( "%s", "%s", "last_login_ip", "required", "0");',
'INSERT INTO `field_settings` VALUES ( "%s", "%s", "last_login_ip", "form_input_type", "view_only");',
'INSERT INTO `field_settings` VALUES ( "%s", "%s", "last_login_ip", "linked_table", "0");',
'INSERT INTO `field_settings` VALUES ( "%s", "%s", "last_login_ip", "list_column_order", "9");',

'INSERT INTO `field_settings` VALUES ( "%s", "%s", "name", "field_title", "Name");',
'INSERT INTO `field_settings` VALUES ( "%s", "%s", "name", "list_page", "1");',
'INSERT INTO `field_settings` VALUES ( "%s", "%s", "name", "add_page", "1");',
'INSERT INTO `field_settings` VALUES ( "%s", "%s", "name", "edit_page", "1");',
'INSERT INTO `field_settings` VALUES ( "%s", "%s", "name", "form_input_type", "text");',
'INSERT INTO `field_settings` VALUES ( "%s", "%s", "name", "linked_table", "0");',
'INSERT INTO `field_settings` VALUES ( "%s", "%s", "name", "list_column_order", "2");',
'INSERT INTO `field_settings` VALUES ( "%s", "%s", "name", "list_searchable", "1");',
'INSERT INTO `field_settings` VALUES ( "%s", "%s", "name", "list_filterable", "0");',
'INSERT INTO `field_settings` VALUES ( "%s", "%s", "name", "list_check_cross", "0");',
'INSERT INTO `field_settings` VALUES ( "%s", "%s", "name", "list_show_linked_table_value", "0");',
'INSERT INTO `field_settings` VALUES ( "%s", "%s", "name", "list_column_width", "");',
'INSERT INTO `field_settings` VALUES ( "%s", "%s", "name", "text_type_default", "TEXT_VALUE");',
'INSERT INTO `field_settings` VALUES ( "%s", "%s", "name", "text_type_default_value", "");',
'INSERT INTO `field_settings` VALUES ( "%s", "%s", "name", "text_type_default_length", "");',
'INSERT INTO `field_settings` VALUES ( "%s", "%s", "name", "text_type_search_linked_table", "0");',
'INSERT INTO `field_settings` VALUES ( "%s", "%s", "name", "required", "1");',
'INSERT INTO `field_settings` VALUES ( "%s", "%s", "name", "model_has_default", "0");',
'INSERT INTO `field_settings` VALUES ( "%s", "%s", "name", "model_default_value", "");',
'INSERT INTO `field_settings` VALUES ( "%s", "%s", "name", "model_default_on_empty", "0");',

'INSERT INTO `field_settings` VALUES ( "%s", "%s", "password", "password_type_confirm", "1");',
'INSERT INTO `field_settings` VALUES ( "%s", "%s", "password", "field_title", "Password");',
'INSERT INTO `field_settings` VALUES ( "%s", "%s", "password", "list_page", "0");',
'INSERT INTO `field_settings` VALUES ( "%s", "%s", "password", "add_page", "1");',
'INSERT INTO `field_settings` VALUES ( "%s", "%s", "password", "edit_page", "1");',
'INSERT INTO `field_settings` VALUES ( "%s", "%s", "password", "required", "0");',
'INSERT INTO `field_settings` VALUES ( "%s", "%s", "password", "list_searchable", "0");',
'INSERT INTO `field_settings` VALUES ( "%s", "%s", "password", "list_filterable", "0");',
'INSERT INTO `field_settings` VALUES ( "%s", "%s", "password", "list_check_cross", "0");',
'INSERT INTO `field_settings` VALUES ( "%s", "%s", "password", "list_show_value", "0");',
'INSERT INTO `field_settings` VALUES ( "%s", "%s", "password", "list_column_width", "");',
'INSERT INTO `field_settings` VALUES ( "%s", "%s", "password", "list_column_order", "5");',
'INSERT INTO `field_settings` VALUES ( "%s", "%s", "password", "form_input_type", "password");',
'INSERT INTO `field_settings` VALUES ( "%s", "%s", "password", "form_input_class", "");',
'INSERT INTO `field_settings` VALUES ( "%s", "%s", "password", "radio_type", "TABLE");',
'INSERT INTO `field_settings` VALUES ( "%s", "%s", "password", "radio_table", "ci_admins");',
'INSERT INTO `field_settings` VALUES ( "%s", "%s", "password", "linked_table", "0");',
'INSERT INTO `field_settings` VALUES ( "%s", "%s", "password", "password_type_name", "confirm_password");',
'INSERT INTO `field_settings` VALUES ( "%s", "%s", "password", "password_type_label", "Confirm Password");',
'INSERT INTO `field_settings` VALUES ( "%s", "%s", "password", "form_input_class_edit", "");',
'INSERT INTO `field_settings` VALUES ( "%s", "%s", "password", "edit_change_deliberately", "1");',

'INSERT INTO `field_settings` VALUES ( "%s", "%s", "username", "field_title", "Username");',
'INSERT INTO `field_settings` VALUES ( "%s", "%s", "username", "list_page", "1");',
'INSERT INTO `field_settings` VALUES ( "%s", "%s", "username", "add_page", "1");',
'INSERT INTO `field_settings` VALUES ( "%s", "%s", "username", "edit_page", "1");',
'INSERT INTO `field_settings` VALUES ( "%s", "%s", "username", "required", "1");',
'INSERT INTO `field_settings` VALUES ( "%s", "%s", "username", "list_searchable", "1");',
'INSERT INTO `field_settings` VALUES ( "%s", "%s", "username", "list_filterable", "0");',
'INSERT INTO `field_settings` VALUES ( "%s", "%s", "username", "list_check_cross", "0");',
'INSERT INTO `field_settings` VALUES ( "%s", "%s", "username", "list_show_value", "0");',
'INSERT INTO `field_settings` VALUES ( "%s", "%s", "username", "list_column_width", "");',
'INSERT INTO `field_settings` VALUES ( "%s", "%s", "username", "list_column_order", "4");',
'INSERT INTO `field_settings` VALUES ( "%s", "%s", "username", "form_input_type", "text");',
'INSERT INTO `field_settings` VALUES ( "%s", "%s", "username", "form_input_class", "");',
'INSERT INTO `field_settings` VALUES ( "%s", "%s", "username", "linked_table", "0");',
'INSERT INTO `field_settings` VALUES ( "%s", "%s", "username", "form_input_class_edit", "");',
'INSERT INTO `field_settings` VALUES ( "%s", "%s", "username", "edit_change_deliberately", "1");',
'INSERT INTO `field_settings` VALUES ( "%s", "%s", "username", "list_show_linked_table_value", "0");',

)
