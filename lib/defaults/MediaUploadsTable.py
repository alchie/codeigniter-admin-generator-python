from lib import sqlite
from lib.mysql import MySQLConnect


class MediaUploads:
	
	dbname = None
	savefile = None
	tblprefix = ''
	
	def __init__(self, savefile, db, tblprefix=''):
		self.savefile = savefile
		self.dbname = db
		self.tblprefix = tblprefix
	
	def create_table(self):
		mysql = MySQLConnect(self.savefile, self.dbname )
		
		table_name = self.tblprefix+"media_uploads"
		sql_string = """CREATE TABLE `%s` (
  `media_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `file_name` varchar(255) NOT NULL,
  `file_type` varchar(100) DEFAULT NULL,
  `file_path` varchar(255) DEFAULT NULL,
  `full_path` varchar(255) DEFAULT NULL,
  `raw_name` varchar(255) DEFAULT NULL,
  `orig_name` varchar(255) DEFAULT NULL,
  `client_name` varchar(255) DEFAULT NULL,
  `file_ext` varchar(100) DEFAULT NULL,
  `file_size` float DEFAULT NULL,
  `is_image` int(1) DEFAULT NULL,
  `image_width` int(10) DEFAULT NULL,
  `image_height` int(10) DEFAULT NULL,
  `image_type` varchar(100) DEFAULT NULL,
  `image_size_str` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`media_id`)
);""" % table_name
			
		if mysql.checkTableExists( self.dbname, table_name ) == False:
			mysql.query( sql_string  )
			self.tableDefalts(self.dbname, table_name)
			return True
		else:
			return False
	
	def tableDefalts(self, dbname, tablename):
		for sql in self.get_settings():
			sql_statement = sql % (dbname, tablename)
			sqlite._query( self.savefile, sql_statement )
			print( sql_statement )
	
	def get_settings(self):
		return (
# table settings
'INSERT INTO `table_settings` VALUES ( "%s", "%s", "upload_config_upload_path", "../uploads/");',
'INSERT INTO `table_settings` VALUES ( "%s", "%s", "upload_config_allowed_types", "jpg,jpeg,gif,png");',
'INSERT INTO `table_settings` VALUES ( "%s", "%s", "upload_config_file_name", "");',
'INSERT INTO `table_settings` VALUES ( "%s", "%s", "upload_config_overwrite", "FALSE");',
'INSERT INTO `table_settings` VALUES ( "%s", "%s", "upload_config_max_size", "100000");',
'INSERT INTO `table_settings` VALUES ( "%s", "%s", "upload_config_max_width", "1024");',
'INSERT INTO `table_settings` VALUES ( "%s", "%s", "upload_config_max_height", "768");',
'INSERT INTO `table_settings` VALUES ( "%s", "%s", "upload_config_max_filename", "0");',
'INSERT INTO `table_settings` VALUES ( "%s", "%s", "upload_config_encrypt_name", "FALSE");',
'INSERT INTO `table_settings` VALUES ( "%s", "%s", "upload_config_remove_spaces", "TRUE");',
'INSERT INTO `table_settings` VALUES ( "%s", "%s", "upload_field_file_name", "file_name");',
'INSERT INTO `table_settings` VALUES ( "%s", "%s", "upload_field_file_type", "file_type");',
'INSERT INTO `table_settings` VALUES ( "%s", "%s", "upload_field_file_path", "file_path");',
'INSERT INTO `table_settings` VALUES ( "%s", "%s", "upload_field_full_path", "full_path");',
'INSERT INTO `table_settings` VALUES ( "%s", "%s", "upload_field_raw_name", "raw_name");',
'INSERT INTO `table_settings` VALUES ( "%s", "%s", "upload_field_orig_name", "orig_name");',
'INSERT INTO `table_settings` VALUES ( "%s", "%s", "upload_field_client_name", "client_name");',
'INSERT INTO `table_settings` VALUES ( "%s", "%s", "upload_field_file_ext", "file_ext");',
'INSERT INTO `table_settings` VALUES ( "%s", "%s", "upload_field_file_size", "file_size");',
'INSERT INTO `table_settings` VALUES ( "%s", "%s", "upload_field_is_image", "is_image");',
'INSERT INTO `table_settings` VALUES ( "%s", "%s", "upload_field_image_width", "image_width");',
'INSERT INTO `table_settings` VALUES ( "%s", "%s", "upload_field_image_height", "image_height");',
'INSERT INTO `table_settings` VALUES ( "%s", "%s", "upload_field_image_type", "image_type");',
'INSERT INTO `table_settings` VALUES ( "%s", "%s", "upload_field_image_size_str", "image_size_str");',
'INSERT INTO `table_settings` VALUES ( "%s", "%s", "parent_menu", "content");',
'INSERT INTO `table_settings` VALUES ( "%s", "%s", "menu_order", "1");',
'INSERT INTO `table_settings` VALUES ( "%s", "%s", "menu_title", "Media Uploads");',
'INSERT INTO `table_settings` VALUES ( "%s", "%s", "title", "Media Uploads");',
'INSERT INTO `table_settings` VALUES ( "%s", "%s", "singular", "Media File");',
'INSERT INTO `table_settings` VALUES ( "%s", "%s", "plural", "Media Files");',
'INSERT INTO `table_settings` VALUES ( "%s", "%s", "table_prefix", "ci_");',
'INSERT INTO `table_settings` VALUES ( "%s", "%s", "order_by", "media_id");',
'INSERT INTO `table_settings` VALUES ( "%s", "%s", "order_orientation", "DESC");',
'INSERT INTO `table_settings` VALUES ( "%s", "%s", "listlimit", "20");',
'INSERT INTO `table_settings` VALUES ( "%s", "%s", "view_template", "default");',
'INSERT INTO `table_settings` VALUES ( "%s", "%s", "parent", "none");',
'INSERT INTO `table_settings` VALUES ( "%s", "%s", "filter", "none");',
'INSERT INTO `table_settings` VALUES ( "%s", "%s", "primary_key", "media_id");',
'INSERT INTO `table_settings` VALUES ( "%s", "%s", "primary_title", "file_name");',
'INSERT INTO `table_settings` VALUES ( "%s", "%s", "slug_field", "none");',
'INSERT INTO `table_settings` VALUES ( "%s", "%s", "slug_value", "none");',
'INSERT INTO `table_settings` VALUES ( "%s", "%s", "active_field", "none");',
'INSERT INTO `table_settings` VALUES ( "%s", "%s", "actions_add", "1");',
'INSERT INTO `table_settings` VALUES ( "%s", "%s", "actions_edit", "1");',
'INSERT INTO `table_settings` VALUES ( "%s", "%s", "actions_delete", "1");',
'INSERT INTO `table_settings` VALUES ( "%s", "%s", "generate_model", "1");',
'INSERT INTO `table_settings` VALUES ( "%s", "%s", "generate_view", "1");',
'INSERT INTO `table_settings` VALUES ( "%s", "%s", "generate_controller", "1");',
'INSERT INTO `table_settings` VALUES ( "%s", "%s", "generate_lang", "1");',
'INSERT INTO `table_settings` VALUES ( "%s", "%s", "menu", "1");',
'INSERT INTO `table_settings` VALUES ( "%s", "%s", "has_children", "0");',
'INSERT INTO `table_settings` VALUES ( "%s", "%s", "upload_table", "1");',
'INSERT INTO `table_settings` VALUES ( "%s", "%s", "upload_config_upload_url", "");',


'INSERT INTO `field_settings` VALUES ( "%s", "%s", "client_name", "field_title", "Client Name");',
'INSERT INTO `field_settings` VALUES ( "%s", "%s", "client_name", "list_page", "0");',
'INSERT INTO `field_settings` VALUES ( "%s", "%s", "client_name", "add_page", "0");',
'INSERT INTO `field_settings` VALUES ( "%s", "%s", "client_name", "edit_page", "1");',
'INSERT INTO `field_settings` VALUES ( "%s", "%s", "client_name", "form_input_type", "text");',
'INSERT INTO `field_settings` VALUES ( "%s", "%s", "client_name", "linked_table", "0");',
'INSERT INTO `field_settings` VALUES ( "%s", "%s", "client_name", "list_column_order", "8");',

'INSERT INTO `field_settings` VALUES ( "%s", "%s", "file_ext", "field_title", "File Extension");',
'INSERT INTO `field_settings` VALUES ( "%s", "%s", "file_ext", "list_page", "0");',
'INSERT INTO `field_settings` VALUES ( "%s", "%s", "file_ext", "add_page", "0");',
'INSERT INTO `field_settings` VALUES ( "%s", "%s", "file_ext", "edit_page", "1");',
'INSERT INTO `field_settings` VALUES ( "%s", "%s", "file_ext", "form_input_type", "text");',
'INSERT INTO `field_settings` VALUES ( "%s", "%s", "file_ext", "linked_table", "0");',
'INSERT INTO `field_settings` VALUES ( "%s", "%s", "file_ext", "list_column_order", "9");',

'INSERT INTO `field_settings` VALUES ( "%s", "%s", "file_name", "field_title", "Filename");',
'INSERT INTO `field_settings` VALUES ( "%s", "%s", "file_name", "list_page", "1");',
'INSERT INTO `field_settings` VALUES ( "%s", "%s", "file_name", "add_page", "0");',
'INSERT INTO `field_settings` VALUES ( "%s", "%s", "file_name", "edit_page", "1");',
'INSERT INTO `field_settings` VALUES ( "%s", "%s", "file_name", "form_input_type", "text");',
'INSERT INTO `field_settings` VALUES ( "%s", "%s", "file_name", "linked_table", "0");',
'INSERT INTO `field_settings` VALUES ( "%s", "%s", "file_name", "list_column_order", "2");',
'INSERT INTO `field_settings` VALUES ( "%s", "%s", "file_name", "list_searchable", "1");',
'INSERT INTO `field_settings` VALUES ( "%s", "%s", "file_name", "list_filter_value", "0");',
'INSERT INTO `field_settings` VALUES ( "%s", "%s", "file_name", "anchor_filter_value", "0");',
'INSERT INTO `field_settings` VALUES ( "%s", "%s", "file_name", "list_filterable", "0");',
'INSERT INTO `field_settings` VALUES ( "%s", "%s", "file_name", "list_check_cross", "0");',
'INSERT INTO `field_settings` VALUES ( "%s", "%s", "file_name", "list_show_linked_table_value", "0");',
'INSERT INTO `field_settings` VALUES ( "%s", "%s", "file_name", "list_column_width", "");',

'INSERT INTO `field_settings` VALUES ( "%s", "%s", "file_path", "field_title", "File Path");',
'INSERT INTO `field_settings` VALUES ( "%s", "%s", "file_path", "list_page", "0");',
'INSERT INTO `field_settings` VALUES ( "%s", "%s", "file_path", "add_page", "0");',
'INSERT INTO `field_settings` VALUES ( "%s", "%s", "file_path", "edit_page", "1");',
'INSERT INTO `field_settings` VALUES ( "%s", "%s", "file_path", "form_input_type", "text");',
'INSERT INTO `field_settings` VALUES ( "%s", "%s", "file_path", "linked_table", "0");',
'INSERT INTO `field_settings` VALUES ( "%s", "%s", "file_path", "list_column_order", "4");',

'INSERT INTO `field_settings` VALUES ( "%s", "%s", "file_size", "field_title", "File Size");',
'INSERT INTO `field_settings` VALUES ( "%s", "%s", "file_size", "list_page", "0");',
'INSERT INTO `field_settings` VALUES ( "%s", "%s", "file_size", "add_page", "0");',
'INSERT INTO `field_settings` VALUES ( "%s", "%s", "file_size", "edit_page", "1");',
'INSERT INTO `field_settings` VALUES ( "%s", "%s", "file_size", "form_input_type", "text");',
'INSERT INTO `field_settings` VALUES ( "%s", "%s", "file_size", "linked_table", "0");',
'INSERT INTO `field_settings` VALUES ( "%s", "%s", "file_size", "list_column_order", "10");',

'INSERT INTO `field_settings` VALUES ( "%s", "%s", "file_type", "field_title", "File Type");',
'INSERT INTO `field_settings` VALUES ( "%s", "%s", "file_type", "list_page", "1");',
'INSERT INTO `field_settings` VALUES ( "%s", "%s", "file_type", "add_page", "0");',
'INSERT INTO `field_settings` VALUES ( "%s", "%s", "file_type", "edit_page", "1");',
'INSERT INTO `field_settings` VALUES ( "%s", "%s", "file_type", "form_input_type", "text");',
'INSERT INTO `field_settings` VALUES ( "%s", "%s", "file_type", "linked_table", "0");',
'INSERT INTO `field_settings` VALUES ( "%s", "%s", "file_type", "list_column_order", "3");',
'INSERT INTO `field_settings` VALUES ( "%s", "%s", "file_type", "list_searchable", "1");',
'INSERT INTO `field_settings` VALUES ( "%s", "%s", "file_type", "list_filter_value", "0");',
'INSERT INTO `field_settings` VALUES ( "%s", "%s", "file_type", "anchor_filter_value", "0");',
'INSERT INTO `field_settings` VALUES ( "%s", "%s", "file_type", "list_filterable", "0");',
'INSERT INTO `field_settings` VALUES ( "%s", "%s", "file_type", "list_check_cross", "0");',
'INSERT INTO `field_settings` VALUES ( "%s", "%s", "file_type", "list_show_linked_table_value", "0");',
'INSERT INTO `field_settings` VALUES ( "%s", "%s", "file_type", "list_column_width", "");',
'INSERT INTO `field_settings` VALUES ( "%s", "%s", "file_type", "form_input_class_edit", "");',
'INSERT INTO `field_settings` VALUES ( "%s", "%s", "file_type", "edit_change_deliberately", "0");',

'INSERT INTO `field_settings` VALUES ( "%s", "%s", "full_path", "field_title", "Full Path");',
'INSERT INTO `field_settings` VALUES ( "%s", "%s", "full_path", "list_page", "0");',
'INSERT INTO `field_settings` VALUES ( "%s", "%s", "full_path", "add_page", "0");',
'INSERT INTO `field_settings` VALUES ( "%s", "%s", "full_path", "edit_page", "1");',
'INSERT INTO `field_settings` VALUES ( "%s", "%s", "full_path", "form_input_type", "text");',
'INSERT INTO `field_settings` VALUES ( "%s", "%s", "full_path", "linked_table", "0");',
'INSERT INTO `field_settings` VALUES ( "%s", "%s", "full_path", "list_column_order", "5");',

'INSERT INTO `field_settings` VALUES ( "%s", "%s", "image_height", "field_title", "Image Height");',
'INSERT INTO `field_settings` VALUES ( "%s", "%s", "image_height", "list_page", "0");',
'INSERT INTO `field_settings` VALUES ( "%s", "%s", "image_height", "add_page", "0");',
'INSERT INTO `field_settings` VALUES ( "%s", "%s", "image_height", "edit_page", "1");',
'INSERT INTO `field_settings` VALUES ( "%s", "%s", "image_height", "form_input_type", "text");',
'INSERT INTO `field_settings` VALUES ( "%s", "%s", "image_height", "linked_table", "0");',
'INSERT INTO `field_settings` VALUES ( "%s", "%s", "image_height", "list_column_order", "13");',

'INSERT INTO `field_settings` VALUES ( "%s", "%s", "image_size_str", "field_title", "Image Size String");',
'INSERT INTO `field_settings` VALUES ( "%s", "%s", "image_size_str", "list_page", "0");',
'INSERT INTO `field_settings` VALUES ( "%s", "%s", "image_size_str", "add_page", "0");',
'INSERT INTO `field_settings` VALUES ( "%s", "%s", "image_size_str", "edit_page", "1");',
'INSERT INTO `field_settings` VALUES ( "%s", "%s", "image_size_str", "form_input_type", "text");',
'INSERT INTO `field_settings` VALUES ( "%s", "%s", "image_size_str", "linked_table", "0");',
'INSERT INTO `field_settings` VALUES ( "%s", "%s", "image_size_str", "list_column_order", "15");',

'INSERT INTO `field_settings` VALUES ( "%s", "%s", "image_type", "field_title", "Image Type");',
'INSERT INTO `field_settings` VALUES ( "%s", "%s", "image_type", "list_page", "0");',
'INSERT INTO `field_settings` VALUES ( "%s", "%s", "image_type", "add_page", "0");',
'INSERT INTO `field_settings` VALUES ( "%s", "%s", "image_type", "edit_page", "1");',
'INSERT INTO `field_settings` VALUES ( "%s", "%s", "image_type", "form_input_type", "text");',
'INSERT INTO `field_settings` VALUES ( "%s", "%s", "image_type", "linked_table", "0");',
'INSERT INTO `field_settings` VALUES ( "%s", "%s", "image_type", "list_column_order", "14");',

'INSERT INTO `field_settings` VALUES ( "%s", "%s", "image_width", "field_title", "Image Width");',
'INSERT INTO `field_settings` VALUES ( "%s", "%s", "image_width", "list_page", "0");',
'INSERT INTO `field_settings` VALUES ( "%s", "%s", "image_width", "add_page", "0");',
'INSERT INTO `field_settings` VALUES ( "%s", "%s", "image_width", "edit_page", "1");',
'INSERT INTO `field_settings` VALUES ( "%s", "%s", "image_width", "form_input_type", "text");',
'INSERT INTO `field_settings` VALUES ( "%s", "%s", "image_width", "linked_table", "0");',
'INSERT INTO `field_settings` VALUES ( "%s", "%s", "image_width", "list_column_order", "12");',


'INSERT INTO `field_settings` VALUES ( "%s", "%s", "is_image", "field_title", "Is Image");',
'INSERT INTO `field_settings` VALUES ( "%s", "%s", "is_image", "list_page", "0");',
'INSERT INTO `field_settings` VALUES ( "%s", "%s", "is_image", "add_page", "0");',
'INSERT INTO `field_settings` VALUES ( "%s", "%s", "is_image", "edit_page", "1");',
'INSERT INTO `field_settings` VALUES ( "%s", "%s", "is_image", "form_input_type", "checkbox");',
'INSERT INTO `field_settings` VALUES ( "%s", "%s", "is_image", "linked_table", "0");',
'INSERT INTO `field_settings` VALUES ( "%s", "%s", "is_image", "list_column_order", "11");',
'INSERT INTO `field_settings` VALUES ( "%s", "%s", "is_image", "checkbox_type_source", "MANUAL");',
'INSERT INTO `field_settings` VALUES ( "%s", "%s", "is_image", "checkbox_type_table_name", "");',
'INSERT INTO `field_settings` VALUES ( "%s", "%s", "is_image", "checkbox_type_table_key", "");',
'INSERT INTO `field_settings` VALUES ( "%s", "%s", "is_image", "checkbox_type_table_value", "");',
'INSERT INTO `field_settings` VALUES ( "%s", "%s", "is_image", "checkbox_type_table_custom", "");',
'INSERT INTO `field_settings` VALUES ( "%s", "%s", "is_image", "checkbox_type_table_filter", "0");',
'INSERT INTO `field_settings` VALUES ( "%s", "%s", "is_image", "checkbox_type_table_filter_key", "");',
'INSERT INTO `field_settings` VALUES ( "%s", "%s", "is_image", "checkbox_type_table_filter_value", "");',
'INSERT INTO `field_settings` VALUES ( "%s", "%s", "is_image", "checkbox_type_table_order", "0");',
'INSERT INTO `field_settings` VALUES ( "%s", "%s", "is_image", "checkbox_type_table_order_by", "");',
'INSERT INTO `field_settings` VALUES ( "%s", "%s", "is_image", "checkbox_type_table_order_sort", "");',
'INSERT INTO `field_settings` VALUES ( "%s", "%s", "is_image", "required", "0");',
'INSERT INTO `field_settings` VALUES ( "%s", "%s", "is_image", "model_has_default", "1");',
'INSERT INTO `field_settings` VALUES ( "%s", "%s", "is_image", "model_default_value", "0");',
'INSERT INTO `field_settings` VALUES ( "%s", "%s", "is_image", "model_default_on_empty", "1");',

'INSERT INTO `field_settings` VALUES ( "%s", "%s", "media_id", "field_title", "Media ID");',
'INSERT INTO `field_settings` VALUES ( "%s", "%s", "media_id", "list_page", "1");',
'INSERT INTO `field_settings` VALUES ( "%s", "%s", "media_id", "add_page", "1");',
'INSERT INTO `field_settings` VALUES ( "%s", "%s", "media_id", "edit_page", "1");',
'INSERT INTO `field_settings` VALUES ( "%s", "%s", "media_id", "form_input_type", "hidden");',
'INSERT INTO `field_settings` VALUES ( "%s", "%s", "media_id", "linked_table", "0");',
'INSERT INTO `field_settings` VALUES ( "%s", "%s", "media_id", "list_column_order", "1");',
'INSERT INTO `field_settings` VALUES ( "%s", "%s", "media_id", "list_searchable", "0");',
'INSERT INTO `field_settings` VALUES ( "%s", "%s", "media_id", "list_filter_value", "0");',
'INSERT INTO `field_settings` VALUES ( "%s", "%s", "media_id", "anchor_filter_value", "0");',
'INSERT INTO `field_settings` VALUES ( "%s", "%s", "media_id", "list_filterable", "0");',
'INSERT INTO `field_settings` VALUES ( "%s", "%s", "media_id", "list_check_cross", "0");',
'INSERT INTO `field_settings` VALUES ( "%s", "%s", "media_id", "list_show_linked_table_value", "0");',
'INSERT INTO `field_settings` VALUES ( "%s", "%s", "media_id", "list_column_width", "100px");',

'INSERT INTO `field_settings` VALUES ( "%s", "%s", "orig_name", "field_title", "Original Name");',
'INSERT INTO `field_settings` VALUES ( "%s", "%s", "orig_name", "list_page", "0");',
'INSERT INTO `field_settings` VALUES ( "%s", "%s", "orig_name", "add_page", "0");',
'INSERT INTO `field_settings` VALUES ( "%s", "%s", "orig_name", "edit_page", "1");',
'INSERT INTO `field_settings` VALUES ( "%s", "%s", "orig_name", "form_input_type", "text");',
'INSERT INTO `field_settings` VALUES ( "%s", "%s", "orig_name", "linked_table", "0");',
'INSERT INTO `field_settings` VALUES ( "%s", "%s", "orig_name", "list_column_order", "7");',

'INSERT INTO `field_settings` VALUES ( "%s", "%s", "raw_name", "field_title", "Raw Name");',
'INSERT INTO `field_settings` VALUES ( "%s", "%s", "raw_name", "list_page", "0");',
'INSERT INTO `field_settings` VALUES ( "%s", "%s", "raw_name", "add_page", "0");',
'INSERT INTO `field_settings` VALUES ( "%s", "%s", "raw_name", "edit_page", "1");',
'INSERT INTO `field_settings` VALUES ( "%s", "%s", "raw_name", "form_input_type", "text");',
'INSERT INTO `field_settings` VALUES ( "%s", "%s", "raw_name", "linked_table", "0");',
'INSERT INTO `field_settings` VALUES ( "%s", "%s", "raw_name", "list_column_order", "6");',

)
