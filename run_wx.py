#!/usr/bin/python
import os, wx
from gui.wx import *

configFile = None
filename = None
if os.path.isfile(".config"):
	configFile = open( ".config", "r" )
	filename = configFile.read()
	if not os.path.exists( os.path.join('save', filename.strip() ) ):
		filename = ''

if __name__ == "__main__":
	app = wx.App(False)
	frame = AppMain()
	app.MainLoop()
